var gulp = require('gulp');
var webpack = require('webpack-stream');
var compass = require('gulp-compass');
var watch   = require('gulp-watch');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var config = require('./webpack.config.js');
var config_loja = require('./webpack.lojistas.js');
var config_prod = require('./webpack.prod.config.js');
var plumber = require('gulp-plumber');
var path = require('path');
var imageminPngquant = require('imagemin-pngquant');
var imageminJpegRecompress = require('imagemin-jpeg-recompress');

// Gulp Default Task

//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
    title: 'Gulp',
    icon: path.join(__dirname, 'gulp.png')
};

var plumberErrorHandler = { errorHandler: notify.onError({
    title: notifyInfo.title,
    icon: notifyInfo.icon,
    message: "Error: <%= error.message %>"
})};

gulp.task('lojistas', ['watchLojistas', 'webpackLojistas', 'stylesLojistas' ]);

gulp.task('default', ['watch', 'webpack', 'styles', 'imagemin']);
//gulp.task('build',  ['imagemin', 'webpack-build', 'stylus', 'imagemin']);

gulp.task('imagemin', function() {
    gulp.src('common/default/images/site/**/*')
        .pipe(imagemin({
            optimizationLevel: 1,
            interlaced: true,
            progressive: true,
            buffer: true,
            use: [
                imageminPngquant({quality: '75-85'}),
                imageminJpegRecompress({
                    progressive: true,
                    max: 80,
                    min: 70
                })
            ]
        }))
        .pipe(gulp.dest('common/default/images/site_dist/'))
});
gulp.task('styles', function () {
    return gulp.src('common/default/scss/**/*.scss')
        .pipe(compass({
            css: 'common/default/css/',
            sass: 'common/default/scss/',
            image: 'common/default/images/',
            font: 'common/default/fonts/',
            sourcemap: true,
        }))
        .on('error', function(err) {
            console.log(err);
        })
        //.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('common/default/css'))
        //.pipe(rename({ suffix: '.min' }))
        .pipe(livereload());
});
gulp.task('webpack', function () {
    return gulp.src('./common/default/es6/main.js')
        .pipe(webpack(config))
        .on('error', function (error) {
            notify().write({
                message: error.message
            });
            this.emit('end');
        })
        .pipe(gulp.dest('./common/default/scripts/bundle/'))
        .pipe(livereload());

});
gulp.task('webpack-build', function () {
    return gulp.src('./common/default/es6/main.js')
        .pipe(webpack(config_prod))
        .on('error', function (error) {
            notify().write({
                message: error.message
            });
            this.emit('end');
        })
        .pipe(gulp.dest('./common/default/scripts/bundle/'))
        .pipe(notify({
            title: 'Webpack',
            message: 'Generated file: <%= file.relative %>',
        }))
        .pipe(livereload());
});
gulp.task('layouts', function() {
  return gulp.src('application/layouts/*.tpl', {read: false})
    .pipe(livereload());
});

gulp.task('views', function() {
  return gulp.src('application/modules/default/views/**/*.tpl', {read: false})
    .pipe(livereload());
});


gulp.task('viewsLojistas', function() {
  return gulp.src('application/modules/default/views/**/*.tpl', {read: false})
    .pipe(livereload());
});
gulp.task('layoutsLojistas', function() {
    return gulp.src('application/layouts/*.tpl', {read: false})
        .pipe(livereload());
});
gulp.task('webpackLojistas', function () {
    return gulp.src('./common/lojistas/es6/main.js')
        .pipe(webpack(config_loja))
        .on('error', function (error) {
            notify().write({
                message: error.message
            });
            this.emit('end');
        })
        .pipe(gulp.dest('./common/lojistas/scripts/bundle/'))
        /*.pipe(notify({
            title: 'Webpack',
            message: 'Generated file: <%= file.relative %>',
        }))*/
        .pipe(livereload());
});

gulp.task('stylesLojistas', function () {
    return gulp.src(['common/lojistas/scss/**/*.scss'])
        .pipe(plumber(plumberErrorHandler))
        .pipe(compass({
            css: 'common/lojistas/css',
            sass: 'common/lojistas/scss',
            image: 'common/lojistas/images',
            sourcemap: true
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('common/lojistas/css'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(livereload());
});

gulp.task('watch', function () {
    gulp.watch(['application/layouts/*.tpl'], ['layouts'])
    gulp.watch(['application/modules/default/views/**/*.tpl'], ['views'])
    gulp.watch(['common/default/scss/**/*'], ['styles'])
    gulp.watch(['common/default/images/site/**/*'], ['imagemin'])
    livereload.listen();
});

gulp.task('watchLojistas', function () {
    gulp.watch(['application/layouts/*.tpl'], ['layoutsLojistas'])
    gulp.watch(['application/modules/lojistas/views/**/*.tpl'], ['viewsLojistas'])
    gulp.watch(['common/lojistas/scss/**/*'], ['stylesLojistas'])
    gulp.watch(['common/lojistas/es6/**/*'], ['webpackLojistas'])
    livereload.listen();
});