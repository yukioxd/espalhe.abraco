var webpack = require('webpack');
module.exports = {
    entry: './common/lojistas/es6/main.js',
    output: {
        filename: '[name].js'
    },
    watch: true,
    progress: true,
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'stage-2']
                }
            }
        ]
    }
};
