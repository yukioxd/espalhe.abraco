var name = this
	.location
	.pathname
	.split('/');

var CACHE_NAME = 'tbb1.0';
var filename = name.splice(name.length - 2, 2);
var basepath = name.join('/').replace(filename[0], '');
basepath = '/common/default';

importScripts(basepath + '/scripts/cache-polyfill.js');
var urlsToCache = [
  basepath + 'images/site_dist/lq-background.jpg',
  basepath + 'images/site_dist/participe-icons/participe.png',
	basepath + 'images/site_dist/participe-icons/loja.png',
	basepath + 'images/site_dist/produto-gota.png',
	basepath + 'images/site_dist/logo.png',
	basepath + 'images/site_dist/participe-icons/qr-code.png',
	basepath + 'images/site_dist/participe-icons/embalagem.png',
	basepath + 'images/site_dist/produto.png',
	basepath + 'images/site_dist/lq-background.jpg',
	basepath + 'images/site_dist/twitter.png',
	basepath + 'images/site_dist/facebook.png',
	basepath + 'images/site_dist/phase2-bg.png',
	basepath + 'images/site_dist/seta-participe.png',
	basepath + 'images/site_dist/the-promo-bg.png',
	basepath + 'images/site_dist/duvidas-bg.png',
	basepath + 'images/site_dist/participe-bg-1.png',
	basepath + 'images/site_dist/participe-form-body.png',
	basepath + 'images/site_dist/fb-logo.png',
	basepath + 'images/site_dist/lojas-participants-bg.png',
	basepath + 'images/site_dist/regulamento-bg.png',
	basepath + 'images/site_dist/bg-objects.png',
	basepath + 'images/site_dist/phase1-bg.png',
	basepath + 'fonts/boutiquescript-webfont.woff2',
	basepath + 'fonts/gothamlight-webfont.woff2',
	basepath + 'fonts/gotham-bold-webfont.woff2',
	basepath + 'fonts/gotham-book-webfont.woff2',
	basepath + 'fonts/gotham_thin_regular-webfont.woff2'
];
// Set the callback for the install step
self.addEventListener('install', function(event) {
	// Perform install steps
	event.waitUntil(
		caches.open(CACHE_NAME)
			.then(function(cache) {
				return cache.addAll(urlsToCache);
			})
	);
});

self.addEventListener('activate', function (event) {
	event.waitUntil(
		caches.keys().then(function(keys){
			return Promise.all(keys.map(function(key, i){
				if(key !== CACHE_NAME){
					return caches.delete(keys[i]);
				}
			}))
		})
	)
});

self.addEventListener('fetch', function (gevent) {
	event.respondWith(
		caches.match(event.request).then(function(res){
			if(res){
				return res;
			}
			requestBackend(event);
		})
	)
});

function requestBackend(event){
	var url = event.request.clone();
	return fetch(url).then(function(res){
		//if not a valid response send the error
		if(!res || res.status !== 200 || res.type !== 'basic'){
			return res;
		}

		var response = res.clone();

		caches.open(CACHE_NAME).then(function(cache){
			cache.put(event.request, response);
		});

		return res;
	})
}
