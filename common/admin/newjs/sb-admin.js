$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
$(function() {
    $(window).bind("load resize", function() {
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.sidebar-collapse').addClass('collapse')
        } else {
            $('div.sidebar-collapse').removeClass('collapse')
        }
    });

    $(document).on('click', '#cadastro-enviar-email' , function ( e ) {
    	e.preventDefault();
		$('#timo-loader').fadeIn();
		var xhrConfig = {
			url: '?sendEmail=1',
			complete: function ( data ) {
				try {
					var content = JSON.parse( data.responseText );
					if ( content.message !== undefined ) {
						alert( content.message );
					}
				}catch( err ) {
				}
				$('#timo-loader').fadeOut();
			}
		};
		$.ajax( xhrConfig );
    });

    $(document).on('click', '#cadastro-enviar-sms' , function ( e  ) {
    	e.preventDefault();
		$('#timo-loader').fadeIn();
		var xhrConfig = {
			url: '?sendSms=1',
			complete: function ( data ) {
				try {
					var content = JSON.parse( data.responseText );
					if ( content.message !== undefined ) {
						alert( content.message );
					}
				}catch( err ) {
				}
				$('#timo-loader').fadeOut();
			}
		};
		$.ajax( xhrConfig );
    });

    $(document).on('click', '#cadastro-remover' , function ( e  ) {
    	e.preventDefault();
    });

	if ( window.location.href.match(/clear\=1/g) ) {
		$.get('?clearjs=1');
	}
})
