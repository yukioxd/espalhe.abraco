/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	var _Masky = __webpack_require__(1);
	
	var _Masky2 = _interopRequireDefault(_Masky);
	
	var _ready = __webpack_require__(2);
	
	var _ready2 = _interopRequireDefault(_ready);
	
	var _participateOne = __webpack_require__(3);
	
	var _participateOne2 = _interopRequireDefault(_participateOne);
	
	var _participateTwo = __webpack_require__(6);
	
	var _participateTwo2 = _interopRequireDefault(_participateTwo);
	
	var _loader = __webpack_require__(8);
	
	var _tingle = __webpack_require__(9);
	
	var _tingle2 = _interopRequireDefault(_tingle);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	location.hash = '';
	window.release = '0.0.1';
	global.Masky = _Masky2.default;
	var client = function (a, b) {
		if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) return 'mobile';else return 'desktop';
	}(navigator.userAgent || navigator.vendor || window.opera);
	
	//http://localhost/local.abraco/
	(function (w, d) {
		w.beginLoader = _loader.beginLoader;
		w.hideLoader = _loader.hideLoader;
		var setup = function setup() {
			setupStyleSelect();
		};
		if (d.body.id.indexOf('participantes') >= 0) {
			__webpack_require__(10);
		}
	
		if (d.body.id.indexOf('escolhe') >= 0) {
			var coupon = document.getElementById('coupon-block');
			if (coupon === null) {
				__webpack_require__(13);
			} else {
				__webpack_require__(14);
			}
		}
		if (d.body.id === 'index') {
			var abrs = d.getElementById('abracometro');
			var abrsCount = d.getElementById('hugmeter-block');
			var currentCount = 1;
			//check date
			var dataDisplay = new Date();
			var now = new Date();
			dataDisplay.setSeconds(1);
			dataDisplay.setMinutes(0);
			dataDisplay.setHours(0);
	
			dataDisplay.setDate(15);
			dataDisplay.setMonth(4);
			dataDisplay.setFullYear(2017);
	
			if (now.getTime() > dataDisplay.getTime()) {
				var abracometroCount = function abracometroCount(number) {
					"use strict";
	
					var sumCount = Math.ceil((number - currentCount) / 78);
					currentCount += sumCount;
					abrsCount.textContent = currentCount;
					if (currentCount < number) {
						setTimeout(function () {
							abracometroCount(number);
						}, 10);
					}
				};
	
				if (abrs !== null) {
					var xhr = new XMLHttpRequest();
					try {
						xhr.open('GET', document.basePath + '?abracometro');
						xhr.addEventListener('readystatechange', function (e) {
							"use strict";
	
							if (xhr.status === 200 && xhr.readyState === 4) {
								var response = JSON.parse(xhr.response);
								if (response.count !== null && response.count > 0) {
									abrs.className = 'visible';
									abracometroCount(response.count);
								}
							}
						});
						xhr.send();
					} catch (err) {}
				}
			}
		}
	
		var setupStyleSelect = function setupStyleSelect() {
			var elements = d.querySelectorAll('.style-select select');
			var elementsArray = Array.prototype.slice.call(elements);
			d.addEventListener('change', function (e) {
				if (elementsArray.indexOf(e.target) >= 0) {
					var text = e.target.options[e.target.selectedIndex].innerHTML;
					e.target.parentElement.querySelector('.select-value').innerHTML = text;
				}
			});
		};
	
		if (d.body.id.match(/participe/g)) {}
	
		var prevScrollTop = void 0;
		if (d.body.id.match(/duvidas/g)) {
			window.onhashchange = function (e) {
				if (location.hash.match(/\!/g)) {
					location.hash = location.hash.replace(/\!/g, '');
				}
				if (prevScrollTop !== undefined) {
					window.scrollTo(0, prevScrollTop);
				}
			};
			var listenclick = function listenclick(e) {
				var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
				prevScrollTop = scrollTop;
				var target = e.target;
				if (e.target.tagName.toLowerCase() === 'li') {
					target = e.target.querySelector('a');
				}
				if (target.tagName.toLowerCase() === 'a') {
					var href = target.href;
					var hash = href.substr(href.indexOf('#')).replace(/\!/g, '');
					setTimeout(function () {
						window.scrollTo(0, scrollTop);
					}, 60);
				}
			};
	
			document.body.addEventListener('mousedown', listenclick);
			document.body.addEventListener('touchdown', listenclick);
		}
	
		var modalFrames = d.querySelectorAll('.modal-iframe');
		var modalIframeBlock = function modalIframeBlock(e) {
			e.preventDefault();
			var target = false;
			if (e.target.tagName.toLowerCase() === 'a') {
				target = e.target;
			}
			if (e.target.tagName.toLowerCase() === 'u') {
				target = e.target.parentElement;
			}
			var url = target.href;
			var urlParts = url.split('/');
			var className = urlParts[urlParts.length - 1];
			className = className.replace(/\W/g, '-');
			var modalIframe = new _tingle2.default.modal({
				cssClass: [className],
				closeLabel: "Fechar",
				onClose: function onClose() {
					modalIframe.destroy();
				}
			});
			//check se pdf
			console.log(url);
			if (url.match(/\.pdf$/)) {
				var content = '<iframe src="http://docs.google.com/gview?url=' + url + '&embedded=true" frameborder="0"></iframe>';
				modalIframe.setContent(content);
			} else {
				modalIframe.setContent('<iframe width="100%" height="100%" src="' + url + '" frameborder="0" allowfullscreen></iframe>');
			}
			modalIframe.open();
		};
		Array.prototype.forEach.call(modalFrames, function (item) {
			item.addEventListener('mousedown', modalIframeBlock);
			item.addEventListener('touchdown', modalIframeBlock);
		});
	
		w.docReady(function () {
			setup();
		});
	})(window, document);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	exports.default = function (cfg, str) {
		var _keys = Object.keys(cfg);
	
		var sample = str.replace(/\D/g, '').split('');
		var output = [];
		sample.forEach(function (c, k) {
			if (_keys.indexOf(k.toString()) >= 0) {
				output.push(cfg[k]);
			}
			output.push(c);
		});
		return output.join('');
	};

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	exports.default = function (funcName, baseObj) {
	    "use strict";
	    // The public function name defaults to window.docReady
	    // but you can modify the last line of this function to pass in a different object or method name
	    // if you want to put them in a different namespace and those will be used instead of 
	    // window.docReady(...)
	
	    funcName = funcName || "docReady";
	    baseObj = baseObj || window;
	    var readyList = [];
	    var readyFired = false;
	    var readyEventHandlersInstalled = false;
	
	    // call this when the document is ready
	    // this function protects itself against being called more than once
	    function ready() {
	        if (!readyFired) {
	            // this must be set to true before we start calling callbacks
	            readyFired = true;
	            for (var i = 0; i < readyList.length; i++) {
	                // if a callback here happens to add new ready handlers,
	                // the docReady() function will see that it already fired
	                // and will schedule the callback to run right after
	                // this event loop finishes so all handlers will still execute
	                // in order and no new ones will be added to the readyList
	                // while we are processing the list
	                readyList[i].fn.call(window, readyList[i].ctx);
	            }
	            // allow any closures held by these functions to free
	            readyList = [];
	        }
	    }
	
	    function readyStateChange() {
	        if (document.readyState === "complete") {
	            ready();
	        }
	    }
	
	    // This is the one public interface
	    // docReady(fn, context);
	    // the context argument is optional - if present, it will be passed
	    // as an argument to the callback
	    baseObj[funcName] = function (callback, context) {
	        if (typeof callback !== "function") {
	            throw new TypeError("callback for docReady(fn) must be a function");
	        }
	        // if ready has already fired, then just schedule the callback
	        // to fire asynchronously, but right away
	        if (readyFired) {
	            setTimeout(function () {
	                callback(context);
	            }, 1);
	            return;
	        } else {
	            // add the function and context to the list
	            readyList.push({ fn: callback, ctx: context });
	        }
	        // if document already ready to go, schedule the ready function to run
	        // IE only safe when readyState is "complete", others safe when readyState is "interactive"
	        if (document.readyState === "complete" || !document.attachEvent && document.readyState === "interactive") {
	            setTimeout(ready, 1);
	        } else if (!readyEventHandlersInstalled) {
	            // otherwise if we don't have event handlers installed, install them
	            if (document.addEventListener) {
	                // first choice is DOMContentLoaded event
	                document.addEventListener("DOMContentLoaded", ready, false);
	                // backup is window load event
	                window.addEventListener("load", ready, false);
	            } else {
	                // must be IE
	                document.attachEvent("onreadystatechange", readyStateChange);
	                window.attachEvent("onload", ready);
	            }
	            readyEventHandlersInstalled = true;
	        }
	    };
	}("docReady", window);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _checkCPF = __webpack_require__(4);
	
	var _checkCPF2 = _interopRequireDefault(_checkCPF);
	
	var _alert = __webpack_require__(5);
	
	var _alert2 = _interopRequireDefault(_alert);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var ParticipateOne = function () {
		function ParticipateOne() {
			_classCallCheck(this, ParticipateOne);
	
			if (document.body.id.match(/participe/)) {
				var d = document;
				this.cpf = d.getElementById('cpf');
				this.sendButton = d.getElementById('do-send-cpf');
				this.form = d.getElementById('participate-form-step-1');
	
				if (this.cpf !== null) {
					this.setup(cpf);
				}
				this.ignoreKeyCodes = [37, 38, 39, 40];
	
				if (location.href.match(/used/g)) {
					var parts = location.href.split('?');
					var last = parts[parts.length - 1];
					var keyValues = last.split('=');
					this.cpf.value = keyValues[1];
					(0, _alert2.default)('Este CPF já está sendo usado !', false);
				}
			}
		}
	
		_createClass(ParticipateOne, [{
			key: 'checkField',
			value: function checkField(e) {
				var me = this;
				if (me.ignoreKeyCodes.indexOf(e.keyCode) < 0) {
					setTimeout(function () {
						var checkValue = me.cpf.value.replace(/\D/g, '');
						var o = global.Masky({
							3: '.',
							6: '.',
							9: '-'
						}, me.cpf.value);
						me.cpf.value = o.substr(0, 14);
					});
				}
			}
		}, {
			key: 'setup',
			value: function setup(cpf) {
				var _this = this;
	
				var me = this;
				cpf.addEventListener('blur', function (e) {
					me.checkField.call(me, e);
				});
				cpf.addEventListener('keydown', function (e) {
					me.checkField.call(me, e);
				});
				cpf.addEventListener('keyup', function (e) {
					me.checkField.call(me, e);
				});
	
				var next = function next(e) {
					e.preventDefault();
					if (localStorage && localStorage.setItem) {
						localStorage.setItem('cpf', me.cpf.value);
					}
					var numericCPF = me.cpf.value.replace(/\D/g, '');
					var result = (0, _checkCPF2.default)(numericCPF);
	
					if (result.length > 1) {
						_this.form.submit();
					} else {
						console.log(numericCPF.length);
						if (numericCPF.length === 11) {
							(0, _alert2.default)('CPF inválido !', false);
						} else {
							(0, _alert2.default)('Digite seu CPF', false);
						}
						me.form.className = me.form.className.match(/invalid/g) ? me.form.className : me.form.className + ' invalid';
						me.cpf.className = me.cpf.className.match(/invalid/g) ? me.cpf.className : me.cpf.className + ' invalid';
					}
				};
	
				this.sendButton.addEventListener('click', next);
				this.form.addEventListener('submit', next);
			}
		}]);
	
		return ParticipateOne;
	}();
	
	;
	
	exports.default = new ParticipateOne();
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	exports.default = function (strCPF) {
	    var Soma = 0,
	        Resto = void 0,
	        i = void 0,
	        map = {},
	        keySize = 0;
	    Soma = 0;
	    if (strCPF == "00000000000") {
	        return '';
	    }
	    for (i = 1; i <= 9; i++) {
	        var char = strCPF.substring(i - 1, i);
	        if (map[char] === undefined) {
	            keySize++;
	            map[char] = true;
	        }
	        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
	    }
	    if (keySize === 1) {
	        return '';
	    }
	    Resto = Soma * 10 % 11;
	    if (Resto == 10 || Resto == 11) {
	        Resto = 0;
	    }
	
	    if (Resto != parseInt(strCPF.substring(9, 10))) {
	        return '';
	    };
	    Soma = 0;
	
	    for (i = 1; i <= 10; i++) {
	        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
	    }
	    Resto = Soma * 10 % 11;
	    if (Resto == 10 || Resto == 11) {
	        Resto = 0;
	    }
	    if (Resto != parseInt(strCPF.substring(10, 11))) {
	        return '';
	    };
	    return strCPF.substr(0, 3) + '.' + strCPF.substr(3, 3) + '.' + strCPF.substr(6, 3) + '-' + strCPF.substr(9, 2);
	};
	
	;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	exports.default = function (message, isSuccess) {
		var prev = document.getElementById('simple-alert');
		if (prev !== null) {
			prev.remove();
		}
	
		var alertBlock = document.createElement('div');
		var messageElement = document.createElement('span');
		var closeAlert = document.createElement('button');
		closeAlert.type = 'button';
		closeAlert.onclick = function (e) {
			e.preventDefault();
			alertBlock.remove();
		};
	
		closeAlert.id = 'close-alert';
		closeAlert.innerHTML = '&#10005;';
	
		alertBlock.id = 'simple-alert';
		alertBlock.className = isSuccess ? 'simple-alert success' : 'simple-alert error';
		messageElement.innerHTML = message;
		alertBlock.appendChild(messageElement);
		alertBlock.appendChild(closeAlert);
		document.body.appendChild(alertBlock);
		return alertBlock;
	};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _searchCEP = __webpack_require__(7);
	
	var _searchCEP2 = _interopRequireDefault(_searchCEP);
	
	var _alert = __webpack_require__(5);
	
	var _alert2 = _interopRequireDefault(_alert);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var ignoreKeyCodes = [8, 17, 37, 39, 46];
	var regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var validateEmail = function validateEmail(email) {
		return regexEmail.test(email);
	};
	
	var ParticipateTwo = function () {
		_createClass(ParticipateTwo, [{
			key: 'testDate',
			value: function testDate(target) {
				var parts = target.value.split('/');
				var newValue = parts.reverse().join('-');
				var dateObj = new Date(newValue);
				return !(target.value.length != 10 || isNaN(dateObj.getTime()));
			}
		}, {
			key: 'restoreData',
			value: function restoreData() {
				var storeDatas = localStorage.getItem(this.storeKey);
				var storedFormDatas = JSON.parse(storeDatas);
				var fields = storedFormDatas.formHistory;
	
				fields.forEach(function (field) {
					var fieldNames = Object.getOwnPropertyNames(field);
					if (fieldNames.length > 0) {
						var fieldName = fieldNames[0];
						var item = document.querySelector('*[name=' + fieldName + ']');
						if (item !== null) {
							if (item.type === 'checkbox') {
								if (field[fieldName] == 'on') {
									item.checked = true;
								}
							} else {
								item.value = field[fieldName];
							}
						}
					}
				});
			}
		}]);
	
		function ParticipateTwo() {
			_classCallCheck(this, ParticipateTwo);
	
			this.storeKey = 'prevForm';
	
			var cpf = localStorage.getItem('cpf');
			var me = this;
			var isCadastre = document.body.id.indexOf('cadastre') >= 0;
			if (!isCadastre) {
				return false;
			}
			try {
				me.restoreData();
			} catch (err) {
				//donothing;
			}
	
			me.cpf = cpf;
			if (cpf === null || cpf === undefined || me.cpf.length < 2) {
				location.href = location.href.replace(/cadastre/g, 'participe');
			}
			var checkEmpty = function checkEmpty(target) {
				var isValid = void 0;
				if (target.className.match(/required/)) {
					isValid = target.value.length > 0;
				}
				return isValid;
			};
	
			var alertOnEmpty = function alertOnEmpty(e) {
				e.target.value = e.target.value.trim();
				var isValid = true;
				if (!isValid) {
					e.target.className = e.target.className.match(/error/g) ? e.target.className : e.target.className + ' error';
				} else {
					e.target.className = e.target.className.replace(/\s?error/g, '');
				}
				checkEmpty(e.target);
			};
	
			var isDate = function isDate(e) {
				if (!me.testDate(e.target)) {
					e.target.value = '';
					(0, _alert2.default)('Data de Nascimento inválido ! ', false);
				}
			};
	
			var formatDate = function formatDate(e) {
				if (ignoreKeyCodes.indexOf(e.keyCode) < 0) {
					e.target.value = global.Masky({
						2: '/',
						4: '/'
					}, e.target.value).substr(0, 10);
				}
			};
			var formatCep = function formatCep(e) {
				if (ignoreKeyCodes.indexOf(e.keyCode) < 0) {
					e.target.value = global.Masky({
						5: '-'
					}, e.target.value).substr(0, 9);
				}
			};
			var searchByCep = function searchByCep(e) {
				(0, _searchCEP2.default)(e.target.value, _alert2.default);
			};
			var checkEmail = function checkEmail(e) {
				var email = e.target.value;
				var result = validateEmail(email);
				if (!result) {
					(0, _alert2.default)(' E-mail inválido ! ', false);
				}
			};
			if (document.body.id.match(/cadastre/)) {
				var d = document;
				this.form = d.getElementById('register-block-form');
				this.fields = {
					nome: {
						label: 'Nome',
						field: d.getElementById('nome'),
						onBlur: [alertOnEmpty]
					},
					nascimento: {
						label: 'Data de Nascimento',
						field: d.getElementById('nascimento'),
						onBlur: [alertOnEmpty, isDate],
						onKeyup: [formatDate]
					},
					email: {
						label: 'E-mail',
						field: d.getElementById('email'),
						onBlur: [alertOnEmpty, checkEmail]
					},
					cep: {
						label: 'CEP',
						field: d.getElementById('cep'),
						onBlur: [alertOnEmpty, formatCep, searchByCep]
					},
					estado: {
						label: 'Estado',
						field: d.getElementById('estado'),
						onBlur: [alertOnEmpty]
					},
					cidade: {
						label: 'Cidade',
						field: d.getElementById('cidade'),
						onBlur: [alertOnEmpty]
					},
					endereco: {
						label: 'Endereço',
						field: d.getElementById('endereco'),
						onBlur: [alertOnEmpty]
					},
					abraco_nome: {
						label: 'Nome(amiga)',
						field: d.getElementById('abraco_nome'),
						onBlur: [alertOnEmpty]
					},
					abraco_email: {
						label: 'E-mail(amiga)',
						field: d.getElementById('abraco_email'),
						onBlur: [alertOnEmpty]
					},
					numero: {
						label: 'Número',
						field: d.getElementById('numero'),
						onBlur: [alertOnEmpty]
					}
				};
	
				this.clube_beauty = d.getElementById('clube_beauty');
				this.social_networks = d.getElementById('social_networks');
				this.agreement = d.getElementById('agreement');
				this.setup();
			}
		}
	
		_createClass(ParticipateTwo, [{
			key: 'checkAllFields',
			value: function checkAllFields() {
				var me = this;
				var fields = Object.getOwnPropertyNames(this.fields);
				var isValidForm = true;
				var messageParts = [];
				fields.forEach(function (propName) {
					var field = me.fields[propName].field;
					var validField = true;
					var currentClassName = field.className;
					if (currentClassName.match(/required/g) && field.value.length < 1) {
						isValidForm = false;
						validField = false;
						messageParts.push('Por favor, verifique o campo <strong> ' + me.fields[propName].label + ' </strong>');
					}
	
					if (currentClassName.match(/date/g)) {
						if (!me.testDate(field)) {
							field.value = '';
							messageParts.push('Por favor, verifique o campo <strong> ' + me.fields[propName].label + ' </strong>');
						}
					}
	
					if (field.id == 'email') {
						validField = validateEmail(field.value);
						if (!validField) {
							isValidForm = false;
							messageParts.push('Campo <strong> ' + me.fields[propName].label + ' </strong> inv\xE1lido');
						}
					}
	
					if (validField) {
						field.className = field.className.replace(/\s?error/g, '');
					} else {
						field.className = field.className.match(/error/g) ? field.className : field.className + ' error';
					}
				});
	
				if (!me.agreement.checked) {
					isValidForm = false;
					messageParts.push('Por favor, confirme se est\xE1 de <strong>acordo</strong> com o Regulamento');
				}
	
				if (messageParts.length > 0) {
					var ul = document.createElement('ul');
					messageParts.map(function (err) {
						return '<li>' + err + '</li>';
					}).filter(function (item, k, all) {
						return all.lastIndexOf(item) === k;
					}).forEach(function (item) {
						return ul.innerHTML += item;
					});
					(0, _alert2.default)(ul.outerHTML, false);
				}
	
				if (isValidForm) {
					var cpf = document.createElement('input');
					cpf.name = 'cpf';
					cpf.type = 'hidden';
					cpf.value = me.cpf;
					var formDatas = Array.prototype.map.call(me.form.elements, function (el) {
						var o = {};
						if (el.type == 'checkbox' || el.type == 'radio') {
							if (el.checked) {
								o[el.name] = el.value;
							}
						} else {
							o[el.name] = el.value;
						}
						return o;
					});
					try {
						var storageData = { formHistory: formDatas };
						var strData = JSON.stringify(storageData);
						localStorage.removeItem(me.storeKey);
						localStorage.setItem(me.storeKey, strData);
					} catch (e) {
						//donothing
					}
					me.form.appendChild(cpf);
	
					setTimeout(function () {
						me.form.submit();
					}, 59);
				}
			}
		}, {
			key: 'setup',
			value: function setup(cpf) {
				var me = this;
				var fields = Object.getOwnPropertyNames(this.fields);
	
				fields.forEach(function (propName) {
					if (me.fields[propName].onBlur !== undefined && me.fields[propName].onBlur.forEach) {
						me.fields[propName].onBlur.forEach(function (fn) {
							me.fields[propName].field.addEventListener('blur', fn);
						});
					}
					if (me.fields[propName].onKeyup !== undefined && me.fields[propName].onKeyup.forEach) {
						me.fields[propName].onKeyup.forEach(function (fn) {
							me.fields[propName].field.addEventListener('keyup', fn);
						});
					}
				});
	
				var formatPhone = function formatPhone(e) {
					if (ignoreKeyCodes.indexOf(e.keyCode) < 0) {
						var prevValue = e.target.value.replace(/\D/g, '');
						var separatorKey = 6;
						var maskyConfig = {
							0: '(',
							2: ')'
						};
						separatorKey = prevValue.length > 10 ? 7 : separatorKey;
						maskyConfig[separatorKey] = '-';
						e.target.value = global.Masky(maskyConfig, e.target.value).substr(0, 14);
					}
				};
	
				document.getElementById('celular').addEventListener('keyup', formatPhone);
	
				this.form.addEventListener('submit', function (e) {
					e.preventDefault();
					me.checkAllFields();
				});
			}
		}]);
	
		return ParticipateTwo;
	}();
	
	;
	
	exports.default = new ParticipateTwo();
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	exports.default = function (cep, myAlert) {
	    var d = document;
	    var resultCEP = cep.replace(/\D/g, '');
	    if (resultCEP.length === 8) {
	        var xhr = new XMLHttpRequest();
	        xhr.open('GET', '//api.postmon.com.br/v1/cep/' + resultCEP);
	        xhr.addEventListener('readystatechange', function () {
	            if (xhr.readyState === 4 && xhr.status === 200) {
	                var response = xhr.response;
	                try {
	                    var parsedResponse = JSON.parse(response);
	                    if (parsedResponse.estado !== undefined) {
	                        d.getElementById('estado').value = parsedResponse.estado;
	                    }
	                    if (parsedResponse.cidade !== undefined) {
	                        d.getElementById('cidade').value = parsedResponse.cidade;
	                    }
	                    if (parsedResponse.logradouro !== undefined) {
	                        d.getElementById('endereco').value = parsedResponse.logradouro;
	                    }
	                } catch (err) {}
	            } else if (xhr.readyState === 4) {
	                myAlert('CEP Não encontrado', false);
	            }
	        });
	        xhr.send();
	    } else {
	        myAlert('CEP Inválido', false);
	        d.getElementById('cep').value = '';
	    }
	    return resultCEP;
	};
	
	;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.beginLoader = beginLoader;
	exports.hideLoader = hideLoader;
	function beginLoader() {
	    "use strict";
	
	    var html = '\n        <div id="peek-loading">\n            <div class="peeek-loading">\n                <ul>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                <li></li>\n                </ul>\n            </div>\n        </div>';
	    var prev = document.getElementById('peek-loading');
	    if (prev === null) {
	        document.body.innerHTML += html;
	    }
	    if (prev !== null) {
	        if (prev.className.match(/disabled/g)) {
	            prev.className = '';
	        } else {
	            //donothing
	        }
	    }
	}
	
	function hideLoader() {
	    "use strict";
	
	    var prev = document.getElementById('peek-loading');
	    if (prev !== null) {
	        if (prev.className.match(/disabled/g)) {
	            //donothing
	        } else {
	            prev.className += 'disabled';
	        }
	    }
	    setTimeout(function () {
	        var prev = document.getElementById('peek-loading');
	        if (prev !== null) {
	            prev.remove();
	        }
	    }, 400);
	}

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	/*!
	 * tingle.js
	 * @author  robin_parisi
	 * @version 0.10.0
	 * @url
	 */
	(function (root, factory) {
	    if (true) {
	        !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
	        module.exports = factory();
	    } else {
	        root.tingle = factory();
	    }
	})(undefined, function () {
	
	    /* ----------------------------------------------------------- */
	    /* == modal */
	    /* ----------------------------------------------------------- */
	
	    var transitionEvent = whichTransitionEvent();
	
	    function Modal(options) {
	
	        var defaults = {
	            onClose: null,
	            onOpen: null,
	            beforeClose: null,
	            stickyFooter: false,
	            footer: false,
	            cssClass: [],
	            closeLabel: 'Close',
	            closeMethods: ['overlay', 'button', 'escape']
	        };
	
	        // extends config
	        this.opts = extend({}, defaults, options);
	
	        // init modal
	        this.init();
	    }
	
	    Modal.prototype.init = function () {
	        if (this.modal) {
	            return;
	        }
	
	        _build.call(this);
	        _bindEvents.call(this);
	
	        // insert modal in dom
	        document.body.insertBefore(this.modal, document.body.firstChild);
	
	        if (this.opts.footer) {
	            this.addFooter();
	        }
	    };
	
	    Modal.prototype.destroy = function () {
	        if (this.modal === null) {
	            return;
	        }
	
	        // unbind all events
	        _unbindEvents.call(this);
	
	        // remove modal from dom
	        this.modal.parentNode.removeChild(this.modal);
	
	        this.modal = null;
	    };
	
	    Modal.prototype.open = function () {
	
	        if (this.modal.style.removeProperty) {
	            this.modal.style.removeProperty('display');
	        } else {
	            this.modal.style.removeAttribute('display');
	        }
	
	        // prevent double scroll
	        document.body.classList.add('tingle-enabled');
	
	        // sticky footer
	        this.setStickyFooter(this.opts.stickyFooter);
	
	        // show modal
	        this.modal.classList.add('tingle-modal--visible');
	
	        // onOpen event
	        var self = this;
	
	        if (transitionEvent) {
	            this.modal.addEventListener(transitionEvent, function handler() {
	                if (typeof self.opts.onOpen === 'function') {
	                    self.opts.onOpen.call(self);
	                }
	
	                // detach event after transition end (so it doesn't fire multiple onOpen)
	                self.modal.removeEventListener(transitionEvent, handler, false);
	            }, false);
	        } else {
	            if (typeof self.opts.onOpen === 'function') {
	                self.opts.onOpen.call(self);
	            }
	        }
	
	        // check if modal is bigger than screen height
	        _checkOverflow.call(this);
	    };
	
	    Modal.prototype.isOpen = function () {
	        return !!this.modal.classList.contains("tingle-modal--visible");
	    };
	
	    Modal.prototype.close = function () {
	
	        //  before close
	        if (typeof this.opts.beforeClose === "function") {
	            var close = this.opts.beforeClose.call(this);
	            if (!close) return;
	        }
	
	        document.body.classList.remove('tingle-enabled');
	
	        this.modal.classList.remove('tingle-modal--visible');
	
	        //Using similar setup as onOpen
	        //Reference to the Modal that's created
	        var self = this;
	
	        if (transitionEvent) {
	            //Track when transition is happening then run onClose on complete
	            this.modal.addEventListener(transitionEvent, function handler() {
	                // detach event after transition end (so it doesn't fire multiple onClose)
	                self.modal.removeEventListener(transitionEvent, handler, false);
	
	                self.modal.style.display = 'none';
	
	                // on close callback
	                if (typeof self.opts.onClose === "function") {
	                    self.opts.onClose.call(this);
	                }
	            }, false);
	        } else {
	            self.modal.style.display = 'none';
	            // on close callback
	            if (typeof self.opts.onClose === "function") {
	                self.opts.onClose.call(this);
	            }
	        }
	    };
	
	    Modal.prototype.setContent = function (content) {
	        // check type of content : String or Node
	        if (typeof content === 'string') {
	            this.modalBoxContent.innerHTML = content;
	        } else {
	            this.modalBoxContent.innerHTML = "";
	            this.modalBoxContent.appendChild(content);
	        }
	    };
	
	    Modal.prototype.getContent = function () {
	        return this.modalBoxContent;
	    };
	
	    Modal.prototype.addFooter = function () {
	        // add footer to modal
	        _buildFooter.call(this);
	    };
	
	    Modal.prototype.setFooterContent = function (content) {
	        // set footer content
	        this.modalBoxFooter.innerHTML = content;
	    };
	
	    Modal.prototype.getFooterContent = function () {
	        return this.modalBoxFooter;
	    };
	
	    Modal.prototype.setStickyFooter = function (isSticky) {
	        // if the modal is smaller than the viewport height, we don't need sticky
	        if (!this.isOverflow()) {
	            isSticky = false;
	        }
	
	        if (isSticky) {
	            if (this.modalBox.contains(this.modalBoxFooter)) {
	                this.modalBox.removeChild(this.modalBoxFooter);
	                this.modal.appendChild(this.modalBoxFooter);
	                this.modalBoxFooter.classList.add('tingle-modal-box__footer--sticky');
	                _recalculateFooterPosition.call(this);
	                this.modalBoxContent.style['padding-bottom'] = this.modalBoxFooter.clientHeight + 20 + 'px';
	            }
	        } else if (this.modalBoxFooter) {
	            if (!this.modalBox.contains(this.modalBoxFooter)) {
	                this.modal.removeChild(this.modalBoxFooter);
	                this.modalBox.appendChild(this.modalBoxFooter);
	                this.modalBoxFooter.style.width = 'auto';
	                this.modalBoxFooter.style.left = '';
	                this.modalBoxContent.style['padding-bottom'] = '';
	                this.modalBoxFooter.classList.remove('tingle-modal-box__footer--sticky');
	            }
	        }
	    };
	
	    Modal.prototype.addFooterBtn = function (label, cssClass, callback) {
	        var btn = document.createElement("button");
	
	        // set label
	        btn.innerHTML = label;
	
	        // bind callback
	        btn.addEventListener('click', callback);
	
	        if (typeof cssClass === 'string' && cssClass.length) {
	            // add classes to btn
	            cssClass.split(" ").forEach(function (item) {
	                btn.classList.add(item);
	            });
	        }
	
	        this.modalBoxFooter.appendChild(btn);
	
	        return btn;
	    };
	
	    Modal.prototype.resize = function () {
	        console.warn('Resize is deprecated and will be removed in version 1.0');
	    };
	
	    Modal.prototype.isOverflow = function () {
	        var viewportHeight = window.innerHeight;
	        var modalHeight = this.modalBox.clientHeight;
	
	        return modalHeight >= viewportHeight;
	    };
	
	    /* ----------------------------------------------------------- */
	    /* == private methods */
	    /* ----------------------------------------------------------- */
	
	    function _checkOverflow() {
	        // only if the modal is currently shown
	        if (this.modal.classList.contains('tingle-modal--visible')) {
	            if (this.isOverflow()) {
	                this.modal.classList.add('tingle-modal--overflow');
	            } else {
	                this.modal.classList.remove('tingle-modal--overflow');
	            }
	
	            // TODO: remove offset
	            //_offset.call(this);
	            if (!this.isOverflow() && this.opts.stickyFooter) {
	                this.setStickyFooter(false);
	            } else if (this.isOverflow() && this.opts.stickyFooter) {
	                _recalculateFooterPosition.call(this);
	                this.setStickyFooter(true);
	            }
	        }
	    }
	
	    function _recalculateFooterPosition() {
	        if (!this.modalBoxFooter) {
	            return;
	        }
	        this.modalBoxFooter.style.width = this.modalBox.clientWidth + 'px';
	        this.modalBoxFooter.style.left = this.modalBox.offsetLeft + 'px';
	    }
	
	    function _build() {
	
	        // wrapper
	        this.modal = document.createElement('div');
	        this.modal.classList.add('tingle-modal');
	
	        // remove cusor if no overlay close method
	        if (this.opts.closeMethods.length === 0 || this.opts.closeMethods.indexOf('overlay') === -1) {
	            this.modal.classList.add('tingle-modal--noOverlayClose');
	        }
	
	        this.modal.style.display = 'none';
	
	        // custom class
	        this.opts.cssClass.forEach(function (item) {
	            if (typeof item === 'string') {
	                this.modal.classList.add(item);
	            }
	        }, this);
	
	        // close btn
	        if (this.opts.closeMethods.indexOf('button') !== -1) {
	            this.modalCloseBtn = document.createElement('button');
	            this.modalCloseBtn.classList.add('tingle-modal__close');
	
	            this.modalCloseBtnIcon = document.createElement('span');
	            this.modalCloseBtnIcon.classList.add('tingle-modal__closeIcon');
	            this.modalCloseBtnIcon.innerHTML = '×';
	
	            this.modalCloseBtnLabel = document.createElement('span');
	            this.modalCloseBtnLabel.classList.add('tingle-modal__closeLabel');
	            this.modalCloseBtnLabel.innerHTML = this.opts.closeLabel;
	
	            this.modalCloseBtn.appendChild(this.modalCloseBtnIcon);
	            this.modalCloseBtn.appendChild(this.modalCloseBtnLabel);
	        }
	
	        // modal
	        this.modalBox = document.createElement('div');
	        this.modalBox.classList.add('tingle-modal-box');
	
	        // modal box content
	        this.modalBoxContent = document.createElement('div');
	        this.modalBoxContent.classList.add('tingle-modal-box__content');
	
	        this.modalBox.appendChild(this.modalBoxContent);
	
	        if (this.opts.closeMethods.indexOf('button') !== -1) {
	            this.modal.appendChild(this.modalCloseBtn);
	        }
	
	        this.modal.appendChild(this.modalBox);
	    }
	
	    function _buildFooter() {
	        this.modalBoxFooter = document.createElement('div');
	        this.modalBoxFooter.classList.add('tingle-modal-box__footer');
	        this.modalBox.appendChild(this.modalBoxFooter);
	    }
	
	    function _bindEvents() {
	
	        this._events = {
	            clickCloseBtn: this.close.bind(this),
	            clickOverlay: _handleClickOutside.bind(this),
	            resize: _checkOverflow.bind(this),
	            keyboardNav: _handleKeyboardNav.bind(this)
	        };
	
	        if (this.opts.closeMethods.indexOf('button') !== -1) {
	            this.modalCloseBtn.addEventListener('click', this._events.clickCloseBtn);
	        }
	
	        this.modal.addEventListener('mousedown', this._events.clickOverlay);
	        window.addEventListener('resize', this._events.resize);
	        document.addEventListener("keydown", this._events.keyboardNav);
	    }
	
	    function _handleKeyboardNav(event) {
	        // escape key
	        if (this.opts.closeMethods.indexOf('escape') !== -1 && event.which === 27 && this.isOpen()) {
	            this.close();
	        }
	    }
	
	    function _handleClickOutside(event) {
	        // if click is outside the modal
	        if (this.opts.closeMethods.indexOf('overlay') !== -1 && !_findAncestor(event.target, 'tingle-modal') && event.clientX < this.modal.clientWidth) {
	            this.close();
	        }
	    }
	
	    function _findAncestor(el, cls) {
	        while ((el = el.parentElement) && !el.classList.contains(cls)) {}
	        return el;
	    }
	
	    function _unbindEvents() {
	        if (this.opts.closeMethods.indexOf('button') !== -1) {
	            this.modalCloseBtn.removeEventListener('click', this._events.clickCloseBtn);
	        }
	        this.modal.removeEventListener('mousedown', this._events.clickOverlay);
	        window.removeEventListener('resize', this._events.resize);
	        document.removeEventListener("keydown", this._events.keyboardNav);
	    }
	
	    /* ----------------------------------------------------------- */
	    /* == confirm */
	    /* ----------------------------------------------------------- */
	
	    // coming soon
	
	    /* ----------------------------------------------------------- */
	    /* == alert */
	    /* ----------------------------------------------------------- */
	
	    // coming soon
	
	    /* ----------------------------------------------------------- */
	    /* == helpers */
	    /* ----------------------------------------------------------- */
	
	    function extend() {
	        for (var i = 1; i < arguments.length; i++) {
	            for (var key in arguments[i]) {
	                if (arguments[i].hasOwnProperty(key)) {
	                    arguments[0][key] = arguments[i][key];
	                }
	            }
	        }
	        return arguments[0];
	    }
	
	    function whichTransitionEvent() {
	        var t;
	        var el = document.createElement('tingle-test-transition');
	        var transitions = {
	            'transition': 'transitionend',
	            'OTransition': 'oTransitionEnd',
	            'MozTransition': 'transitionend',
	            'WebkitTransition': 'webkitTransitionEnd'
	        };
	
	        for (t in transitions) {
	            if (el.style[t] !== undefined) {
	                return transitions[t];
	            }
	        }
	    }
	
	    /* ----------------------------------------------------------- */
	    /* == return */
	    /* ----------------------------------------------------------- */
	
	    return {
	        modal: Modal
	    };
	});

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _request = __webpack_require__(11);
	
	var _request2 = _interopRequireDefault(_request);
	
	var _tingle = __webpack_require__(9);
	
	var _tingle2 = _interopRequireDefault(_tingle);
	
	var _ListaLojas2 = __webpack_require__(12);
	
	var _ListaLojas3 = _interopRequireDefault(_ListaLojas2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var pageLojas = function (_ListaLojas) {
		_inherits(pageLojas, _ListaLojas);
	
		function pageLojas() {
			_classCallCheck(this, pageLojas);
	
			var _this = _possibleConstructorReturn(this, (pageLojas.__proto__ || Object.getPrototypeOf(pageLojas)).call(this));
	
			var me = _this;
			var isEscolhe = document.body.id.indexOf('participantes') >= 0;
			_this.getEstados(_this.setup);
			if (!isEscolhe) {
				var _ret;
	
				return _ret = false, _possibleConstructorReturn(_this, _ret);
			}
			var modal = new _tingle2.default.modal({
				footer: true,
				stickyFooter: false,
				closeMethods: ['overlay', 'button', 'escape'],
				closeLabel: "Fechar",
				cssClass: ['google-maps-modal']
			});
			_this.modal = modal;
	
			var modalStore = new _tingle2.default.modal({
				footer: true,
				stickyFooter: false,
				closeMethods: ['overlay', 'button', 'escape'],
				closeLabel: "Fechar",
				cssClass: ['store-confirm-modal']
			});
	
			modalStore.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--danger', function () {
				modalStore.close();
			});
			return _this;
		}
	
		_createClass(pageLojas, [{
			key: 'renderStores',
			value: function renderStores() {
				var me = this;
				var stores = me.cidades[me.selectedCidade.value].lojas;
				var d = document;
				var storeContent = d.getElementById('list-store-content');
				if (!storeContent.className.match(/loaded/g)) {
					storeContent.className += ' loaded';
				}
				d.getElementById('stores-list').innerHTML = '';
				me.storesCountNode.innerHTML = stores.length;
				if (stores.length > 1) {
					me.storesCountUnitNode.innerHTML = 'lojas';
				} else {
					me.storesCountUnitNode.innerHTML = 'loja';
				}
	
				var storesItems = stores.map(function (store) {
					var item = d.createElement('li');
					item.id = 'store-item-' + store.loja_id;
					item.className = 'stores-items';
	
					var itemText = d.createElement('div');
					var itemName = d.createElement('h3');
					var itemAddr = d.createElement('div');
					var itemPhone = d.createElement('div');
					var itemSchedule = d.createElement('div');
	
					itemText.className = 'stores-items-text stores-items-parts';
					itemName.innerHTML = store.nome;
					itemAddr.innerHTML = store.endereco;
					itemPhone.innerHTML = 'Telefone: ' + store.telefone;
					itemSchedule.innerHTML = store.horarios_atendimento;
					itemSchedule.className = 'schedule-text';
	
					itemText.appendChild(itemName);
					itemText.appendChild(itemAddr);
					itemText.appendChild(itemPhone);
					itemText.appendChild(itemSchedule);
	
					var itemActions = d.createElement('div');
					var gmapButton = d.createElement('button');
					itemActions.className = 'stores-items-actions stores-items-parts';
	
					gmapButton.innerHTML = 'Ver no mapa';
					gmapButton.className = 'google-maps-irame';
					gmapButton.setAttribute('data-src', store.gmapIframe);
	
					gmapButton.addEventListener('mousedown', function (e) {
						if (e.preventDefault !== undefined) {
							e.preventDefault();
						}
						me.displayGoogleMaps(e.target);
					});
	
					gmapButton.addEventListener('touchdown', function (e) {
						if (e.preventDefault !== undefined) {
							e.preventDefault();
						}
						me.displayGoogleMaps(e.target);
					});
	
					itemActions.appendChild(gmapButton);
	
					item.appendChild(itemText);
					item.appendChild(itemActions);
	
					return item;
				}).forEach(function (item) {
					d.getElementById('stores-list').appendChild(item);
				});
			}
		}]);
	
		return pageLojas;
	}(_ListaLojas3.default);
	
	;
	
	exports.default = new pageLojas();

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	exports.default = function (endPoint, type, cfg) {
		var url = document.basePath + '/' + endPoint;
		var xhr = new XMLHttpRequest();
		xhr.open(type, endPoint);
		xhr.addEventListener('readystatechange', function (e) {
			if (xhr.readyState === 4 && xhr.status === 200) {
				if (cfg.success !== undefined && cfg.success.call !== undefined) {
					if (cfg.context !== undefined) {
						cfg.success.call(cfg.context, xhr);
					} else {
						cfg.success.call(xhr, xhr);
					}
				}
			} else if (xhr.readyState === 4) {
				if (cfg.error !== undefined && cfg.error.call !== undefined) {
					if (cfg.context !== undefined) {
						cfg.error.call(xhr, cfg.context);
					} else {
						cfg.error.call(xhr, xhr);
					}
				}
			}
		});
	
		if (cfg.headers !== undefined) {
			cfg.headers.forEach(function (item) {
				cfg.setRequestHeader(item.name, item.value);
			});
		}
	
		if (cfg.data !== undefined) {
			xhr.send(JSON.stringify(cfg.data));
		} else {
			xhr.send();
		}
	};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _request = __webpack_require__(11);
	
	var _request2 = _interopRequireDefault(_request);
	
	var _tingle = __webpack_require__(9);
	
	var _tingle2 = _interopRequireDefault(_tingle);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var _class = function () {
		function _class() {
			_classCallCheck(this, _class);
	
			this.estadosSelect = false;
			this.estados = {};
			this.estadosOptions = [];
			this.selectedEstadoIndex = 0;
			this.selectedEstado = false;
			this.cidadesSelect = false;
			this.cidades = {};
			this.cidadesOptions = [];
			this.selectedCidadeIndex = 0;
			this.selectedCidade = false;
			this.storesCountUnitNode = false;
			this.storesCountNode = false;
			this.storesCityNode = false;
			this.storeId = 0;
		}
	
		_createClass(_class, [{
			key: 'setup',
			value: function setup() {
				var me = this;
				me.estadosSelect = document.getElementById('filtro_estado');
				me.cidadesSelect = document.getElementById('filtro_cidade');
	
				me.storesCountNode = document.getElementById('lojas-count');
				me.storesCityNode = document.getElementById('current-filter-cidade');
				me.storesCountUnitNode = document.getElementById('lojas-count-unit');
	
				var estadosIDs = Object.getOwnPropertyNames(me.estados);
				var estadosOptions = estadosIDs.map(function (id) {
					var optionElement = document.createElement('option');
					optionElement.innerHTML = me.estados[id].uf;
					optionElement.value = me.estados[id].estado_id;
					return optionElement;
				});
				this.estadosOptions = estadosOptions;
				this.renderEstados();
			}
		}, {
			key: 'getEstados',
			value: function getEstados(cb) {
				var me = this;
				(0, _request2.default)('ws/lista-estados', 'GET', {
					success: function success(xhr) {
						var response = JSON.parse(xhr.response);
						response.estados.forEach(function (estado) {
							me.estados[estado.estado_id] = estado;
						});
						if (cb !== undefined) {
							cb.call(me);
						}
					}
				});
			}
		}, {
			key: 'getCidades',
			value: function getCidades(cb) {
				var me = this;
				var estadoObj = me.estados[me.selectedEstado.value];
				if (estadoObj.cidades === undefined) {
					estadoObj.cidades = [];
				}
				if (estadoObj.cidades.length < 1) {
					(0, _request2.default)('ws/lista-cidades', 'POST', {
						data: { 'estado_id': me.selectedEstado.value },
						success: function success(xhr) {
							try {
								var response = JSON.parse(xhr.response);
								estadoObj.cidades = response.cidades.map(function (item) {
									return item;
								});
								estadoObj.cidades.forEach(function (cidade) {
									me.cidades[cidade.cidade_id] = cidade;
								});
								me.renderCidades();
							} catch (err) {
								console.log(err);
							}
						}
					});
				} else {
					me.renderCidades();
				}
			}
		}, {
			key: 'getLojas',
			value: function getLojas() {
				var me = this;
				me.selectedCidadeIndex = me.cidadesSelect.selectedIndex;
				me.selectedCidade = me.cidadesSelect.options[me.selectedCidadeIndex];
				(0, _request2.default)('ws/lista-lojas', 'POST', {
					data: { 'cidade_id': me.selectedCidade.value },
					success: function success(xhr) {
						try {
							var stores = JSON.parse(xhr.response);
							if (me.cidades[me.selectedCidade.value].lojas === undefined) {
								me.cidades[me.selectedCidade.value].lojas = [];
							}
							me.storesCityNode.innerHTML = me.selectedCidade.innerHTML;
							me.cidades[me.selectedCidade.value].lojas = stores.lojas;
							me.renderStores();
						} catch (err) {}
					}
				});
			}
		}, {
			key: 'renderCidades',
			value: function renderCidades() {
				var me = this;
				var estadoObj = me.estados[me.selectedEstado.value];
	
				me.cidadesOptions = estadoObj.cidades.map(function (cidade) {
					var cidadeOption = document.createElement('option');
					cidadeOption.innerHTML = cidade.cidade;
					cidadeOption.value = cidade.cidade_id;
					return cidadeOption;
				});
	
				estadoObj.cidadesOptions = me.cidadesOptions;
				if (estadoObj.cidadesOptions[0] !== undefined) {
					estadoObj.cidadesOptions[0].selected = true;
				}
	
				me.selectedCidade = me.cidadesOptions[0].selected;
				me.cidadesSelect.innerHTML = '';
				estadoObj.cidadesOptions.forEach(function (option) {
					me.cidadesSelect.appendChild(option);
				});
				me.cidadesSelect.parentElement.querySelector('.select-value').innerHTML = estadoObj.cidadesOptions[0].innerHTML;
				me.getLojas();
			}
		}, {
			key: 'renderEstados',
			value: function renderEstados() {
				var me = this;
				this.estadosOptions.forEach(function (option) {
					me.estadosSelect.appendChild(option);
				});
	
				me.estadosSelect.addEventListener('change', function (e) {
					me.selectedEstadoIndex = me.estadosSelect.selectedIndex;
					me.selectedEstado = me.estadosSelect.options[me.selectedEstadoIndex];
					me.getCidades();
				});
	
				me.cidadesSelect.addEventListener('change', function (e) {
					me.selectedCidadeIndex = me.cidadesSelect.selectedIndex;
					me.selectedCidade = me.cidadesSelect.options[me.selectedCidadeIndex];
					me.getLojas();
				});
			}
		}, {
			key: 'displayGoogleMaps',
			value: function displayGoogleMaps(btn) {
				var me = this;
				var src = btn.getAttribute('data-src');
				var iframe = '<iframe src="' + src + '" style="width: 100%; height: 100%;"/>';
				this.modal.setContent(iframe);
				this.modal.open();
			}
		}]);
	
		return _class;
	}();
	
	exports.default = _class;
	;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _request = __webpack_require__(11);
	
	var _request2 = _interopRequireDefault(_request);
	
	var _tingle = __webpack_require__(9);
	
	var _tingle2 = _interopRequireDefault(_tingle);
	
	var _ListaLojas2 = __webpack_require__(12);
	
	var _ListaLojas3 = _interopRequireDefault(_ListaLojas2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var ParticipateThree = function (_ListaLojas) {
		_inherits(ParticipateThree, _ListaLojas);
	
		function ParticipateThree() {
			_classCallCheck(this, ParticipateThree);
	
			var _this = _possibleConstructorReturn(this, (ParticipateThree.__proto__ || Object.getPrototypeOf(ParticipateThree)).call(this));
	
			var me = _this;
			_this.getEstados(_this.setup);
			var modal = new _tingle2.default.modal({
				footer: true,
				stickyFooter: false,
				closeMethods: ['overlay', 'button', 'escape'],
				closeLabel: "Fechar",
				cssClass: ['google-maps-modal']
			});
			_this.modal = modal;
	
			var modalStore = new _tingle2.default.modal({
				footer: true,
				stickyFooter: false,
				closeMethods: ['overlay', 'button', 'escape'],
				closeLabel: "Fechar",
				cssClass: ['store-confirm-modal']
			});
	
			modalStore.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--danger', function () {
				modalStore.close();
			});
	
			modalStore.addFooterBtn('Sim, trocar nesta loja', 'tingle-btn tingle-btn--pull-right tingle-btn--primary', function () {
				var form = document.createElement('form');
				var store = document.createElement('input');
				form.method = 'POST';
				store.name = 'store';
				store.type = 'hidden';
				store.value = me.storeId;
				form.appendChild(store);
				document.body.appendChild(form);
				form.submit();
				modalStore.close();
			});
			_this.modalStore = modalStore;
			return _this;
		}
	
		_createClass(ParticipateThree, [{
			key: 'aqcuireCoupon',
			value: function aqcuireCoupon(btn) {
				var me = this;
				var loja = me.cidades[me.selectedCidade.value].lojas.filter(function (loja) {
					return loja.loja_id == me.storeId;
				});
				var message = void 0;
				var nome = void 0;
				me.storeId = btn.getAttribute('data-id');
				if (loja && loja[0] !== undefined && loja[0].nome !== undefined) {
					nome = loja[0].nome;
				} else {
					var row = document.getElementById('store-item-' + me.storeId);
					nome = row.querySelector('h3').innerHTML;
				}
				message = ' <div class="warn">Aten\xE7\xE3o:</div>\n\t\t\t\t\t<h4>Voc\xEA Selecionou a loja <strong>' + nome + '</strong></h4>\n\t\t\t\t\t<p>Uma vez selecionado a loja, n\xE3o ter\xE1 como alterar depois.</p>';
				me.modalStore.setContent(message);
				/*
	   me.modalStore.open();
	   chamada direta 
	   */
	
				var form = document.createElement('form');
				var store = document.createElement('input');
				form.method = 'POST';
				store.name = 'store';
				store.type = 'hidden';
				store.value = me.storeId;
				form.appendChild(store);
				document.body.appendChild(form);
				form.submit();
				modalStore.close();
			}
		}, {
			key: 'renderStores',
			value: function renderStores() {
				var me = this;
				var stores = me.cidades[me.selectedCidade.value].lojas;
				var d = document;
				var storeContent = d.getElementById('select-store-content');
				if (!storeContent.className.match(/loaded/g)) {
					storeContent.className += ' loaded';
				}
				d.getElementById('stores-list').innerHTML = '';
				if (me.storesCountUnitNode == null) {
					me.storesCountUnitNode = d.getElementById('lojas-count-unit');
				}
				me.storesCountUnitNode.innerHTML = stores.length > 1 ? 'lojas' : 'loja';
				me.storesCountNode.innerHTML = stores.length;
				var storesItems = stores.map(function (store) {
					var item = d.createElement('li');
					item.id = 'store-item-' + store.loja_id;
					item.className = 'stores-items';
					if (store.estoque_brinde < 1) {
						item.className += ' no-unavailable';
					}
	
					var itemText = d.createElement('div');
					var itemName = d.createElement('h3');
					var itemAddr = d.createElement('div');
					var itemPhone = d.createElement('div');
					var itemSchedule = d.createElement('div');
	
					itemText.className = 'stores-items-text stores-items-parts';
					itemName.innerHTML = store.nome;
					itemAddr.innerHTML = store.endereco;
					itemPhone.innerHTML = 'Telefone: ' + store.telefone;
					itemSchedule.innerHTML = store.horarios_atendimento;
					itemSchedule.className = 'schedule-text';
	
					itemText.appendChild(itemName);
					itemText.appendChild(itemAddr);
					itemText.appendChild(itemPhone);
					itemText.appendChild(itemSchedule);
	
					var itemActions = d.createElement('div');
					var gmapButton = d.createElement('button');
					var exchangeButton = d.createElement('button');
					itemActions.className = 'stores-items-actions stores-items-parts';
	
					gmapButton.innerHTML = 'Ver no mapa';
					gmapButton.className = 'google-maps-irame';
					gmapButton.setAttribute('data-src', store.gmapIframe);
	
					gmapButton.addEventListener('mousedown', function (e) {
						if (e.preventDefault !== undefined) {
							e.preventDefault();
						}
						me.displayGoogleMaps(e.target);
					});
	
					gmapButton.addEventListener('touchdown', function (e) {
						if (e.preventDefault !== undefined) {
							e.preventDefault();
						}
						me.displayGoogleMaps(e.target);
					});
					if (store.estoque_brinde > 0) {
						exchangeButton.addEventListener('mousedown', function (e) {
							if (e.preventDefault !== undefined) {
								e.preventDefault();
							}
							me.aqcuireCoupon(e.target);
						});
	
						exchangeButton.addEventListener('touchdown', function (e) {
							if (e.preventDefault !== undefined) {
								e.preventDefault();
							}
							me.aqcuireCoupon(e.target);
						});
	
						exchangeButton.innerHTML = 'Trocar nesta loja';
						exchangeButton.className = 'exchange-button';
						exchangeButton.setAttribute('data-id', store.loja_id);
					} else {
						exchangeButton.innerHTML = 'Esgotado';
						exchangeButton.className = 'unavailable-exchange-button';
					}
	
					itemActions.appendChild(gmapButton);
					itemActions.appendChild(exchangeButton);
	
					item.appendChild(itemText);
					item.appendChild(itemActions);
	
					return item;
				}).forEach(function (item) {
					d.getElementById('stores-list').appendChild(item);
				});
			}
		}]);
	
		return ParticipateThree;
	}(_ListaLojas3.default);
	
	;
	
	exports.default = new ParticipateThree();

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _tingle = __webpack_require__(9);
	
	var _tingle2 = _interopRequireDefault(_tingle);
	
	var _Masky = __webpack_require__(1);
	
	var _Masky2 = _interopRequireDefault(_Masky);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var cupom = function () {
		function cupom() {
			_classCallCheck(this, cupom);
	
			this.injectFb();
			this.setup();
			this.setupBtns();
		}
	
		_createClass(cupom, [{
			key: 'injectFb',
			value: function injectFb() {
				var me = this;
				window.fbAsyncInit = function () {
					FB.init({
						appId: 246464092427685,
						status: true,
						xfbml: true,
						version: 'v2.9'
					});
					me.setupFB();
				};
				(function (d, s, id) {
					var js,
					    fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {
						return;
					}
					js = d.createElement(s);js.id = id;js.async = true;
					js.src = "//connect.facebook.net/pt_BR/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				})(document, 'script', 'facebook-jssdk');
			}
		}, {
			key: 'showSuccess',
			value: function showSuccess(message) {
				var me = this;
				var modal = new _tingle2.default.modal({
					footer: true,
					stickyFooter: false,
					closeMethods: ['overlay', 'button', 'escape'],
					closeLabel: "Fechar",
					cssClass: ['success-modal', 'beauty-success-modal', 'beauty-modal']
				});
				me.successModal = modal;
				var html = '<button id="beauty-modal-close-btn"><span>×</span></button>';
				html += message;
				modal.setContent(html);
				modal.open();
	
				document.getElementById('beauty-modal-close-btn').addEventListener('mousedown', function (e) {
					me.closeSMSModal();
				});
			}
		}, {
			key: 'showError',
			value: function showError(message) {
				var modal = new _tingle2.default.modal({
					footer: true,
					stickyFooter: false,
					closeMethods: ['overlay', 'button', 'escape'],
					closeLabel: "Fechar",
					cssClass: ['error-modal']
				});
				modal.setContent(message);
				modal.open();
			}
		}, {
			key: 'sendEmail',
			value: function sendEmail() {
				var me = this;
				window.beginLoader();
				var url = document.basePath + '/index/send-email';
				var xhr = new XMLHttpRequest();
				xhr.open('GET', url);
				xhr.addEventListener('readystatechange', function (e) {
					if (xhr.readyState === 4) {
						hideLoader();
					}
					if (xhr.readyState === 4 && xhr.status === 200) {
						//success
						me.showSuccess("E-mail enviado com sucesso ! ");
					} else if (xhr.readyState === 4) {
						//error
						me.showError("<span class='ferr1'>Erro ao tentar enviar o E-mail </span> <br> <span class='ferr2'>por favor, tente mais tarde</span>");
					}
				});
				xhr.send();
			}
		}, {
			key: 'sendSms',
			value: function sendSms(numero) {
				var me = this;
				numero = numero.replace(/\D/g, '');
				window.beginLoader();
				var url = document.basePath + '/index/send-sms?numero=' + numero;
				var xhr = new XMLHttpRequest();
				xhr.open('GET', url);
				xhr.addEventListener('readystatechange', function (e) {
					if (xhr.readyState === 4) {
						hideLoader();
					}
					if (xhr.readyState === 4 && xhr.status === 200) {
						//success
						me.showSuccess("SMS enviado com sucesso ! ");
					} else if (xhr.readyState === 4) {
						//error
						me.showError("<span class='ferr1'>Erro ao tentar enviar o SMS </span> <br> <span class='ferr2'>por favor, tente mais tarde</span>");
					}
				});
				xhr.send();
			}
		}, {
			key: 'buildSmsModal',
			value: function buildSmsModal() {
				var me = this;
				var modal = new _tingle2.default.modal({
					footer: true,
					stickyFooter: false,
					closeMethods: ['overlay', 'button', 'escape'],
					closeLabel: "Fechar",
					cssClass: ['beauty-modal', 'sms-modal']
				});
				me.smsModal = modal;
				var html = '\n\t\t\t<button id="beauty-modal-close-btn"><span>\xD7</span></button>\n\t\t\t<div id="sms-phone-block">\n\t\t\t\t<label for="sms-phone">N\xFAmero do Celular</label>\n\t\t\t\t<input type="tel" name="sms-phone" id="sms-phone">\n\t\t\t</div>';
				modal.setContent(html);
	
				modal.addFooterBtn('Enviar', 'tingle-btn tingle-btn--primary', function () {
					var numero = document.getElementById('sms-phone').value;
					modal.close();
					me.sendSms(numero);
				});
				modal.open();
				var sms = document.getElementById('sms-phone');
	
				try {
					var datas = JSON.parse(localStorage.getItem('prevForm'));
					var num = datas.formHistory.filter(function (item) {
						return item.celular !== undefined;
					});
					if (num !== undefined && num.length === 1) {
						if (sms !== null) {
							sms.value = num[0].celular;
						}
					}
				} catch (err) {
					console.log(err);
				}
	
				sms.addEventListener('keyup', function (e) {
					var numberLength = e.target.value.replace(/\D/g, '').length;
					var separatorKey = 6;
					var maskProp = {};
					var o = '';
					setTimeout(function () {
						maskProp[0] = '(';
						maskProp[2] = ')';
						separatorKey = numberLength > 10 ? 7 : separatorKey;
						maskProp[separatorKey] = '-';
						o = global.Masky(maskProp, e.target.value);
						e.target.value = o.substr(0, 14);
					}, 59);
				});
				document.getElementById('beauty-modal-close-btn').addEventListener('mousedown', function (e) {
					me.closeSMSModal();
				});
			}
		}, {
			key: 'closeSMSModal',
			value: function closeSMSModal() {
				var me = this;
				if (me.smsModal !== undefined && me.smsModal !== null) {
					if (me.smsModal.close !== undefined) {
						me.smsModal.close();
					}
				}
	
				if (me.successModal !== undefined && me.successModal !== null) {
					if (me.successModal.close !== undefined) {
						me.successModal.close();
					}
				}
				if (me.emailShareModal !== undefined && me.emailShareModal !== null) {
					if (me.emailShareModal.close !== undefined) {
						me.emailShareModal.close();
					}
				}
			}
		}, {
			key: 'shareEmail',
			value: function shareEmail(name, email) {
				var me = this;
				window.beginLoader();
				var url = document.basePath + ('/index/share-email?email=' + email + '&name=' + name);
				var xhr = new XMLHttpRequest();
				xhr.open('GET', url);
				xhr.addEventListener('readystatechange', function (e) {
					if (xhr.readyState === 4) {
						hideLoader();
					}
					if (xhr.readyState === 4 && xhr.status === 200) {
						//success
						me.showSuccess("E-mail enviado com sucesso ! ");
					} else if (xhr.readyState === 4) {
						//error
						me.showError("<span class='ferr1'>Erro ao tentar enviar o E-mail </span> <br> <span class='ferr2'>por favor, tente mais tarde</span>");
					}
				});
				xhr.send();
			}
		}, {
			key: 'buildEmailShareModal',
			value: function buildEmailShareModal() {
				var me = this;
				var modal = new _tingle2.default.modal({
					footer: true,
					stickyFooter: false,
					closeMethods: ['overlay', 'button', 'escape'],
					closeLabel: "Fechar",
					cssClass: ['email-share-modal', 'beauty-modal']
				});
				me.emailShareModal = modal;
	
				var html = '\n\t\t\t<button id="beauty-modal-close-btn"><span>\xD7</span></button>\n\t\t\t<div class="email-share-block">\n\t\t\t\t<label for="share_name">Nome</label>\n\t\t\t\t<input type="text" name="share_name" id="share_name">\n\t\t\t</div>\n\t\t\t<div class="email-share-block">\n\t\t\t\t<label for="share_email">E-mail</label>\n\t\t\t\t<input type="email" name="share_email" id="share_email">\n\t\t\t</div>';
				modal.setContent(html);
	
				modal.addFooterBtn('Enviar', 'tingle-btn tingle-btn--primary', function () {
					var email = document.getElementById('share_email').value;
					var name = document.getElementById('share_name').value;
					modal.close();
					me.shareEmail(name, email);
				});
				modal.open();
				var sms = document.getElementById('sms-phone');
	
				document.getElementById('beauty-modal-close-btn').addEventListener('mousedown', function (e) {
					me.closeSMSModal();
				});
			}
		}, {
			key: 'setup',
			value: function setup() {
				var d = document;
				var me = this;
				d.body.addEventListener('mousedown', function (e) {
					if (e.target.id === 'dispatch-email') {
						me.sendEmail();
					} else if (e.target.id === 'dispatch-sms') {
						me.buildSmsModal();
					} else if (e.target.id === 'send-using-email') {
						me.buildEmailShareModal();
					} else if (e.target.id === 'beauty-modal-close-btn') {
						console.log(e.target);
						me.closeSMSModal();
					}
				});
				//aqui configurar o listener para disparo de SMS, E-mail
				//verificar como que será o compartilhamento por E-mail também.
			}
		}, {
			key: 'setupFB',
			value: function setupFB() {
				var d = document;
				var fbBtn = d.getElementById('share-on-facebook');
				fbBtn.addEventListener('click', function (e) {
					e.preventDefault();
					FB.ui({
						method: 'share',
						display: 'popup',
						hashtag: '#abraceaBeauty',
						quote: 'Semana do Abraço - The Beauty Box',
						href: 'https://www.semanadoabraco.com.br' }, function (response) {
						console.log(response);
						if (response !== undefined) {
							//undefined == cancelou
							//update database
							var url = document.basePath + '/index/facebook-share';
							var xhr = new XMLHttpRequest();
							xhr.open('GET', url);
							xhr.send();
						}
					});
				});
			}
		}, {
			key: 'setupBtns',
			value: function setupBtns() {
				var mobBlock = document.getElementById('lets-mobile-share');
				if (navigator.userAgent.match(/android/)) {
					//setup datas dor android
				} else if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
					//setup datas for ios
				} else {
						//return mobBlock.remove();
					}
				var downloadCupom = function downloadCupom(e) {
					var url = e.target.href;
					if (vendor == 'iOS') {
						url += "?html=1";
					}
					//iOS: abrir janela com as instruções para Download
					//android: download direto.
				};
				/*
	    let whatsappBtn = document.getElementById('send-using-whatsapp');
	    let downloadBtn = document.getElementById('share-on-whatsapp');
	    downloadBtn.addEventListener('mousedown', downloadCupom );
	    downloadBtn.addEventListener('touchdown', downloadCupom );
	    
	   if(downloadBtn !== undefined) {
	       let url = `/ws/get-image/id/${result.brinde.brinde_id}/loja_id/${result.brinde.loja_id}/pic/${result.uniq}`;
	       if ( ! result.uniq.match(/\.jpg$/) ) {
	           url += ".jpg";
	       }
	       let origin = window.location !== undefined && window.location.origin !== undefined ? window.location.origin: '';
	       let pathname = window.location !== undefined && window.location.pathname !== undefined ? window.location.pathname: '';
	       let baseURL = '' + origin.toString() + pathname.toString();
	         if ( vendor == 'iOS' ) {
	           url += "?html=1";
	       }
	       if( baseURL.length > 0) {
	           downloadBtn.href = baseURL + url;
	       } else {
	           downloadBtn.href = url;
	       }
	       downloadBtn.target = "_blank";
	   }
	     */
			}
		}]);
	
		return cupom;
	}();
	
	exports.default = new cupom();
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map