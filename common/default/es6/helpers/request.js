export default ( endPoint, type, cfg ) => {
	let url = document.basePath + '/' + endPoint;
	let xhr = new XMLHttpRequest();
	xhr.open( type, endPoint  );
	xhr.addEventListener( 'readystatechange', ( e ) => {
		if ( xhr.readyState === 4 && xhr.status === 200 ) {
			if ( cfg.success !== undefined && cfg.success.call !== undefined ) {
				if ( cfg.context !== undefined ) {
					cfg.success.call ( cfg.context, xhr );
				} else {
					cfg.success.call ( xhr, xhr );
				}
			}
		} else if ( xhr.readyState === 4 ) {
			if ( cfg.error !== undefined && cfg.error.call !== undefined ) {
				if ( cfg.context !== undefined ) {
					cfg.error.call ( xhr, cfg.context );
				} else {
					cfg.error.call ( xhr, xhr );
				}
			}
		}
	});

	if ( cfg.headers !== undefined ) {
		cfg.headers.forEach ( function ( item ) {
			cfg.setRequestHeader( item.name, item.value );
		})
	}

	if( cfg.data !== undefined ) {
		xhr.send( JSON.stringify( cfg.data ) );
	} else {
		xhr.send( );
	}
};
