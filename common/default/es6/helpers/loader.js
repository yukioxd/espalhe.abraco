export function beginLoader () {
    "use strict";
    let html = `
        <div id="peek-loading">
            <div class="peeek-loading">
                <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                </ul>
            </div>
        </div>`;
    let prev = document.getElementById('peek-loading');
    if ( prev === null ) {
        document.body.innerHTML += html;
    }
    if ( prev !== null ) {
        if ( prev.className.match(/disabled/g) ) {
            prev.className= '';
        } else {
            //donothing
        }
    }
}

export function hideLoader () {
    "use strict";
    let prev = document.getElementById('peek-loading');
    if ( prev !== null ) {
        if ( prev.className.match(/disabled/g) ) {
            //donothing
        } else {
            prev.className += 'disabled';
        }
    }
    setTimeout ( () => {
        let prev = document.getElementById('peek-loading');
        if ( prev !== null ) {
            prev.remove();
        }
    }, 400 );

}