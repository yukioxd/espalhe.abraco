export default function ( cep, myAlert ) {
    let d = document;
    let resultCEP = cep.replace(/\D/g, '');
    if ( resultCEP.length === 8 ) {
        let xhr = new XMLHttpRequest();
        xhr.open( 'GET', '//api.postmon.com.br/v1/cep/' +  resultCEP );
        xhr.addEventListener('readystatechange', () => {
            if ( xhr.readyState === 4 && xhr.status === 200 ) {
                let response = xhr.response;
                try {
                    let parsedResponse = JSON.parse( response );
                    if ( parsedResponse.estado !== undefined ) {
                        d.getElementById('estado').value = parsedResponse.estado;
                    }
                    if ( parsedResponse.cidade !== undefined ) {
                        d.getElementById('cidade').value = parsedResponse.cidade;
                    }
                    if ( parsedResponse.logradouro !== undefined ) {
                        d.getElementById('endereco').value = parsedResponse.logradouro;
                    }
                } catch( err ) {
                }
            } else if ( xhr.readyState === 4 ) {
                myAlert( 'CEP Não encontrado', false );
            }
        });
        xhr.send();
    } else {
        myAlert( 'CEP Inválido', false );
        d.getElementById( 'cep' ).value = '';
    }
    return resultCEP;
};
