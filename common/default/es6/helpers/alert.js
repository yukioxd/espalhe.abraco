export default function ( message, isSuccess ) {
	let prev = document.getElementById( 'simple-alert' );
	if ( prev !== null ) {
		prev.remove();
	}

	let alertBlock = document.createElement('div');
	let messageElement = document.createElement('span');
	let closeAlert = document.createElement('button');
	closeAlert.type = 'button';
	closeAlert.onclick = ( e ) => {
		e.preventDefault();
		alertBlock.remove();
	}

	closeAlert.id = 'close-alert';
	closeAlert.innerHTML = '&#10005;';

	alertBlock.id = 'simple-alert';
	alertBlock.className  = isSuccess ? 'simple-alert success' :'simple-alert error';
	messageElement.innerHTML = message;
	alertBlock.appendChild( messageElement );
	alertBlock.appendChild( closeAlert );
	document.body.appendChild( alertBlock );
	return alertBlock;
}