export default (cfg, str) => {
	var _keys = Object.keys(cfg);
	
	var sample = str.replace(/\D/g, '').split('');
	var output = [];
	sample.forEach(function (c, k) {
		  if (_keys.indexOf(k.toString()) >= 0) {
			    output.push(cfg[k]);
		  }
		  output.push(c);
	});
	return output.join('');
};
