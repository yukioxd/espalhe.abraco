import request from './../helpers/request.js';
import tingle from './../helpers/tingle.js';

export default class {
	estadosSelect = false;
	estados = { };
	estadosOptions = [];
	selectedEstadoIndex = 0;
	selectedEstado = false;

	cidadesSelect = false;
	cidades = { };
	cidadesOptions = [];
	selectedCidadeIndex = 0;
	selectedCidade = false;

	storesCountUnitNode = false;
	storesCountNode = false;
	storesCityNode = false;
	storeId = 0;

	setup ( ) {
		let me = this;
		me.estadosSelect = document.getElementById( 'filtro_estado' );
		me.cidadesSelect = document.getElementById( 'filtro_cidade' );

		me.storesCountNode = document.getElementById('lojas-count');
		me.storesCityNode = document.getElementById('current-filter-cidade');
		me.storesCountUnitNode = document.getElementById('lojas-count-unit');

		let estadosIDs = Object.getOwnPropertyNames( me.estados );
		let estadosOptions = estadosIDs.map ( id => {
			let optionElement = document.createElement( 'option' );
			optionElement.innerHTML = me.estados[ id ].uf;
			optionElement.value = me.estados[ id ].estado_id;
			return optionElement;
		});
		this.estadosOptions = estadosOptions;
		this.renderEstados();
	}

	getEstados ( cb ) {
		let me = this;
		request('ws/lista-estados', 'GET', {
			success: ( xhr ) => {
				let response = JSON.parse( xhr.response );
				response.estados.forEach ( estado => {
					me.estados[ estado.estado_id ] = estado;
				});
				if ( cb !== undefined ) {
					cb.call ( me );
				}
			}
		});
	}

	getCidades ( cb ) {
		let me = this;
		let estadoObj = me.estados[ me.selectedEstado.value ];
		if ( estadoObj.cidades === undefined ) {
			estadoObj.cidades = [];
		}
		if ( estadoObj.cidades.length < 1 ) {
			request( 'ws/lista-cidades', 'POST', {
				data: { 'estado_id' : me.selectedEstado.value },
				success: ( xhr ) => {
					try {
						let response = JSON.parse( xhr.response );
						estadoObj.cidades = response.cidades.map ( item => item );
						estadoObj.cidades.forEach ( cidade => {
							me.cidades[ cidade.cidade_id ] = cidade;
						});
						me.renderCidades( );
					} catch( err ) {
						console.log( err );
					}
				}
			});
		} else {
			me.renderCidades( );
		}
	}

	getLojas ( ) {
		let me = this;
		me.selectedCidadeIndex = me.cidadesSelect.selectedIndex;
		me.selectedCidade = me.cidadesSelect.options[ me.selectedCidadeIndex ];
		request( 'ws/lista-lojas', 'POST', {
			data: {'cidade_id': me.selectedCidade.value},
			success: ( xhr ) => {
				try {
					let stores = JSON.parse( xhr.response );
					if ( me.cidades[ me.selectedCidade.value].lojas === undefined ) {
						me.cidades[ me.selectedCidade.value].lojas = [ ];
					}
					me.storesCityNode.innerHTML = me.selectedCidade.innerHTML;
					me.cidades[ me.selectedCidade.value].lojas = stores.lojas;
					me.renderStores( );
				} catch( err ) {
				}
			}
		});
	}
	renderCidades ( ) {
		let me = this;
		let estadoObj = me.estados[ me.selectedEstado.value ];

		me.cidadesOptions = estadoObj.cidades.map ( cidade => {
			let cidadeOption = document.createElement('option');
			cidadeOption.innerHTML = cidade.cidade;
			cidadeOption.value = cidade.cidade_id;
			return cidadeOption;
		});

		estadoObj.cidadesOptions = me.cidadesOptions;
		if ( estadoObj.cidadesOptions[0] !== undefined ) {
			estadoObj.cidadesOptions[0].selected = true;
		}

		me.selectedCidade = me.cidadesOptions[0].selected;
		me.cidadesSelect.innerHTML = '';
		estadoObj.cidadesOptions.forEach ( option => {
			me.cidadesSelect.appendChild( option );
		});
		me.cidadesSelect.parentElement.querySelector('.select-value').innerHTML = estadoObj.cidadesOptions[0].innerHTML;
		me.getLojas( );
	}
	renderEstados ( ) {
		let me = this;
		this.estadosOptions.forEach ( option => {
			me.estadosSelect.appendChild ( option );
		});

		me.estadosSelect.addEventListener( 'change', ( e ) => {
			me.selectedEstadoIndex = me.estadosSelect.selectedIndex;
			me.selectedEstado = me.estadosSelect.options[ me.selectedEstadoIndex ];
			me.getCidades( );
		});

		me.cidadesSelect.addEventListener( 'change', ( e ) => {
			me.selectedCidadeIndex = me.cidadesSelect.selectedIndex;
			me.selectedCidade = me.cidadesSelect.options[ me.selectedCidadeIndex ];
			me.getLojas( );
		});
	}
	displayGoogleMaps ( btn ) {
		let me = this;
		let src = btn.getAttribute('data-src');
		let iframe = `<iframe src="${src}" style="width: 100%; height: 100%;"/>`;
		this.modal.setContent( iframe );
		this.modal.open();
	}
};