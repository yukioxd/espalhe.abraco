import searchCEP from './../helpers/searchCEP.js';
import simplerAlert from './../helpers/alert.js';
let ignoreKeyCodes = [8, 17, 37, 39, 46 ];
const regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const validateEmail = ( email ) => {
	return regexEmail.test( email );
};
class ParticipateTwo {
	storeKey = 'prevForm'
	testDate ( target ) {
		let parts = target.value.split('/');
		let newValue = parts.reverse().join('-');
		let dateObj = new Date( newValue );
		return ! ( target.value.length != 10 || isNaN( dateObj.getTime() ) );
	}
	restoreData ( ) {
		let storeDatas = localStorage.getItem( this.storeKey );
		let storedFormDatas = JSON.parse( storeDatas );
		let fields = storedFormDatas.formHistory;

		fields.forEach ( field => {
			let fieldNames = Object.getOwnPropertyNames( field );
			if ( fieldNames.length > 0 ) {
				let fieldName = fieldNames[ 0 ];
				let item = document.querySelector( `*[name=${fieldName}]` );
				if ( item !== null ) {
					if ( item.type === 'checkbox' ) {
						if ( field [ fieldName ] == 'on' ) {
							item.checked = true;
						}
					} else {
						item.value = field [ fieldName ];
					}
				}
			}
		});
	}
	constructor () {
		let cpf = localStorage.getItem('cpf');
		let me = this;
		let isCadastre = document.body.id.indexOf('cadastre') >= 0;
		if ( ! isCadastre ) {
			return false;
		}
		try {
			me.restoreData();
		} catch( err ) {
			//donothing;
		}
		
		me.cpf = cpf;
		if ( cpf === null || cpf === undefined || me.cpf.length < 2 ) {
			location.href = location.href.replace(/cadastre/g, 'participe');
		}
		let checkEmpty = ( target ) => {
			let isValid;
			if ( target.className.match(/required/) ) {
				isValid =  target.value.length > 0;
			} 
			return isValid;
		};

		let alertOnEmpty = ( e ) => {
			e.target.value = e.target.value.trim();
			let isValid = true;
			if ( ! isValid ) {
				e.target.className = e.target.className.match(/error/g) ? e.target.className: e.target.className + ' error';
			} else {
				e.target.className = e.target.className.replace(/\s?error/g, '' );
			}
			checkEmpty( e.target );
		};

		let isDate = ( e ) => {
			if ( ! me.testDate( e.target )) {
				e.target.value = '';
				simplerAlert( 'Data de Nascimento inválido ! ', false );
			}
		};

		let formatDate = ( e ) => {
			if ( ignoreKeyCodes.indexOf( e.keyCode ) < 0 ) {
				e.target.value = global.Masky({
					2: '/',
					4: '/'
				}, e.target.value).substr(0, 10);
			}
		};
		let formatCep = ( e ) => {
			if ( ignoreKeyCodes.indexOf( e.keyCode ) < 0 ) {
				e.target.value = global.Masky({
					5: '-',
				}, e.target.value).substr(0, 9);
			}
		};
		let searchByCep = ( e ) => {
			searchCEP( e.target.value , simplerAlert );
		};
		const checkEmail = ( e ) => {
			let email = e.target.value;
			let result = validateEmail( email );
			if ( ! result ) {
				simplerAlert( ' E-mail inválido ! ', false );
			}
		};
		if ( document.body.id.match(/cadastre/) ) {
			let d = document;
			this.form 		= d.getElementById( 'register-block-form' );
			this.fields = {
				nome 		: {
					label: 'Nome',
					field: d.getElementById( 'nome' ),
					onBlur: [
						alertOnEmpty
					]
				},
				nascimento 	: {
					label: 'Data de Nascimento',
					field: d.getElementById( 'nascimento' ),
					onBlur: [
						alertOnEmpty,
						isDate
					],
					onKeyup: [ formatDate ]
				},
				email 		: {
					label: 'E-mail',
					field: d.getElementById( 'email' ),
					onBlur: [
						alertOnEmpty,
						checkEmail
					]
				},
				cep 		: {
					label: 'CEP',
					field: d.getElementById( 'cep' ),
					onBlur: [
						alertOnEmpty,
						formatCep,
						searchByCep
					]
				},
				estado 		: {
					label: 'Estado',
					field: d.getElementById( 'estado' ),
					onBlur: [
						alertOnEmpty
					]
				},
				cidade 		: {
					label: 'Cidade',
					field: d.getElementById( 'cidade' ),
					onBlur: [
						alertOnEmpty
					]
				},
				endereco 	: {
					label: 'Endereço',
					field: d.getElementById( 'endereco' ),
					onBlur: [
						alertOnEmpty
					]
				},
				abraco_nome 		: {
					label: 'Nome(amiga)',
					field: d.getElementById( 'abraco_nome' ),
					onBlur: [
						alertOnEmpty
					]
				},
				abraco_email 		: {
					label: 'E-mail(amiga)',
					field: d.getElementById( 'abraco_email' ),
					onBlur: [
						alertOnEmpty
					]
				},
				numero 		: {
					label: 'Número',
					field: d.getElementById( 'numero' ),
					onBlur: [
						alertOnEmpty
					]
				}
			};

			this.clube_beauty 		= d.getElementById( 'clube_beauty' );
			this.social_networks 	= d.getElementById( 'social_networks' );
			this.agreement 			= d.getElementById( 'agreement' );
			this.setup();
		}
	}


	checkAllFields( ){
		let me = this;
		let fields = Object.getOwnPropertyNames( this.fields );
		let isValidForm = true;
		let messageParts = [ ];
		fields.forEach ( ( propName ) => {
			let field = me.fields[ propName ].field;
			let validField = true;
			let currentClassName = field.className;
			if ( currentClassName.match(/required/g) && field.value.length < 1 ) {
				isValidForm = false;
				validField = false;
				messageParts.push( `Por favor, verifique o campo <strong> ${me.fields[ propName ].label} </strong>` );
			}

			if (  currentClassName.match(/date/g) ) {
				if ( ! me.testDate( field ) ) {
					field.value = '';
					messageParts.push( `Por favor, verifique o campo <strong> ${me.fields[ propName ].label} </strong>` );
				}
			}

			if ( field.id == 'email' ) {
				validField = validateEmail( field.value );
				if ( ! validField ) {
					isValidForm = false;
					messageParts.push( `Campo <strong> ${me.fields[ propName ].label} </strong> inválido` );
				}
			}

			if ( validField ) {
				field.className = field.className.replace(/\s?error/g, '' );
			} else {
				field.className = field.className.match(/error/g) ? field.className: field.className + ' error';
			}
		});

		if ( ! me.agreement.checked ) {
			isValidForm = false;
			messageParts.push( `Por favor, confirme se está de <strong>acordo</strong> com o Regulamento` );
		}

		if ( messageParts.length > 0 ) {
			let ul = document.createElement('ul');
			messageParts
				.map ( err => `<li>${err}</li>` )
				.filter ( (item, k, all ) =>  all.lastIndexOf( item ) === k  )
				.forEach ( item => ul.innerHTML += item );
			simplerAlert( ul.outerHTML, false );
		}

		if ( isValidForm ) {
			let cpf = document.createElement('input');
			cpf.name = 'cpf';
			cpf.type = 'hidden';
			cpf.value = me.cpf;
			let formDatas = Array.prototype.map.call( me.form.elements, function ( el ) {
				let o = {};
				if ( el.type == 'checkbox' || el.type == 'radio' ) {
					if ( el.checked ) {
						o[el.name] = el.value;
					}
				} else {
					o[el.name] = el.value;
				}
				return o;
			});
			try {
				let storageData = { formHistory: formDatas };
				let strData = JSON.stringify( storageData );
				localStorage.removeItem( me.storeKey );
				localStorage.setItem( me.storeKey , strData );
			}catch( e ) {
				//donothing
			}
			me.form.appendChild( cpf );

			setTimeout( () => {
				me.form.submit();
			}, 59 );
		}
	}

	setup ( cpf ) {
		let me = this;
		let fields = Object.getOwnPropertyNames( this.fields );

		fields.forEach ( ( propName ) => {
			if ( me.fields[ propName ].onBlur !== undefined && me.fields[ propName ].onBlur.forEach ) {
				me.fields[ propName ].onBlur.forEach (  fn => {
					me.fields[ propName ].field.addEventListener('blur', fn );
				});
			}
			if ( me.fields[ propName ].onKeyup !== undefined && me.fields[ propName ].onKeyup.forEach ) {
				me.fields[ propName ].onKeyup.forEach (  fn => {
					me.fields[ propName ].field.addEventListener('keyup', fn );
				});
			}
		});

		let formatPhone = ( e ) => {
			if ( ignoreKeyCodes.indexOf( e.keyCode ) < 0 ) {
				let prevValue = e.target.value.replace(/\D/g, '');
				let separatorKey = 6;
				let maskyConfig = {
					0: '(',
					2: ')',
				};
				separatorKey = prevValue.length > 10 ? 7 : separatorKey;
				maskyConfig[ separatorKey ] = '-';
				e.target.value = global.Masky( maskyConfig, e.target.value).substr(0, 14);
			}
		};

		document.getElementById('celular').addEventListener( 'keyup' ,  formatPhone );

		this.form.addEventListener('submit' , ( e ) => {
			e.preventDefault();
			me.checkAllFields( )
		});
	}
};

export default ( new ParticipateTwo );