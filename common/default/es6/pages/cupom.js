import tingle from './../helpers/tingle.js';
import Masky from './../helpers/Masky.js';
class cupom {

	constructor () {
		this.injectFb();
		this.setup();
		this.setupBtns();
	}
	injectFb ( ) {
		let me = this;
		window.fbAsyncInit = function() {
			 FB.init({
				appId      : 246464092427685,
				status     : true,
				xfbml      : true,
				version    : 'v2.9'
			});
			 me.setupFB();
		};
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/pt_BR/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}
	showSuccess ( message ) {
		let me = this;
		let modal = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['success-modal', 'beauty-success-modal', 'beauty-modal'],
		});
		me.successModal = modal;
		let html = '<button id="beauty-modal-close-btn"><span>×</span></button>';
		html += message;
		modal.setContent( html );
		modal.open();

		document.getElementById('beauty-modal-close-btn').addEventListener('mousedown', ( e ) => {
			me.closeSMSModal();
		});
	}
	showError ( message ) {
		let modal = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['error-modal'],
		});
		modal.setContent( message );
		modal.open();
	}
	sendEmail() {
		let me = this;
		window.beginLoader();
		let url = document.basePath + '/index/send-email';
		let xhr = new XMLHttpRequest();
		xhr.open( 'GET', url );
		xhr.addEventListener('readystatechange', ( e ) => {
			if ( xhr.readyState === 4 ) {
				hideLoader();
			}
			if ( xhr.readyState === 4 && xhr.status === 200 ) {
				//success
				me.showSuccess( "E-mail enviado com sucesso ! " );
			} else if ( xhr.readyState === 4) {
				//error
				me.showError( "<span class='ferr1'>Erro ao tentar enviar o E-mail </span> <br> <span class='ferr2'>por favor, tente mais tarde</span>" );
			}
		});
		xhr.send();
	}
	sendSms( numero ) {
		let me = this;
		numero = numero.replace(/\D/g, '');
		window.beginLoader();
		let url = document.basePath + '/index/send-sms?numero=' + numero;
		let xhr = new XMLHttpRequest();
		xhr.open( 'GET', url );
		xhr.addEventListener('readystatechange', ( e ) => {
			if ( xhr.readyState === 4 ) {
				hideLoader();
			}
			if ( xhr.readyState === 4 && xhr.status === 200 ) {
				//success
				me.showSuccess( "SMS enviado com sucesso ! " );
			} else if ( xhr.readyState === 4 ) {
				//error
				me.showError( "<span class='ferr1'>Erro ao tentar enviar o SMS </span> <br> <span class='ferr2'>por favor, tente mais tarde</span>" );
			}
		});
		xhr.send();
	}
	buildSmsModal ( ) {
		let me = this;
		let modal = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['beauty-modal', 'sms-modal'],
		});
		me.smsModal = modal;
		let html = `
			<button id="beauty-modal-close-btn"><span>×</span></button>
			<div id="sms-phone-block">
				<label for="sms-phone">Número do Celular</label>
				<input type="tel" name="sms-phone" id="sms-phone">
			</div>`;
		modal.setContent( html );

		modal.addFooterBtn('Enviar', 'tingle-btn tingle-btn--primary', function() {
			let numero = document.getElementById( 'sms-phone').value;
			modal.close();
			me.sendSms( numero );
		});
		modal.open();
		let sms = document.getElementById('sms-phone');

		try {
			let datas = JSON.parse( localStorage.getItem('prevForm') );
			let num = datas.formHistory.filter ( function ( item ) {
				return item.celular !== undefined;
			});
			if ( num !== undefined && num.length === 1 ) {
				if ( sms !== null ) {
					sms.value = num[0].celular;
				}
			}
		}catch( err ) {
			console.log (err );
		}

		sms.addEventListener('keyup', ( e ) => {
			let numberLength = e.target.value.replace(/\D/g, '').length;
			let separatorKey = 6;
			let maskProp = {};
			let o = '';
			setTimeout ( () => {
				maskProp[ 0 ] = '(';
				maskProp[ 2 ] = ')';
				separatorKey =  ( numberLength > 10 ) ? 7: separatorKey;
				maskProp[ separatorKey ]  = '-';
				o = global.Masky( maskProp, e.target.value );
				e.target.value = o.substr(0, 14);
			}, 59 );
		});
		document.getElementById('beauty-modal-close-btn').addEventListener('mousedown', ( e ) => {
			me.closeSMSModal();
		});
	}
	closeSMSModal ( ) {
		let me = this;
		if ( me.smsModal !== undefined && me.smsModal !== null ) {
			if ( me.smsModal.close !== undefined ) {
				me.smsModal.close();
			}
		}

		if ( me.successModal !== undefined && me.successModal !== null ) {
			if ( me.successModal.close !== undefined ) {
				me.successModal.close();
			}
		}
		if ( me.emailShareModal !== undefined && me.emailShareModal !== null ) {
			if ( me.emailShareModal.close !== undefined ) {
				me.emailShareModal.close();
			}
		}
	}
	shareEmail ( name, email ) {
		let me = this;
		window.beginLoader();
		let url = document.basePath + `/index/share-email?email=${email}&name=${name}`;
		let xhr = new XMLHttpRequest();
		xhr.open( 'GET', url );
		xhr.addEventListener('readystatechange', ( e ) => {
			if ( xhr.readyState === 4 ) {
				hideLoader();
			}
			if ( xhr.readyState === 4 && xhr.status === 200 ) {
				//success
				me.showSuccess( "E-mail enviado com sucesso ! " );
			} else if ( xhr.readyState === 4 ) {
				//error
				me.showError( "<span class='ferr1'>Erro ao tentar enviar o E-mail </span> <br> <span class='ferr2'>por favor, tente mais tarde</span>" );
			}
		});
		xhr.send();
	}
	buildEmailShareModal ( ) {
		let me = this;
		let modal = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['email-share-modal', 'beauty-modal'],
		});
		me.emailShareModal = modal;

		let html = `
			<button id="beauty-modal-close-btn"><span>×</span></button>
			<div class="email-share-block">
				<label for="share_name">Nome</label>
				<input type="text" name="share_name" id="share_name">
			</div>
			<div class="email-share-block">
				<label for="share_email">E-mail</label>
				<input type="email" name="share_email" id="share_email">
			</div>`;
		modal.setContent( html );

		modal.addFooterBtn('Enviar', 'tingle-btn tingle-btn--primary', function() {
			let email = document.getElementById( 'share_email').value;
			let name = document.getElementById( 'share_name').value;
			modal.close();
			me.shareEmail( name, email );
		});
		modal.open();
		let sms = document.getElementById('sms-phone');

		document.getElementById('beauty-modal-close-btn').addEventListener('mousedown', ( e ) => {
			me.closeSMSModal();
		});
	}
	setup ( ) {
		let d = document;
		let me = this;
		d.body.addEventListener('mousedown', ( e ) => {
			if ( e.target.id === 'dispatch-email') {
				me.sendEmail();
			}  else if ( e.target.id === 'dispatch-sms' ) {
				me.buildSmsModal();
			} else if ( e.target.id === 'send-using-email') {
				me.buildEmailShareModal();
			} else if ( e.target.id === 'beauty-modal-close-btn') {
				console.log( e.target );
				me.closeSMSModal();
			}

		});
		//aqui configurar o listener para disparo de SMS, E-mail
		//verificar como que será o compartilhamento por E-mail também.
		
	}
	setupFB ( ) {
		let d = document;
		let fbBtn = d.getElementById('share-on-facebook');
		fbBtn.addEventListener('click', ( e ) => {
			e.preventDefault();
			FB.ui({
				method: 'share',
				display: 'popup',
				hashtag: '#abraceaBeauty',
				quote: 'Semana do Abraço - The Beauty Box',
				href: 'https://www.semanadoabraco.com.br', //'https://'+ d.basePath,
			}, function( response ){
				console.log( response );
				if ( response !== undefined ) { //undefined == cancelou
					//update database
					let url = document.basePath + '/index/facebook-share';
					let xhr = new XMLHttpRequest();
					xhr.open( 'GET', url );
					xhr.send();
				}
			});
		})
	}

	setupBtns( ) {
		let mobBlock = document.getElementById('lets-mobile-share');
		if ( navigator.userAgent.match(/android/) ) {
			//setup datas dor android
		} else if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
			//setup datas for ios
		} else {
			//return mobBlock.remove();
		}
		let downloadCupom = ( e ) => {
			let url = e.target.href;
            if ( vendor == 'iOS' ) {
                url += "?html=1";
            }
			//iOS: abrir janela com as instruções para Download
			//android: download direto.
		};
        /*
	        let whatsappBtn = document.getElementById('send-using-whatsapp');
	        let downloadBtn = document.getElementById('share-on-whatsapp');
	        downloadBtn.addEventListener('mousedown', downloadCupom );
	        downloadBtn.addEventListener('touchdown', downloadCupom );
	        
        if(downloadBtn !== undefined) {
            let url = `/ws/get-image/id/${result.brinde.brinde_id}/loja_id/${result.brinde.loja_id}/pic/${result.uniq}`;
            if ( ! result.uniq.match(/\.jpg$/) ) {
                url += ".jpg";
            }
            let origin = window.location !== undefined && window.location.origin !== undefined ? window.location.origin: '';
            let pathname = window.location !== undefined && window.location.pathname !== undefined ? window.location.pathname: '';
            let baseURL = '' + origin.toString() + pathname.toString();

            if ( vendor == 'iOS' ) {
                url += "?html=1";
            }
            if( baseURL.length > 0) {
                downloadBtn.href = baseURL + url;
            } else {
                downloadBtn.href = url;
            }
            downloadBtn.target = "_blank";
        }

        */
	}
}

export default ( new cupom );