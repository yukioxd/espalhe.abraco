import request from './../helpers/request.js';
import tingle from './../helpers/tingle.js';
import ListaLojas from './ListaLojas.js';
class ParticipateThree extends ListaLojas {
	constructor () {
		super();

		let me = this;
		this.getEstados( this.setup );
		let modal = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['google-maps-modal'],
		});
		this.modal = modal;

		let modalStore = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['store-confirm-modal'],
		});


		modalStore.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--danger', function() {
			modalStore.close();
		});

		modalStore.addFooterBtn('Sim, trocar nesta loja', 'tingle-btn tingle-btn--pull-right tingle-btn--primary', function() {
			let form = document.createElement('form');
			let store = document.createElement('input');
			form.method = 'POST';
			store.name = 'store';
			store.type = 'hidden';
			store.value = me.storeId;
			form.appendChild( store );
			document.body.appendChild( form );
			form.submit()
			modalStore.close();
		});
		this.modalStore = modalStore;
	}
	aqcuireCoupon ( btn ) {
		let me = this;
		let loja = me.cidades[ me.selectedCidade.value].lojas.filter ( loja => loja.loja_id ==  me.storeId );
		let message;
		let nome;
		me.storeId = btn.getAttribute('data-id');
		if ( loja && loja[0] !== undefined && loja[0].nome !== undefined ) {
			nome = loja[0].nome;
		} else {
			let row = document.getElementById('store-item-' + me.storeId);
			nome = row.querySelector('h3').innerHTML;
		}
		message = ` <div class="warn">Atenção:</div>
					<h4>Você Selecionou a loja <strong>${nome}</strong></h4>
					<p>Uma vez selecionado a loja, não terá como alterar depois.</p>`;
		me.modalStore.setContent( message );
		/*
		me.modalStore.open();
		chamada direta 
		*/

		let form = document.createElement('form');
		let store = document.createElement('input');
		form.method = 'POST';
		store.name = 'store';
		store.type = 'hidden';
		store.value = me.storeId;
		form.appendChild( store );
		document.body.appendChild( form );
		form.submit()
		modalStore.close();
	}
	renderStores ( ) {
		let me = this;
		let stores = me.cidades[ me.selectedCidade.value ].lojas;
		let d = document;
		let storeContent = d.getElementById('select-store-content');
		if ( ! storeContent.className.match(/loaded/g) ) {
			storeContent.className += ' loaded';
		}
		d.getElementById('stores-list').innerHTML = '';
		if ( me.storesCountUnitNode == null ) {
			me.storesCountUnitNode = d.getElementById('lojas-count-unit');
		}
		me.storesCountUnitNode.innerHTML = stores.length > 1 ? 'lojas': 'loja';
		me.storesCountNode.innerHTML = stores.length;
		let storesItems = stores.map ( ( store ) => {
			let item = d.createElement('li');
			item.id = 'store-item-' + store.loja_id;
			item.className = 'stores-items';
			if ( store.estoque_brinde < 1 ) {
				item.className += ' no-unavailable';
			}

			let itemText = d.createElement('div');
			let itemName = d.createElement('h3');
			let itemAddr = d.createElement('div');
			let itemPhone = d.createElement('div');
			let itemSchedule = d.createElement('div');


			itemText.className = 'stores-items-text stores-items-parts';
			itemName.innerHTML = store.nome;
			itemAddr.innerHTML = store.endereco;
			itemPhone.innerHTML = 'Telefone: ' + store.telefone;
			itemSchedule.innerHTML = store.horarios_atendimento;
			itemSchedule.className = 'schedule-text';

			itemText.appendChild( itemName );
			itemText.appendChild( itemAddr );
			itemText.appendChild( itemPhone );
			itemText.appendChild( itemSchedule );

			
			let itemActions = d.createElement('div');
			let gmapButton = d.createElement('button');
			let exchangeButton = d.createElement('button');
			itemActions.className = 'stores-items-actions stores-items-parts';

			gmapButton.innerHTML = 'Ver no mapa';
			gmapButton.className = 'google-maps-irame';
			gmapButton.setAttribute('data-src', store.gmapIframe );

			gmapButton.addEventListener('mousedown', (e) => {
				if ( e.preventDefault !== undefined ) {
					e.preventDefault();
				}
				me.displayGoogleMaps( e.target );
			});

			gmapButton.addEventListener('touchdown', (e) => {
				if ( e.preventDefault !== undefined ) {
					e.preventDefault();
				}
				me.displayGoogleMaps( e.target );
			});
			if ( store.estoque_brinde > 0 ) {
				exchangeButton.addEventListener('mousedown', (e) => {
					if ( e.preventDefault !== undefined ) {
						e.preventDefault();
					}
					me.aqcuireCoupon( e.target );
				});

				exchangeButton.addEventListener('touchdown', (e) => {
					if ( e.preventDefault !== undefined ) {
						e.preventDefault();
					}
					me.aqcuireCoupon( e.target );
				});

				exchangeButton.innerHTML = 'Trocar nesta loja';
				exchangeButton.className = 'exchange-button';
				exchangeButton.setAttribute('data-id', store.loja_id );
			} else {
				exchangeButton.innerHTML = 'Esgotado';
				exchangeButton.className = 'unavailable-exchange-button';
			}

			itemActions.appendChild( gmapButton );
			itemActions.appendChild( exchangeButton );

			item.appendChild( itemText );
			item.appendChild( itemActions );

			return item;
		})
		.forEach ( item => {
			d.getElementById('stores-list').appendChild( item );
		});
	}
};

export default ( new ParticipateThree );