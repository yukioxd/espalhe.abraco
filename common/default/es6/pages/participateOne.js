import checkCPF from './../helpers/checkCPF.js';
import simplerAlert from './../helpers/alert.js';
class ParticipateOne {

	constructor () {
		if ( document.body.id.match(/participe/) ) {
			let d = document;
			this.cpf = d.getElementById( 'cpf' );
			this.sendButton = d.getElementById('do-send-cpf');
			this.form = d.getElementById('participate-form-step-1');

			if ( this.cpf !== null ) {
				this.setup( cpf );
			}
			this.ignoreKeyCodes = [ 37, 38, 39, 40 ];

			if ( location.href.match(/used/g) ) {
				let parts = location.href.split('?');
				let last = parts[parts.length-1]
				let keyValues = last.split('=');
				this.cpf.value = keyValues[ 1 ];
				simplerAlert( 'Este CPF já está sendo usado !', false );
			}
		}
	}

	checkField ( e ) {
		let me = this;
		if ( me.ignoreKeyCodes.indexOf( e.keyCode ) < 0 ) {
			setTimeout( () => {
				let checkValue = me.cpf.value.replace(/\D/g, '');
				let o = global.Masky({
				    3: '.',
				    6: '.',
				    9: '-'
				}, me.cpf.value);
				me.cpf.value = o.substr(0, 14);
			});
		}
	}

	setup ( cpf ) {
		let me = this;
		cpf.addEventListener( 'blur', ( e ) => {
			me.checkField.call( me, e );
		});
		cpf.addEventListener( 'keydown', ( e ) => {
			me.checkField.call( me, e );
		});
		cpf.addEventListener( 'keyup', ( e ) => {
			me.checkField.call( me, e );
		});

		let next = ( e ) =>  {
			e.preventDefault();
			if ( localStorage && localStorage.setItem ) {
				localStorage.setItem( 'cpf',  me.cpf.value );
			}
			let numericCPF = me.cpf.value.replace(/\D/g, '');
			let result = checkCPF(  numericCPF );

			if ( result.length > 1 ) {
				this.form.submit();
			} else {
				console.log( numericCPF.length );
				if ( numericCPF.length === 11 ) {
					simplerAlert( 'CPF inválido !', false );
				} else {
					simplerAlert( 'Digite seu CPF', false );
				}
				me.form.className = me.form.className.match(/invalid/g) ? me.form.className : me.form.className + ' invalid';
				me.cpf.className = me.cpf.className.match(/invalid/g) ? me.cpf.className : me.cpf.className + ' invalid';
			}
		};

		this.sendButton.addEventListener('click', next );
		this.form.addEventListener('submit', next );
	}
};

export default ( new ParticipateOne );