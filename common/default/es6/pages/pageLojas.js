import request from './../helpers/request.js';
import tingle from './../helpers/tingle.js';
import ListaLojas from './ListaLojas.js';
class pageLojas extends ListaLojas {
	constructor () {
		super();

		let me = this;
		let isEscolhe = document.body.id.indexOf('participantes') >= 0;
		this.getEstados( this.setup );
		if( !isEscolhe ) {
			return false;
		}
		let modal = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['google-maps-modal'],
		});
		this.modal = modal;

		let modalStore = new tingle.modal({
			footer: true,
			stickyFooter: false,
			closeMethods: ['overlay', 'button', 'escape'],
			closeLabel: "Fechar",
			cssClass: ['store-confirm-modal'],
		});


		modalStore.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--danger', function() {
			modalStore.close();
		});
	}
	renderStores ( ) {
		let me = this;
		let stores = me.cidades[ me.selectedCidade.value ].lojas;
		let d = document;
		let storeContent = d.getElementById('list-store-content');
		if ( ! storeContent.className.match(/loaded/g) ) {
			storeContent.className += ' loaded';
		}
		d.getElementById('stores-list').innerHTML = '';
		me.storesCountNode.innerHTML = stores.length;
		if ( stores.length > 1 ) {
			me.storesCountUnitNode.innerHTML = 'lojas';
		} else {
			me.storesCountUnitNode.innerHTML = 'loja';
		}

		let storesItems = stores.map ( ( store ) => {
			let item = d.createElement('li');
			item.id = 'store-item-' + store.loja_id;
			item.className = 'stores-items';

			let itemText = d.createElement('div');
			let itemName = d.createElement('h3');
			let itemAddr = d.createElement('div');
			let itemPhone = d.createElement('div');
			let itemSchedule = d.createElement('div');

			itemText.className = 'stores-items-text stores-items-parts';
			itemName.innerHTML = store.nome;
			itemAddr.innerHTML = store.endereco;
			itemPhone.innerHTML = 'Telefone: ' + store.telefone;
			itemSchedule.innerHTML = store.horarios_atendimento
			itemSchedule.className = 'schedule-text'

			itemText.appendChild( itemName );
			itemText.appendChild( itemAddr );
			itemText.appendChild( itemPhone );
			itemText.appendChild( itemSchedule );
			
			let itemActions = d.createElement('div');
			let gmapButton = d.createElement('button');
			itemActions.className = 'stores-items-actions stores-items-parts';

			gmapButton.innerHTML = 'Ver no mapa';
			gmapButton.className = 'google-maps-irame';
			gmapButton.setAttribute('data-src', store.gmapIframe );

			gmapButton.addEventListener('mousedown', (e) => {
				if ( e.preventDefault !== undefined ) {
					e.preventDefault();
				}
				me.displayGoogleMaps( e.target );
			});

			gmapButton.addEventListener('touchdown', (e) => {
				if ( e.preventDefault !== undefined ) {
					e.preventDefault();
				}
				me.displayGoogleMaps( e.target );
			});

			itemActions.appendChild( gmapButton );

			item.appendChild( itemText );
			item.appendChild( itemActions );

			return item;
		})
		.forEach ( item => {
			d.getElementById('stores-list').appendChild( item );
		});
	}
};

export default ( new pageLojas );