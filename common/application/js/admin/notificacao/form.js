$(document).ready(function() {
	var mappedArray = document.src.map(function(it){
		var o = {
			'android_pushable_devices': it.android_pushable_devices,
			'ios_pushable_devices': it.ios_pushable_devices,
			'android_devices': it.android_devices,
			'ios_devices': it.ios_devices,
			'deviceCount': it.deviceCount,
			'label':    it.label
		};
		return o;
	});
	console.log(mappedArray);
	Morris.Area({
		element: 'morris-area-chart',
		data: mappedArray,
		xkey: 'label',
		ykeys: ['deviceCount', 'ios_devices', 'android_devices', 'ios_pushable_devices', 'android_pushable_devices'],
		labels: ['Total', 'iOS (Geral)', 'Android (Geral)', 'iOS (Push)',  'Android (Push)'],
		pointSize: 2,
		hideHover: 'auto',
		resize: true
	});
	var mappedArrayBar = document.barSrc.map(function(it){
		var o = {
			'a': it.target_count,
			'b': it.apn_success,
			'c': it.gcm_success,
			'd': it.apn_fail,
			'f': it.gcm_fail,
			'label': it.notificacao_id,
		};
		return o;
	});
	Morris.Bar({
		element: 'morris-bar-chart',
		data: mappedArrayBar,
		xkey: 'label',
		ykeys: ['a', 'b', 'c', 'd', 'f'],
		labels: ['Total Disp.', 'iOS (Ok)', 'Android (Ok)', 'iOS (Erro)', 'Android (Erro)'],
		hideHover: 'auto',
		resize: true
	});
	/*
	Morris.Bar({
		element: 'morris-bar-chart',
		data: [{
			y: '2006',
			a: 100,
			b: 90
		}, {
			y: '2007',
			a: 75,
			b: 65
		}, {
			y: '2008',
			a: 50,
			b: 40
		}, {
			y: '2009',
			a: 75,
			b: 65
		}, {
			y: '2010',
			a: 50,
			b: 40
		}, {
			y: '2011',
			a: 75,
			b: 65
		}, {
			y: '2012',
			a: 100,
			b: 90
		}],
		xkey: 'y',
		ykeys: ['a', 'b'],
		labels: ['Series A', 'Series B'],
		hideHover: 'auto',
		resize: true
	});
	 */
});
