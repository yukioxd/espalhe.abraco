/**
 * Created by Usuario on 07/04/2017.
 */

export default class {

    constructor () {
        global.Helpers.storage.listen( 'utilizados',  this.update );
        global.Helpers.storage.listen( 'estoque',  this.update );
    }

    update( ) {
        let storage = global.Helpers.storage;
        let storeUtilizados = storage.get('utilizados');
        let storeEstoque = storage.get('estoque');
        let utilizados = Number (  isNaN( storeUtilizados ) ? 0: storeUtilizados );
        let estoque = Number( isNaN( storeEstoque ) ? 0: storeEstoque  );
        if ( ( ! isNaN(utilizados) ) &&  ( ! isNaN(estoque) )  ) {
            let remaining = estoque - utilizados;
            let percent = 0;
            if ( estoque == utilizados ) {
                percent = 100;
            } else {
                percent = ( remaining * 100 / estoque );
            }
            if ( percent.toFixed !== undefined ) {
                percent = percent.toFixed(2);
            }
            $("#current-progress")
                .html( percent + '%' )
                .css({width: percent + '%'});
        }
    }
}
