import request from './../../../helpers/request';
export default function ( e ) {
    "use strict";
    if ( e !== undefined && e.preventDefault !== undefined ){
        e.preventDefault();
    }
    let validEmail = global.Helpers.validator.isEmail( this.login.value );
    let validString = ! global.Helpers.validator.isEmpty( this.senha.value );

    if ( ! validEmail ) {
        $( this.login.parentElement ).addClass('has-error');
    }
    if ( ! validString ) {
        $( this.senha.parentElement ).addClass('has-error');
    }
    if ( validEmail && validString ) {
        //let a = global.Helpers.alerts.success('olar');
        let params = { login: this.login.value,  senha: this.senha.value };
        let fallback = ( e ) => {
            let message = 'Erro ao tentar realizar o Login, por favor verifique o Login/Senha e tente novamente.';
            if ( e !== undefined && e.responseJSON !== undefined ) { message = e.responseJSON.error; }
            global.Helpers.alerts.error( message );
        };
        request.post( params, this.action, undefined, fallback );
    }
}