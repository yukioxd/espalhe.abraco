import submit from './handlers/submit';
export default class {

    constructor ( id ) {
        let $el = $(id);
        $el.on( 'submit', submit );
    }
}