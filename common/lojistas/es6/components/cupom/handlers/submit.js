import request from './../../../helpers/request';
let isSending = false;
export default ( e ) => {
    if ( e !== undefined && e.preventDefault !== undefined ){
        e.preventDefault();
    }
    if ( isSending ) {
        return false;
    }

    Helpers.beginLoader();
    isSending = true;

    let cpfTrimmed = document.getElementById( 'cpf').value;
    let cpf = global.Helpers.checkCPF( cpfTrimmed.replace(/\D/g, '') );
    if ( cpf.length < 1 ) {
        Helpers.hideLoader();
        isSending = false;
        return Helpers.alerts.error( 'CPF inválido' );
    }

    let cupom = $('#cupom').val();
    if ( ! cupom.match(/^(\d{3}\-)+\d{3}$/) ) {
        Helpers.hideLoader();
        isSending = false;
        return Helpers.alerts.error( 'Cupom inválido' );
    }

    let params = { validate: true, cpf: cpf, cupom: cupom };
    let endpoint = '/lojistas';
    let callback = ( r ) => {
        Helpers.hideLoader();
        isSending = false;
        Helpers.alerts.success('Código validado com sucesso ! ');
    };
    let fallback = ( r ) => {
        try {
            let message = r.responseJSON.msg;
            Helpers.hideLoader();
            isSending = false;
            Helpers.alerts.error( message );
        } catch( er ) {
            let message = 'Erro ao tentar validar os dados, por favor, verifique novamente os campos';
            Helpers.hideLoader();
            isSending = false;
            Helpers.alerts.error( message );
        }
    };
    request.post(params, endpoint, callback, fallback );
}