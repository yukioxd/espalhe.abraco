import submit from './handlers/submit'
export default class {
    constructor ( selector ) {
        let ignoreKeyCodes = [8, 17, 37, 39, 46 ];
        $( selector ).on( 'submit', submit );

        $( selector).find('#cpf').on('keyup', ( e ) => {
            if ( ignoreKeyCodes.indexOf( e.keyCode ) < 0 ) {
                let maskProp = {
                    3: '.',
                    6: '.',
                    9: '-'
                };
                let newValue = global.Helpers.Masky(maskProp, e.target.value );
                e.target.value = newValue.substr(0, 14);
            }
        });
        $( selector).find('#cupom').on('keyup', ( e ) => {
            if ( ignoreKeyCodes.indexOf( e.keyCode ) < 0 ) {
                let maskProp = {
                    3: '-',
                    6: '-'
                };
                let newCupomValue = global.Helpers.Masky(maskProp, e.target.value );
                e.target.value = newCupomValue.substr(0, 11);
            }
        });
    }
}