export default ( key, hash ) => {
    if ( hash && hash.length > 0 ) {
        $('body').addClass('logged');
        $('.login-container').slideUp();
    } else {
        $('body').removeClass('logged');
        $('.login-container')
                .slideDown()
                .fadeIn(300);
    }
}