export default ( key, estoque ) => {
    "use strict";
    if ( estoque && estoque.length > 0 ) {
        if ( isNaN( estoque ) ) {
            $('#saldo-inicial').html( 0 );
        } else {
            $('#saldo-inicial').html( estoque );
        }
    }
}