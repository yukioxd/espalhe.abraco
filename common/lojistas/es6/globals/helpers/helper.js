import mixin from './mixin';
import validator from 'validator';
import alert from './alert';
import storage from './storage';
import checkCPF from './checkCPF';
import Masky from './Masky';
import { beginLoader, hideLoader } from './loader';
class Helper{
    constructor(){
        this.mixin = mixin;
        this.validator = validator;
        this.alerts = alert;
        this.storage = storage;
        this.checkCPF = checkCPF;
        this.Masky = Masky;
        this.beginLoader = beginLoader;
        this.hideLoader = hideLoader;
    }
}
export default (new Helper);