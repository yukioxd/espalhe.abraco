export function beginLoader () {
    "use strict";
    let peekLoading = document.createElement('div');
    let peeekLoading = document.createElement('div');
    let ul = document.createElement('ul');

    peekLoading.id = 'peek-loading';
    peeekLoading.className = 'peeek-loading';
    for ( let i = 0; i < 9; i++ ) {
        let li = document.createElement('li');
        ul.appendChild( li )
    }
    peeekLoading.appendChild( ul );
    peekLoading.appendChild ( peeekLoading );

    let prev = document.getElementById('peek-loading');
    if ( prev === null ) {
        document.body.appendChild( peekLoading );
    }
    if ( prev !== null ) {
        if ( prev.className.match(/disabled/g) ) {
            prev.className= '';
        } else {
            //donothing
        }
    }
}

export function hideLoader () {
    "use strict";
    let prev = document.getElementById('peek-loading');
    if ( prev !== null ) {
        if ( prev.className.match(/disabled/g) ) {
            //donothing
        } else {
            prev.className += 'disabled';
        }
    }
    setTimeout ( () => {
        let prev = document.getElementById('peek-loading');
        if ( prev !== null ) {
            prev.remove();
        }
    }, 400 );

}