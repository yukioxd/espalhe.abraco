/**
 * Created by Usuario on 05/04/2017.
 * source: http://stackoverflow.com/questions/30732241/mixins-for-es6-classes-transpiled-with-babel
 */
export default function(target, source) {
    target = target.prototype; source = source.prototype;

    Object.getOwnPropertyNames(source).forEach(function (name) {
        if (name !== "constructor") Object.defineProperty(target, name,
            Object.getOwnPropertyDescriptor(source, name));
    });
}