class Storage {
    constructor () {

    }
    listen( key, cb ) {
        if ( this.listeners === undefined || this.listeners === null ) {
            this.listeners = {};
        }
        if ( this.listeners[key] === undefined || this.listeners[key] === null ) {
            this.listeners[key] = [];
        }
        this.listeners[key].push( cb );
        let currentValue = localStorage.getItem( key );
        if ( currentValue!== undefined || currentValue !== null ) {
            this.dispatch( key, currentValue );
        }
    }
    dispatch ( key, value ) {
        if ( this.listeners[ key ] !== undefined ) {
            if ( this.listeners[ key ].forEach !== undefined ) {
                this.listeners[ key ].forEach( callback => {
                    callback( key, value );
                })
            }
        }
    }
    set ( key, value ) {
        localStorage.setItem( key, value );
        this.dispatch( key, value );
    }
    get ( key ) {
        return localStorage.getItem( key );
    }
    unset ( key, value ) {
        localStorage.removeItem( key );
        this.dispatch( key, false );
    }
    clear ( key, value ) {
        localStorage.clear();
        this.dispatch( 'ALL' );
    }
}

export default (new Storage())