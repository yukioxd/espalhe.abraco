/**
 * Created by Usuario on 05/04/2017.
 */
class Alerts {
    constructor() {
    }
    message( message, alertClass, label, icon ) {
        var $message = $('<div>', {
            class: `alert  ${alertClass} alert-lojistas alert-dismissible`,
            role: 'alert',
            html: [
                $('<button>', {
                    class:'close',
                    'data-dismiss': 'alert',
                    'aria-label':   'Close',
                    html: [
                        $('<span>', {
                            'aria-hidden': true,
                            html: '&times'
                        })
                    ]
                }),
                $('<strong>', {
                    html: ( () => {
                        let html = '';
                        let iconClass = 'fa fa-thumbs-up';
                        if ( icon !== undefined ) {
                            iconClass = icon;
                        }
                        return html += `<i class="${iconClass}"></i>  <strong>${message}</strong>`
                    })
                })
            ]
        });

        $('body').append( $message );

        setTimeout ( () => {
            $message.fadeOut(300, () => {
                $(this).remove();
            });
        }, 5000 );

        return $message;
    }
    error( message ) {
        return this.message( message, 'alert-danger', 'Ops !' , 'fa fa-warning' );
    }
    success( message ) {
        return this.message( message, 'alert-success', 'Sucesso !' , 'fa fa-check' );
    }
}

export default ( new Alerts );