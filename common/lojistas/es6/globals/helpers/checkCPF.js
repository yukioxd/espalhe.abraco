export default function (strCPF) {
    let Soma = 0, Resto, i, map = {}, keySize = 0;
    Soma = 0;
    if (strCPF == "00000000000") {
        return '';
    }
    for (i=1; i<=9; i++) {
        let char = strCPF.substring(i-1, i);
        if( map[char] === undefined ) {
            keySize++;
            map[char] = true;
        }
        Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
    }
    if (keySize === 1) {
        return '';
    }
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) {
        Resto = 0;
    }

    if (Resto != parseInt(strCPF.substring(9, 10)) ) {
        return '';
    };
    Soma = 0;

    for (i = 1; i <= 10; i++) {
        Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    }
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) {
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) {
        return '';
    };
    return strCPF.substr(0,3) + '.' + strCPF.substr(3, 3)+ '.' + strCPF.substr(6, 3) + '-' + strCPF.substr(9, 2);
};
