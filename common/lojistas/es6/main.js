import Helpers from './globals/helpers/helper';
import Login from './components/login/login';
import Cupom from './components/cupom/cupom';
import Grafico from './components/grafico/grafico';


import hashListener from './listeners/hash';
import nameListener from './listeners/name';

import utilizadosListener from './listeners/utilizados';
import estoqueListener from './listeners/estoque';


import Request from './helpers/request';

global.Helpers = Helpers;
( function ( d, w, undefined ) {
    global.Helpers.storage.listen( 'hash',  hashListener );
    global.Helpers.storage.listen( 'name',  nameListener );

    global.Helpers.storage.listen( 'utilizados',  utilizadosListener );
    global.Helpers.storage.listen( 'estoque',  estoqueListener );


    $(d).on('ready', function ()  {
        let loginForm = new Login( '#login-form' );
        let cupomForm = new Cupom( '#validate-cupom' );
        let grafico = new Grafico();
        let logout = d.getElementById('logout');

        if ( logout !== null ) {
            logout.addEventListener('click', ( e ) => {
                localStorage.clear();
                location.reload();
            });
        }

        if ( $('body').hasClass('logged') ) {
            let endpoint = '/lojistas';
            let params = {loadData: true};
            Request.post( params, endpoint );
        }
    });

}( document, window ));