class Request {
    buildBaseParams ( data, endpoint, type, callback, fallback ) {
        let url = endpoint.match('http') ? endpoint : document.basePath + endpoint;
        let success = data => {
            if ( $.isPlainObject( data ) ) {
                let keys = Object.getOwnPropertyNames( data );
                keys.forEach ( key => {
                    //console.log( key, data[ key ] );
                    global.Helpers.storage.set( key, data[ key ] );
                } );
            }
            if ( callback !== undefined ) { callback( data ); }
        };
        let error = err => {
            if ( fallback !== undefined ) { fallback( err ) }
        };
        var xhrParams = {
            url: url,
            type: type,
            data: data,
            dataType: 'json',
            success: success,
            error: error
        };

        if ( $('body').hasClass('logged') ) {
            let _hash = global.Helpers.storage.get('hash');
            let setHeader = xhr =>  { xhr.setRequestHeader("X-Authorization",  _hash ); };
            xhrParams.beforeSend = setHeader ;
        }
        return xhrParams;
    }

    get ( params, endpoint, cb, fb ) {
        var xhrProps = this.buildBaseParams( params, endpoint, "GET", cb, fb);
        return $.ajax( xhrProps );

    }

    post( params, endpoint, cb, fb ) {
        var xhrProps = this.buildBaseParams( params, endpoint, "POST", cb, fb);
        return $.ajax( xhrProps );
    }

}

export default ( new Request );