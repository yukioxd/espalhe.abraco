{include file="head.tpl"}
<body id="{$currentAction}" class="{if $currentAction !=='index'}type2{/if}">
    {include file="header.tpl"}

    <main class="layout-content type-b" style="text-align:center;">
        <div class="container">
            <div>
                <h1 style="font-size: 185%;font-weight: bold; max-width: 600px; margin: 0 auto; padding-top: 26px;">Olá! Infelizmente os produtos se esgotaram para novos cadastros e por isso
                    <span style="color: #fcf4a9; font-size: 130%;">a promoção já acabou.</span></h1>
            </div>
            <div>
                <p style="padding: 60px 0; max-width: 660px; font-size: 132%; margin: 0 auto;">
                    Mas você pode nos seguir nas redes sociais e ficar por
                    dentro de todas as promoções e novidades!
                </p>
            </div>
            <div>
                <ul class="di nav-footer-list social">
                    <li class="nav-footer-social-items nometoque" style="background: #7abdab; border-radius: 4px; padding: 16px; margin-right: 16px !important;">
                        <a target="_blank" title="Visite a nossa página do Facebook" class="nav-footer-a nav-footer-fb" style="width: 60px;" href="https://www.facebook.com/facedabeauty">
                            <span class="ir">Página do Facebook</span>
                        </a>
                    </li>
                    <li class="nav-footer-social-items nometoque" style="background: #7abdab; border-radius: 4px; padding: 16px 24px; margin-right: 16px !important;">
                        <a target="_blank" title="Visite o nosso perfil do Instagram " class="nav-footer-a nav-footer-insta" href="https://www.instagram.com/instadabeauty/">
                            <span class="ir">Perfil do Instagram</span>
                        </a>
                    </li>
                    <li class="nav-footer-social-items nometoque" style="background: #7abdab; border-radius: 4px; padding: 16px 26px;">
                        <a target="_blank" title="Visite o nosso canal do YouTube" class="nav-footer-a nav-footer-youtube" href="https://www.youtube.com/user/videosdabeauty">
                            <span class="ir">Canal do YouTube</span>
                        </a>
                    </li>
                </ul>
                <div style="margin: 0 auto; height: 200px; padding: 50px 0;">
                    <img src="{$basePath}/common/default/images/site/modal-decorator.png" alt="Enfeite">
                </div>
            </div>
        </div>
    </main>


    {include file="footer.tpl"}
</body>
{include file="after-footer.tpl"}
