<header id="header">
    <div class="container desk-only">
        <h1 class="di">
            <a id="hugweek" href="{$this->url([], 'home')}">
                <picture>
                    <source media="(max-width: 1440px)" srcset="{$imgPath}/logo-v3.md.png">
                    <source media="(min-width: 1440px)" srcset="{$imgPath}/logo-v3.png">
                    <img src="{$imgPath}/logo-v3.md.png" alt="Semana do Abraço">
                </picture>
            </a>
        </h1>
        <nav class="di" id="navigation">
            <ul id="nav-list">
                <li class="nav-items">
                    <a class="nav-a nav-regular" href="{$this->url([], 'promocao')}">
                        A promoção
                    </a>
                </li>
                <li class="nav-items">
                    <a class="nav-a nav-regular" href="{$this->url([], 'duvidas')}">
                        Dúvidas
                    </a>
                </li>
                <li class="nav-items">
                    <a class="nav-a nav-regular" href="{$this->url([], 'participantes')}">
                        Lojas participantes
                    </a>
                </li>
                <li class="nav-items">
                    <a class="nav-a nav-participe b" href="{$this->url([], 'participe')}">
                        Participe
                    </a>
                </li>
                <li class="nav-items nav-tbb-item">
                    <a class="nav-a nav-tbb c-ir" href="http://www.thebeautybox.com.br/" title="Loja de Cosméticos e Perfumes - The Beauty Box" target="_blank">
                        <picture>
                            <source media="(max-width: 1440px)" srcset="{$imgPath}/logo-tbb-v2.md.png">
                            <source media="(min-width: 1440px)" srcset="{$imgPath}/logo-tbb-v2.png">
                            <img src="{$imgPath}/logo-tbb-v2.md.png" alt="The Beauty Box">
                        </picture>
                        <span class="ir">The Beauty Box</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="mobile-only mobile-navigation" id="mobile-menu-bar">
        <input type="checkbox" id="mobile-menu-controller">
        <label class="switch-decorator" for="mobile-menu-controller"></label>
        <span class="switch-label">Menu</span>
        <ul id="mobile-menu-list">
            <li class="mobile-menu-bar-item">
                <a href="{$this->url([], 'home')}">Home</a>
            </li>
            <li class="mobile-menu-bar-item">
                <a href="{$this->url([], 'promocao')}">A promoção</a>
            </li>

            <li class="mobile-menu-bar-item">
                <a href="{$this->url([], 'participe')}">Participe</a>
            </li>

            <li class="mobile-menu-bar-item">
                <a href="{$this->url([], 'duvidas')}">Dúvidas</a>
            </li>
            <li class="mobile-menu-bar-item">
                <a href="{$this->url([], 'participantes')}">Lojas Participantes</a>
            </li>
            <li class="mobile-menu-bar-item">
                <a class="modal-iframe" href="{$this->url([], 'home')}common/default/regulamento.pdf">Regulamento</a>
            </li>
            <li class="mobile-menu-bar-item">
                <a href="{$this->url([], 'sustentabilidade')}">Sustentabilidade</a>
            </li>
            <li class="mobile-menu-bar-item">
                <a href="http://www.thebeautybox.com.br/institucional/duvidas" target="_blank">Contato</a>
            </li>

            <li class="mobile-menu-bar-item">
                <a href="http://www.thebeautybox.com.br/" target="_blank" title="Visite também a loja de Cosméticos e Perfumes - The Beauty Box">Site The Beauty Box</a>
            </li>
        </ul>
    </div>
</header>