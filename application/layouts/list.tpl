<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="{$this->CreateUrl("form", NULL, NULL, [])}" class="btn btn-outline btn-primary btn-new">Novo</a>
            <a href="{$this->CreateUrl("form", NULL, NULL, [])}/{$primary}/" class="btn btn-outline btn-primary btn-edit">Editar</a>
            <a href="{$this->CreateUrl("delete", NULL, NULL, [])}/{$primary}/" class="btn btn-outline btn-danger btn-remove">Remover</a>
            {foreach from=$listExtraIcons item=icon}
                <td>
                    <a class="{$icon['class']}" href="{$this->url($icon['url'], "default", $icon['clear'])}">{$icon['value']}</a>
                </td>
            {/foreach}
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
			<tr>
                            <th></th>
                            <th>#</th>
                            {foreach $paginator->getAdapter()->getItems(0,1)->getTable()->getCampo() as $column=>$value}
                                {if $paginator->getAdapter()->getItems(0,1)->getTable()->getVisibility($column, 'list')}
                                        <th>
                                                {$value}
                                        </th>
                                {/if}
                            {/foreach}
			</tr>
			<tr class="filter" data-search="{$basePath}/admin/{$currentController}/search/{key($requireParam)}/{current($requireParam)}">
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            {foreach $paginator->getAdapter()->getItems(0,1)->getTable()->getCampo() as $column=>$value}
                                {if $paginator->getAdapter()->getItems(0,1)->getTable()->getVisibility($column, 'list')}
                                        <th>
                                                {$form->getElement($column)}
                                        </th>
                                {/if}
                            {/foreach}
			</tr>
                    </thead>
                    <tbody>
                        {foreach from=$paginator item=row}
                            <tr>
                                <td><input type="radio" name="{$primary}" value="{$row[$primary]}" /></td>
                                <td>{$row[$primary]}</td>
                                {foreach $paginator->getAdapter()->getItems(0,1)->getTable()->getCampo() as $column=>$value}
                                    {if $paginator->getAdapter()->getItems(0,1)->getTable()->getVisibility($column, 'list')}
                                        <td>
                                            {$this->GetColumnValue($row, $column)}
                                        </td>
                                    {/if}
                                {/foreach}
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<div class="footer-bar">
	{$this->paginationControl($paginator, NULL, 'paginator.tpl')}
</div>