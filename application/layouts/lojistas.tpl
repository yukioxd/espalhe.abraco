<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Semana do Abraço</title>
    <!-- Core CSS - Include with every page -->
    <link href="{$basePath}/common/admin/css/bootstrap.css" rel="stylesheet">
    <link href="{$basePath}/common/admin/css/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="{$basePath}/common/admin/css/sb-admin.css" rel="stylesheet">
    <link href="{$basePath}/common/lojistas/css/main.css" rel="stylesheet">
    <style>
        html { height: 100%; }
        html, body { min-height: 100%; }
        body {
            margin: 0;
            padding: 0;
            position: relative;
            background: url('{$imgPath}/green-pattern-v6.png') repeat;
            font-family: 'OpenSans', 'tahoma';
            font-weight: 300;
            overflow: hidden;
            color: #fff;
        }
        body:before{
            background: url('{$imgPath}/pattern/purple-v8.png') repeat;
            content: '';
            width: 100%;
            height: 63%;
            bottom: 0; left: 0;
            position: absolute;
        }
        .login-panel {
            margin-top: 25%;
            padding: 30px;
            border-radius: 25px;
        }
    </style>
</head>
<body id="lojista-ui">
<div class="login-container-row container">
    <div class="row">
        <div class="login-container col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12" style="display: none;">
            <div class="login-panel panel panel-default" >
                <div class="panel-heading">
                    <img src="{$basePath}/common/default/images/site_dist/logozao.png" style="display: block; margin: 0 auto;">
                </div>
                <h2 style="text-align: center;">
                    Sistema Lojistas
                </h2>
                <hr>
                <div class="panel-body">
                    <form action="{$this->url(['module' => 'lojistas', 'controller' => 'usuarios', 'action' => 'login'])}" method="post" id="login-form">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="login" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="senha" type="password" value="">
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" id="do-login" style="opacity: 0.9;"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="main">
    <header id="lojista-header">
        <nav id="lojista-nav-menu">
            <ul id="lojista-nav-list">
                <li class="lojista-nav-items" id="store-name">
                    <span id="store-label"></span>
                </li>
                <li class="lojista-nav-items" id="logout">
                    <a href="#"> Sair </a>
                </li>
            </ul>
        </nav>
    </header>
    <main id="lojista-content">
        <div id="lojista-content-heading">
            <div id="lojista-content-logo-tbb">
                <a class="nav-a nav-tbb c-ir" href="http://www.thebeautybox.com.br/" title="Loja de Cosméticos e Perfumes - The Beauty Box" target="_blank">
                    <picture>
                        <source media="(max-width: 1440px)" srcset="{$imgPath}/logo-tbb.md.png">
                        <source media="(min-width: 1440px)" srcset="{$imgPath}/logo-tbb.png">
                        <img src="{$imgPath}/logo-tbb.md.png" alt="The Beauty Box">
                    </picture>
                </a>
            </div>
            <div id="lojista-content-logo-hug">
                <picture>
                    <source media="(max-width: 1440px)" srcset="{$imgPath}/logozao.md.png">
                    <source media="(min-width: 1440px)" srcset="{$imgPath}/logozao.png">
                    <img src="{$imgPath}/logozao.md.png" alt="Semana do Abraço">
                </picture>
            </div>
        </div>
        <hr>
        <div class="lojistas-blocks" id="lojista-content-body">
            <div id="lojista-estoques">
                <h3>Estoques</h3>
                <div>
                    <div id="numbers-block">
                        <div>Saldo inicial de estoque: <strong id="saldo-inicial">100</strong> </div>
                        <div>Saldo atual de estoque: <strong id="saldo-atual">1</strong> </div>
                    </div>
                    <div id="progress-block">
                        <div id="progress">
                            <div id="current-progress" style="width: 70%;"> 70% </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="lojistas-blocks" id="lojista-cupom">
                <h3>Validação de cupom</h3>
                <form method="POST" id="validate-cupom">
                    <div class="input-blocks">
                        <div class="cupom-field">
                            <label class="cupom-label" for="cpf">CPF:</label>
                            <input class="cupom-field" type="text" name="cpf" id="cpf" required>
                        </div>
                        <div class="cupom-field">
                            <label  class="cupom-label" for="cupom">Código do cupom:</label>
                            <input class="cupom-field" type="text" name="cupom" id="cupom" required>
                        </div>
                    </div>
                    <div class="button-blocks">
                        <button type="submit" id="do-validate" style="background-color: #fcf4a9; color: #7AAFA2">Validar</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>
<script>
    document.basePath = '{$basePath}';
</script>
<!-- Core Scripts - Include with every page -->
<script src="{$basePath}/common/admin/newjs/jquery-1.10.2.js"></script>
<script src="{$basePath}/common/admin/newjs/bootstrap.min.js"></script>
<script src="{$basePath}/common/lojistas/scripts/bundle/main.js"></script>
</body>

</html>
