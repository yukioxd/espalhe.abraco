<ul class="pagination">
    <li class="paginate_button previous {if !isset($previous)} disabled {/if}" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous">
        <a {if isset($previous)} href="{$this->url(['page' => $previous])}" {/if}>Anterior</a>
    </li>
    {foreach from=$pagesInRange item=page}
        {if $page != $this->current}
            <li class="paginate_button"><a href="{$this->url(['page' => $page])}">{$page}</a></li>
        {else}
            <li class="paginate_button active"><a href="{$this->url(['page' => $page])}">{$page}</a></li>
        {/if}
    {/foreach}
    
    <li class="paginate_button next {if !isset($next)} disabled {/if}" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next">
        <a {if isset($next)} href="{$this->url(['page' => $next])}" {/if}>Próximo</a>
    </li>
</ul>