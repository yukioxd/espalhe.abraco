<div class="col-lg-6">
    <form enctype="multipart/form-data" action="{$form->getAction()}" method="post">
            
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-buttons">
                    <button type="submit" class="btn btn-outline btn-primary">{$form->getElement('submit')->getLabel()}</button>
                    <button type="button" id="cancel" class="btn btn-outline btn-warning" data-url="{$form->getElement('cancel')->getAttrib("data-url")}">Cancelar</button>
                </div>
            </div>
                
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#geral" data-toggle="tab">Geral</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="geral" class="tab-pane fade active in">
                        {foreach from=$form->getElements() item=element}
                            {if $element->getName() != "submit" && $element->getName() != "cancel"}
                                <div class="form-group">
                                {$element}
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
