<script> document.basePath = '{$basePath}';</script>
<script src="{$basePath}/common/default/scripts/bundle/main.js"></script>
<script src="{$basePath}/common/default/js/storage.polyfill.js"></script>
<script>
    {literal}
    window.showMid = function ( ) {
        var rule = document.createElement('div');
        rule.style.position = 'fixed';
        rule.style.left = '50%';
        rule.style.top = '0%';
        rule.style.height = '100%';
        rule.style.width = '1px';
        rule.style.marginLeft = '-0.5px';
        rule.style.background = '#000';
        document.body.appendChild (rule);
    }
    window.debugWidth = function ( ) {
        var elements = document.querySelectorAll('body *');
        Array.prototype.forEach.call( elements, function ( item ) {
            var r =  item.getBoundingClientRect();
            if ( ( r.width + r.left )> window.innerWidth )
                console.log( { e: item, left: r.left, width: r.width } );
        });
    }
    {/literal}
</script>

{* livereload *}
{if preg_match('/local\.abraco/', $smarty.server.HTTP_HOST)}
    <script src="http://local.abraco:35729/livereload.js?ext=Chrome&amp;extver=2.1.0"></script>
{elseif preg_match('/abraco\.dev/', $smarty.server.HTTP_HOST)}
    <script src="http://localhost:35729/livereload.js?ext=Chrome&amp;extver=2.1.0"></script>
{elseif preg_match('/localhost/', $smarty.server.HTTP_HOST)}
    <script src="http://localhost:35729/livereload.js?ext=Chrome&amp;extver=2.1.0"></script>
{elseif preg_match('/192\.168\.0\.16/', $smarty.server.HTTP_HOST)}
    <script src="http://192.168.0.16:35729/livereload.js?ext=Chrome&amp;extver=2.1.0"></script>
{/if}

<script>{literal}
    /*! loadCSS. [c]2017 Filament Group, Inc. MIT License */
    !function(a){"use strict";var b=function(b,c,d){function e(a){return h.body?a():void setTimeout(function(){e(a)})}function f(){i.addEventListener&&i.removeEventListener("load",f),i.media=d||"all"}var g,h=a.document,i=h.createElement("link");if(c)g=c;else{var j=(h.body||h.getElementsByTagName("head")[0]).childNodes;g=j[j.length-1]}var k=h.styleSheets;i.rel="stylesheet",i.href=b,i.media="only x",e(function(){g.parentNode.insertBefore(i,c?g:g.nextSibling)});var l=function(a){for(var b=i.href,c=k.length;c--;)if(k[c].href===b)return a();setTimeout(function(){l(a)})};return i.addEventListener&&i.addEventListener("load",f),i.onloadcssdefined=l,l(f),i};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);
    /*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License */
    !function(a){if(a.loadCSS){var b=loadCSS.relpreload={};if(b.support=function(){try{return a.document.createElement("link").relList.supports("preload")}catch(b){return!1}},b.poly=function(){for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d,d.getAttribute("media")),d.rel=null)}},!b.support()){b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){b.poly(),a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){a.clearInterval(c)})}}}(this);{/literal}
</script>
{if preg_match('/semanadoabraco/', $smarty.server.HTTP_HOST)}
{literal}
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '632198336936856');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=632198336936856&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-37428835-3', 'auto');
      ga('send', 'pageview');

    </script>
{/literal}
{/if}

</html>
