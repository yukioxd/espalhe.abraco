<!DOCTYPE HTML>
<html lang="pt-br" {* manifest="{$basePath}/common/default/sustentamais.appcache" *}>
<head>
    <meta charset="utf-8"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Promoção Semana do Abraço | The Beauty Box </title>
    <meta charset="utf-8">
    <meta name="description" content="Aqui seu abraço vale um hidratante! Se cadastre e ganhe um Hug Me Bombom de Baunilha 300ml.">
    <meta property="og:locale" content="pt_BR">
    <meta property="og:description" content="Aqui seu abraço vale um hidratante! Se cadastre e ganhe um Hug Me Bombom de Baunilha 300ml.">
    <meta property="og:title" content="Promoção Semana do Abraço | The Beauty Box"/>
    <meta property="og:url" content="http://www.semanadoabraco.com.br/"/>
    <meta property="og:site_name" content="Promoção Semana do Abraço | The Beauty Box"/>
    <meta property="og:image" content="{$basePath}/common/default/images/site_dist/logo-tbb.png">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="140">
    <meta property="og:image:height" content="139">

    <link rel="apple-touch-icon" sizes="57x57" href="{$basePath}/common/default/meta/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{$basePath}/common/default/meta/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{$basePath}/common/default/meta/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{$basePath}/common/default/meta/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{$basePath}/common/default/meta/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{$basePath}/common/default/meta/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{$basePath}/common/default/meta/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{$basePath}/common/default/meta/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{$basePath}/common/default/meta/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{$basePath}/common/default/meta/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{$basePath}/common/default/meta/favicon.ico">
    <link rel="manifest" href="{$basePath}/common/default/meta/manifest.json">
    <meta name="msapplication-TileColor" content="#5fb8a9">
    <meta name="msapplication-TileImage" content="{$basePath}/common/default/meta/ms-icon-144x144.png">
    <meta name="theme-color" content="#5fb8a9">

    <link rel="preload" href="{$basePath}/common/default/css/main.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{$basePath}/common/default/css/main.css"></noscript>
    <style type="text/css">
        .container {
            display: block; margin: 0 auto; position: relative;
        }
        html { height: 100%; }
        html, body { min-height: 100%; }
        h1, h2, h3, h4, body { margin: 0; padding: 0; font-weight: 300; }
        h1, h2, h3, h4 { font-size: inherit; }
        body#index {
            background: url('{$basePath}/common/default/images/site/pattern/purple-v8.png') repeat;
        }
        body{
            margin: 0;
            padding: 0;
            position: relative;
            font-family: 'OpenSans', 'tahoma';
            font-weight: 300;
        //overflow: hidden;
            color: #fff;
        }
        @media all and ( min-height:  644px ) {
            body#index:before{
                height: 37%;
                top: 0%;
            }
        }
        @media all and ( min-height:  520px ) and ( max-height: 643px ) {
            body#index:before{
                /* height: 37%; */
                height: 25%;
                top: 0%;
            }
        }
        body#index:before{
            /*background-color: #5fb8a9;
            background: url('{$imgPath}/pattern/green.png') repeat;*/
            background: url('{$imgPath}/green-pattern-v6.png') repeat;
            content: '';
            width: 100%;
            left: 0;
            position: absolute;
        }
        @media all and ( max-width: 1023px ) {
            body:before{
                height: 30%;
                top: 0%;
            }
        }
        #header {
            position: relative;
            left: 0;
            width: 100%;
            height: 22.5%;
            white-space: nowrap;
            z-index: 2;
        }
        #header > .container {
            height: 100%;
            top: 1.5%;
            text-align: center;
        }

        #mobile-menu-bar {
            position: absolute;
            width: 100%;
            min-height: 60px;
            top: 0;
            left: 0;
            text-align: right;
            z-index: 4;
            padding: 0;
            /* background-color: #9772ac; */

            -webkit-box-sizing:  border-box;
            -moz-box-sizing:  border-box;
            -ms-box-sizing:  border-box;
            -o-box-sizing:  border-box;
            box-sizing:  border-box;
        }
        #mobile-menu-controller {
            -webkit-appearance:  none;
            -moz-appearance:  none;
            -ms-appearance:  none;
            -o-appearance:  none;
            appearance:  none;
            opacity: 0;
            z-index: -1;
        }
        .switch-decorator{
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
            width: 35px;
            height: 18px;
            position: relative;
            z-index: 1;
            cursor: pointer;
            border-top: 2px solid rgb(255, 255, 255);
            border-bottom: 2px solid rgb(255, 255, 255);
        }
        .switch-decorator:before {
            content: "";
            position: absolute;
            display: block;
            top: 50%;
            width: 100%;
            height: 2px;
            margin-top: -1px;
            background-color: rgb(255, 255, 255);
        }
        .switch-label {
            padding-right: 0.5em;
            line-height: 60px;
            position: relative;
            z-index: 1;
        }
        #mobile-menu-controller:checked ~ #mobile-menu-list {
            display: block;
        }
        #mobile-menu-list {
            margin: 0;
            padding: 0;
            text-align: left;
            display: none;
            background: url('{$imgPath}/green-pattern-v6.png') repeat;
            position: relative;
        }
        #mobile-menu-list:before {
            background: url('{$imgPath}/green-pattern-v6.png') repeat;
            position: absolute;
            content: '';
            top: -60px;
            left: 0;
            width:  100%;
            height: 60px;
        }
        .mobile-menu-bar-item {
            display: block;
            width: 100%;
            line-height: 44px;
            border-bottom: 1px solid #fff;
            padding-left: 0.5em;
            @include box-sizing(border-box);
            &:first-child {
                border-top: 1px solid #fff;
            }
        }
        @media all and (max-width: 1023px) { /* menu hamburguer */
            #nav-list .nav-items { font-size: 0.80em }
            #header { margin: 0; padding: 0; z-index: 3;}
            .nav-items { vertical-align: middle;}
            .mobile-only { display: block; }
            .desk-only,.container.desk-only { display: none; }
        }
        @media all and (min-width: 1024px) {
            #navigation:before { content: ''; position: relative; width: 0px; height: 100%;  }
            #navigation:before, #nav-list { display: inline-block; vertical-align: middle; }
            .nav-items { margin: 0 1em; vertical-align: top;}
            .mobile-only { display: none; }
            .desk-only,.container.desk-only { display: block; }
        }
        @media all and (min-width: 1023px) and (max-width: 1366px){
            .nav-items { font-size: 1.1em; margin: 0 1.4em;}
        }
        @media all and ( min-width: 1024px ) {
            .nav-items {
                top: 20px;
                position: relative;
            }
        }
        @media all and ( max-width: 1025px ) {
            .container { max-width: 100%; }
        }
        @media all and ( min-width: 1116px ) {
            .container { width: 78%; }
            body#index #index-container.container { width: 96% }
        }
        @media all and ( min-width: 1160px ) {
            .container { width: 78%; }
            body#index #index-container.container { width: 94% }
        }
        @media all and ( min-width: 1200px ) {
            .container { width: 76%; }
            header .container { width: 90%; }
            body#index #index-container.container { width: 92% }
        }
        @media all and (min-width: 1280px) and ( max-width: 1366px) {
            .nav-items { font-size: 1.2em; margin: 0 1.4em; }
        }
        @media all and (max-width: 1366px) {
            .participe-btn { padding: 0.5em 1em; }
        }
        @media all and (min-width: 1367px) {
            .nav-items { font-size: 1.2em }
            .participe-btn { padding: 0.25em 1em;  }
        }
        @media all and (min-width: 1440px) {
            #header { font-size: 1.1em; }
            #navigation { margin-left: 3em; }
            .nav-items { margin: 0px 1.1em; }
            .container { max-width: 1580px; }
        }
        @media all and (min-width: 1660px) {
            .nav-items { margin: 0px 1.8em; }
        }
        a{ color: inherit; text-decoration: none; }
        {literal}
        .r-25 { border-radius: 25px; }
        .di { display: inline-block; vertical-align: middle; height: 100%; }
        #nav-list { margin: 0; padding: 0; }
        .nav-items { display: inline-block; box-sizing: border-box;}
        .nav-tbb-item { margin-right: 0; }
        .nav-a { text-decoration: none;}
        .c-ir { display: block; overflow: hidden; position: relative;}
        .ir { display: inline-block; position: absolute; left: -7000px;}
        .participe-btn { color: #7aafa0; background: #fff; font-weight: 600;}
        #hugweek { color: transparent; }
        #hugweek picture:before { position: relative; content: ''; display: inline-block;vertical-align: middle; width: 0px; height: 100%; }
        #hugweek picture img { max-height: 90%; display: inline-block; vertical-align: middle;}
        {/literal}
    </style>
</head>