<html>
<body style="margin:0; padding:0">
<div style="background-color:#9772ac; margin:0; padding:0">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="{$basePath}/common/default/images/site_dist/pattern/purple.png" color="#9772ac"/>
  </v:background>
  <![endif]-->
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td valign="top" align="center" background="{$basePath}/common/default/images/site_dist/pattern/purple.png">


		<table cellpadding="0" cellspacing="0" border="0" height="171" width="100%">
		  <tr>
		    <td background="{$basePath}/common/default/images/site_dist/green-pattern-v6.png" bgcolor="#60b8a9" valign="middle" align="center" height="189">
		      <!--[if gte mso 9]>
		      <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:136px;">
		        <v:fill type="tile" src="{$basePath}/common/default/images/site_dist/green-pattern.png" color="#60b8a9" />
		        <v:textbox inset="0,0,0,0">
		      <![endif]-->
		      <table cellpadding="0" cellspacing="0" border="0"><tr>
		      	<td width="142" valign="top" align="center"><img src="{$basePath}/common/emkt/email-amiga-logo-header.png" width="92" height="38"></td>
		      	<td width="216"><img src="{$basePath}/common/emkt/email-logo.png" width="216" height="171" style="display: block;"></td>
		      	<td width="142"></td>
		      </tr></table>
		      <!--[if gte mso 9]>
		        </v:textbox>
		      </v:rect>
		      <![endif]-->
		    </td>
		  </tr>
		</table>

		<table style="max-width: 600px;" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
					<br>
					<font face="open sans, arial" size="6" color="#fff5a7"><b>{$nome} mandou um abraço para você</b></font><br>
					<br>
					<img src="{$basePath}/common/emkt/email-amiga-abraco-anim.gif" width="125" height="49">
					<br><br>
					<table width="500" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<table cellpadding="6">
									<tr>
										<td>
											<font face="open sans, arial" size="5" color="#ffffff">E a Beauty te abraça com um hidratante <b>Hug Me Bombom de Baunilha 300ml</b>.</font>
										</td>
									</tr>
								</table>
								<br><br>
								<center>
								<a href="http://www.semanadoabraco.com.br/?utm_source=amiga&utm_medium=email" target="_blank"><img src="{$basePath}/common/emkt/email-amiga-participe.png" width="160" height="61" border="0"></a><br><br>
								<font face="open sans, arial" size="5" color="#fff5a7">Descubra como ganhar:</font>
								</center>
							</td>
							<td valign="bottom">
								<img src="{$basePath}/common/emkt/email-amiga-pack.png" width="150" height="242" style="display: block;">
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<img src="{$basePath}/common/emkt/email-amiga-steps.png" width="500" height="627"><br><br>
								<a href="http://www.semanadoabraco.com.br/?utm_source=amiga&utm_medium=email" target="_blank"><img src="{$basePath}/common/emkt/email-amiga-participe.png" width="160" height="61" border="0"></a><br>
								<br>
								<font face="open sans, arial" size="1" color="#ffffff">Consulte regulamento no site <a href="http://www.semanadoabraco.com.br/?utm_source=amiga&utm_medium=email" target="_blank"><font color="#ffffff">www.semanadoabraco.com.br</font></a>.<br><br></font>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	  	<table width="100%" cellpadding="0" cellspacing="0">
	  		<tr>
	  			<td bgcolor="#7abdab" align="center" height="98">

	  				<table width="426">
	  					<tr>
	  						<td align="left">
	  							<a href="https://www.facebook.com/facedabeauty" target="_blank"><img src="{$basePath}/common/emkt/email-facebook.png" width="25" height="25" border="0"></a>&nbsp;&nbsp;&nbsp;
	  							<a href="https://www.instagram.com/instadabeauty/" target="_blank"><img src="{$basePath}/common/emkt/email-instagram.png" width="25" height="25" border="0"></a>&nbsp;&nbsp;&nbsp;
	  							<a href="https://www.youtube.com/user/videosdabeauty" target="_blank"><img src="{$basePath}/common/emkt/email-youtube.png" width="35" height="26" border="0"></a>
	  						</td>
	  						<td align="right">
	  							<a href="http://www.thebeautybox.com.br/" target="_blank"><img src="{$basePath}/common/emkt/email-logo-tbb.png" width="113" height="47" border="0"></a>
	  						</td>
	  					</tr>
	  				</table>

	  			</td>
	  		</tr>
	  	</table>


      </td>
    </tr>
  </table>
</div>

</body>
</html>