{include file="head.tpl"}
<style>
    @media all and ( min-width: 1024px ) {
        .layout-content.type-b {
            margin-top: 4em;
        }
    }
</style>
<body class="type2">
<main class="layout-content type-b" id="coupon-block">
	<h2 id="page-title" class="page-titles"><span style="font-size: 0.7em;">Baixe o Cupom</span> </h2>
    <img id="cupom-image" src="{$cupomPath}" alt="Grave estecupom e salve no seu celular" style="max-width: 300px; width: 80%; margin: 0 auto; display: block; position: relative; left: 2%;">
    <p style="text-align: center; color: #000">
        Segure na imagem e selecione <span class="b">Salvar Imagem</span>
    </p>
</main>
</body>
