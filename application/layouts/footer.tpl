<footer id="footer">
    <nav id="footer-nav">
        <ul class="di desk-only nav-footer-list">
            <li class="di nav-footer-items">
                <a href="{$this->url([], 'sustentabilidade')}">
                    Sustentabilidade
                </a>
            </li>
            <li class="di nav-footer-items">
                <a class="modal-iframe" href="{$this->url([], 'home')}common/default/regulamento.pdf" target="_blank">Regulamento</a>
            </li>
            <li class="di nav-footer-items">
                <a href="http://www.thebeautybox.com.br/institucional/duvidas" target="_blank">Contato</a>
            </li>
            <li class="di nav-footer-items">
                <a href="http://www.thebeautybox.com.br/" title="Visite também a loja de Cosméticos e Perfumes - The Beauty Box" target="_blank">Site The Beauty Box</a>
            </li>
        </ul>
        <hr class="di desk-only v">
        <ul class="di nav-footer-list social">
            <li class="nav-footer-social-items">
                <a target="_blank" title="Visite a nossa página do Facebook" class="nav-footer-a nav-footer-fb" href="https://www.facebook.com/facedabeauty">
                    <span class="ir">Página do Facebook</span>
                </a>
            </li>
            <li class="nav-footer-social-items">
                <a target="_blank" title="Visite o nosso perfil do Instagram " class="nav-footer-a nav-footer-insta" href="https://www.instagram.com/instadabeauty/">
                    <span class="ir">Perfil do Instagram</span>
                </a>
            </li>
            <li class="nav-footer-social-items">
                <a target="_blank" title="Visite o nosso canal do YouTube" class="nav-footer-a nav-footer-youtube" href="https://www.youtube.com/user/videosdabeauty">
                    <span class="ir">Canal do YouTube</span>
                </a>
            </li>
        </ul>
    </nav>
</footer>