<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>TIMO Panel v2.0</title>
        
        <link href="{$basePath}/common/admin/css/jquery-ui-1.8.20.custom.css" rel="stylesheet">
        
        <!-- Core CSS - Include with every page -->
        <link href="{$basePath}/common/admin/css/bootstrap.css" rel="stylesheet">
        <link href="{$basePath}/common/admin/css/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Dashboard -->
        <link href="{$basePath}/common/admin/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
        <link href="{$basePath}/common/admin/css/plugins/timeline/timeline.css" rel="stylesheet">

        <!-- SB Admin CSS - Include with every page -->
        <link href="{$basePath}/common/admin/css/sb-admin.css" rel="stylesheet">
        
        <script type="text/javascript">
            document.basePath = '{$basePath}';
            document.openedController = '{$openedController}';
        </script>

	    <!-- Core Scripts - Include with every page -->
	    <script src="{$basePath}/common/admin/js/jquery-1.7.2.min.js"></script>
	    <script src="{$basePath}/common/admin/js/jquery-ui-1.8.20.custom.min.js"></script>
	    <script src="{$basePath}/common/admin/newjs/bootstrap.min.js"></script>
	    <script src="{$basePath}/common/admin/newjs/plugins/metisMenu/jquery.metisMenu.js"></script>
    </head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <!-- /.navbar-header -->

            {$this->navigation()->menu()->renderPartial(null,'nav.tpl')}

            <!-- /.navbar-top-links -->

            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            {if $success|default:"" != ""}
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {$success}
                </div>
            {/if}
            
            {if $error|default:"" != ""}
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {$error}
                </div>
            {/if}

            {if $alert|default:"" != ""}
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {$alert}
                </div>
            {/if}



            <!-- /.row -->
            <div class="row">
                {$this->layout()->content}
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <div id="timo-loader">
        <div id="timo-loader-inner"></div>
    </div>
    
    {if $openedController == 'index'}
        {*<script src="{$basePath}/common/admin/newjs/demo/dashboard-demo.js"></script>*}
    {/if}
    <script src="{$basePath}/common/admin/newjs/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="{$basePath}/common/admin/newjs/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="{$basePath}/common/admin/newjs/sb-admin.js"></script>
    <script src="{$basePath}/common/admin/js/autocomplete.js"></script>
    <script src="{$basePath}/common/admin/js/application.js"></script>
</body>
</html>
