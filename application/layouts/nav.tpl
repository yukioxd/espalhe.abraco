{assign var=navigation value=$this->container}
<ul class="nav navbar-top-links navbar-left" id="side-menu">
    {foreach from=$navigation item=page}
        {assign var=subPages value=$page->getPages()}
        {foreach from=$subPages item=subPage}
            {if $subPage->getClass() != 'hide'}
                <li>
                    <a href="{$subPage->getHref()}">
                        <i class="{$subPage->fa}"></i>
                        {$subPage->label}
                    </a>
                </li>
            {/if}
        {/foreach}
    {/foreach}
    <li style="height: 50px;">
        <a href="http://www.thebeautybox.com.br/" target="_blank" style="padding: 0px 10px">
            <img src="{$basePath}/common/default/images/site_dist/logo-tbb.png" alt="" style="max-height: 50px;">
        </a>
    </li>
</ul>