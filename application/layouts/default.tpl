{include file="head.tpl"}
<body id="{$currentAction}" class="{if $currentAction !=='index'}type2{/if}">
    {include file="header.tpl"}
    {$this->layout()->content}
    {include file="footer.tpl"}
</body>
{include file="after-footer.tpl"}
