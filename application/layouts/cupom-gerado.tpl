{include file="head.tpl"}
<body id="{$currentAction}" class="{if $currentAction !=='index'}type2{/if}">
{include file="header.tpl"}
<main class="layout-content type-b" id="coupon-block">
	<h2 id="page-title" class="page-titles">Cupom gerado</h2>
	<p class="cupom-regular-text" id="cupon-instruction-p" style="text-align: center;">
		Grave este cupom no seu celular para apresentar na loja do <strong id="store-name">{$nome_loja}</strong>
		<span id="cupom-attention">Lembre-se que a retirada é valida somente nesta loja.</span>
	</p>

	<div id="cupom-image-block">
		<div id="cupom-wrap-image-block"><img id="cupom-image" src="{$cupomPath}" alt="Grave estecupom e salve no seu celular"></div>
		<div id="promo-info-block">
			<table class="pseudo-table">
				<tr class="pseudo-row">
					<td class="img-action-cells"></td>
					<td class="img-action-cells w-btn" rowspan="2">
						<div class="img-action-cells-block">
							<button class="img-actions" id="dispatch-email"> Enviar o cupom por e-mail </button>
						</div>
					</td>
					<td class="img-action-cells"></td>
					<td class="img-action-cells w-btn" rowspan="2">
						<div class="img-action-cells-block">
							<button class="img-actions" id="dispatch-sms"> <i></i>Enviar o cupom  por SMS</button>
						</div>
					</td>
					<td class="img-action-cells"></td>
				</tr>
				<tr class="pseudo-row">
					<td class="img-action-cells brd-t"> </td>
					<td class="img-action-cells brd-t"> </td>
					<td class="img-action-cells brd-t right-top-corner"> </td>
				</tr>
			</table>
			<div class="lets-mobile-share" id="do-download">
				<a class="share-btn" id="download-on-device" target="_blank" href="{$downloadURL}" title="Semana do Abraço">
					Gravar no rolo da Câmera
				</a>
			</div>
			<div id="lets-share">
				{* <h3 class="cupom-block-titles cupom-regular-text" id="share-title">Avise esta promoção para suas amigas: </h3> *}
				<p id="lets-share-description">
					Que tal aproveitar para publicar um <br>
					abraço e espalhar essa ideia?<br>
					Use a hashtag <strong>#abracenaBeauty</strong><br>
					e marque também o <strong>@InstaDaBeauty</strong>
				</p>
				<button class="share-btn" id="share-on-facebook">
					<i id="icon-facebook"></i> Publique
				</button>
				<a href="{$mailTo}" class="share-btn" id="send-using-email">
					<i id="icon-email"></i>	Enviar por e-mail
				</a>
			</div>
			<div class="lets-mobile-share" id="do-share-whatsapp">
				Avise essa promoção para suas amigas:
				<a href="whatsapp://send?text=www.semanadoabraco.com.br%20Olha%20que%20incr%C3%ADvel%20a%20Promo%C3%A7%C3%A3o%20Semana%20do%20Abra%C3%A7o%20da%20The%20Beauty%20Box:%20%C3%89%20s%C3%B3%20se%20cadastrar,%20apresentar%20o%20cupom%20na%20loja%20selecionada%20e%20voc%C3%AA%20ganha%20a%20Lo%C3%A7%C3%A3o%20Hidratante%20Hug%20Me%20Bombom%20de%20Baunilha%20300ml" data-action="share/whatsapp/share" class="share-btn" id="send-using-whatsapp">
					Compartilhar por WhatsApp
				</a>
			</div>
			<div class="cupom-regular-text" id="lets-recycle">
				<h3 class="cupom-block-titles" id="recycle-title">Reciclagem de embalagens</h3>
				<p id="recycle-p">
					Aproveite sua visita na loja The Beauty Box para reciclar embalagens vazias de qualquer fabricante por meio de nosso programa permanente em todas as nossas lojas. Traga 5 embalagens da nossa marca própria The Beauty Box e ganhe 1 Produtinho da Beauty.
				</p>
			</div>
		</div>
	</div>
</main>
{include file="footer.tpl"}
</body>
{include file="after-footer.tpl"}
