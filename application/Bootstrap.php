<?php

/**
 * Classe de inicialização da aplicação
 * 
 * @name Bootstrap
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	/**
	 * Armazena o view
	 * 
	 * @name $_view
	 */
	protected $_view = NULL;
	
	/**
	 * Armazena a configuração
	 * 
	 * @name $_config
	 */
	protected $_config = NULL;
	
	/**
	 * Busca a configuração do INI
	 *
	 * @name _initConfig
	 */
	protected function _initConfig() {
		// Busca a configuração
		$this->_config = new Zend_Config($this->getOptions());
		
		// Registra o config na sessão
		Zend_Registry::set("config", $this->_config);
		
		// Inicia os loaders
		$this->bootstrap("autoloaders");
	}
	
	/**
	 * Inicia o objeto para cache
	 *
	 * @name _initCache
	 */
	protected function _initCache() {
		$frontendOptions = array(
			'lifetime' => $this->_config->cms->cache->time,
			'automatic_serialization' => TRUE
		);
		
		$backendOptions = array (
			'cache_dir' => $this->_config->cms->cache->cache_dir 
		);
		
		$cache = Zend_Cache::factory("Core", "File", $frontendOptions, $backendOptions);
		Zend_Registry::set("cache", $cache);
	}
	
	/**
	 * Inicializa as rotas
	 * 
	 * @name _initRouter
	 */
	protected function _initRouter() {
		
		
		try {
			$this->bootstrap("frontController");
			$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/routes.ini", "routes");
			$router = $this->getResource("frontController")
				->getRouter()
				->addConfig($config, "routes");
			
			return $router;
		}
		catch(Exception $e) {
			return FALSE;
		}
	}
	
	/**
	 * Inicializa os loaders
	 * 
	 * @name _initAutoloaders
	 */
	protected function _initAutoloaders() {
		$this->getApplication()->setAutoloaderNamespaces(array("Cms_", "ZendX_"));
	}

	/**
	 * Inicializa a conexão com o banco de dados
	 * 
	 * @name _initConnection
	 */
	protected function _initConnection() {
		
		//
		$debug = getenv("APPLICATION_DEBUG");
		if($debug == 1) {
			$log = "[" . date("H:i:s") . "] Initializing database connection";
			Zend_Registry::get("debug")->Log($log);
		}
		
		// Busca as opções de configuração
		$options = $this->getOption("resources");
		$enabled = $options['db']['enabled'];
		
		// Verifica se está habilitado
		if($enabled) {
			$db_adapter = $options['db']['adapter'];
			$params = $options['db']['params'];
			
			try {
				// Carrega a classe adaptadoda
				$db = Zend_Db::factory($db_adapter, $params);
				
				// Busca a conexão
				$db->getConnection();
				
				// Registra a conexão
				$registry = Zend_Registry::getInstance();
				$registry->set("db", $db);
				//$db->closeConnection();
			}
			catch(Exception $e) {
				// Verifica se deve fazer o debug
				if(APPLICATION_ENV == "production") {
					// Mostra o problema com a conexão de dados
					die("Estamos com problemas no momento, retorne em alguns instantes. Obrigado.");
				}
				else {
					// Debuga a conexão
					var_dump($e);
					die();
				}
				
			}
		}
		
		//
		if($debug == 1) {
			$log = "[" . date("H:i:s") . "] Database connection initialized";
			Zend_Registry::get("debug")->Log($log);
		}
	}

	/**
	 * Inicializa o view
	 * 
	 * @name _initView
	 */
	protected function _initView() {
		// Busca a sessão das mensagens 
		$session = new Zend_Session_Namespace("messages");
		
		// Busca as configurações do smarty
		$options = $this->getOption("smarty");
		
		// Cria o objeto smarty
		$this->_view = new Cms_View_Smarty($options);
		Zend_Registry::set("view", $this->_view);
		
		// Seta a codificação
		$this->_view->setEncoding("UTF-8");
		
		// Adiciona o renderer
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper("ViewRenderer");
		$viewRenderer->setView($this->_view)
			->setViewScriptPathSpec(APPLICATION_PATH . "/modules/:module/views/:controller/:action.:suffix")
			->setViewSuffix("tpl");

		$inflector = new Zend_Filter_Inflector(APPLICATION_PATH . "/layouts/:script.:suffix");
		$inflector->addRules(
		array(
			'script' => "layout",
			'suffix' => "tpl"
			)
		);

		Zend_Layout::startMvc(
			array(
			'view' => $this->_view,
			'inflector' => $inflector
			)
		);
		
		// Registra o viewer
		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
		
		// Adiciona o basepath ao view
		$options = $this->getOption("cms");
		$basePath = $options['config']['basepath'];
		$this->_view->basePath = $basePath;

		$common = $basePath . '/common/default';
		$this->_view->common = $common;

		$imgPath = $common . '/images/site_dist';
		$this->_view->imgPath =  $imgPath ;
		
		// Verifica se houve erros
		$this->_view->error = $session->error;
		
		// verifica se houve sucessos
		$this->_view->success = $session->success;

		// verifica se houve alert
		$this->_view->alert = $session->alert;
	}
	
	/**
	 * Inicia os plugins e helpers
	 * 
	 * @name _initPlugins
	 */
	protected function _initPlugins() {
		// Registra o view helper que cria a URL de forma facilitada
		$helper = new Cms_View_Helper_CreateUrl();
		$this->_view->registerHelper($helper, "CreateUrl");
		
		// Registra o view helper que busca registros de parentes dinamicos
		$helper = new Cms_View_Helper_DynamicParentRow();
		$this->_view->registerHelper($helper, "dynamicParentRow");
		
		// Registra o helper para criar bbcode
		$helper = new Cms_View_Helper_Bbcode();
		$this->_view->registerHelper($helper, "bbcode");
		
		// Registra o helper para criar o tradutor
		$helper = new Cms_View_Helper_Translate();
		$this->_view->registerHelper($helper, "translate");
		
		// Registra o helper para criar o tradutor
		$helper = new Cms_View_Helper_Dateformat();
		$this->_view->registerHelper($helper, "dateformat");
		
		// Registra o helper para buscar as colunas
		$helper = new Cms_View_Helper_GetColumnValue();
		$this->_view->registerHelper($helper, "GetColumnValue");
		
		// Registra o helper para criar o tradutor
		$helper = new Cms_View_Helper_CreateSlug();
		$this->_view->registerHelper($helper, "createslug");
		
		// Registra o helper da existencia do produto em estoque
		$helper = new Cms_View_Helper_Jsondecode();
		$this->_view->registerHelper($helper, "jsondecode");
		
		// Registra o helper para o qrcode
		$helper = new Cms_View_Helper_GoogleQRCode();
		$this->_view->registerHelper($helper, "googleQRCode");
		
// 		// Registra o helper para o qrcode
// 		$helper = new Cms_View_Helper_Qrcode();
// 		$this->_view->registerHelper($helper, "qrcode");
		
		// Registra o plugin do commons
		//$this->frontController->registerPlugin(new Cms_Controller_Plugin_Commons);
		
		// Registra o plugin do custom css
		//$this->frontController->registerPlugin(new Cms_Controller_Plugin_Customcss);
		
		// Registra o plugin do custom js
		//$this->frontController->registerPlugin(new Cms_Controller_Plugin_Customjs);
		
		// Registra o plugin de navigation
		$this->frontController->registerPlugin(new Cms_Controller_Plugin_Navigation);
		
		// Registra o plugin de verificação do usuario
		$this->frontController->registerPlugin(new Cms_Controller_Plugin_Userverify);
		
		// Registra o plugin de adição das meta tags
		//$this->frontController->registerPlugin(new Cms_Controller_Plugin_Metas);
		
		// Registra o plugin para carregar informações para todo o site
		$this->frontController->registerPlugin(new Cms_Controller_Plugin_Geral);
		
		// Plugin para ativar o ssl
		//$this->frontController->registerPlugin(new Cms_Controller_Plugin_Ssl());
		
		// Verifica se utiliza um layout por modulo
		if(!$this->config->cms->layout->justone) {
			$plugin = new Cms_View_ModularLayout();
			$this->frontController->registerPlugin($plugin);
		}
	}
}
