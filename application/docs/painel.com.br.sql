CREATE DATABASE  IF NOT EXISTS `tbb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tbb`;
-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: painel
-- ------------------------------------------------------
-- Server version	5.5.31-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `erros`
--

DROP TABLE IF EXISTS `erros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `erros` (
  `iderro` int(11) NOT NULL AUTO_INCREMENT,
  `data_execucao` datetime NOT NULL,
  `mensagem` text NOT NULL,
  `parametros` text NOT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `trace` text,
  PRIMARY KEY (`iderro`),
  KEY `fk_erros_usuarios1` (`idusuario`),
  CONSTRAINT `fk_erros_usuarios1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `erros`
--

LOCK TABLES `erros` WRITE;
/*!40000 ALTER TABLE `erros` DISABLE KEYS */;
INSERT INTO `erros` VALUES (1,'2013-07-09 11:07:04','Invalid argument: $page must be an instance of Zend_Navigation_Page or Zend_Config, or an array','{\"module\":\"admin\",\"controller\":\"usuarios\",\"action\":\"login\"}',NULL,'#0 /home/daniel/projetos/painel.com.br/library/Cms/Controller/Plugin/Navigation.php(138): Zend_Navigation_Container->addPage(\'\')\n#1 /home/daniel/projetos/painel.com.br/library/Zend/Controller/Plugin/Broker.php(309): Cms_Controller_Plugin_Navigation->preDispatch(Object(Zend_Controller_Request_Http))\n#2 /home/daniel/projetos/painel.com.br/library/Zend/Controller/Front.php(941): Zend_Controller_Plugin_Broker->preDispatch(Object(Zend_Controller_Request_Http))\n#3 /home/daniel/projetos/painel.com.br/library/Zend/Application/Bootstrap/Bootstrap.php(97): Zend_Controller_Front->dispatch()\n#4 /home/daniel/projetos/painel.com.br/library/Zend/Application.php(366): Zend_Application_Bootstrap_Bootstrap->run()\n#5 /home/daniel/projetos/painel.com.br/index.php(39): Zend_Application->run()\n#6 {main}');
/*!40000 ALTER TABLE `erros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `tabela` varchar(50) NOT NULL,
  `json_data` longblob NOT NULL,
  `acao_executada` varchar(20) NOT NULL,
  `data_execucao` datetime NOT NULL,
  PRIMARY KEY (`idlog`),
  KEY `fk_logs_usuarios1` (`idusuario`),
  CONSTRAINT `fk_logs_usuarios1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,5,'menu_itens','{\"iditem\":\"30\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"An\\u00fancio\",\"modulo\":\"admin\",\"controlador\":\"anuncio\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:35'),(2,5,'menu_itens','{\"iditem\":\"29\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"Not\\u00edcias\",\"modulo\":\"admin\",\"controlador\":\"noticia\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:39'),(3,5,'menu_itens','{\"iditem\":\"28\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"Campeonato\",\"modulo\":\"admin\",\"controlador\":\"campeonato\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:42'),(4,5,'menu_itens','{\"iditem\":\"27\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"Tipo de Campeonato\",\"modulo\":\"admin\",\"controlador\":\"tipo-campeonato\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:45'),(5,5,'menu_itens','{\"iditem\":\"26\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"\\u00c1rbitro\",\"modulo\":\"admin\",\"controlador\":\"arbitro\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:47'),(6,5,'menu_itens','{\"iditem\":\"25\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"Academia\",\"modulo\":\"admin\",\"controlador\":\"academia\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:49'),(7,5,'menu_itens','{\"iditem\":\"24\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"Treinador\",\"modulo\":\"admin\",\"controlador\":\"treinador\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:52'),(8,5,'menu_itens','{\"iditem\":\"23\",\"idperfil\":\"2\",\"idcategoria\":\"2\",\"descricao\":\"Federa\\u00e7\\u00e3o\",\"modulo\":\"admin\",\"controlador\":\"federacao\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:55'),(9,5,'menu_itens','{\"iditem\":\"22\",\"idperfil\":\"99\",\"idcategoria\":\"2\",\"descricao\":\"Presidente\",\"modulo\":\"admin\",\"controlador\":\"presidente\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:09:57'),(10,5,'menu_itens','{\"iditem\":\"21\",\"idperfil\":\"99\",\"idcategoria\":\"2\",\"descricao\":\"Confedera\\u00e7\\u00e3o\",\"modulo\":\"admin\",\"controlador\":\"confederacao\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:10:00'),(11,5,'menu_itens','{\"iditem\":\"19\",\"idperfil\":\"99\",\"idcategoria\":\"2\",\"descricao\":\"Categorias\",\"modulo\":\"admin\",\"controlador\":\"categoria\",\"acao\":\"list\",\"parametros\":\"\"}','DELETE','2013-07-09 11:10:09');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_categorias`
--

DROP TABLE IF EXISTS `menu_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_categorias` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT COMMENT '\n',
  `descricao` varchar(50) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_categorias`
--

LOCK TABLES `menu_categorias` WRITE;
/*!40000 ALTER TABLE `menu_categorias` DISABLE KEYS */;
INSERT INTO `menu_categorias` VALUES (1,'Administração',0),(2,'Cadastros',0);
/*!40000 ALTER TABLE `menu_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_itens`
--

DROP TABLE IF EXISTS `menu_itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_itens` (
  `iditem` int(11) NOT NULL AUTO_INCREMENT,
  `idperfil` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `controlador` varchar(50) NOT NULL,
  `acao` varchar(50) NOT NULL,
  `parametros` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iditem`),
  KEY `fk_menu_itens_menu_categorias1_idx` (`idcategoria`),
  KEY `fk_menu_itens_perfis1_idx` (`idperfil`),
  CONSTRAINT `fk_menu_itens_menu_categorias1` FOREIGN KEY (`idcategoria`) REFERENCES `menu_categorias` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_menu_itens_perfis1` FOREIGN KEY (`idperfil`) REFERENCES `tipoUsuario` (`tipoUsuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_itens`
--

LOCK TABLES `menu_itens` WRITE;
/*!40000 ALTER TABLE `menu_itens` DISABLE KEYS */;
INSERT INTO `menu_itens` VALUES (15,99,1,'Categorias de Menus','admin','menuscategorias','list',''),(16,99,1,'Itens de Menu','admin','menusitens','list',''),(17,2,1,'Usuários','admin','usuarios','list',''),(18,2,1,'Trocar Senha','admin','usuarios','trocarsenha','');
/*!40000 ALTER TABLE `menu_itens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_usuario`
--

DROP TABLE IF EXISTS `status_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_usuario` (
  `status_usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `sigla` varchar(2) DEFAULT NULL COMMENT 'BL - Bloqueado\nNA - Não Aprovado\nLB - Liberado',
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`status_usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_usuario`
--

LOCK TABLES `status_usuario` WRITE;
/*!40000 ALTER TABLE `status_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoUsuario`
--

DROP TABLE IF EXISTS `tipoUsuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoUsuario` (
  `tipoUsuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL COMMENT '- Usuário Administrador\n- Usuário Confederação\n- Usuário Federação\n- Usuário Atleta',
  `datacadastro` datetime DEFAULT NULL,
  `codigo` varchar(45) DEFAULT NULL COMMENT '01 - Administrador Portal\n02 - Administrador Empresa\n03 - Usuário\n',
  PRIMARY KEY (`tipoUsuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoUsuario`
--

LOCK TABLES `tipoUsuario` WRITE;
/*!40000 ALTER TABLE `tipoUsuario` DISABLE KEYS */;
INSERT INTO `tipoUsuario` VALUES (2,'Usuário',NULL,NULL),(3,'Cliente',NULL,NULL),(99,'Administrador',NULL,NULL);
/*!40000 ALTER TABLE `tipoUsuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_usuario_id` int(11) DEFAULT NULL,
  `tipoUsuario_id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `rg` varchar(45) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `datanascimento` datetime DEFAULT NULL,
  `datacadastro` datetime DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  KEY `fk_usuario_status_usuario1_idx` (`status_usuario_id`),
  KEY `fk_usuario_tipoUsuario2_idx` (`tipoUsuario_id`),
  CONSTRAINT `fk_usuario_status_usuario1` FOREIGN KEY (`status_usuario_id`) REFERENCES `status_usuario` (`status_usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipoUsuario2` FOREIGN KEY (`tipoUsuario_id`) REFERENCES `tipoUsuario` (`tipoUsuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (5,NULL,99,'Daniel','daniel@timo.com.br',NULL,NULL,NULL,NULL,NULL,NULL,'89794b621a313bb59eed0d9f0f4e8205','admin'),(6,NULL,2,'Usuario teste','teste@teste.com.br',NULL,NULL,NULL,NULL,NULL,NULL,'4297f44b13955235245b2497399d7a93','teste');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-09 11:17:42
