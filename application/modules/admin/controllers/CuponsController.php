<?php

/**
 * Controlador dos usuarios
 *
 * @name CuponsController
 */
class Admin_CuponsController extends Cms_Controller_Action {
	/**
	 * Armazena o model padrão da tela
	 *
	 * @access protected
	 * @name $_model
	 * @var Default_Model_Usuarios
	 */
	protected $_model = NULL;

	/**
	 *
	 */
	public function init() {
		// Inicializa o model da tela
		$this->_model = new Admin_Model_User();
		
		// Chama o parent
		parent::init();
	}

	/**
	 *
	 */
	public function indexAction() {
		$modelLoja = new Admin_Model_Loja();
		$selectLoja = $modelLoja->select();
		$selectLoja->from(['l' => 'loja'], [
			'nome' => 'l.nome' ,
			'estoque' => 'l.estoque_brinde' ,
			'numero' => "(SELECT COUNT(b.brinde_id) FROM brinde b WHERE b.loja_id = l.loja_id)"
		]);
		$selectLoja->join(['c' => 'cidade'], 'c.cidade_id = l.cidade_id' , [
			'cidade' => 'c.cidade',
			'cidade_id' => 'c.cidade_id'
		]);
		$selectLoja->join(['e' => 'estado'], 'e.estado_id = c.estado_id' , [
			'estado_id'	=> 'e.estado_id',
			'estado'	=> 'e.estado',
			'uf'		=> 'e.uf',
		]);
		$selectLoja->setIntegrityCheck( FALSE );
		$lojas = $modelLoja->fetchAll( $selectLoja );
		$lojasArray = $lojas->toArray();

		$mapeadoPorEstados = [ ];

		foreach ( $lojasArray as $loja ) {
			$estado_id = $loja['estado_id'];
			if ( ! isset(  $mapeadoPorEstados[ $estado_id ] ) ) {
				$mapeadoPorEstados[ $estado_id ] = [];
			}
			$porcento = ( $loja['numero'] / $loja['estoque'] ) * 100;
			$porcento = number_format( $porcento, 2, '.', '');
			$loja['porcento'] = $porcento . '%';
			$mapeadoPorEstados[ $estado_id ][] = $loja;
		}
		$this->view->estados = $mapeadoPorEstados;
	}
}
