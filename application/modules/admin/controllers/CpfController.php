<?php

/**
 * Controlador dos usuarios
 *
 * @name CpfController
 */
class Admin_CpfController extends Cms_Controller_Action {
	/**
	 * Armazena o model padrão da tela
	 *
	 * @access protected
	 * @name $_model
	 * @var Default_Model_Usuarios
	 */
	protected $_model = NULL;

	/**
	 *
	 */
	public function init() {
		// Inicializa o model da tela
		$this->_model = new Admin_Model_User();
		
		// Chama o parent
		parent::init();
	}

	/**
	 *
	 */
	public function indexAction() {

		if ( $this->_request->isPost() ) {
			$params = $this->_request->getParams();
			if ( isset( $params['cpf'] ) ) {
				//do search.
				$userModel = new Admin_Model_User();
				$selectUser = $userModel->select();
				$selectUser->from( [  'u' => 'user' ] );
				$selectUser->where("cpf = '{$params['cpf']}'");
				$userRow = $userModel->fetchRow($selectUser);
				if ( $userRow ) {
					$redirectParam = [
						'module' => 'admin',
						'controller' => 'cadastros',
						'action'	=> 'form',
						'user_id'	=> $userRow->user_id,
					];
					$url = $this->view->url( $redirectParam );
					$parts = explode('/' ,$url );
					$length = sizeof( $parts );
					$path = $parts[$length - 5] . '/' . $parts[$length - 4] . '/' . $parts[$length - 3] . '/' .$parts[$length - 2] . '/' . $parts[$length - 1];
					$session = new Zend_Session_Namespace('painel');
					$session->dados = [];
					$this->redirect( $path );
				} else {
					$session = new Zend_Session_Namespace('painel');
					$session->dados = [ 'error' => "CPF Não encontrado" ];
					if ( preg_match ('/\?/', $_SERVER['HTTP_REFERER'] ) ) {
						$key = strpos( $_SERVER['HTTP_REFERER'] , '?' );
						$url = substr( $_SERVER['HTTP_REFERER'], 0, $key );
					} else {
						$url = $_SERVER['HTTP_REFERER'];
					}
					$this->redirect( $url . '?clear=1' );
				}
			}
		} else {
			$params = $this->_request->getParams();
			if( isset($params['clearjs']) ) {
				$session = new Zend_Session_Namespace('painel');
				$session->dados = [];
			}
		}
	}
}
