<?php

/**
 *
 */
class Admin_IndexController extends Cms_Controller_Action
{
	/**
	 *
	 */

	public $_messages;

	public function init()
	{
		$this->_messages = new Zend_Session_Namespace("messages");
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$modelBrinde = new Admin_Model_Brinde();
		$select = $modelBrinde->select();
		$select->from([ 'b' => 'brinde' ], []);
		$select->columns([
			'gerados' 				=> 'count(b.brinde_id)',
			'resgatados' 			=> '(SELECT COUNT(b2.brinde_id) FROM brinde b2 WHERE b2.validado > 0)',
			'compartilhamentos' 	=> '(SELECT SUM(u.shared_facebook) FROM user u)',
		]);
		$select->setIntegrityCheck( FALSE );
		$brindeRow = $modelBrinde->fetchRow( $select );
		$this->view->dados = $brindeRow;
	}

	public function getXlsAction()
	{
		$modelUser = new Admin_Model_User();
        $selectUser = $modelUser->select();
        $selectUser->from(array('u' => 'user'), [
        		'nome',
        		'cpf',
        		'email',
        		'nascimento' => "DATE_FORMAT(u.nascimento, '%d/%m/%y')",
        		'cep',
        		'endereco',
        		'cidade',
        		'estado',
        		'facebook_id',
        		'news_letter'=>"IF(u.news_letter=0,'Não','Sim')",
        		'cadastro' => "DATE_FORMAT(u.stamp_cadastro, '%d/%m/%y')"
        ]);
        
        $selectUser->join(array('b' => 'brinde'), 'b.user_id = u.user_id');
        $selectUser->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', array('loja' => 'l.nome'));
        $selectUser->join(array('c' => 'cidade'), 'c.cidade_id = l.cidade_id', array('cidade' => 'c.cidade'));
        $selectUser->join(array('e' => 'estado'), 'c.estado_id = e.estado_id', array('estado' => 'e.uf'));
        $selectUser->setIntegrityCheck(false); 
       	$users =  $selectUser->query()->fetchAll();

		//$users = $modelUser->fetchAll($selectUser)->toArray();

		$objPHPExcel = new Cms_Geraexcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();

		$theadRow = array(
			"Contato",
			"CPF/CNPJ",
			"Nome",
			"Segundo Nome",
			"Sobrenome",
			"Email Principal",
			"Sexo",
			"Aniversário",
			"Telefone Celular",
			"Endereço 1: CEP",
			"Endereço 1: Logradouro",
			"Endereço 1: Cidade",
			"Endereço 1: Estado",
			"Facebook Handle",
			"Facebook Name",
			"Newsletter",
			"Cadastro",
			"Loja",
			"Cidade",
			"Estado",
		);

		$docArray = array(
			$theadRow
		);

		foreach ($users as $key => $user) {
			$row = array(
				'' . $user['nome'], //nome
				'' . $user['cpf'], //cpf
				'' . $user['nome'], //nome
				'', //segundo nome
				'', //sobrenome
				'' . $user['email'], //email principal
				'', //Sexo
				'' . $user['nascimento'],  //Aniversário
				'', //Telefone
				'' . $user['cep'], //CEP
				'' . $user['endereco'], //Endereco
				'' . $user['cidade'], //Cidade
				'' . $user['estado'], //Estado
				'' . $user['facebook_id'], //fbhandle
				'' . $user['nome'], //nome
				'' . $user['news_letter'], //nome
				'' . $user['cadastro'], //nome
				'' . $user['loja'], //nome
				'' . $user['cidade'], //nome
				'' . $user['estado'], //nome
			);

			array_push($docArray, $row);
		}

		$objPHPExcel->getActiveSheet()
			->getStyle('A1')
			->getNumberFormat()
			->setFormatCode(
				PHPExcel_Style_NumberFormat::FORMAT_TEXT
			);

		header('Content-Type: application/vnd.ms-excel');
		header('Cache-Control: max-age=0');
		$sheet->fromArray($docArray);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		$objPHPExcel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); // header for .xlxs file
		header('Content-Disposition: attachment;filename=users.xls'); // specify the download file name
		header('Cache-Control: max-age=0');

		exit;
	}

	/**
	 * Método que busca os auto completes
	 *
	 * @name autocompleteAction
	 */
	public function autocompleteAction()
	{
		// Busca o termo passado
		$filter = $this->_request->getParam("term", "");
		// Inicializa os dados de retorno
		$data = array();

		// Busca o auto-complete passado
		$autocomplete = $this->_request->getParam("ac");

		// Verifica se existe tabela do autocomplete
		$ac_table = $this->_request->getParam("ac_table", NULL);
		if ($ac_table !== NULL) {
			$ac_table = "U_" . $ac_table;
		}

		// Instancia o model
		$model = new $autocomplete($ac_table);

		// Verifica se existe query de autocomplete
		$ac_name = $this->_request->getParam("ac_name", "default");

		// Busca o select
		$select = $model->getQueryAutoComplete($ac_name);

		// Busca o campo da chave primaria
		$primary_field = $model->getPrimaryField();
		$description_field = $model->getDescription();

		// Verifica se é um espaço, para mostrar tudo
		if ($filter == " ") {
			$filter = "";
		}

		// Da o bind nos parametros like
		$select->where($model->getTableName() . "." . $description_field . " LIKE lower(?)", "%" . strtolower($filter) . "%");

// 		echo("<pre>");
// 		var_dump($select);
// 		die();

		// Ordena
		$select->order($description_field);

		// Limita
		$select->limit(30);

		// Busca a query do auto-complete
		$records = $model->fetchAll($select);

		// Percorre os registros
		foreach ($records as $row) {
			// Busca os valores iniciais
			$label = ($row[$description_field]);
			$value = $row[$primary_field[1]];
			$line = array('label' => $label, 'identifier' => $value);

			// Percorre as colunas para os valores adicionais
			foreach ($row as $column_name => $column_value) {
				// Só adicionar caso não for chave primaria ou descrição da tabela
				if (($column_name != $description_field) && ($column_name != $primary_field[1])) {
					$line[$column_name] = $column_value;
				}
			}

			// Monta o vetor
			$data[] = $line;
		}

		// Desabilita o layout e da o parse para json
		$this->_helper->json($data);
	}

	public function alertAction()
	{

		set_time_limit(0);
		ini_set('max_execution_time', -1);

		$dados = $this->_request->getParams();
		$push = new Cms_Push();

		$pushResult = $push->globalPush($dados['message']);

		$this->_messages->alert = implode("<br>", array(
			"Push disparado para iOS com Sucesso: {$pushResult['successiOS']}",
			"Push disparado para Android com Sucesso: {$pushResult['successAndroid']}",
			"Push não disparado para iOS com Sucesso : {$pushResult['failOS']}",
			"Push não disparado para Android com Sucesso: {$pushResult['failAndroid']}",
		));

		$this->_redirect("/admin");
	}
	public function getCsvAction(){

		$modelUser = new Admin_Model_User();
		$selectUser = $modelUser->select();
		$selectUser->from(array('u' => 'user'), [
				'nome',
				'cpf',
				'email',
				'nascimento' => "DATE_FORMAT(u.nascimento, '%d/%m/%y')",
				'cep',
				'endereco',
				'cidade',
				'estado',
				'facebook_id',
				'news_letter'=>"IF(u.news_letter=0,'Não','Sim')",
				'cadastro' => "DATE_FORMAT(u.stamp_cadastro, '%d/%m/%y')",
				'user_id'
		]);
		
		$selectUser->join(array('b' => 'brinde'), 'b.user_id = u.user_id',array('codigo'=>'b.codigo','b.path','b.validado'));
		$selectUser->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', array('loja' => 'l.nome','loja_id'));
		$selectUser->join(array('c' => 'cidade'), 'c.cidade_id = l.cidade_id', array('cidade' => 'c.cidade'));
		$selectUser->join(array('e' => 'estado'), 'c.estado_id = e.estado_id', array('estado' => 'e.uf'));
		$selectUser->setIntegrityCheck(false);
		$users =  $selectUser->query()->fetchAll();
		
		//$users = $modelUser->fetchAll($selectUser)->toArray();
		
		$theadRow = array(
				"Contato",
				"CPF/CNPJ",
				"Nome",
				"Segundo Nome",
				"Sobrenome",
				"Email Principal",
				"Sexo",
				"Aniversário",
				"Telefone Celular",
				"Endereço 1: CEP",
				"Endereço 1: Logradouro",
				"Endereço 1: Cidade",
				"Endereço 1: Estado",
				"Facebook Handle",
				"Facebook Name",
				"Newsletter",
				"Cadastro",
				"Loja",
				"Cidade",
				"Estado",
				"Codigo",
				"QR CODE",
				"Status"
		);
		
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=usuarios.csv');
		
		$out = fopen('php://output', 'w');
		fputcsv($out, $theadRow);
		
		
		foreach ($users as $key => $user) {
			$row = array(
					'' . $user['nome'], //nome
					'' . $user['cpf'], //cpf
					'' . $user['nome'], //nome
					'', //segundo nome
					'', //sobrenome
					'' . $user['email'], //email principal
					'', //Sexo
					'' . $user['nascimento'],  //Aniversário
					'', //Telefone
					'' . $user['cep'], //CEP
					'' . $user['endereco'], //Endereco
					'' . $user['cidade'], //Cidade
					'' . $user['estado'], //Estado
					'' . $user['facebook_id'], //fbhandle
					'' . $user['nome'], //nome
					'' . $user['news_letter'], //nome
					'' . $user['cadastro'], //nome
					'' . $user['loja'], //nome
					'' . $user['cidade'], //nome
					'' . $user['estado'], //nome
					'' . $user['codigo'], //nome
					'' . str_ireplace('.jpg', '.png', str_ireplace('http:/', 'http://', $user['path'])),
					'' . $user['validado']?'Utilizado':'Pendente'
					//'http://semanadoabraco.com.br/common/uploads/qrcode/'. $user['loja_id'].'/'.$user['user_id'].'-'. $user['codigo'].'.png'
			);
		
			fputcsv($out, $row);
				
		}
		fclose($out);
		
		exit;
		
	}
}

