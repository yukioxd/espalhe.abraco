<?php

/**
 * Controlador dos usuarios
 *
 * @name UsuariosController
 */
class Admin_CadastrosController extends Cms_Controller_Action {
	/**
	 * Armazena o model padrão da tela
	 *
	 * @access protected
	 * @name $_model
	 * @var Default_Model_Usuarios
	 */
	protected $_model = NULL;

	/**
	 *
	 */
	public function init() {
		// Inicializa o model da tela
		$this->_model = new Admin_Model_User();
		
		// Chama o parent
		parent::init();
	}

	public function doBeforePopulate ( $data ) {
		if (! is_null( $data ) ) {
			$params = $this->_request->getParams();
			if ( isset( $params['sendEmail']) ) {
				$this->enviarEmail(  );
			} else if ( isset( $params['sendSms']) ) {
				$this->enviarSms(  );
			} else if ( isset( $params['delete'] ) ) {
				$this->excluirCadastro();
			}
			$modelBrinde = new Admin_Model_Brinde();
			$select = $modelBrinde->select();
			$select->from(['b' => 'brinde']);
			$select->join(['l' => 'loja'], 'l.loja_id = b.loja_id');
			$select->setIntegrityCheck( FALSE );


			$select->where("user_id = '{$data['user_id']}'");
			$this->view->brindes = $modelBrinde->fetchAll( $select )->toArray();
			$this->view->celular = $data->celular;
			return parent::doBeforePopulate( $data );
		}
	}

	public function buildEmail( $src = '' ) {
		if ( strlen( $src ) < 1 )  {
			$src = 'http://entreinaweb.com/teste-rafa/111-722-245.png';
		}
		$html = '
			<html>
			    <head>
			        <meta charset="UTF-8">
			        <title></title>
			    </head>
			    <body style="width: 100%;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:16px;background-color:#9772ac;color: #fff; text-align: center;" topmargin="0"  bottommargin="0"  leftmargin="auto" rightmargin="auto">
					<table width="320px" border="0" cellpadding="0"  cellspacing="0" align="center"  style="width: 320px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0 auto">
						<tr>
							<td colspan="3" height="47">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td height="101" colspan="3">
							<img align="center" src="http://entreinaweb.com/teste-rafa/logo-tbb-v2.png" style="display: block; margin: 0 auto; padding: 0;" width="142" height="59"/>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								<img align="center" src="' . $src . '" style="display: block; margin: 0 auto; padding: 0;" height="410" width="347"/>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align: center; line-height: 1.4;" colspan="3">
								Entre os dias 15/5 e 22/05, apresente este
								<b>CÓDIGO</b> e o seu CPF na loja<br> <span style="color: #fff;">que você selecionou e ganhe
								uma <b><br>
								Loção Hidratante HugMe Bombom de Baunilha 300ml</b>.<br></span>
							</td>
						</tr>
			
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align: center; line-height: 1.4; font-size: 18px;" colspan="3"><b>#abracenaBeauty</b></td>
						</tr>
					</table>
			    </body>
			</html>
		';
		return $html;
	}


	private function SendGrid ( $name, $email, $src ) {

		require( APPLICATION_PATH . '/../library/Cms/Sendgridphp/sendgrid-php.php' );
		$subject = 'Semana do Abraço by TheBeautyBox.com.br';
		$to = new SendGrid\Email($name, $email);
		$from = new SendGrid\Email('TheBeautyBox', 'sacdabeauty@thebeautybox.com.br');
		$html = $this->buildEmail( $src );
		$content = new SendGrid\Content("text/html", $html);
		$mail = new SendGrid\Mail($from, $subject, $to, $content);
		//$sg = new \SendGrid('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');
		$sg = new \SendGrid('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');
		$response = $sg->client->mail()->send()->post($mail);
		if ( preg_match('/202 Accepted/', $response->headers() ) ){
			die(json_encode([ 'message' => 'E-mail enviado com sucesso !']));
		}
		exit;
	}

	private function getUserById () {
		$user_id = $this->_request->getParam('user_id');
		$modelUser = new Admin_Model_User();
		$userRow = $modelUser->fetchRow( "user_id = '{$user_id}'" );
		if ( is_null( $userRow ) ) {
			throw new Exception("Usuário não encontrado");
			//or redirect with err.
		} else {
			return $userRow;
		}
	}

	private function getBrindeByUserId (  ) {
		$user_id = $this->_request->getParam('user_id');
		$modelBrinde = new Admin_Model_Brinde();
		$brindeRow = $modelBrinde->fetchRow("user_id = '{$user_id}'");
		if ( is_null( $brindeRow ) ) {
			throw new Exception("Usuário não encontrado");
			//or redirect with err.			
		} else {
			return $brindeRow;
		}
	}

	public  function enviarEmail () {
		$userRow = $this->getUserById();
		//enviar
		$brinde = $this->getBrindeByUserId();
		if ( $_SERVER['HTTP_HOST'] === '54.207.76.236') {
			$image = 'http://54.207.76.236' . $brinde->path;
		} else if ( preg_match('/homologacao/', $_SERVER['REQUEST_URI'] ) ) { //caso de env localhost/local.abraco
			$image = 'http://www.semanadoabraco.com.br/homologacao'. $brinde->path;
		} else {
			$image = 'http://www.semanadoabraco.com.br' . $brinde->path;
		}

		$html = $this->buildEmail( $image );

		$mail = new Cms_Mail();
		$mail->addTo($userRow->email, $userRow->nome);
		$mail->setSubject( 'Semana do Abraço by TheBeautyBox.com.br' );
		$mail->setBodyHtml($html);
		$mail->send();
		exit;
	}

	public  function enviarSms () {
		$userRow = $this->getUserById();
		if ( ! class_exists( 'Cms_SMS' )) {
			require_once( APPLICATION_PATH . '/../library/Cms/Sms.php' );
		}
		if ( isset(  $userRow->celular ) && ( !is_null( $userRow ) ) ) {
			$brinde = $this->getBrindeByUserId();
			$sms = new Cms_SMS('sms_espalhe', '123456');
			$dados = $this->_request->getParams();
			$celular = isset( $dados['celular'] ) ? $dados['celular'] : $userRow->celular;
			$respo = $sms->envie_sms($userRow->celular, "Código: {$brinde->codigo} | CPF: {$brinde->cpf} ", "TheBeautyBox");
			$response = [ 'message' => $respo->StatusMensagem ];
			die(json_encode( $response ));
		}
	}

	public  function excluirCadastro () {
		$force = $this->_request->getParam( 'force' );
		if ( !is_null( $force ) ) {
			$modelUser = new Admin_Model_User();
			$user_id = $this->_request->getParam('user_id');
			$userRow = $modelUser->fetchRow("user_id = '{$user_id}'");

			$brinde = $this->getBrindeByUserId();
			if ( $brinde ) {
				$brinde->delete();
			}
			$userRow->delete();
			die(json_encode( [ 'message' => "Cadastro deletado  com sucesso ! " ] ));
		} else {
			$brinde = $this->getBrindeByUserId();
			if ( $brinde ) {
				die(json_encode([ 'error' => "Já existe um brinde vinculado à este CPF "]));
			} else {
				$userRow->delete();
				die(json_encode( [ 'message' => "Cadastro deletado com sucesso ! " ] ));
			}
		}
	}

	public function getXlsAction()
	{
		ini_set('memory_limit', -1);
		set_time_limit(0);
		$modelUser = new Admin_Model_User();
		$selectUser = $modelUser->select();
		$selectUser->from(array('u' => 'user'), [
			'nome',
			'cpf',
			'email',
			'nascimento' => "DATE_FORMAT(u.nascimento, '%d/%m/%y')",
			'cep',
			'endereco',
			'cidade',
			'estado',
			'facebook_id',
			'clube_beauty'		=>	"IF(u.clube_beauty=0,'Não','Sim')",
			'social_networks'	=>	"IF(u.social_networks=0,'Não','Sim')",
			'celular'			=>	"u.celular",
			'cadastro' => "DATE_FORMAT(u.stamp_cadastro, '%d/%m/%y')"
		]);

		$selectUser->join(array('b' => 'brinde'), 'b.user_id = u.user_id', [
			'gerado' => "DATE_FORMAT(b.gerado, '%d/%m/%y %H:%i:%s')"
		]);
		$selectUser->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', array('loja' => 'l.nome'));
		$selectUser->join(array('c' => 'cidade'), 'c.cidade_id = l.cidade_id', array('cidade' => 'c.cidade'));
		$selectUser->join(array('e' => 'estado'), 'c.estado_id = e.estado_id', array('estado' => 'e.uf'));
		$selectUser->setIntegrityCheck(false);
		$users =  $selectUser->query()->fetchAll();

		$objPHPExcel = new Cms_Geraexcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();

		$theadRow = array(
			"Contato",
			"CPF/CNPJ",
			"Nome",
			"Segundo Nome",
			"Sobrenome",
			"Email Principal",
			"Sexo",
			"Aniversário",
			"Telefone Celular",
			"Endereço 1: CEP",
			"Endereço 1: Logradouro",
			"Endereço 1: Cidade",
			"Endereço 1: Estado",
			"Facebook Handle",
			"Facebook Name",
			"Clube da Beauty",
			"Newsletter",
			"Cadastro",
			"Loja",
			"Data/Hora resgate",
			"Cidade",
			"Estado",
		);

		$docArray = array(
			$theadRow
		);

		foreach ($users as $key => $user) {
			$nameParts = explode( ' ', $user['nome'] );
			$first_name = $middle_name = $last_name = '';
			if ( sizeof( $nameParts ) > 4) {
				$first_name = $nameParts[0];
				$middle_name = $nameParts[1] . ' ' . $nameParts[2];
				$last_name = $nameParts[3] . ' ' . $nameParts[4];
			} else if ( sizeof( $nameParts ) > 3) {
				$first_name = $nameParts[0];
				$middle_name = $nameParts[1];
				$last_name = $nameParts[2] . ' ' . $nameParts[3];
			} else if ( sizeof( $nameParts ) > 2) {
				$first_name = $nameParts[0];
				$middle_name = $nameParts[1];
				$last_name = $nameParts[2];
			} else if ( sizeof( $nameParts ) > 1) {
				$first_name = $nameParts[0];
				$middle_name = '';
				$last_name = $nameParts[1];
			}
			$row = array(
				'' . $user['nome'], //nome
				'' . $user['cpf'], //cpf
				'' . $first_name, //nome
				'' . $middle_name, //segundo nome
				'' . $last_name, //sobrenome
				'' . $user['email'], //email principal
				'', //Sexo
				'' . $user['nascimento'],  //Aniversário
				'' . $user['celular'], //Telefone
				'' . $user['cep'], //CEP
				'' . $user['endereco'], //Endereco
				'' . $user['cidade'], //Cidade
				'' . $user['estado'], //Estado
				'', //fbhandle
				'' . $user['nome'], //fb_name
				'' . $user['clube_beauty'], //clube_beauty
				'' . $user['social_networks'], //news_letter
				'' . $user['cadastro'], //nome
				'' . $user['loja'], //nome
				'' . $user['gerado'], //nome
				'' . $user['cidade'], //nome
				'' . $user['estado'], //nome
			);
			array_push($docArray, $row);
		}

		$objPHPExcel->getActiveSheet()
			->getStyle('A1')
			->getNumberFormat()
			->setFormatCode(
				PHPExcel_Style_NumberFormat::FORMAT_TEXT
			);

		header('Content-Type: application/vnd.ms-excel');
		header('Cache-Control: max-age=0');
		$sheet->fromArray($docArray);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		$objPHPExcel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); // header for .xlxs file
		header('Content-Disposition: attachment;filename=users.xls'); // specify the download file name
		header('Cache-Control: max-age=0');

		exit;
	}
}
