<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            {$this->navigation()->breadcrumbs()->setLinkLast(FALSE)->setSeparator(' - ')->setMinDepth(-1)->render()}
        </h1>
    </div>
</div>

<div class="col-lg-6">
    <form enctype="multipart/form-data" action="{$form->getAction()}" method="post">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-buttons">
                    <button type="submit" class="btn btn-outline btn-primary">{$form->getElement('submit')->getLabel()}</button>
                    <button type="button" id="cancel" class="btn btn-outline btn-warning" data-url="{$form->getElement('cancel')->getAttrib("data-url")}">Cancelar</button>
                </div>
            </div>
            <div class="panel-body">
                {foreach from=$form->getElements() item=element}
                    {if $element->getName() != "submit" && $element->getName() != "cancel"}
                    <div class="col-xs-6">
                        <div class="form-group">
                            {$element}
                        </div>
                    </div>
                    {/if}
                {/foreach}
            </div>
        </div>
    </form>
</div>

{if sizeof( $brindes ) > 0}
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Dados do Cupon
        </div>
        <div class="panel-body">
            {foreach from=$brindes item=brinde}
                <p>Loja escolhida: <strong>{$brinde['nome']}</strong></p>
                <p>Cupom: <strong>{$brinde['codigo']}</strong></p>
                <p>Resgatou brinde? <strong>{if $brinde['validado'] > 0} Sim {else} Não {/if}</strong> </p>
            {/foreach}

            <button id="cadastro-enviar-email" class="btn btn-primary"> <i class="fa fa-envelope"></i> Enviar cupom por E-mail</button>
            <button data-toggle="modal" data-target="#smsmodal" id="" class="btn btn-warn" style="margin: 0 1em;"> <i class="fa fa-envelope"></i> Enviar cupom por SMS</button>
            <button id="cadastro-remover" style="display: none;" class="btn btn-danger"><i class="fa fa-trash-o"></i> Excluir Cadastro </button>
        </div>
    </div>
</div>


<div class="modal fade" id="smsmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Enviar por SMS</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-cel" class="control-label">Número:</label>
                        <input type="text" class="form-control" id="recipient-cel" value="{$celular}">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" id="do-send-sms" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("#do-send-sms").on('click', function ( e ) {
        $.ajax({
            dataType: 'json',
            data: {
                sendSms: true,
                celular: $("recipient-cel").val()
            },
            success: function ( data ) {
                alert( data.message );
                $('#smsmodal').modal('hide')
            }
        });
    })
</script>
{/if}