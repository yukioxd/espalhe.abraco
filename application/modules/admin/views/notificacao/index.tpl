<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-bar-chart-o fa-fw"></i> Notificações
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-5">
	                <div class="panel panel-default">
		                <div class="panel-heading">
			                <i class="fa glyphicon-plus fa-fw"></i> Nova notificação
		                </div>
		                <div class="panel-body">
			                <form action="" method="POST">
				                <div class="form-group col-xs-12">
					                <label for="push-message">Mensagem da notificação</label>
					                <input id="push-message" name="message" class="form-control" placeholder="Mensagem da notificação" required>
				                </div>
				                <div class="form-group col-xs-6">
					                <label for="push-count">Ativos nos últimos </label>
					                <input id="push-count" name="count" class="form-control" placeholder="5" value="3" required>
				                </div>
				                <div class="form-group col-xs-6">
					                <label for="chunk-size">&nbsp;</label>
					                <div>dias</div>
				                </div>
				                <div class="form-group col-xs-6 pull-right">
					                <button type="submit" class="btn btn-success pull-right">Enviar</button>
				                </div>
			                </form>
		                </div>
	                </div>
	                <div class="panel panel-default">
		                <div class="panel-heading">
			                <i class="fa fa-list fa-fw"></i> Enviados
		                </div>
	                    <div class="panel-body">
		                <div class="table-responsive">
			                <table class="table table-bordered table-hover table-striped">
				                <thead>
				                <tr>
					                <th>#</th>
					                <th>Data</th>
					                <th>Total Disp.</th>
					                <th>Mensagem</th>
					                <th>iOS Ok</th>
					                <th>Android Ok</th>
					                <th>iOS Erro</th>
					                <th>Android Erro</th>
				                </tr>
				                </thead>
				                <tbody>
				                {foreach from=$sentDatas item=sentData key=key}
					                <tr>
						                <td>{$sentData['notificacao_id']}</td>
						                <td>{$sentData['criado']|date_format:"%d/%m/%Y"}</td>
						                <td>{$sentData['target_count']}</td>
						                <td>{$sentData['notificacao']}</td>
						                <td>{$sentData['apn_success']}</td>
						                <td>{$sentData['gcm_success']}</td>
						                <td>{$sentData['apn_fail']}</td>
						                <td>{$sentData['gcm_fail']}</td>
					                </tr>
				                {/foreach}
				                </tbody>
			                </table>
		                </div>
	                </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div id="morris-area-chart">

                    </div>
                    <div id="morris-bar-chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	document.src = {json_encode($stasticsDatas)};
	document.barSrc = {json_encode($sentDatas)};
</script>
<script src="{$basePath}/common/application/js/admin/notificacao/form.js"></script>