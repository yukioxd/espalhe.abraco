<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Semana do Abraço</title>
    <!-- Core CSS - Include with every page -->
    <link href="{$basePath}/common/admin/css/bootstrap.css" rel="stylesheet">
    <link href="{$basePath}/common/admin/css/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="{$basePath}/common/admin/css/sb-admin.css" rel="stylesheet">
    <style>
        html { height: 100%; }
        html, body { min-height: 100%; }
        body {
            margin: 0;
            padding: 0;
            position: relative;
            background: url('{$imgPath}/pattern/green.png') repeat;
            font-family: 'OpenSans', 'tahoma';
            font-weight: 300;
            overflow: hidden;
            color: #fff;
        }
        body:before{
            background: url('{$imgPath}/pattern/purple-v8.png') repeat;
            content: '';
            width: 100%;
            height: 63%;
            bottom: 0; left: 0;
            position: absolute;
        }
        .login-panel {
            margin-top: 25%;
            padding: 30px;
            border-radius: 25px;
        }
    </style>
</head>
<body class="login-ui">
    <div class="container">
        <div class="row">
            <div class="login-container col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                <div class="login-panel panel panel-default" >
                    <div class="panel-heading">
                        <img src="{$basePath}/common/default/images/site_dist/logozao.png" style="display: block; margin: 0 auto;">
                    </div>
                    <h2 style="text-align: center;">Sistema Administrador</h2>
                    <hr>
                    <div class="panel-body">
                        <form action="{$basePath}/admin/usuarios/login" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="login" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="senha" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" id="do-login" value="Login" style="opacity: 0.9;"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts - Include with every page -->
    <script src="{$basePath}/common/admin/newjs/jquery-1.10.2.js"></script>
    <script src="{$basePath}/common/admin/newjs/bootstrap.min.js"></script>
    <script src="{$basePath}/common/admin/newjs/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="{$basePath}/common/admin/newjs/sb-admin.js"></script>

</body>

</html>
