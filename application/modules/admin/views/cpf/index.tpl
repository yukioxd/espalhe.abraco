<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            {$this->navigation()->breadcrumbs()->setLinkLast(FALSE)->setSeparator(' - ')->setMinDepth(-1)->render()}
        </h1>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Preencha o Campo CPF
	</div>
	<div class="panel-body">
		<form action="" method="POST">

			<div class="form-group">
				<div class="input-group">
					<input type="text" name="cpf" id="cpf" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary btn-outline" type="button">Consultar</button>
					</span>
				</div>
			</div>
		</form>
	</div>
</div>

{if is_object($painel) && isset($painel->dados) && strlen($painel->dados['error']) > 0}
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {$painel->dados['error']}
</div>
{/if}