<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
        	{$this->navigation()->breadcrumbs()->setLinkLast(FALSE)->setSeparator(' - ')->setMinDepth(-1)->render()}
        	<button class="btn btn-primary btn-outline pull-right" style="display: none;">
        		<i class="fa fa-download"></i> Exportar XLS
    		</button>
    	</h1>
    </div>
</div>
<div class="list-cupons lista">
	{foreach from=$estados item=estadoList}
		<div class="col-xs-12 col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading"> {$estadoList[0]['estado']} </div>
				<div class="panel-body">
					<table class="tabler-w-graphs table table-striped table-bordered table-hover">
						{foreach from=$estadoList item=loja}
							<tr>
								<td style="width: 20em;"> {$loja['nome']} </td>
								<td style="width: 80em;">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="min-width: 5em;width: {$loja['porcento']};">
											{$loja['numero']} de {$loja['estoque']} ({$loja['porcento']})
										</div>
									</div>
								</td>
							</tr>
						{/foreach}

					</table>
				</div>
			</div>
		</div>
	{/foreach}