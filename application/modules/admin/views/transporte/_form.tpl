<div class="col-lg-6">
    <form action="{$form->getAction()}" method="post">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-buttons">
                    <button type="submit" class="btn btn-outline btn-primary">{$form->getElement('submit')->getLabel()}</button>
                    <button type="button" id="cancel" class="btn btn-outline btn-warning" data-url="{$form->getElement('cancel')->getAttrib("data-url")}">Cancelar</button>
                </div>
            </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#geral" data-toggle="tab">Geral</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="geral" class="tab-pane fade active in">
                        <div class="form-group">
                            {$form->getElement('checkin')}
                        </div>
                        {foreach from=$form->getElements() item=element}
                            {if $element->getName() != "submit" && $element->getName() != "cancel" && $element->getName() != "checkin"}
                                <div class="form-group">
                                    {if $element->getName() == "categoria"}
                                        <div class="form-group">
                                            <div class="zend-form-element" id="element-categoria">
                                                <label for="categoria" class="">
                                                    Categoria <p class="help-block"></p>
                                                </label>
                                                <select class="form-control" name="categoria" id="categoria" class="int form-control">
                                                    <option selected disabled>Transporte categoria</option>
                                                    {foreach from=$categorias key=id item=categoria}
                                                        <option value='{$id}'>{$categoria}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                    {else}
                                        {$element}
                                    {/if}
                                </div>                            
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>