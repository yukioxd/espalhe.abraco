<style>
    #map{
        width: 500px;
        height: 500px;
        border: 1px solid #dadada;
        float: left;
        position: relative;
        
    }
    #estabelecimento-point{
        border-radius: 50%;
        border: 2px solid #dabdab;
        width: 20px;
        height: 20px;
    }
</style>
<div class="col-lg-6">
    <form enctype="multipart/form-data" action="{$form->getAction()}" method="post">
            
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-buttons">
                    <button type="submit" class="btn btn-outline btn-primary">{$form->getElement('submit')->getLabel()}</button>
                    <button type="button" id="cancel" class="btn btn-outline btn-warning" data-url="{$form->getElement('cancel')->getAttrib("data-url")}">Cancelar</button>
                </div>
            </div>
                
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#geral" data-toggle="tab">Geral</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="geral" class="tab-pane fade active in">
                        <div class="form-group">
                            <div class="zend-form-element" id="element-y">
                                <label for="positionX" class="optional">PosiçãoX <p class="help-block"></p></label>
                                <input type="number" name="x" id="positionX" value="{$dadosEstabelecimento->x}" field-type="text" class="varchar form-control">
                            </div>
                            <div class="zend-form-element" id="element-x">
                                <label for="positionY" class="optional">PosiçãoY <p class="help-block"></p></label>
                                <input type="number" name="y" id="positionY" value="{$dadosEstabelecimento->y}" field-type="text" class="varchar form-control">
                            </div>
                            {$form->getElement('checkin')}
                        </div>

                        {foreach from=$form->getElements() item=element}
                            {if $element->getName() != "submit" && $element->getName() != "cancel" && $element->getName() != "checkin"}
                                <div class="form-group">
                                {$element}
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="col-lg-5">
    <div id="map">
        
    </div>
</div>
