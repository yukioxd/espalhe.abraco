<div class="buttons" id="buttons-bar">
    <a href="{$this->CreateUrl("lista-atletas", NULL, NULL, [])}/{$primary}/" class="btn-edit">Ver Atletas</a>
    <a href="{$this->CreateUrl("relatorio", NULL, NULL, [])}/{$primary}/" class="btn-edit">Gerar PDF</a>
    <a href="{$this->CreateUrl("sumula", NULL, NULL, [])}/{$primary}/" class="btn-edit">Gerar Súmula</a>
</div>

<div class="table-list">
    <table class="list">
        <thead>
            <tr>
                <th></th>
                <th>#</th>
                {foreach $paginator->getAdapter()->getItems(0,1)->getTable()->getCampo() as $column=>$value}
                    {if $paginator->getAdapter()->getItems(0,1)->getTable()->getVisibility($column, 'list')}
                        <th>
                            {$value}
                        </th>
                    {/if}
                {/foreach}
            </tr>
            <tr class="filter" data-search="{$basePath}/admin/{$currentController}/search/{key($requireParam)}/{current($requireParam)}">
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                {foreach $paginator->getAdapter()->getItems(0,1)->getTable()->getCampo() as $column=>$value}
                    {if $paginator->getAdapter()->getItems(0,1)->getTable()->getVisibility($column, 'list')}
                        <th>
                            {$form->getElement($column)}
                        </th>
                    {/if}
                {/foreach}
            </tr>
        </thead>
        <tbody>
            {foreach from=$paginator item=row}
                <tr>
                    <td><input type="radio" name="{$primary}" value="{$row[$primary]}" /></td>
                    <td>{$row[$primary]}</td>
                    {foreach $paginator->getAdapter()->getItems(0,1)->getTable()->getCampo() as $column=>$value}
                        {if $paginator->getAdapter()->getItems(0,1)->getTable()->getVisibility($column, 'list')}
                            <td>
                                {$this->GetColumnValue($row, $column)}
                            </td>
                        {/if}
                    {/foreach}
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>

<div class="footer-bar">
    {$this->paginationControl($paginator, NULL, 'paginator.tpl')}
</div>