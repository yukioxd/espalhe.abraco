<div class="zend_form">
	<form enctype="multipart/form-data" action="{$form->getAction()}" method="post">
		<div class="form-buttons">
			<input type="submit" name="submit" id="submit" value="{$form->getElement('submit')->getLabel()}">
			<button name="cancel" id="cancel" type="button" data-url="{$form->getElement('cancel')->getAttrib("data-url")}">Cancelar</button>
		</div>
		<div class="form-tab">
			<ul>
				<li><a href="#tab-geral">Geral</a></li>
			</ul>
		
			<div id="tab-geral">
				{foreach from=$form->getElements() item=element}
					{if $element->getName() != "submit" && $element->getName() != "cancel"}
						{$element}
					{/if}
				{/foreach}
			</div>
		</div>
	</form>
</div>