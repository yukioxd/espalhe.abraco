<div class="col-xs-12 col-lg-4">
    <div class="panel panel-default h3">
        <div class="panel-heading">
            <i class="fa fa-ticket"></i> Cupons gerados
        </div>
        <div class="panel-body">
            {$dados.gerados}
        </div>
    </div>
</div>
<div class="col-xs-12 col-lg-4">
    <div class="panel panel-default h3">
        <div class="panel-heading">
            <i class="fa fa-trophy"></i> Brindes resgatados
        </div>
        <div class="panel-body">
            {$dados.resgatados}
        </div>
    </div>
</div>
<div class="col-xs-12 col-lg-4">
    <div class="panel panel-default h3">
        <div class="panel-heading">
            <i class="fa fa-facebook"></i> Compartilhamentos no Facebook
        </div>
        <div class="panel-body">
            {$dados.compartilhamentos}
        </div>
    </div>
</div>