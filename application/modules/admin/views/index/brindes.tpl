<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
        	{$this->navigation()->breadcrumbs()->setLinkLast(FALSE)->setSeparator(' - ')->setMinDepth(-1)->render()}
        	<button class="btn btn-primary btn-outline pull-right">
        		<i class="fa fa-download"></i> Exportar XLS
    		</button>
    	</h1>
    </div>
</div>
<div class="list-cupons lista">
	<div class="col-xs-12 col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading"> São Paulo </div>
			<div class="panel-body">
				<table class="tabler-w-graphs table table-striped table-bordered table-hover">
					<tr>
						<td style="width: 20em;"> Shopping Morumbi </td>
						<td style="width: 80em;">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
									154 de 220 (70%)
								</div>
							</div>
						</td>
					</tr>

					<tr>
						<td style="width: 20em;"> Shopping Villa Lobos </td>
						<td style="width: 80em;">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
									154 de 220 (70%)
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading"> São Paulo </div>
			<div class="panel-body">
				<table class="tabler-w-graphs table table-striped table-bordered table-hover">
					<tr>
						<td style="width: 20em;"> Shopping Morumbi </td>
						<td style="width: 80em;">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
									154 de 220 (70%)
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td style="width: 20em;"> Shopping Villa Lobos </td>
						<td style="width: 80em;">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
									154 de 220 (70%)
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>