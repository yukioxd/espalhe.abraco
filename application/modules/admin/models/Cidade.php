<?php

/**
 * Modelo da tabela de cidade
 *
 * @name Admin_Model_Cidade
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_Cidade extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "cidade";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "cidade_id";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		//
		$this->setCampo("cidade", "Nome da cidade");
		$this->setCampo("estado_id", "Nome do Estado");

		$this->setAutocomplete("estado_id", "Admin_Model_Estado");
		$this->setDescription("cidade");

		parent::init();
	}
}

