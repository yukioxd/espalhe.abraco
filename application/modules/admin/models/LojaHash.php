<?php

/**
 * Modelo da tabela de loja
 *
 * @name Admin_Model_LojaHash
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_LojaHash extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "loja_hash";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "loja_hash_id";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		//
		$this->setCampo("hash", "Hash da sessão");
		$this->setAutocomplete("loja_id", "Admin_Model_Loja");

		$this->setDescription("hash");

		parent::init();
	}
}

