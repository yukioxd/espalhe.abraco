<?php

/**
 * Modelo da tabela de estado
 *
 * @name Admin_Model_Estado
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_Estado extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "estado";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "estado_id";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		//
		$this->setCampo("estado", "Nome da estado");
		$this->setCampo("estado_id", "Nome do Estado");
		
		$this->setDescription("estado");

		parent::init();
	}
}

