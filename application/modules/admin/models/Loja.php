<?php

/**
 * Modelo da tabela de loja
 *
 * @name Admin_Model_Loja
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_Loja extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "loja";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "loja_id";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		//
		$this->setCampo("nome", "Nome da loja");
		$this->setCampo("email", "E-mail da loja");
		$this->setCampo("cidade_id", "Cidade da loja");
		$this->setCampo("telefone", "Telefone da loja");
		$this->setCampo("endereco", "Endereço da loja");
		//$this->setCampo("latitude", "Latitude da loja");
		//$this->setCampo("longitude", "Longitude da loja");
		$this->setCampo("horarios_atendimento", "Horários de Atendimento da Loja");
		$this->setCampo("estoque", "Estoque da Loja");
		$this->setCampo("estoque_brinde", "Estoque Brinde da Loja");
		$this->setCampo("cep", "CEP da Loja");
		$this->setCampo("piso", "O Piso da Loja");
		$this->setCampo("gmapIframe", "URL para Google Maps Iframe");
		$this->setCampo("senha", "Senha do lojista");

		$this->setAutocomplete("cidade_id", "Admin_Model_Cidade");
		$this->setVisibility('horarios_atendimento', TRUE, TRUE, FALSE, TRUE);
		$this->setVisibility('senha', TRUE, TRUE, FALSE, FALSE);
		$this->setVisibility('piso', TRUE, TRUE, FALSE, FALSE);
		$this->setVisibility('cep', TRUE, TRUE, FALSE, FALSE);
		$this->setDescription("nome");

		parent::init();
	}
}

