<?php

/**
 * Modelo da tabela de brinde
 *
 * @name Admin_Model_Brinde
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_Brinde extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "brinde";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "brinde_id";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		//
		$this->setCampo("user_id", "User que levou o brinde");
		$this->setCampo("loja_id", "Loja que conecedeu o brinde");
		$this->setCampo("gerado", "Data e a hora em que foi gerado");
		$this->setCampo("path", "Local onde ficou salvo o png");
		$this->setCampo("codigo", "Código que foi gerado");


		$this->setAutocomplete("user_id", "Admin_Model_User");
		$this->setAutocomplete("loja_id", "Admin_Model_Loja");

		$this->setDescription("gerado");
		parent::init();
	}
}

