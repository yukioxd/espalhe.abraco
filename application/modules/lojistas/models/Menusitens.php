<?php

/**
 * Modelo da tabela de menus_itens
 *
 * @name Admin_Model_Menusitens
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_Menusitens extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "menu_itens";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "iditem";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		//
		$this->setCampo("descricao", "Descrição", "Nome que aparecerá no menu");
		$this->setCampo("idcategoria", "Categoria", "Categoria onde o menu aparecerá");
		$this->setCampo("modulo", "Modulo", "Nome do modulo Zend");
		$this->setCampo("controlador", "Controlador", "Nome do controlador Zend");
		$this->setCampo("acao", "Ação", "Nome da ação Zend");
		$this->setCampo("parametros", "Parametros", "Parametros para serem concatenados à url ex: /parametro1/valor1");
		$this->setCampo("idperfil", "Perfil", "Perfil hierárquico de acesso ao item de menu");
		
		//
		$this->setAutocomplete("idcategoria", "Admin_Model_Menuscategorias");
		$this->setAutocomplete(array("idperfil", "tipoUsuario_id"), "Admin_Model_Perfis");
		
		// Seta a visibilidade do campo
		$this->setVisibility("parametros", TRUE, TRUE, FALSE, FALSE);
		$this->setVisibility("idcategoria", TRUE, TRUE, FALSE, FALSE);
		
		//
		$this->setDescription("descricao");
		
		//
		parent::init();
	}
}

