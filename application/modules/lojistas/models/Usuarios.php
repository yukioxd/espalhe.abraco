<?php

/**
 * Modelo da tabela de usuarios
 *
 * @name Admin_Model_Usuarios
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_Usuarios extends Cms_Db_Table {
	/**
	 * Armazena o nome da tabela
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "usuario";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "usuario_id";
	
	/**
	 * Inicializa o model
	 * 
	 * @name init
	 */
	public function init() {
		// Seta os campos 
                $this->setCampo("nome", "Nome", "Nome do usuário");
		$this->setCampo("login", "Login", "Login do usuário");
		$this->setCampo("email", "Email", "Email do usuário, tambem utilizado para recuperação de senha");
		$this->setCampo("senha", "Senha", "Senha do usuário no sistema");
		$this->setCampo("tipoUsuario_id", "Perfil", "Perfil hierárquico do usuário");
		
		// Seta os modificadores do form
		$this->setModifier("senha", array('type'=>"password"));
		
		// Seta os auto complete
                $this->setAutocomplete("tipoUsuario_id", "Admin_Model_Perfis");
		
		// Seta a visibilidade dos campos
		$this->setVisibility("senha", TRUE, TRUE, FALSE, FALSE);
		
		// Seta o campo descritivo da tabela
		$this->setDescription("nome");
		
		// Continua carregar o model
		parent::init();
	}
}