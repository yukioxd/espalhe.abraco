<?php

/**
 * Modelo da tabela de users
 *
 * @name Admin_Model_User
 * @see Zend_Db_Table_Abstract
 */
class Admin_Model_User extends Cms_Db_Table
{
	/**
	 * Armazena o nome da user
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "user";

	/**
	 * Armazena o nome do campo da tabela primaria
	 *
	 * @access protected
	 * @name $_primary
	 * @var string
	 */
	protected $_primary = "user_id";

	/**
	 * Inicializa o model
	 *
	 * @name init
	 */
	public function init()
	{
		// Seta os campos 
		$this->setCampo("nome", "Nome", "Nome do usuário");
		$this->setCampo("cpf", "Login", "Login do usuário");
		$this->setCampo("email", "Email", "Email do usuário, tambem utilizado para recuperação de senha");
		$this->setCampo("nascimento", "Senha", "Senha do usuário no sistema");
		$this->setCampo("cep", "Perfil", "Perfil hierárquico do usuário");
		$this->setCampo("endereco", "Perfil", "Perfil hierárquico do usuário");
		$this->setCampo("cidade_id", "Perfil", "Perfil hierárquico do usuário");

		// Seta os auto complete
		$this->setAutocomplete("cidade_id", "Admin_Model_Cidade");
		// Seta a visibilidade dos campos
		$this->setVisibility("senha", TRUE, TRUE, FALSE, FALSE);

		// Seta o campo descritivo da tabela
		$this->setDescription("nome");
		// Continua carregar o model
		parent::init();
	}

	private function validEmail($email)
	{
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		if($email) {
			return $email;
		} else {
			throw new Exception("O E-mail {$email} é inválido");
		}
	}

	private function validCpf($cpf)
	{
		if (empty($cpf)) {
			throw new Exception("CPF {$cpf} é inválido");
		}
		$cpf = preg_replace('/[^0-9]/', '', $cpf);
		$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

		if (strlen($cpf) != 11) {
			throw new Exception("CPF {$cpf} é inválido");
		} else if ($cpf == '00000000000' ||
			$cpf == '11111111111' ||
			$cpf == '22222222222' ||
			$cpf == '33333333333' ||
			$cpf == '44444444444' ||
			$cpf == '55555555555' ||
			$cpf == '66666666666' ||
			$cpf == '77777777777' ||
			$cpf == '88888888888' ||
			$cpf == '99999999999'
		) {
			throw new Exception("CPF {$cpf} é inválido");
		} else {
			for ($t = 9; $t < 11; $t++) {
				for ($d = 0, $c = 0; $c < $t; $c++) {
					$d += $cpf{$c} * (($t + 1) - $c);
				}
				$d = ((10 * $d) % 11) % 10;
				if ($cpf{$c} != $d) {
					throw new Exception("CPF {$cpf} é inválido");
				}
			}
			return $cpf;
		}
	}

	private function validCep($cep)
	{
		if(preg_match('/^\d{5}\-?\d{3}$/', $cep)) {
			return $cep;
		} else {
			throw new Exception("CEP {$cep} é inválido");
		}
	}

	private function validNascimento($nacimento)
	{
		if( preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $nacimento) ) {
			return $nacimento;
		}  else if( preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $nacimento) ) {
			$newDate = implode("-", array_reverse(explode('/', $nacimento)));
			$today = date('Y-m-d');
			if( strtotime($newDate) > strtotime($today) ) {
				throw new Exception("Data de Nacimento {$nacimento} é maior que a data atual.");
			}
			return $newDate;
		} else {
			throw new Exception("Data de Nacimento {$nacimento} é inválido");
		}
	}

	public function createUser($userData = [])
	{
		$userRow = [
			'nome'  => $userData['nome'],
			'cpf' => $this->validCpf($userData['cpf']),
			'email' => $this->validEmail($userData['email']),
			'nascimento' => $this->validNascimento($userData['nascimento']),
			'cep' => $this->validCep($userData['cep']),
			'endereco' => $userData['endereco'],
			'cidade' => $userData['user_cidade'],
			'estado' => $userData['user_estado'],
			'facebook_id' => $userData['facebook_id'],
			'news_letter' => $userData['news_letter']
		];

		try {
			$user_id = $this->insert($userRow);
			$userRow['user_id'] = $user_id;
		}catch (Exception $er) {
			throw new Exception($er);
		}
		return $userRow;
	}
}