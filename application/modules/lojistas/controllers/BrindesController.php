<?php

/**
 * Controlador dos usuarios
 *
 * @name BrindesController
 */
class Lojistas_BrindesController extends Cms_Controller_Action {
	/**
	 *
	 */

	public $_messages;

	public function init()
	{
		$this->_messages = new Zend_Session_Namespace("messages");
	}


	public function validarAction()
	{
		$dados = $this->_request->getParams();
		$modelBrinde = new Admin_Model_Brinde();
		$modelBrinde->update([
			'validado' => 1
		], "loja_id = {$dados['loja_id']} AND cpf = '{$dados['cpf']}'");
		$select = $modelBrinde->select();
		$select->from(['b' => 'brinde'], [
			'n' => "COUNT(brinde_id)"
		]);
		$select->where( "validado > 0 AND loja_id = {$dados['loja_id']}" );
		$row = $modelBrinde->fetchRow( $select );
		$array = $row->toArray();
		die(json_encode( $array ));
	}
}
