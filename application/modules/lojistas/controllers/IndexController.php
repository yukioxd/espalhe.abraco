<?php

/**
 *
 */
class Lojistas_IndexController extends Cms_Controller_Action
{
	/**
	 *
	 */

	public $_messages;

	public function init()
	{
		$this->_messages = new Zend_Session_Namespace("messages");
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$modelBrinde = new Admin_Model_Brinde();
		$modelUser = new Admin_Model_User();
		
		
		$dados = $this->getAllParams();
		if($dados['purgardados']=='sim'){
			//$modelBrinde->delete('brinde_id > 0');
			//$modelUser->delete('user_id > 0');
		}

		$modelBrinde = new Admin_Model_Brinde();
		$selectBrinde = $modelBrinde->select();
		$selectBrinde->from(array('b' => 'brinde'));
		$selectBrinde->join(array('u' => 'user'), 'u.user_id = b.user_id', array('user' => 'u.nome'));
		$selectBrinde->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', array('loja' => 'l.nome'));
        $selectBrinde->columns(array(
            'u.cpf',
            'u.email',
            'u.nascimento',
            'u.cep',
            'u.endereco',
            'u.cidade',
            'u.estado',
            'u.facebook_id',
            'u.news_letter'
        		
        ));
		$selectBrinde->setIntegrityCheck(FALSE);

		$paginator = Zend_Paginator::factory($selectBrinde);
		$paginator->setItemCountPerPage(50);
		$current_page = $paginator->getCurrentPageNumber();

		$total_paginas = (int)($paginator->getTotalItemCount() / $paginator->getItemCountPerPage());
		$total_paginas++;
		$this->view->brindes = $paginator;
		$this->view->next_page = ($current_page < $total_paginas) ? $current_page + 1 : $total_paginas;
		$this->view->previous_page = ($current_page > 1) ? $current_page - 1 : 1;
		$this->view->query_string = $_SERVER['QUERY_STRING'];

		if( $this->_request->getParam('page')){
			$paginator->setCurrentPageNumber($this->_request->getParam('page'));
		}else{
			$paginator->setCurrentPageNumber(1);
		}

		unset($this->_messages->alert);
	}

	public function preDispatch()
	{
		if ( $this->_request->isPost() ) {
			$request = new Zend_Controller_Request_Http();
			$params = $this->_request->getParams();
			$hash = $request->getHeader('X-Authorization');
			if ( $hash ) {
				$result = $this->loadDatas( $hash, $params );
				die(json_encode( $result ));
			}
		}
		parent::preDispatch();
	}

	private function loadDatas ( $hash, $params ) {
		if ( $this->_request->isPost() ) {
			if ( isset( $params['validate'] ) ) {
				$modelBrinde = new Admin_Model_Brinde();
				$cpf = $params['cpf'];
				$cupom = $params['cupom'];
				$whereCPF = "REPLACE(REPLACE(cpf, '-', ''), '.', '') = '{$cpf}' OR cpf = '{$cpf}'";
				$whereCupom = "codigo = '{$cupom}' OR REPLACE(codigo, '-', '') = '{$cupom}'";
				$where = "({$whereCPF}) AND ({$whereCupom})";
				$select = $modelBrinde->select();
				$select->where( $where );
				$exist = $modelBrinde->fetchRow( $select );
				if ( is_null( $exist ) ) {
					header("HTTP/1.1 404 Not Found");
					//header("HTTP/1.1 404 Bad Request");
					echo json_encode( [ 'msg' => 'Nenhum cupom foi encontrado para este código/CPF' ] );
					exit;
				} else {
					$modelLojaHash = new Admin_Model_LojaHash();
					$select = $modelLojaHash->select();
					$select->from(['l' => 'loja'], ['nome', 'loja_id']);
					$select->join(['lh' => 'loja_hash'], 'lh.loja_id = l.loja_id', []);
					$select->where("hash = '{$hash}'");
					$select->setIntegrityCheck( FALSE );
					$lojaRow = $modelLojaHash->fetchRow( $select );
					if ( $lojaRow->loja_id !== $exist->loja_id ) {
						header("HTTP/1.1 400 Bad Request");
						echo json_encode( [ 'msg' => "Cupom gerado para a outra loja: {$lojaRow->nome}" ] );
						exit;
					}

					if ( $exist->validado > 0 ) {
						header("HTTP/1.1 400 Bad Request");
						echo json_encode( [ 'msg' => 'Cupom já validado' ] );
						exit;
					} else {
						$modelBrinde->update( ['validado' => 1], $where );
						echo json_encode( [ 'msg' => 'Cupom validado com sucesso!' ] );
						exit;
					}
				}
			}
			$selectColumns['estoque'] = 'l.estoque';
			if ( $params['loadData'] ) {
				$selectColumns['id'] = 'l.loja_id';
				$selectColumns['name']	= 'l.nome';
			}

			$modelLoja = new Admin_Model_Loja();
			$select = $modelLoja->select();
			$select->from([ 'l' => 'loja' ], [
				'utilizados'			=> "l.estoque - ( SELECT COUNT(brinde_id) FROM brinde b WHERE validado > 0 AND b.loja_id = l.loja_id )"
			]);
			$select->join([ 'lh' => 'loja_hash'], 'l.loja_id = lh.loja_id', []);
			$select->columns( $selectColumns );
			$select->where("lh.hash = '{$hash}'");
			$select->setIntegrityCheck( FALSE );
			$result = $modelLoja->fetchRow( $select );
			if ( is_null( $result ) ) {
				header('HTTP/1.0 401 Forbidden');
				json_encode([ 'message' => 'Hash inválido' ]);
				exit;
			} else {
				die(json_encode( $result->toArray() ));
			}
		}
	}

	public function getXlsAction()
	{
		$modelUser = new Admin_Model_User();
        $selectUser = $modelUser->select();
        $selectUser->from(array('u' => 'user'), [
        		'nome',
        		'cpf',
        		'email',
        		'nascimento' => "DATE_FORMAT(u.nascimento, '%d/%m/%y')",
        		'cep',
        		'endereco',
        		'cidade',
        		'estado',
        		'facebook_id',
        		'news_letter'=>"IF(u.news_letter=0,'Não','Sim')",
        		'cadastro' => "DATE_FORMAT(u.stamp_cadastro, '%d/%m/%y')"
        ]);
        
        $selectUser->join(array('b' => 'brinde'), 'b.user_id = u.user_id');
        $selectUser->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', array('loja' => 'l.nome'));
        $selectUser->join(array('c' => 'cidade'), 'c.cidade_id = l.cidade_id', array('cidade' => 'c.cidade'));
        $selectUser->join(array('e' => 'estado'), 'c.estado_id = e.estado_id', array('estado' => 'e.uf'));
        $selectUser->setIntegrityCheck(false); 
       	$users =  $selectUser->query()->fetchAll();

		//$users = $modelUser->fetchAll($selectUser)->toArray();

		$objPHPExcel = new Cms_Geraexcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();

		$theadRow = array(
			"Contato",
			"CPF/CNPJ",
			"Nome",
			"Segundo Nome",
			"Sobrenome",
			"Email Principal",
			"Sexo",
			"Aniversário",
			"Telefone Celular",
			"Endereço 1: CEP",
			"Endereço 1: Logradouro",
			"Endereço 1: Cidade",
			"Endereço 1: Estado",
			"Facebook Handle",
			"Facebook Name",
			"Newsletter",
			"Cadastro",
			"Loja",
			"Cidade",
			"Estado",
		);

		$docArray = array(
			$theadRow
		);

		foreach ($users as $key => $user) {
			$row = array(
				'' . $user['nome'], //nome
				'' . $user['cpf'], //cpf
				'' . $user['nome'], //nome
				'', //segundo nome
				'', //sobrenome
				'' . $user['email'], //email principal
				'', //Sexo
				'' . $user['nascimento'],  //Aniversário
				'', //Telefone
				'' . $user['cep'], //CEP
				'' . $user['endereco'], //Endereco
				'' . $user['cidade'], //Cidade
				'' . $user['estado'], //Estado
				'' . $user['facebook_id'], //fbhandle
				'' . $user['nome'], //nome
				'' . $user['news_letter'], //nome
				'' . $user['cadastro'], //nome
				'' . $user['loja'], //nome
				'' . $user['cidade'], //nome
				'' . $user['estado'], //nome
			);

			array_push($docArray, $row);
		}

		$objPHPExcel->getActiveSheet()
			->getStyle('A1')
			->getNumberFormat()
			->setFormatCode(
				PHPExcel_Style_NumberFormat::FORMAT_TEXT
			);

		header('Content-Type: application/vnd.ms-excel');
		header('Cache-Control: max-age=0');
		$sheet->fromArray($docArray);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		$objPHPExcel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); // header for .xlxs file
		header('Content-Disposition: attachment;filename=users.xls'); // specify the download file name
		header('Cache-Control: max-age=0');

		exit;
	}

	/**
	 * Método que busca os auto completes
	 *
	 * @name autocompleteAction
	 */
	public function autocompleteAction()
	{
		// Busca o termo passado
		$filter = $this->_request->getParam("term", "");
		// Inicializa os dados de retorno
		$data = array();

		// Busca o auto-complete passado
		$autocomplete = $this->_request->getParam("ac");

		// Verifica se existe tabela do autocomplete
		$ac_table = $this->_request->getParam("ac_table", NULL);
		if ($ac_table !== NULL) {
			$ac_table = "U_" . $ac_table;
		}

		// Instancia o model
		$model = new $autocomplete($ac_table);

		// Verifica se existe query de autocomplete
		$ac_name = $this->_request->getParam("ac_name", "default");

		// Busca o select
		$select = $model->getQueryAutoComplete($ac_name);

		// Busca o campo da chave primaria
		$primary_field = $model->getPrimaryField();
		$description_field = $model->getDescription();

		// Verifica se é um espaço, para mostrar tudo
		if ($filter == " ") {
			$filter = "";
		}

		// Da o bind nos parametros like
		$select->where($model->getTableName() . "." . $description_field . " LIKE lower(?)", "%" . strtolower($filter) . "%");

// 		echo("<pre>");
// 		var_dump($select);
// 		die();

		// Ordena
		$select->order($description_field);

		// Limita
		$select->limit(30);

		// Busca a query do auto-complete
		$records = $model->fetchAll($select);

		// Percorre os registros
		foreach ($records as $row) {
			// Busca os valores iniciais
			$label = ($row[$description_field]);
			$value = $row[$primary_field[1]];
			$line = array('label' => $label, 'identifier' => $value);

			// Percorre as colunas para os valores adicionais
			foreach ($row as $column_name => $column_value) {
				// Só adicionar caso não for chave primaria ou descrição da tabela
				if (($column_name != $description_field) && ($column_name != $primary_field[1])) {
					$line[$column_name] = $column_value;
				}
			}

			// Monta o vetor
			$data[] = $line;
		}

		// Desabilita o layout e da o parse para json
		$this->_helper->json($data);
	}

	public function alertAction()
	{

		set_time_limit(0);
		ini_set('max_execution_time', -1);

		$dados = $this->_request->getParams();
		$push = new Cms_Push();

		$pushResult = $push->globalPush($dados['message']);

		$this->_messages->alert = implode("<br>", array(
			"Push disparado para iOS com Sucesso: {$pushResult['successiOS']}",
			"Push disparado para Android com Sucesso: {$pushResult['successAndroid']}",
			"Push não disparado para iOS com Sucesso : {$pushResult['failOS']}",
			"Push não disparado para Android com Sucesso: {$pushResult['failAndroid']}",
		));

		$this->_redirect("/admin");
	}
	public function getCsvAction(){

		$modelUser = new Admin_Model_User();
		$selectUser = $modelUser->select();
		$selectUser->from(array('u' => 'user'), [
				'nome',
				'cpf',
				'email',
				'nascimento' => "DATE_FORMAT(u.nascimento, '%d/%m/%y')",
				'cep',
				'endereco',
				'cidade',
				'estado',
				'facebook_id',
				'news_letter'=>"IF(u.news_letter=0,'Não','Sim')",
				'cadastro' => "DATE_FORMAT(u.stamp_cadastro, '%d/%m/%y')",
				'user_id'
		]);
		
		$selectUser->join(array('b' => 'brinde'), 'b.user_id = u.user_id',array('codigo'=>'b.codigo','b.path','b.validado'));
		$selectUser->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', array('loja' => 'l.nome','loja_id'));
		$selectUser->join(array('c' => 'cidade'), 'c.cidade_id = l.cidade_id', array('cidade' => 'c.cidade'));
		$selectUser->join(array('e' => 'estado'), 'c.estado_id = e.estado_id', array('estado' => 'e.uf'));
		$selectUser->setIntegrityCheck(false);
		$users =  $selectUser->query()->fetchAll();
		
		//$users = $modelUser->fetchAll($selectUser)->toArray();
		
		$theadRow = array(
				"Contato",
				"CPF/CNPJ",
				"Nome",
				"Segundo Nome",
				"Sobrenome",
				"Email Principal",
				"Sexo",
				"Aniversário",
				"Telefone Celular",
				"Endereço 1: CEP",
				"Endereço 1: Logradouro",
				"Endereço 1: Cidade",
				"Endereço 1: Estado",
				"Facebook Handle",
				"Facebook Name",
				"Newsletter",
				"Cadastro",
				"Loja",
				"Cidade",
				"Estado",
				"Codigo",
				"QR CODE",
				"Status"
		);
		
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=usuarios.csv');
		
		$out = fopen('php://output', 'w');
		fputcsv($out, $theadRow);
		
		
		foreach ($users as $key => $user) {
			$row = array(
					'' . $user['nome'], //nome
					'' . $user['cpf'], //cpf
					'' . $user['nome'], //nome
					'', //segundo nome
					'', //sobrenome
					'' . $user['email'], //email principal
					'', //Sexo
					'' . $user['nascimento'],  //Aniversário
					'', //Telefone
					'' . $user['cep'], //CEP
					'' . $user['endereco'], //Endereco
					'' . $user['cidade'], //Cidade
					'' . $user['estado'], //Estado
					'' . $user['facebook_id'], //fbhandle
					'' . $user['nome'], //nome
					'' . $user['news_letter'], //nome
					'' . $user['cadastro'], //nome
					'' . $user['loja'], //nome
					'' . $user['cidade'], //nome
					'' . $user['estado'], //nome
					'' . $user['codigo'], //nome
					'' . str_ireplace('.jpg', '.png', str_ireplace('http:/', 'http://', $user['path'])),
					'' . $user['validado']?'Utilizado':'Pendente'
					//'http://semanadoabraco.com.br/common/uploads/qrcode/'. $user['loja_id'].'/'.$user['user_id'].'-'. $user['codigo'].'.png'
			);
		
			fputcsv($out, $row);
				
		}
		fclose($out);
		
		exit;
		
	}
}

