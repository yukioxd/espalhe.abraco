<main class="layout-content type-b" id="select-store-block">
	<h2 id="page-title" class="page-titles">Escolha da Loja</h2>

	<div id="select-store-container">
		<p class="instruction-stores-list">
			Agora é só escolher a loja em que gostaria de retirar o hidratante.
		</p>
		<div class="stores-filter-block" id="stores-filter-block">
			<div class="stores-filter-estado">
				<label for="estado">Estado</label>
				<div class="style-select type-1">
					<select name="filtro_estado" id="filtro_estado">
						<option value="" selected disabled> Estado </option>
					</select>
					<span class="select-value">Selecione</span>
				</div>

			</div>
			<div class="stores-filter-cidade">
				<label for="estado">Cidade</label>
				<div class="style-select type-1">
					<select name="filtro_cidade" id="filtro_cidade">
						<option value="" selected disabled> Cidade </option>
					</select>
					<span class="select-value">Selecione</span>
				</div>
			</div>
		</div>

		<div class="stores-list-content" id="select-store-content">
			<table cellpadding="0" class="select-store-content-header">
				<tr>
					<td cellpadding="0" class="header-left" style="width: 20%"></td>
					<td class="header-middle">
						<div><span id="lojas-count">19091090</span> <span id="lojas-count-unit">lojas</span> em <span id="current-filter-cidade">São Paulo</span></div>
					</td>
					<td class="header-right" style="width: 80%"></td>
				</tr>
			</table>
			<div class="select-store-content-body">
				<ul class="stores-list" id="stores-list"></ul>
			</div>
			<div class="select-store-content-footer">
			</div>
		</div>
	</div>
</main>