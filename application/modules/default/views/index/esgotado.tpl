<main class="layout-content type-b" id="index-content">
    <div class="container" id="index-container">
        <div class="index-body">
            <div style="font-size: 32px;">
                Olá! Infelizmente os produtos se esgotaram para novos cadastros e por isso <span class="page-titles">a promoção já acabou.</span>
            </div>
            <div>
                Mas você pode nos seguir nas redes sociais e ficar por dentro de todas as promoções e novidades!
            </div>
            <div>
                <ul class="di nav-footer-list social">
                    <li class="nav-footer-social-items">
                        <a target="_blank" title="Visite a nossa página do Facebook" class="nav-footer-a nav-footer-fb" href="https://www.facebook.com/facedabeauty">
                            <span class="ir">Página do Facebook</span>
                        </a>
                    </li>
                    <li class="nav-footer-social-items">
                        <a target="_blank" title="Visite o nosso perfil do Instagram " class="nav-footer-a nav-footer-insta" href="https://www.instagram.com/instadabeauty/">
                            <span class="ir">Perfil do Instagram</span>
                        </a>
                    </li>
                    <li class="nav-footer-social-items">
                        <a target="_blank" title="Visite o nosso canal do YouTube" class="nav-footer-a nav-footer-youtube" href="https://www.youtube.com/user/videosdabeauty">
                            <span class="ir">Canal do YouTube</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>