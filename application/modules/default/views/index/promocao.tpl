<main class="layout-content type-b" id="the-promo">
	<h2 id="page-title" class="page-titles">A promoção</h2>
	<section id="promotion-steps">
		<ul id="promotion-steps-list">

			<li class="promotion-steps-items" id="promo-step-item-1">
				<div class="promo-figure" id="promo-figure-1">
					<figure>
						<source media="(max-width: 1440px)" srcset="{$imgPath}/2x/promocao/one.png">
						<source media="(min-width: 1440px)" srcset="{$imgPath}/promocao/one.png">
						<img src="{$imgPath}/promocao/one.png" alt="The Beauty Box">
					</figure>
					<a class="a participe-btn r-25" href="{$this->url([], 'participe')}"> Participe </a>
				</div>
				<div class="promo-description" id="promo-description-1">
                    <h3 class="steps-titles" id="steps-titles-1">Cadastre-se</h3>
					<p class="step-how-to" id="step-how-to-1">
						Preencha seus dados
						pelo menu <strong>participe</strong>
					</p>
				</div>
			</li>
			<li class="promotion-steps-items" id="promo-step-item-2">
				<div class="promo-figure" id="promo-figure-2">
					<figure>
						<source media="(max-width: 1440px)" srcset="{$imgPath}/2x/promocao/two.png">
						<source media="(min-width: 1440px)" srcset="{$imgPath}/promocao/two.png">
						<img src="{$imgPath}/promocao/two.png" alt="The Beauty Box">
					</figure>
				</div>
				<div class="promo-description" id="promo-description-2">
					<h3 class="steps-titles" id="steps-titles-2">Abrace</h3>
					<p class="step-how-to" id="step-how-to-2">
						Envie um abraço para
						alguém querido também
						ganhar um <strong>Hidratante
							Hug Me Bombom de Baunilha 300ml.</strong>
					</p>
				</div>
			</li>
			<li class="promotion-steps-items" id="promo-step-item-3">
				<div class="promo-figure" id="promo-figure-3">
					<figure>
						<source media="(max-width: 1440px)" srcset="{$imgPath}/2x/promocao/cell-v5.png">
						<source media="(min-width: 1440px)" srcset="{$imgPath}/promocao/cell-v5.png">
						<img src="{$imgPath}/promocao/cell-v5.png" alt="The Beauty Box">
					</figure>
				</div>
				<div class="promo-description" id="promo-description-3">
					<h3 class="steps-titles" id="steps-titles-3">Cupom</h3>
					<p class="step-how-to" id="step-how-to-3">
						Guarde em seu
						celular o <strong>cupom</strong>
						que será gerado
					</p>
				</div>
			</li>
			<li class="promotion-steps-items" id="promo-step-item-4">
				<div class="promo-figure" id="promo-figure-4">
					<figure>
						<source media="(max-width: 1440px)" srcset="{$imgPath}/2x/promocao/pack-v5.png">
						<source media="(min-width: 1440px)" srcset="{$imgPath}/promocao/pack-v5.png">
						<img src="{$imgPath}/promocao/pack-v5.png" alt="The Beauty Box">
					</figure>
				</div>
				<div class="promo-description" id="promo-description-4">
					<h3 class="steps-titles" id="steps-titles-4">Retire</h3>
					<p class="step-how-to" id="step-how-to-4">
						Apresente seu cupom na loja
						escolhida e retire sua Loção
						<strong>Hidratante Hug Me Bombom de Baunilha 300 ml</strong>
						em nossas lojas entre 15/05 a 22/05.
					</p>
				</div>
			</li>
			<li class="promotion-steps-items" id="promo-step-item-5">
				<div class="promo-figure" id="promo-figure-5">
					<figure>
						<source media="(max-width: 1440px)" srcset="{$imgPath}/2x/promocao/insta-v5.png">
						<source media="(min-width: 1440px)" srcset="{$imgPath}/promocao/insta-v5.png">
						<img src="{$imgPath}/promocao/insta-v5.png" alt="The Beauty Box">
					</figure>
				</div>
				<div class="promo-description" id="promo-description-5">
					<h3 class="steps-titles" id="steps-titles-5">Compartilhe</h3>
					<p class="step-how-to" id="step-how-to-5">
						Que tal aproveitar para publicar um<br>
						abraço e espalhar essa ideia?<br>
						Use a hashtag <strong>#abracenaBeauty</strong><br>
						e marque também o <strong>@InstadaBeauty</strong>
					</p>
				</div>
			</li>
		</ul>
	</section>
</main>
