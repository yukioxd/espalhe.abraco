<main class="layout-content type-b" id="duvidas">
    <h2 id="page-title" class="page-titles">Dúvidas</h2>
    <div class="duvidas-block" id="duvidas-block">
        <div class="container">
            <ul class="wrappers" id="faq-list">
                <li class="faq-items" id="faq-item-1" >
                    <span class="faq-items-title"><a href="#!faq-item-1">Como funciona a promoção?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            É simples, se cadastrou, convidou um(a) amigo (a), ganhou! Basta clicar no link “Participe”, preencher seu cadastro, escolher a The Beauty Box mais próxima, receber e salvar no seu celular o cupom da promoção.
                            Durante a Semana do Abraço (15/05 a 22/05) é só ir à loja escolhida, apresentar o código, junto com um documento com foto e CPF, e retirar seu brinde!
                        </p>
                </li>
                <li class="faq-items" id="faq-item-2" >
                    <span class="faq-items-title"><a href="#!faq-item-2">Como é feito o cadastro?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Fácil e rápido. Clique em “Participe” e preencha os dados. Após a escolha da loja, seu cupom com número será gerado e você poderá escolher como prefere recebê-lo: salvando no seu celular, recebendo por e-mail ou por SMS.
                        </p>
                </li>
                <li class="faq-items" id="faq-item-3" >
                    <span class="faq-items-title"><a href="#!faq-item-3">Quando ocorre a retirada dos brindes nas lojas?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Durante a semana de 15 de maio (segunda feira) a 22 de maio (segunda feira), a partir da abertura da loja, até 30 minutos antes do fechamento da mesma. Lembre-se de checar o horário de funcionamento da loja escolhida e de levar seu cupom e um documento com foto e o CPF.
                        </p>
                </li>
                <li class="faq-items" id="faq-item-4" >
                    <span class="faq-items-title"><a href="#!faq-item-4">É possível escolher o brinde?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Já escolhemos a dedo o melhor para você: nosso best seller Hidratante Hug Me Everyday Bombom de Baunilha 300ml. Infelizmente, não tem como escolher.
                        </p>
                </li>
                <li class="faq-items" id="faq-item-5" >
                    <span class="faq-items-title"><a href="#!faq-item-5">Quantos  cupons codes serão distribuídos nesta ação?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Serão distribuídos mais de 29.624 mil cupons, que poderão ser trocados   pelo brinde.
                        </p>
                </li>
                <li class="faq-items" id="faq-item-6" >
                    <span class="faq-items-title"><a href="#!faq-item-6">A promoção é válida para o e-commerce?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Não há a mesma promoção no e-commerce, mas não deixe de conferir as ofertas exclusivas que preparamos pra você no nosso site, que também está entrando no clima da semana do abraço. 
                        </p>
                </li>
                <li class="faq-items" id="faq-item-7" >
                    <span class="faq-items-title"><a href="#!faq-item-7">Quantas lojas participarão?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            A ação vai acontecer em 53 lojas da The Beauty Box, em todo o Brasil. A lista completa está no regulamento, facinho de conferir no hotsite (www.semanadoabraco.com.br).
                        </p>
                </li>
                <li class="faq-items" id="faq-item-8" >
                    <span class="faq-items-title"><a href="#!faq-item-8">Existe um limite de brinde por pessoa?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Sim, cada CPF tem direito a um  cupom e um brinde.
                        </p>
                </li>
                <li class="faq-items" id="faq-item-9" >
                    <span class="faq-items-title"><a href="#!faq-item-9">Perdi meu cupom. E agora?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Calma, é só acessar o site, clicar em participar e colocar o seu CPF cadastrado. Caso não consiga, entre em contato com o nosso SAC, no telefone 0800-729-9070 ou na página de contato do hotsite da promo (www.semanadoabraco.com.br).
                        </p>
                </li>
                <li class="faq-items" id="faq-item-10">
                    <span class="faq-items-title"><a href="#!faq-item-10">Posso trocar de loja depois que emiti meu cupom?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Infelizmente, não pode. Pra garantir que ninguém vai ficar sem brinde, estamos enviando loja a loja a quantidade necessária, por isso, não vai dar pra trocar a loja. 
                        </p>
                </li>
                <li class="faq-items" id="faq-item-11">
                    <span class="faq-items-title"><a href="#!faq-item-11">Posso retirar meu brinde numa loja diferente da qual eu escolhi no momento do cadastro?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Infelizmente, não pode. Pra garantir que ninguém vai ficar sem brinde, estamos enviando loja a loja a quantidade necessária, por isso, não vai dar pra trocar em uma loja diferente.
                        </p>
                </li>
                <li class="faq-items" id="faq-item-12">
                    <span class="faq-items-title"><a href="#!faq-item-12">Posso trocar meu brinde por outro produto da Beauty?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Não. Escolhemos a dedo um produto que nós adoramos e achamos que você também vai!
                        </p>
                </li>
                <li class="faq-items" id="faq-item-13">
                    <span class="faq-items-title"><a href="#!faq-item-13">Posso trocar meu cupom por um brinde depois do dia 23 de maio?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Não. Temos um planejamento pra sua troca acontecer da maneira mais simples e agradável possível. Isso envolve uma operação muito especial. Por isso, a troca só pode acontecer na semana da ação (15 a 22/05/2017)
                        </p>
                </li>
                <li class="faq-items" id="faq-item-14">
                    <span class="faq-items-title"><a href="#!faq-item-14">Outra pessoa pode retirar o brinde no meu lugar com o meu cupom?</a></span>
                    <div class="faq-item-content-wrapper">
                        <p class="faq-items-contents">
                            Não pode, infelizmente precisamos que você vá até a loja e troque o seu brinde, assim conseguimos garantir que ninguém fica sem. Além disso, você pode aproveitar pra conhecer nossa loja e nossas ofertas especiais. 
                        </p>
                </li>
            </ul>
        </div>
    </div>
</main>