<main class="layout-content type-a" id="index-content">
	<div class="container" id="index-container">
		<div class="index-body">
			<h2 id="main-title">
				<span class="main-title text1 line1">AQUI SEU</span>
				<span class="main-title text2 line2">abraço</span>
				<span class="main-title text1 line3">VALE UM </span>
				<span class="main-title text3 line4">HIDRATANTE</span>
				<span class="main-title text4 line5">HUG ME</span>
			</h2>


			<div id="index-headline" class="b">
				Abrace alguém querido e a<br>
				Beauty te abraça com um presente !
			</div>
			<div id="index-resume">
				<span class="b">Faça o seu cadastro,</span> indique um amigo<br>
				<span>para receber o abraço e retire o seu hidratante</span><br>
				<span class="b b-c2">Hug Me Bombom de Baunilha 300ml</span><br>
				em nossas lojas entre <span class="b">15/05 a 22/05</span>.

				<div id="index-join">
					<a href="{$this->url([], 'participe')}" class="index-join-btn btn r-25">
						Participe
					</a>
				</div>
			</div>

            <div id="abracometro" class="hidden">
            	<div id="abracometro-container">
	                <span id="hugmeter-title">Abraçômetro</span>
	                <div id="hugmeter-block"> 0 </div>
	                <span id="hugmeter-subtitle"> abraços compartilhados </span>
            	</div>
            </div>

		</div>

		<div class="index-bottle">
	        <picture>
	            <source media="(max-width: 1440px)" srcset="{$imgPath}/pack-v5.md.png">
	            <source media="(min-width: 1440px)" srcset="{$imgPath}/pack-v5.png">
	            <img src="{$imgPath}/pack-v5.md.png" alt="The Beauty Box">
	        </picture>
		</div>
	</div>
</main>