<main class="layout-content type-b" id="participate-block">
	<h2 id="page-title" class="page-titles">Participe</h2>
	<div id="participate-1-form-block">
		<form action="" id="participate-form-step-1" method="POST">
			<div id="check-cpf-block">
				<label id="cpf-flabel" for="cpf">
					Para se cadastrar,
					informe o seu CPF
				</label>
				<div id="wrap-field-cpf" class="wrap-fields">
					<input class="regular-field cpf-field" type="tel" name="cpf" id="cpf" >
				</div>
				<button type="submit" class="do-continue-btn" id="do-send-cpf">
					<span>Continuar</span>
				</button>
			</div>
		</form>
	</div>
</main>
