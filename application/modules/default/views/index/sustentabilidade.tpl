<main class="layout-content type-b" id="m-sustentabilidade">
    <h2 id="page-title" class="page-titles">Sustentabilidade</h2>
    <div class="sustentabilidade-block" id="sustentabilidade-block">
        <div class="container">

                <div class="wrappers">
                    <iframe id="the-promo-frame" width="640" height="390" title="A Beauty Box" frameborder="0" allowfullscreen="1" src="https://www.youtube.com/embed/RB8mk8d-oOo"></iframe>
                    <article id="the-promo-desc">
                        <p><span class="b">A The Beauty Box cuida do meio ambiente e contamos com você para abraçar essa causa com a gente!</span></p>
                        <p>O Sustenta + Beauty é a ação de recolhimento de embalagens de The Beauty Box, onde embalagens de cosméticos vazias de qualquer marca podem ser depositados para reciclagem no coletor de embalagem localizados em nossas lojas.</p>
                        <p>Sabendo da importância de cuidar do meio ambiente temos uma campanha perene onde você leva 5 embalagens de “Produtinhos da Beauty” e ganha um produto da marca Produtinhos da Beauty.</p>
                        <p>Que tal ir até uma de nossas lojas para deixar embalagens e abraçar essa causa?  Aproveite e conheça as ações de conservação da Fundação Grupo Boticário de Proteção à Natureza em nosso <u class="b"><a href="http://www.fundacaogrupoboticario.org.br/" target="_blank" title="Boticário Group Foundation for Nature Protection">site</a></u> e entenda porque quando você compra um produto das empresas do Grupo Boticário está nos ajudando a deixar o mundo mais belo.</p>
                        <p>Vamos juntos deixar o mundo mais <span class="b">Beauty</span>!</p>
                    </article>
                </div>
                
        </div>
    </div>
</main>