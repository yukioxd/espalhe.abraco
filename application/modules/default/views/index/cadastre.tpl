<main class="layout-content type-b" id="register-block">
	<h2 id="page-title" class="page-titles">Cadastro</h2>
	<div class="container">
		<form action="" id="register-block-form" method="POST">
			<div id="register-block-form-body">
				<div id="register-block-form-wrapper">
					{*
					(div.register-wrap-field-blocks#field-n-$>(div.register-field-blocks>(label[for="campo-$"]{campo-$}+(div>input[id="campo-$" name="campo-$"]))))*8
					*}
					<div class="register-wrap-field-blocks" id="field-n-1">
						<div class="register-field-blocks r-space register-field-blocks-type1">
							<label class="register-field-label" for="nome">Nome</label>
							<div><input class="register-fields required" id="nome" type="text" name="nome"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-2">
						<div class="register-field-blocks register-field-blocks-type1">
							<label class="register-field-label" for="nascimento">Data de nascimento</label>
							<div class="masks mask-date"><input class="register-fields required date" id="nascimento" type="tel" name="nascimento" value=""></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-3">
						<div class="register-field-blocks r-space register-field-blocks-type1">
							<label class="register-field-label" for="email">E-mail</label>
							<div><input class="register-fields required" id="email" type="email" name="email"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-9">
						<div class="register-field-blocks register-field-blocks-type1">
							<label class="register-field-label" for="celular">Celular</label>
							<div><input class="register-fields" id="celular" type="tel" name="celular"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-4">
						<div class="register-field-blocks r-space register-field-blocks-type1">
							<label class="register-field-label" for="cep">CEP</label>
							<div><input class="register-fields required" id="cep" type="tel" name="cep"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-5">
						<div class="register-field-blocks register-field-blocks-type1">
							<label class="register-field-label" for="estado">Estado</label>
							<div><input class="register-fields required" id="estado" type="text" name="estado"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-6">
						<div class="register-field-blocks r-space register-field-blocks-type1">
							<label class="register-field-label" for="cidade">Cidade</label>
							<div><input class="register-fields required" id="cidade" type="text" name="cidade"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-7">
						<div class="register-field-blocks register-field-blocks-type1">
							<label class="register-field-label" for="endereco">Endereço</label>
							<div><input class="register-fields required" id="endereco" type="text" name="endereco"></div>
						</div>
					</div>
					<div class="register-wrap-field-blocks" id="field-n-8">
						<div class="register-field-blocks r-space register-field-blocks-type1">
							<label class="register-field-label" for="numero">Número</label>
							<div><input class="register-fields required" id="numero" type="number" name="numero"></div>
						</div>

						<div class="register-field-blocks register-field-blocks-type1">
							<label class="register-field-label" for="complemento">Complemento</label>
							<div><input class="register-fields" id="complemento" type="text" name="complemento"></div>
						</div>
					</div>

					<div class="register-field-boxes">
						<div>
							<input id="clube_beauty" name="clube_beauty" type="checkbox" class="checkbox" checked>
							<div class="styled-checkbox"></div>
							<label class="register-label" for="clube_beauty">Quero fazer parte do <a target="_blank" href="http://www.thebeautybox.com.br/clube-da-beauty" target="_blank"><u>Clube da Beauty</u></a></label>
						</div>
						<div>
							<input id="social_networks" name="social_networks" type="checkbox" class="checkbox" checked>
							<div class="styled-checkbox"></div>
							<label class="register-label" for="social_networks">Quero receber as novidades da Beauty</label>
						</div>
						<div>
							<input id="agreement" name="agreement" type="checkbox" class="checkbox">
							<div class="styled-checkbox"></div>
							<label class="register-label" for="agreement">Estou de acordo com o</label> <a class="modal-iframe register-label" href="http://www.semanadoabraco.com.br/common/default/regulamento.pdf"><u>Regulamento</u></a> <label class="register-label" for="agreement">receber informações do Clube da Beauty</label>
						</div>
					</div>

					<div id="send-hug-block">
						<h3 id="send-hug-title">
							<span>Quero enviar</span>
							<span>meu abraço para</span>
						</h3>
						<div class="register-wrap-field-blocks" id="field-n-10">
							<div class="register-field-blocks r-space register-field-blocks-type1">
								<label class="register-field-label" for="abraco_nome">Nome</label>
								<div><input class="register-fields required" id="abraco_nome" type="text" name="abraco_nome"></div>
							</div>
						</div>
						<div class="register-wrap-field-blocks" id="field-n-11">
							<div class="register-field-blocks register-field-blocks-type1">
								<label class="register-field-label" for="abraco_email">E-mail</label>
								<div class=""><input class="register-fields required" id="abraco_email" type="email" name="abraco_email" value=""></div>
							</div>
						</div>
						<div id="cadastre-tip">
							<div id="cadastre-tip-title"><strong> Compartilhe a promoção para sua amiga ganhar também um Hidratante Hug Me 300ml. * </strong></div>
							<small><strong>*Consulte regulamento.</strong></small>
						</div>
					</div>
				</div>
			</div>
			<div id="register-submit-block"> <button class="do-continue-btn" id="register-submit"> <span>Continuar</span> </button> </div>
		</form>
	</div>
</main>