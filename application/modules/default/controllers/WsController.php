<?php

/**
 * Controlador do WS
 *
 * @name WsController
 */
class WsController extends Cms_Controller_Api
{


	private $db;
	private $uploadDir;

	/**
	 *
	 */
	public function init()
	{
		$this->uploadDir = APPLICATION_PATH . '/../common/uploads/';
		parent::init();

		if (!is_writable($this->uploadDir)) {
			throw new Exception("Diretório uploads não está com permissão adequado.");
		}
	}

	/*
	 * @param exception $er
	 *
	 * */
	private function setErr(Exception $er, $headerStatus = 400, $headerText = 'BAD REQUEST')
	{
		header("HTTP/1.1 {$headerStatus}{$headerText}");
		if ($this->_params['dbg'] > 0) {
			die(json_encode([
				'msg' => $er->getMessage(),
				'trace' => $er->getTrace()
			]));
		} else {
			die($er->getMessage());
		}
	}

	private function initTransaction()
	{
		if (is_null($this->db)) {
			$this->db = Cms_Db_Table::getDefaultAdapter();
			$this->db->beginTransaction();
		}
	}

	private function closeTransaction()
	{
		if (!is_null($this->db)) {
			$this->db->commit();
			$this->db = NULL;
		}
	}

	private function allocDir($path)
	{
		$perm = 0755;
		$_path = $path;
		$pathComponents = explode("/", $path);
		$writeableDir = "";
		$l = 0;
		while ($_path !== $this->uploadDir && $l < 10 && strlen($writeableDir) < 1) {
			$_dir = implode("/", array_slice($pathComponents, 0, sizeof($pathComponents) - $l));
			$writeableDir = is_writeable($_dir) ? $_dir : "";
			$l++;
		}
		$l = 0;

		while ($l < 10 && sizeof(explode("/", $writeableDir)) !== sizeof(explode("/", $path))) {
			$writeableParts = explode("/", $writeableDir);
			$key = sizeof($writeableParts);
			$append = $pathComponents[$key];
			$writeableDir .= "/" . $append;
			if (!is_dir($writeableDir)) {
				mkdir($writeableDir, $perm);
			}
			$l++;
		}

		if (!is_dir($path)) {
			mkdir($path, $perm);
		}
		if (!is_writable($path)) {
			chmod($path, $perm);
		}

		return $path;
	}

    private function getCidadeEstado ( $cidade_id = NULL ) {
        $modelCidade = new Admin_Model_Cidade();
        $select = $modelCidade->select();
        $select->from(array('c' => 'cidade'));
        $select->join(array('e' => 'estado'), 'c.estado_id = e.estado_id');
        $select->where("cidade_id = {$cidade_id}");
        $select->setIntegrityCheck(FALSE);

        $cidadeRow = $modelCidade->fetchRow($select);
	    if( ! is_null ($cidadeRow) ) {
		    return $cidadeRow->toArray();
	    } else {
            return [];
	    }
    }

	private function createQrCode($brindeArray, $cpf = NULL)
	{
		$cpfString = substr($cpf, 0, 3) . '.' . substr($cpf, 3, 3) . '.' . substr($cpf, 6, 3) . '-' . substr($cpf, 9, 2);
		$target_directory = $this->uploadDir . 'qrcode/' . $brindeArray['loja_id'] . '/';
		$uniq = uniqid();
		$filename = $brindeArray['user_id'] . '-' . $uniq . '.png';
		$filenameLabel = $brindeArray['user_id'] . '-' . $uniq . '.jpg';
		$filepath = $target_directory . $filename;
		$labelpath = $target_directory . $filenameLabel;
		$this->allocDir($target_directory);

		$qrCode = new Cms_Qr_Code();
		$qrCode
			->setText(json_encode($brindeArray))
			->setSize(300)
			->setPadding(30)
			->setDrawBorder(FALSE)
			->setErrorCorrection('high')
			->setForegroundColor(array('r' => 55, 'g' => 97, 'b' => 114, 'a' => 0))
			->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
			->setLogo(APPLICATION_PATH . '/../common/default/images/site/b.png')
			->setLogoSize(90)
			->setLabel("Código: {$brindeArray['codigo']}")
			->setLabelFontSize(12)
			->setImageType(Cms_Qr_Code::IMAGE_TYPE_PNG);
		$image = $qrCode->get();
		file_put_contents($filepath, $image);

		$im = imagecreatefrompng($filepath);

		imagesavealpha($im, true);

		$black = imagecolorallocate($im, 0, 0, 0);

		$width = 300;
		$height = 300;
		$fontSize = 12;
		$text = "CPF: {$cpfString}";
		$x = ($width / 2) - (strlen($text) * 2.2);
		$y = $height + 50;
		$outputImage = $labelpath;
		imagettftext($im, $fontSize, 0, $x, $y, $black, $qrCode->getLabelFontPath(), $text);
		imagejpeg($im, $outputImage, 100);


		$zend_registry = Zend_Registry::get("config")->cms->config;
		$protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
		$domain = $zend_registry->domain;
		$basepath = $zend_registry->basepath;
		$url = "{$protocol}:/{$domain}{$basepath}/common/uploads/qrcode/{$brindeArray['loja_id']}/$filenameLabel";
		return array(
			'url' => $url,
			'uniq' => $uniq
		);
	}

	private function isValidLojaId($id, $model)
	{
		$selectLoja = $model->select();
		$selectLoja->from(['l' => 'loja'], ['loja_id', 'estoque', 'cidade_id']);
		$selectLoja->where("loja_id = {$id}");
		$loja = $model->fetchRow($selectLoja);

		if (is_null($loja)) {
			throw new Exception("Loja inválida, por favor, tente novamente mais tarde atualizando o Navegador.", 400);
			exit;
		}
		return $loja;
	}

	private function availStok($loja, $model)
	{
		$selectBrinde = $model->select();
		$selectBrinde->from(['b' => 'brinde'], ['convertido' => "COUNT(brinde_id)"]);
		$selectBrinde->where("b.loja_id = {$loja->loja_id}");
		$selectBrinde->where("b.validado > 0");
		$brindeInfo = $model->fetchRow($selectBrinde);
		if ($loja->estoque < 1 || ($loja->estoque <= $brindeInfo->convertido)) {
			throw new Exception("Estoque Indisponível", 400);
			exit;
		}
		return $brindeInfo;
	}

    private function buildHTMLHeader ($basePath) {
        return "<!DOCTYPE HTML>
                <html lang='pt-br' manifest='{$basePath}/common/default/sustentamais.appcache'>
                <head>
                    <meta charset='utf-8'/>
                    <title> Sustenta+Beauty - The Beauty Box </title>
                    <meta charset='utf-8'>
                    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                    <meta name='description' content='Sustenta+Beauty - The Beauty Box | QRCode'>
                    <meta property='og:locale' content='pt_BR'>
                    <meta property='og:description' content='Sustenta+Beauty - The Beauty Box'>
                    <meta property='og:title' content='Sustenta+Beauty - The Beauty Box'/>
                    <meta property='og:url' content='http://www.semanadoabraco.com.br/'/>
                    <meta property='article:modified_time' content='2016-09-14'/>
                    <meta property='og:site_name' content='Sustenta+Beauty - The Beauty Box'/>
                    <meta property='og:image' content='{$basePath}/common/default/images/site_dist/logo.png'>
                    <meta property='og:image:type' content='image/jpeg'>
                    <meta property='og:image:width' content='200'>
                    <meta property='og:image:height' content='200'>
                    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
                    <link rel='apple-touch-icon' sizes='57x57' href='{$basePath}/common/default/images/icos/apple-icon-57x57.png'>
                    <link rel='apple-touch-icon' sizes='60x60' href='{$basePath}/common/default/images/icos/apple-icon-60x60.png'>
                    <link rel='apple-touch-icon' sizes='72x72' href='{$basePath}/common/default/images/icos/apple-icon-72x72.png'>
                    <link rel='apple-touch-icon' sizes='76x76' href='{$basePath}/common/default/images/icos/apple-icon-76x76.png'>
                    <link rel='apple-touch-icon' sizes='114x114' href='{$basePath}/common/default/images/icos/apple-icon-114x114.png'>
                    <link rel='apple-touch-icon' sizes='120x120' href='{$basePath}/common/default/images/icos/apple-icon-120x120.png'>
                    <link rel='apple-touch-icon' sizes='144x144' href='{$basePath}/common/default/images/icos/apple-icon-144x144.png'>
                    <link rel='apple-touch-icon' sizes='152x152' href='{$basePath}/common/default/images/icos/apple-icon-152x152.png'>
                    <link rel='apple-touch-icon' sizes='180x180' href='{$basePath}/common/default/images/icos/apple-icon-180x180.png'>
                    <link rel='icon' type='image/png' sizes='192x192' href='{$basePath}/common/default/images/icos/android-icon-192x192.png'>
                    <link rel='icon' type='image/png' sizes='96x96' href='{$basePath}/common/default/images/site/favicon.ico'>
                    <link rel='icon' type='image/png' sizes='32x32' href='{$basePath}/common/default/images/site/favicon.ico'>
                    <link rel='icon' type='image/png' sizes='16x16' href='{$basePath}/common/default/images/site/favicon.ico'>
                    <link rel='manifest' href='{$basePath}/common/default/images/icos/manifest.json'>
                    <meta name='msapplication-TileColor' content='#54e3c9'>
                    <meta name='msapplication-TileImage' content='{$basePath}/common/default/images/icos/ms-icon-144x144.png'>
                    <meta name='msapplication-navbutton-color' content='#54e3c9'>
                    <meta name='theme-color' content='#54e3c9'>
                    <meta name='mobile-web-app-capable' content='yes'>
                    <meta name='apple-mobile-web-app-capable' content='yes'>
                    <meta name='apple-mobile-web-app-status-bar-style' content='black-translucent'>
                    <link rel='apple-touch-startup-image' href='{$basePath}/common/default/images/site_dist/logo.png'>
                    <style> body {
                                font-family: Tahoma, Helvetica,Arial; line-height 1.4;
                            }
                            p { margin: 0.25em; }</style>
                    </head>
                ";
    }

    private function buildDownloadImageHTML ($src) {
        $html = "<body>";
        $html .= "<table width=\"80%\">";
        $html .= "    <tbody>";
        $html .= "    <tr>";
        $html .= "        <td>";
        $html .= "            <img src='{$src}' title='Pressione e segure o dedo na imagem até que abra uma tela de menu.'>";
        $html .= "        <td>";
        $html .= "    </tr>";
        $html .= "    <tr>";
        $html .= "        <td>";
        $html .= "            <p>1) Pressione e segure o dedo na imagem até que abra uma tela de menu.</p>";
        $html .= "        <td>";
        $html .= "    </tr>";
        $html .= "    <tr>";
        $html .= "        <td>";
        $html .= "            <p>2) Selecione “Salvar imagem”.</p>";
        $html .= "        <td>";
        $html .= "    </tr>";
        $html .= "    <tr>";
        $html .= "        <td>";
        $html .= "            <p>3) Pronto! O QR Code estará gravado no seu Rolo de Câmera.</p>";
        $html .= "        </td>";
        $html .= "    </tr>";
        $html .= "    </tbody>";
        $html .= "</table>";
        $html .= "</body>";
        return $html;
    }


    /**
     *
	 *
	 * @api {get} /ws/get-cpfs Listar os cps utilizados
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 * @apiSuccess {Object[]} estados lista de estados.
	 * @apiSuccess {String} estados.estado_id ID do estado.
	 * @apiSuccess {String} estados.estado Nome do estado.
	 *
	 */
	public function getCpfsAction()
	{
		$modelUser = new Admin_Model_User();
		$select = $modelUser->select();
		$select->from(array('u' => 'user'), array('cpf' => 'group_concat(distinct cpf)'));
		$cpfs = $modelUser->fetchRow($select);
		$this->_return['cpfs'] = $cpfs->cpf;
	}

	/**
	 *
	 *
	 * @api {post} /ws/lista-estados Listar os estados
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 * @apiSuccess {Object[]} estados lista de estados.
	 * @apiSuccess {String} estados.estado_id ID do estado.
	 * @apiSuccess {String} estados.estado Nome do estado.
	 *
	 */

	public function listaEstadosAction()
	{
		try {
			$modelEstado = new Admin_Model_Estado();
			$selectEstado = $modelEstado->select();
			$selectEstado->from(['e' => 'estado']);
			$selectEstado->join(['c' => 'cidade'], 'c.estado_id = e.estado_id', []);
			$selectEstado->join(['l' => 'loja'], 'c.cidade_id = l.cidade_id', []);
			$selectEstado->setIntegrityCheck( FALSE );
			$estados = $modelEstado->fetchAll($selectEstado)->toArray();
			$this->_return['estados'] = $estados;
		} catch (Exception $e) {
			$this->setErr($e);
		}
	}


	/**
	 *
	 *
	 * @api {post} /ws/lista-cidades Listar os cidades
	 * @apiParam {String} estado_id estado_id para filtrar as cidade
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 * @apiSuccess {Object[]} cidades lista de cidades.
	 * @apiSuccess {String} cidades.cidade_id ID do cidade.
	 * @apiSuccess {String} cidades.cidade Nome do cidade.
	 *
	 */

	public function listaCidadesAction()
	{
		try {
			$modelCidade = new Admin_Model_Cidade();
			$selectCidade = $modelCidade->select();
			$selectCidade->from(['c' => 'cidade']);
			$selectCidade->join(['l' => 'loja'], 'c.cidade_id = l.cidade_id', []);
			$selectCidade->where( "estado_id = {$this->_rawParams['estado_id']}" );
			$selectCidade->setIntegrityCheck( FALSE );
			$selectCidade->group('c.cidade_id');

			$cidades = $modelCidade->fetchAll( $selectCidade )->toArray();
			$this->_return['cidades'] = $cidades;
		} catch (Exception $e) {
			$this->setErr($e);
		}
	}


	/**
	 *
	 *
	 * @api {post} /ws/lista-lojas Listar as lojas
	 * @apiParam {String} estado_id estado_id para filtrar com o estado
	 * @apiParam {String} cidade_id cidade_id para filtrar a cidade
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 * @apiSuccess {Object[]} lojas lista de lojas.
	 * @apiSuccess {String} lojas.loja_id ID das lojas
	 * @apiSuccess {String} lojas.nome Nome das lojas
	 * @apiSuccess {String} lojas.endereco Endereço da loja
	 * @apiSuccess {String} lojas.latitude Latitude do endereço da loja
	 * @apiSuccess {String} lojas.longitude Longitude do endereço da loja
	 * @apiSuccess {String} lojas.cidade Nome da Cidade da loja
	 * @apiSuccess {String} lojas.cidade_id id da Cidade da loja
	 * @apiSuccess {String} lojas.estado Nome do estado da loja
	 * @apiSuccess {String} lojas.estado_id id do estado da loja
	 * @apiSuccess {Number} lojas.estoque Quantidade de estoque da loja
	 *
	 */

	public function listaLojasAction()
	{
		try {
			$cols = [
				'loja_id' => 'COALESCE(l.loja_id, 0)',
				'nome' => 'COALESCE(l.nome, "")',
				'endereco' => 'COALESCE(l.endereco, "")',
				'latitude' => 'COALESCE(l.latitude, "")',
				'longitude' => 'COALESCE(l.longitude, "")',
				'gmapIframe' => 'COALESCE(l.gmapIframe, "")',
				'cidade' => 'c.cidade',
				'cidade_id' => 'c.cidade_id',
				'uf' => 'e.uf',
				'estado' => 'e.estado',
				'estado_id' => 'e.estado_id',
				//'estoque' => 'l.estoque - (SELECT count(*) FROM brinde WHERE loja_id = l.loja_id)',
				'estoque' => 'l.estoque',
				'estoque_brinde' => '( l.estoque_brinde - ( SELECT count( * ) FROM brinde WHERE loja_id = l.loja_id ) ) ',
				'horarios_atendimento' => 'COALESCE(l.horarios_atendimento, "")',
				'telefone' => 'COALESCE(l.telefone, 0)',
				'usado' => "( SELECT COUNT( * ) FROM brinde WHERE loja_id = l.loja_id )"
			];
			$modelLoja = new Admin_Model_Loja();
			$selectLojas = $modelLoja->select();
			$selectLojas->from(['l' => 'loja'], []);
			$selectLojas->join(['c' => 'cidade'], 'c.cidade_id = l.cidade_id', []);
			$selectLojas->join(['e' => 'estado'], 'e.estado_id = c.estado_id', []);
			$selectLojas->columns($cols);
			//$selectLojas->where ("( SELECT COUNT( * ) FROM brinde WHERE loja_id = l.loja_id ) < l.estoque_brinde" );
			$selectLojas->setIntegrityCheck(FALSE);

			if ($this->validVar($this->_rawParams['estado_id'])) {
				$selectLojas->where("e.estado_id = {$this->_rawParams['estado_id']}");
			}

			if ($this->validVar($this->_rawParams['cidade_id'])) {
				$selectLojas->where("c.cidade_id = {$this->_rawParams['cidade_id']}");
			}

			$this->_return['lojas'] = $modelLoja->fetchAll($selectLojas)->toArray();
			$list = [];
			foreach( $this->_return[ 'loja' ] as $key => $loja ) {
					$list[] =  $this->_return['loja'][$key];
			}
			$this->_return['loja'] = $list;
		} catch (Exception $e) {
			$this->setErr($e);
		}
	}

	/**
	 *
	 *
	 * @api {post} /ws/cadastrar Cadastrar o usuário e já trocar por código ou QRCode, e já disparar um e-mail também.
	 * @apiParam {String} nome Nome da pessoa que está cadastrando.
	 * @apiParam {String} cpf CPF da pessoa que está cadastrando.
	 * @apiParam {String} email E-mail da pessoa que está  cadastrando.
	 * @apiParam {String} nascimento Data de nacimento.
	 * @apiParam {String} cep CEP do usuário.
	 * @apiParam {String} endereco Endereço do Usuário.
	 * @apiParam {String} user_cidade Cidade do Usuário.
	 * @apiParam {String} cidade_id Cidade da Loja.
	 * @apiParam {String} loja_id Identificador Único da loja.
	 * @apiParam {Number} news_letter Tinyint para opção do usuário.
	 * @apiParam {String} facebook_id Identificador Facebook.
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 * @apiSuccess {String} user_id chave primária do usuário.
	 *
	 * @TODO disparo do E-mail
	 *
	 */

	public function cadastrarAction()
	{
		return;
		try {

			$this->initTransaction();

			$modelLoja = new Admin_Model_Loja();
			$lojaRow = $this->isValidLojaId($this->_rawParams['loja_id'], $modelLoja);

			$modelBrinde = new Admin_Model_Brinde();
			$this->availStok($lojaRow, $modelBrinde);

			$modelUser = new Admin_Model_User();
			$userRow = $modelUser->createUser($this->_rawParams);

			$brindeRow = $modelBrinde->createRow([
				"loja_id" => $this->_rawParams['loja_id'],
				"user_id" => $userRow['user_id'],
				"codigo" => Cms_Hash::genCode($this->_rawParams['loja_id']),
				"path" => "",
			]);
			$brindeRow->save();

			$brindeArray = $brindeRow->toArray();
			$qrCode = $this->createQrCode($brindeArray, $userRow['cpf']);

			$brindeRow->path = $qrCode['url'];
			$brindeRow->save();

			$this->closeTransaction();

			$this->_return['brinde'] = $brindeRow->toArray();
			$this->_return['cpf'] = $userRow['cpf'];
			$this->_return['uniq'] = $qrCode['uniq'];
			$this->_return['cidade'] = $this->getCidadeEstado( $lojaRow->cidade_id );

		} catch (Exception $e) {
			$this->setErr($e);
		}
	}

	/**
	 *
	 *
	 * @api {post} /ws/contato Enviar os dados para entrar em contato.
	 * @apiParam {String} nome Nome da pessoa que está entrando em contato.
	 * @apiParam {String} email E-mail da pessoa que está entrando em contato.
	 * @apiParam {String} mensagem Mensagem da pessoa que está entrando em contato.
	 * @apiParam {String} captcha Mensagem Captcha caso esteja validando.
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 *
	 * @TODO implementar Disparo de e-mail após receber as configurações.
	 */

	public function contatoAction()
	{
		$return = array();
		try {

			$this->_rawParams['Nome'];
			$this->_rawParams['Email'];
			$this->_rawParams['Mensagem'];
			$this->_rawParams['Captcha'];

		} catch (Exception $e) {
			$this->setErr($e);
		}

		$this->_helper->json($return);
	}


	/**
	 *
	 *
	 * @api {post} /ws/enviar-sms Enviar SMS com as informações dos codigos
	 * @apiParam {String} brind_id ID do brinde
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 *
	 */

	public function enviarSmsAction()
	{
		try {
			$modelBrinde = new Admin_Model_Brinde();

			$select = $modelBrinde->select();
			$select->from(array('b' => 'brinde'), ['codigo', 'path']);
			$select->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', ['loja_id', 'loja' => 'l.nome']);
			$select->join(array('u' => 'user'), 'b.user_id = u.user_id', ['cpf' => 'u.cpf']);
			$select->where("b.brinde_id = {$this->_rawParams['brind_id']}");
			$select->setIntegrityCheck(FALSE);

			$brindeRow = $modelBrinde->fetchRow($select);
			$numero = $this->_rawParams['tel_number'];

			$sms = new Cms_SMS('sms_espalhe', '123456');
			//__construct($usuario, $senha){
			
			$brindeRow->path = str_replace('http:/s', 'http://s', $brindeRow->path);
			$brindeRow->path = str_replace('https:/s', 'http://s', $brindeRow->path);
				
			if (!is_null($brindeRow)) {
				$respo = $sms->envie_sms($numero, "Código: {$brindeRow->codigo} | CPF: {$brindeRow->cpf} | QRCode: {$brindeRow->path}", "TheBeautyBox");
				die(json_encode($respo));
			} else {
				die(json_encode([
					'msg' => 'brinde inválido'
				]));
			}

		} catch (Exception $e) {
			die(json_encode([
				'msg' => $e->getMessage()
			]));
		}
	}


	/**
	 *
	 *
	 * @api {post} /ws/enviar-sms Enviar Email com as informações dos codigos
	 * @apiParam {String} brind_id ID do brinde
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 *
	 */


	public function enviarMailAction()
	{
		try {
			$modelBrinde = new Admin_Model_Brinde();
			$modelUser = new Admin_Model_User();

			$select = $modelBrinde->select();
			$select->from(array('b' => 'brinde'), ['codigo', 'user_id', 'path', 'brinde_id']);
			$select->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', ['loja_id', 'loja' => 'l.nome']);
			$select->join(array('u' => 'user'), 'b.user_id = u.user_id', ['nome' => 'nome', 'cpf' => 'u.cpf', 'email' => 'u.email']);
			$select->where("b.brinde_id = {$this->_rawParams['brind_id']}");
			$select->setIntegrityCheck(FALSE);

			$brindeRow = $modelBrinde->fetchRow($select);

			$loja_id = $brindeRow->loja_id;
			$imageInfos = pathinfo($brindeRow->path);
			$imageAttachment = APPLICATION_PATH . "/../common/uploads/qrcode/{$loja_id}/{$imageInfos['basename']}";
			$remoteURL = "https://semanadoabraco.com.br/common/uploads/qrcode/{$loja_id}/{$imageInfos['basename']}";


			if (!is_null($brindeRow)) {
				$grid = new Cms_Sendgrid();
				$content = $this->buildEmail( $remoteURL );
				$grid->sendEmail($brindeRow->nome, $brindeRow->email, $content, $imageAttachment);
			}

			exit;
		} catch (Exception $e) {
			die(json_encode([
				'msg' => $e->getMessage()
			]));
		}
	}

	/**
	 *
	 *
	 * @api {post} /ws/enviar-wallet Criar passbook e retornar path
	 * @apiParam {String} brind_id ID do brinde
	 *
	 * @apiGroup ws
	 *
	 * @apiSuccess {String} success indicador se foi executado com sucesso.
	 *
	 */

	public function enviarWalletAction()
	{
		try {
			$modelBrinde = new Admin_Model_Brinde();
			$select = $modelBrinde->select();
			$select->from(array('b' => 'brinde'), ['codigo']);
			$select->join(array('l' => 'loja'), 'b.loja_id = l.loja_id', ['loja_id', 'loja' => 'l.nome']);
			$select->join(array('u' => 'user'), 'b.user_id = u.user_id', ['cpf' => 'u.cpf']);
			$select->where("b.brinde_id = {$this->_rawParams['brind_id']}");
			$select->setIntegrityCheck(FALSE);

			$brindeRow = $modelBrinde->fetchRow($select);
			$params = [
				'loja' => $brindeRow->loja,
				'cpf' => $brindeRow->cpf,
				'codigo' => $brindeRow->codigo
			];
			$strParams = json_encode($params);

			$ch = curl_init();
			$header = array();
			$header[] = 'Content-length:' . strlen($strParams);
			$header[] = 'Content-type: application/json';
			$header[] = 'Authorization: rbeOXbGGOShtyVFcxnaxIkaGJeceitZbvOiTWDRVlfcugrTzKdBxiHctFuOSMDUh';
			//loja:cpf:codigo
			curl_setopt($ch, CURLOPT_URL, "https://api.passslot.com/v1/templates/4824184582045696/pass");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $strParams);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$rest = curl_exec($ch);

			$this->_helper->json($rest);
		} catch (Exception $e) {
			die(json_encode([
				'msg' => $e->getMessage()
			]));
		}
	}

	public function getImageAction()
	{

		$id = $this->_request->getParam('id');
		$loja_id = $this->_request->getParam('loja_id');
		$image = $this->_request->getParam('pic');
		$file = APPLICATION_PATH . "/../common/uploads/qrcode/{$loja_id}/{$id}-${image}";
		if (is_readable($file)) {
            if ( $_GET['html'] == 1) {

                $zend_registry = Zend_Registry::get("config")->cms->config;
                $protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
                $domain = $zend_registry->domain;
                $basepath = $zend_registry->basepath;
                $basepath = "{$protocol}://{$domain}";
              //  $url = "{$protocol}://{$domain}{$basepath}/common/uploads/qrcode/{$loja_id}/{$id}-${image}";
                $url = "{$protocol}://{$domain}/common/uploads/qrcode/{$loja_id}/{$id}-${image}";
                $html = $this->buildHTMLHeader($basePath);
                $html .= $this->buildDownloadImageHTML($url);
                die($html);
            } else {
                $quoted = sprintf('"%s"', addcslashes(basename($file), '"\\'));
                $size = filesize($file);

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . $quoted);
                header('Content-Transfer-Encoding: binary');
                header('Connection: Keep-Alive');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . $size);
                readfile($file);
                exit;

            }
		}
		die();
	}


	public function saveEventAction()
	{

		$iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
		$iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
		$iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");

		//do something with this information
		if ($iPod || $iPhone || $iPad) {

		} else if ($Android) {

		}
	}

	public function buildEmail($src) {
		$html = '
			<html>
			    <head>
			        <meta charset="UTF-8">
			        <title></title>
			    </head>
			    <body style="width: 100%;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:16px;background-color:#1fc2bd;color: #fff; text-align: center;" topmargin="0"  bottommargin="0"  leftmargin="auto" rightmargin="auto">
					<table width="320px" border="0" cellpadding="0"  cellspacing="0" align="center"  style="width: 320px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0 auto">
						<tr>
							<td colspan="3" height="47">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td height="101" colspan="3">
							<img align="center" src="https://s3-sa-east-1.amazonaws.com/timo.com.br/emaillogo.png" style="display: block; margin: 0 auto; padding: 0;" width="112" height="101"/>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								<img align="center" src="' . $src . '" style="display: block; margin: 0 auto; padding: 0;" height="275" width="233"/>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align: center; line-height: 1.4;" colspan="3">
								No dia 22 de outubro, leve esse<br>
								<b>QR CODE</b>, seu CPF e uma<br>
								embalagem vazia de qualquer cosmético<br>
								à loja que você selecionou e ganhe<br>
								um <b>Hidratante Detox</b>.<br>
							</td>
						</tr>
			
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align: center; line-height: 1.4; font-size: 18px;" colspan="3"><b>#</b>PorUm<b>Mundo</b>Mais<b>Beauty</b></td>
						</tr>
					</table>
			    </body>
			</html>
		';
		return $html;
	}

	public function checkCpfAction () {
		try {
			$cpf = $this->_rawParams['cpf'];
			$modelUser = new Admin_Model_User();
			$selectUser = $modelUser->select();
			$selectUser->from(array('u' => 'user'), ['cpf']);
			$selectUser->where("u.cpf = '{$cpf}'");
			$selectUser->limit(1);
			$userRow = $modelUser->fetchRow($selectUser);
			$this->_return['used'] = is_null($userRow) ? FALSE: $userRow->toArray();

		} catch (Exception $er) {
			die(json_encode([
				'msg' => $er->getMessage()
			]));
		}
	}
}
