<?php

/**
 * Controlador do json
 *
 * @name JsonController
 */
class JsonController extends Cms_Controller_Api {

    /**
     *
     */
    public function init() {
        parent::init();
    }

    /**
     *
     *
     * @api {post} /json/enviar-estatistica Enviar estatistica
     * @apiParam {String[]} codigos códigos que o client Android recebeu.
     *
     * @apiGroup json 
     *
     * @apiSuccess {String} success indicador se foi executado com sucesso.
     *
     */

    public function enviarEstatisticaAction() {
        $return = array('success' => TRUE);
        try {
            $modelBrinde = new Admin_Model_Brinde();
            foreach($this->_rawParams['codigos'] as $key => $codigo) {
                $modelBrinde->update([
                    "validado"  => 1
                ], "codigo = {$codigo}");
            }
        } catch (Exception $e) {
            // Define o retorno
            $return['success'] = FALSE;
            $return['message'] = $e->getMessage();
        }

        $this->_helper->json($return);
    }

    public function readXlsAction () {
        require APPLICATION_PATH . '/../library/Cms/Library/PHPExcel.php';
        $n = 1;
        $xlsPATH = APPLICATION_PATH . "/../common/uploads/xls/lojas{$n}.xlsx";

        try {
            $inputFileType = PHPExcel_IOFactory::identify($xlsPATH);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($xlsPATH);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $modelEstado = new Admin_Model_Estado();
        $modelCidade = new Admin_Model_Cidade();
        $modelLojas = new Admin_Model_Loja();
        for ($row = 1; $row <= $highestRow; $row++){
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);


            if( $row > 1 ) {
                $rowData = $rowData[0];

                $abertura = $sheet->getCellByColumnAndRow(7, $row)->getFormattedValue();
                $fechamento = $sheet->getCellByColumnAndRow(8, $row)->getFormattedValue();

                $uf = $rowData[0];
                $cidade = $rowData[1];
                if($cidade === 'Piracibaba') {
                    $cidade = 'Piracicaba';
                }
                $shopping = $rowData[2];
                $endereco = $rowData[3];
                $piso = $rowData[4];
                $cep = $rowData[5];
                $telefone = $rowData[6];

                $estoque = $rowData[10];

                if ( strlen( $cidade ) > 0 ) {

                    $ufRow = $modelEstado->fetchRow("uf = \"{$uf}\"");
                    $cidadeRow = $modelCidade->fetchRow("estado_id = {$ufRow->estado_id} AND cidade = \"{$cidade}\"");

                    if(is_null($cidadeRow)) {
                        // ? :D
                        if( $cidade === "Taguatinga" ) {
                            $cidade_id = $modelCidade->insert([
                                'cidade'    => $cidade,
                                'estado_id'    => $ufRow->estado_id,
                            ]);
                        }
                    } else {
                        $cidade_id = $cidadeRow->cidade_id;
                    }

                    $where = [
                        "nome = \"{$shopping}\"",
                        "endereco = \"{$endereco}\"",
                        "telefone = \"{$telefone}\"",
                        "cidade_id = \"{$cidade_id}\"",
                    ];
                    $imploded_wheres = implode(" AND ", $where);
                    $lojaRow = $modelLojas->fetchRow($imploded_wheres);

                    $iframeurl = $rowData[9] . '&output=embed';

                    if(is_null($lojaRow)) {
                        $modelLojas->insert([
                            'nome'  => $shopping,
                            'endereco'  => $endereco,
                            'telefone'  => $telefone,
                            'cep'  => $cep,
                            'piso'  => $piso,
                            'cidade_id' => $cidade_id,
                            'estoque' => $estoque,
                            'email' => "sac{$row}@thebeautybox.com.br",
                            'senha' => password_hash("tamojunt0@b34uty@{$row}", PASSWORD_DEFAULT ),
                            'gmapIframe'    => $iframeurl,
                            "horarios_atendimento" => "2ª à Sábado das {$abertura} às {$fechamento}<br> Domingo das 14:00 às 20:00"
                        ]);
                    } else {
                        $iframeurl = $rowData[9] . '&output=embed';
                        $lojaRow->gmapIframe = $iframeurl;
                        $lojaRow->horarios_atendimento = "Horário de atendimento no dia da troca (22/10): das {$abertura} às {$fechamento}";
                        $lojaRow->save();
                    }
                }
            }
        }

        die('read');

    }
}
