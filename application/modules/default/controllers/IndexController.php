<?php

/**
 * Controlador do index
 * 
 * @name IndexControlle
 */
require_once(APPLICATION_PATH . '/../library/Cms/Mailgun/autoload.php');
use Mailgun\Mailgun;
class IndexController extends Zend_Controller_Action {
	/**
	 *
	 */

	private $mgKey = 'key-07331b0af7e8a29b6c1a911a6e1d002d';
	private $domain = 'semanadoabraco.com.br';

	public function init() {

		if ( !is_dir( APPLICATION_PATH . "/../common/resultado/") ) {
			mkdir( APPLICATION_PATH . "/../common/resultado/", 0777, TRUE );
		}
	}

	private function log ( $data, $name = '') {
		$time_stamp = date("Y-m-d H:i:s");
		$date = date("Y-m-d");
		$log_dir = APPLICATION_PATH . '/logs/';
		$filename = $date . '.log';
		if ( ! is_string( $data ) ) {
			$data = json_encode( $data, JSON_PRETTY_PRINT );
		}
		$log_id = uniqid();
		$pre = "======== START: {$log_id} ========" . PHP_EOL;
		$pre .= "========[Time: {$time_stamp}] | [IP: {$_SERVER['REMOTE_ADDR']}] | [PROXY: {$_SERVER['HTTP_X_FORWARDED_FOR']}]=====";
		$end = "======== END: {$log_id} ========" . PHP_EOL;
		$data = $pre . $data . $end;

		if ( strlen( $name ) > 0 ) {
			$log_dir = $log_dir . "{$name}/";
		}
		mkdir( $log_dir, 0777, TRUE );

		if ( is_writable( $log_dir ) ) {
			$log_path = $log_dir . $filename;
			file_put_contents( $log_path, $data, FILE_APPEND );
		}
	}

	private function allocDir ( $dir ) {
		if ( ! is_dir( $dir ) ) {
			try {
				mkdir( $dir, 0777, true );
			} catch( Exception $er ) {
				//nothing
			}
		}
	}

	private function encode( $data ) {
		$json = json_encode( $data );
		$inverse = strrev( $json );
		return base64_encode( $inverse );
	}

	private function decode( $data ) {
		$decoded = base64_decode( $data );
		$inverse = strrev( $decoded );
		return  json_decode( $inverse );
	}

	private function getBasePath ( ) {
		$basePath = "";
		if ( $_SERVER['HTTP_HOST'] === 'localhost') { //caso de env localhost/local.abraco
			$basePath  = 'http://localhost/local.abraco';
		}
		if ( $_SERVER['HTTP_HOST'] === '192.168.0.16') { //caso de env 192.168.0.16/local.abraco
			$basePath = 'http://192.168.0.16/local.abraco';
		}
		if ( preg_match('/homologacao/', $_SERVER['REQUEST_URI'] ) ) { //caso de env localhost/local.abraco
			$basePath  = 'http://www.semanadoabraco.com.br/homologacao';
		}
		return $basePath;
	}

	private function buildCupomHtml ( $basePath, $brindeRow, $userRow, $lojaRow, $cupom_path ) {
		$mailToParams = [
			'subject' => 'Olha a Promoção Semana do Abraço da The Beauty Box',
			'body'	=> 'Acesse o site http://www.semanadoabraco.com.br ! É só se cadastrar, escolher a loja e mostrar o código e você ganha a Loção Hidratante Hug Me Bombom de Baunilha 300ml'
		];

		$this->view->brinde = $brindeRow;
		$this->view->user = $userRow;
		$this->view->nome_loja = $lojaRow->nome;
		$this->view->cupomPath = $cupom_path;
		$this->view->mailTo = 'mailto:?' . http_build_query( $mailToParams );
		$this->view->downloadURL = $basePath . "/common/resultado/{$lojaRow->loja_id}_{$brindeRow['codigo']}.download.html";
		return $this->view->fetch( 'cupom-gerado.tpl' );
	}

	private function getResultadPath( $loja_id, $codigo ) {
		return APPLICATION_PATH . "/../common/resultado/{$loja_id}_{$codigo}.html";
	}

	/**
	 *
	 */

	public function indexAction() {
		$lojaModel = new Admin_Model_Loja();
		$selectLoja = $lojaModel->select();
		$selectLoja->from(['l' => 'loja'], [ 'estoque_brinde', 'loja_id' ]);
		$lojaRows = $lojaModel->fetchAll( $selectLoja );

		$modelBrinde = new Admin_Model_Brinde();
		$selectBrinde = $modelBrinde->select();
		$selectBrinde->from(['b' => 'brinde'], ['cupons' => 'count(*)', 'loja_id']);
		$selectBrinde->group('loja_id');
		$brindes = $modelBrinde->fetchAll( $selectBrinde );
		$mappedBrindes = [ ];
		$combinedMap = [];
		$restante = []; //debug
		$sem_estoque = TRUE;
		foreach( $brindes as $key => $brinde ) {
			if ( ! isset( $mappedBrindes[ $brinde->loja_id ] ) ) {
				$combinedMap[ $brinde->loja_id ] = [];
			}
			$combinedMap[ $brinde->loja_id ]['brinde'] = $brinde->toArray();
		}
		foreach( $lojaRows as $key => $lojaRow ) {
			$combinedMap[ $lojaRow->loja_id ]['loja'] = $lojaRow->toArray();
		}
		foreach( $combinedMap as $combinedMapItem ) {
			if ( $combinedMapItem[ 'loja' ]['estoque_brinde'] > $combinedMapItem[ 'brinde' ]['cupons'] ) {
				$sem_estoque = FALSE;
			}
		}

		if ( $sem_estoque ) {
			//$esgotado = $this->view->fetch('esgotado.tpl');
			$this->_helper->_layout->setLayout('esgotado');
			//die( $esgotado );
		}

		if ( ! is_null( $this->_request->getParam('abracometro') ) ) {
			$modelBrinde = new Admin_Model_Brinde();
			$select = $modelBrinde->select();
			$select->from(['b' => 'brinde'], [ 'count' => "count(brinde_id)"]);
			$brindeRow = $modelBrinde->fetchRow( $select );
			if ( !is_null( $brindeRow ) ) {
//				die(json_encode([ 'count' =>  999999 ]));
				die(json_encode([ 'count' =>  $brindeRow->count ]));
			} else {
				die(json_encode([ 'count' =>  0 ]));
			}
		}
	}

	public function promocaoAction () {

	}

	public function duvidasAction () {

	}

	public function sustentabilidadeAction () {

	}
	public function participeAction () {
		if ( $this->_request->isPost() ) {
			$default_cpf = $this->_request->getParam('cpf');
			$cpf = addslashes( preg_replace('/\D/', '', $default_cpf ) );
			$modelBrinde = new Admin_Model_Brinde();
			$brindeRow = $modelBrinde->fetchRow("`cpf` = '{$cpf}' OR REPLACE(REPLACE(cpf, '.', ''), '-', '') = '{$cpf}'");
			if ( is_null( $brindeRow ) ) {
				$url = $this->view->url( [], 'cadastre');
				$parts = explode('/' ,$url );
				$path = end($parts);
				$this->redirect( $path );
			} else {
				$url = $this->view->url( [], 'participe') . '?used=' . $cpf;
				$parts = explode('/' ,$url );
				$path = end($parts);
				$parts_cupom = explode("/", $brindeRow->path );
				$name = str_replace('.png', '.html', $parts_cupom[ sizeof( $parts_cupom ) - 1 ]);

				$modelUser = new Admin_Model_User();
				$modelLoja = new Admin_Model_Loja();

				$userRow = $modelUser->fetchRow("user_id = {$brindeRow->user_id}");
				$lojaRow = $modelLoja->fetchRow("loja_id = {$brindeRow->loja_id}");
				$basePath = $this->getBasePath();
				$cupom_path = $basePath . $brindeRow->path;
				$output = $this->buildCupomHtml( $basePath, $brindeRow, $userRow, $lojaRow, $cupom_path );

				$filename = $this->getResultadPath( $lojaRow->loja_id, $brindeRow->codigo );
				$downloadName = str_replace( '.html', '.download.html', $filename );
				$downloadHtml = $this->view->fetch( 'download-cupom-gerado.tpl' );
				file_put_contents( $downloadName, $downloadHtml );
				file_put_contents( $filename, $output );

				$loja_id = $brindeRow->loja_id;
				$path = "/common/resultado/{$loja_id}_{$name}";
				$this->redirect( $path );
			}
		}
	}
	public function cadastreAction() {
		if ( $this->_request->isPost() ) {
			$dados = $this->_request->getParams();
			unset( $dados['module'] );
			unset( $dados['controller'] );
			unset( $dados['action'] );
			$nome = $dados['abraco_nome'];
			$email = $dados['abraco_email'];

			if ( preg_match('/semanadoabraco/', $_SERVER['HTTP_HOST'] ) ) { //caso de env localhost/local.abraco
				$this->view->basePath  = 'http://www.semanadoabraco.com.br';
			}

			if ( preg_match('/homologacao/', $_SERVER['REQUEST_URI'] ) ) { //caso de env localhost/local.abraco
				$this->view->basePath  = 'http://www.semanadoabraco.com.br/homologacao';
			}


			$nome_parts = explode( " ", $dados['nome'] );
			$first_name = reset( $nome_parts );
			$this->view->nome = $first_name;
			$html = $this->view->fetch( 'email-amiga.tpl' );
			$subject = $first_name . ' compartilhou um abraço com você';
			$this->email( $nome, $email, $subject, $html );

			$encoded = $this->encode( $dados );
			$url = $this->view->url( [], 'escolhe' );
			$parts = explode('/' ,$url );
			$path = end($parts);
			$this->redirect( "{$path}?token={$encoded}" );
			exit;
		}
	}
	private function isChecked ( $field ) {
		$result = 0;
		if ( ! is_null ( $field ) ) { //tá setado
			if ( $field == 1 || $field == 'on') {
				$result = 1;
			}
		}
		return $result;
	}
	public function escolheAction() {
		$datas = $this->_request->getParams();
		$dados = $this->decode( $datas['token'] );
		if ( $this->_request->isPost() ) {
			//executar build da imagem, e HTML estatico.
			$dados = ( array ) $dados;
			$modelLoja = new Admin_Model_Loja();
			$lojaRow = $modelLoja->fetchRow("loja_id = '{$datas['store']}'");
			if ( ! is_null( $lojaRow ) ) {
				//redirect para referrer com err.
			}

			$cupomDatas = $this->montarCupom( $lojaRow );
			$cupom_path = $cupomDatas[ 'path' ];
			//montar user, brinde e insert.
			$modelUser = new Admin_Model_User();
			$db = $modelUser->getDefaultAdapter();
			$db->beginTransaction();
			$modelBrinde = new Admin_Model_Brinde();
			try {
				$paramsInsertUser = [
					'nome'				=> $dados['nome'],
					'cpf'				=> $dados['cpf'],
					'email'				=> $dados['email'],
					'nascimento'		=> $dados['nascimento'],
					'cep'				=> $dados['cep'],
					'endereco'			=> $dados['endereco'],
					'user_cidade'		=> $dados['cidade'],
					'user_estado'		=> $dados['estado'],
					'celular'			=> $dados['celular'],
					'facebook_id'		=> '',
					'clube_beauty'		=> $this->isChecked( $dados['clube_beauty'] ),
					'social_networks'	=> $this->isChecked( $dados['social_networks'] ),

					'abraco_nome'	=> $dados['abraco_nome'],
					'abraco_email'	=> $dados['abraco_email']
				];
				$userRow = $modelUser->createUser( $paramsInsertUser );
				$brindeRow = [
					'path'		=> $cupom_path,
					'user_id'	=> $userRow['user_id'],
					'loja_id'	=> $lojaRow->loja_id,
					'codigo'	=> $cupomDatas['codigo'],
					'cpf' 		=> $dados['cpf']
				];
				$brindeRow['brinde_id'] = $modelBrinde->insert( $brindeRow );

				$basePath = $this->getBasePath();
				$cupom_path = $basePath . $cupom_path;
				$output = $this->buildCupomHtml( $basePath, $brindeRow, $userRow, $lojaRow, $cupom_path );
				$downloadHtml = $this->view->fetch( 'download-cupom-gerado.tpl' );
				$filename = $this->getResultadPath( $lojaRow->loja_id, $brindeRow['codigo'] );
				$downloadName = str_replace( '.html', '.download.html', $filename );
				file_put_contents( $downloadName, $downloadHtml );
				file_put_contents( $filename, $output );
				$db->commit();
				$this->redirect( "/common/resultado/{$lojaRow->loja_id}_{$brindeRow['codigo']}.html" );
			} catch ( Exception $err) {
				$message = $err->getMessage();
				$log = [
					'action'	=> 'escolhe',
					'data'		=> $datas,
					'hashDatas'	=> $dados,
					'message'	=> $message
				];
				$this->log( $log, 'cpf_escolhe');

				$url = $this->view->url( [], 'participe') . '?used=' . $dados['cpf'];
				$parts = explode('/' ,$url );
				$path = end($parts);
				$this->redirect( $path );
			}
		}
	}
	public function cupomGeradoAction() {
		$dados = $this->_request->getParams();
		$filename = APPLICATION_PATH . "/../resultado/{$dados['loja_id']}_{$dados['code']}.html";

		if ( ! file_exists( $filename ) ) {
			//$cupomPath = "{$dados['loja_id']}/{$dados['code']}";
			//$this->view->setCaching( TRUE )
		}
	}
	public function participantesAction () {

	}
	public function regulamentoAction () {

	}
	public function contatoAction () {

	}
	public function cupomBuildSkinAction ( ) {
		try {
			header("Content-Type: image/png");
			$logo = APPLICATION_PATH  . '/../common/default/images/site/cupom/logo-v3.md.png';
			$skin = APPLICATION_PATH  . '/../common/default/images/site/cupom/frame.png';
			$bold = APPLICATION_PATH  . '/../common/default/fonts/opensans-bold-webfont.ttf';
			$im = imagecreatefrompng( $skin );
			imagealphablending($im, true);
			imagesavealpha($im, true);
			$logo = imagecreatefrompng( $logo );
			imagealphablending($logo, false);
			imagesavealpha($logo, true);
			imagecolortransparent($im, imagecolorallocate($im, 0,0,0));
			imagecolortransparent($logo, imagecolorallocate($logo, 0,0,0));
			$color = imagecolorallocate($im, 252, 244, 169);

			$image_width = imagesx($im) - 14; //sombra
			$image_height = imagesy($im);

			$ratio = 0.55;
			$logo_height = imagesy($logo);
			$logo_width = imagesx($logo);

			$new_logo_width = $logo_width * $ratio;
			$new_logo_height = $logo_height * $ratio;
			$dst_x = ( $image_width - $new_logo_width ) / 2;
			$dst_y = 20;
			$dst_w = $new_logo_width;
			$dst_h = $new_logo_height;

			$src_x = 0;
			$src_y = 0;
			$src_w = $logo_width;
			$src_h = $logo_height;
			imagecopyresampled($im, $logo, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

			$dot = '. . . . . . . . . . . . . . . . . . . . . . . . . . .';
			$font_size_dot = 7;
			$dotY = $image_height * 0.42;

			$dot_text_box = imagettfbbox($font_size_dot, 0, $bold, $dot );
			$dot_text_width = $dot_text_box[2]-$dot_text_box[0];
			$dotx = ($image_width/2) - ($dot_text_width/2);

			imagettftext($im, $font_size_dot, 0, $dotx, $dotY, $color, $bold, $dot );

			$save = APPLICATION_PATH  . "/../common/default/images/cupom-combined.png" ;
			imagepng( $im, $save );
			imagepng( $im );
			imagedestroy( $im );
			header( "Content-type: image/png" );
		}catch( Exception $er ) {
			die($er->getMessage());
		}
		exit;
	}

	public function montarCupom ( $lojaRow ) {

		$lojasDir = "/common/uploads/cupom/{$lojaRow->loja_id}/";
		$cupomDir = APPLICATION_PATH . "/..{$lojasDir}";
		$this->allocDir( $cupomDir );

		$skin = APPLICATION_PATH  . '/../common/default/images/cupom-combined.png';
		$regular = APPLICATION_PATH  . '/../common/default/fonts/opensans-regular-webfont.ttf';
		$bold = APPLICATION_PATH  . '/../common/default/fonts/opensans-bold-webfont.ttf';
		$im = imagecreatetruecolor( 347, 410 );
		$im = imagecreatefrompng( $skin );
		imagealphablending($im, true);
		imagesavealpha($im, true);

		$font_size_code = 30;
		$font_size = 10;

		$angle = 0;
		$color = imagecolorallocate($im, 252, 244, 169);
		$white = imagecolorallocate($im, 255, 255, 255);
		$duplicatedName = TRUE;
		while ( $duplicatedName ) {
			$code = Cms_Hash::genCode( $lojaRow->loja_id );
			$cupom = substr($code, 0, 3) . '-' . substr($code, 3, 3) . '-' . substr($code, 6, 3);
			$cupomFileName = $cupom . '.png';
			$cupomFilePath = $cupomDir . $cupomFileName;
			$duplicatedName = file_exists( $cupomFilePath );
		}

		// Get Bounding Box Size
		$code_text_box = imagettfbbox($font_size_code, $angle, $bold, $cupom );

		$image_width = imagesx( $im ) - 14;//sombra
		$image_height = imagesy( $im );

		$codeY = $image_height * 0.365;
		// recueprar a largura
		$text_width = $code_text_box[2]-$code_text_box[0];

		// Calculo das coords
		$x = ($image_width/2) - ($text_width/2);
		imagettftext($im, $font_size_code, $angle, $x, $codeY, $color, $bold, $cupom );
		$texts = [
			"Apresente este cupom na loja do",
			"<b>{$lojaRow->nome}</b> entre",
			"os dias 15/05 a 22/05 para receber uma",
			"<b>Loção Hidratante Hug Me Bombom</b>",
			"<b>de Baunilha 300ml.</b>"
		];
		$baseY = $image_height * 0.48;

		$word_spacing = 15;
		foreach( $texts as $key => $text) {
			$lineY = $baseY + ( 19 * $key );
			$boldPos = strpos( $text, '<b>' );
			if ( is_bool( $boldPos ) ) {
				$text_box = imagettfbbox($font_size, $angle, $regular, $text );
				$text_width = $text_box[2]-$text_box[0];
				$xText = ($image_width/2) - ($text_width/2);
				imagettftext($im, $font_size, $angle, $xText, $lineY, $white, $regular, $text );
			} else {
				//por sorte, sempre bold e depois regular, nunca misturado. caso misturar, implementar melhoria.
				$boldFinish = strpos( $text, '</b>' );
				$boldText = substr($text, $boldPos + 3, $boldFinish - 3 );
				$regularText = substr($text, $boldFinish + 4 );
				if (  strlen( $regularText ) > 0  ) {
					$boldBox = imagettfbbox($font_size, $angle, $bold, $boldText );
					$regularBox = imagettfbbox($font_size, $angle, $regular, $regularText );

					$boldWidth 		= $boldBox[2]		-	$boldBox[0];
					$regularWidth 	= $regularBox[2]	-	$regularBox[0];

					$totalTextBoxWidth = $boldWidth + $regularWidth + $word_spacing;
					$xText1 = ($image_width/2) - ($totalTextBoxWidth/2);
					$xText2 = $xText1 +  $boldWidth;

					imagettftext($im, $font_size, $angle, $xText1, $lineY, $white, $bold, $boldText );
					imagettftext($im, $font_size, $angle, $xText2, $lineY, $white, $regular, $regularText );
				} else { //somente bold
					$boldBox = imagettfbbox($font_size, $angle, $bold, $boldText );
					$boldWidth 		= $boldBox[2]		-	$boldBox[0];
					$xText = ($image_width/2) - ($boldWidth/2);
					imagettftext($im, $font_size, $angle, $xText, $lineY, $white, $bold, $boldText );
				}
			}

		}
		imagepng( $im, $cupomFilePath );
		imagedestroy($im);
		return [
			'codigo' => $cupom,
			'path' => $lojasDir . $cupomFileName
		];
	}

	public function cupomAction ( $loja_id = 22 ) {
		$r = $this->montarCupom( $loja_id );
	}

	private function enviarCupom ( $name, $email, $src, $_html = '') {
		$subject = 'Semana do Abraço by TheBeautyBox.com.br';
		$html = strlen( $_html ) > 0 ? $_html : $this->buildEmail( $src );
		$this->email( $name, $email, $subject, $html );
		if ( strlen( $_html ) < 1 ) {
			die(json_encode([
				'message' => 'Mensagem enviado com sucesso ! ',
			]));
		}
	}

	private function checkReferrer ( $throwError = TRUE ) {
		$parts = explode('/', $_SERVER['HTTP_REFERER']);
		$last = end( $parts );
		if ( preg_match('/\.html/', $last ) ) {
			$code = substr( preg_replace( '/\.html/', '', $last ), 4, 11 );
			return $code;
		} else {
			if ( $throwError ) {
				header("HTTP/1.1 401 Bad Request");
				echo json_encode([ 'error' => 'codigo invalido']);
				exit;
			}
			return FALSE;
		}
	}
	private function getProtocol( ) {
		$isSecure = false;
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
			$isSecure = true;
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
			$isSecure = true;
		}
		$REQUEST_PROTOCOL = $isSecure ? 'https' : 'http';
		return $REQUEST_PROTOCOL;
	}
	private function getBrindeFromCode ( $code, $throwError = TRUE ) {
		$modelBrinde = new Admin_Model_Brinde();
		$select = $modelBrinde->select();
		$select->from(['b' => 'brinde'], ['codigo', 'cpf', 'path']);
		$select->join(['u' => 'user'], 'u.user_id = b.user_id', [ 'nome', 'email' ]);
		$select->where( "codigo = '{$code}'" );
		$select->setIntegrityCheck( FALSE );
		$brinde = $modelBrinde->fetchRow( $select );
		if ( is_null( $brinde ) && $throwError ) {
			header("HTTP/1.1 401 Bad Request");
			echo json_encode([ 'error' => 'codigo invalido']);
			exit;
		}
		return $brinde;
	}

	public function sendSmsAction ( ) {
		$code = $this->checkReferrer( );
		if ( $code ) {
			$brinde = $this->getBrindeFromCode( $code );
			$numero = $this->_request->getParam('numero');
			$protocol = $this->getProtocol();
			$host = '//' . $this->view->basePath;
			$host = preg_replace('/\/\/\//', '//', $host);
			$basePath = $protocol . ':' . $host;
			$filepath = $basePath . $brinde->path;
			if ( ! class_exists( 'Cms_SMS' )) {
				require_once( APPLICATION_PATH . '/../library/Cms/Sms.php' );
			}
			$sms = new Cms_SMS('sms_espalhe', '123456');
			$respo = $sms->envie_sms($numero, "Código: {$brinde->codigo} | CPF: {$brinde->cpf} ", "TheBeautyBox");
			die(json_encode($respo));
		}
	}

	public function sendEmailAction ( ) {
		$code = $this->checkReferrer( );
		if ( $code ) {
			$brinde = $this->getBrindeFromCode( $code );
			if ( $_SERVER['HTTP_HOST'] === '54.207.76.236') {
				$image = 'http://54.207.76.236' . $brinde->path;
			} else if ( preg_match('/homologacao/', $_SERVER['REQUEST_URI'] ) ) { //caso de env localhost/local.abraco
				$image = 'http://www.semanadoabraco.com.br/homologacao'. $brinde->path;
			} else {
				$image = 'http://www.semanadoabraco.com.br' . $brinde->path;
			}


			$this->enviarCupom( $brinde->nome, $brinde->email, $image);
		}
	}

	public function facebookShareAction() {
		//criar tabela para este count.
		//coluna de acordo com as responses do Facebook.
		$code = $this->checkReferrer( );
		if ( $code ) {
			$modelUser = new Admin_Model_User();
			$select = $modelUser->select();
			$select->from(['u' => 'user'], [ 'user_id' ]);
			$select->join(['b' => 'brinde'], 'u.user_id = b.user_id', []);
			$select->where( "codigo = '{$code}'" );
			$select->setIntegrityCheck( FALSE );
			$userRow = $modelUser->fetchRow( $select );
			if ( (!is_null( $userRow )) && (is_numeric( $userRow->user_id )) ) {
				$modelUser->update([
					'shared_facebook' => 1
				], "user_id = '{$userRow->user_id}'" );
			}
			exit;
		}



	}

	public function buildEmail( $src = '' ) {
		if ( strlen( $src ) < 1 )  {
			$src = '';
		}
		$html = '
			<html>
			    <head>
			        <meta charset="UTF-8">
			        <title></title>
			    </head>
			    <body style="width: 100%;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:16px;background-color:#9772ac;color: #fff; text-align: center;" topmargin="0"  bottommargin="0"  leftmargin="auto" rightmargin="auto">
					<table width="320px" border="0" cellpadding="0"  cellspacing="0" align="center"  style="width: 320px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0 auto">
						<tr>
							<td colspan="3" height="47">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td height="101" colspan="3">
							<img align="center" src="http://entreinaweb.com/teste-rafa/logo-tbb-v2.png" style="display: block; margin: 0 auto; padding: 0;" width="142" height="59"/>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								<img align="center" src="' . $src . '" style="display: block; margin: 0 auto; padding: 0;" height="410" width="347"/>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align: center; line-height: 1.4;" colspan="3">
								<font face="Arial">Entre os dias 15/5 e 22/5, apresente este
								<b>CÓDIGO</b> e o seu CPF na loja<br><span style="color: #fff;"> que você selecionou e ganhe
								uma <b><br>
								Loção Hidratante HugMe Bombom de Baunilha 300ml</b>.<br></span>
								</font>
							</td>
						</tr>
			
						<tr>
							<td colspan="3" height="40">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align: center; line-height: 1.4; font-size: 18px;" colspan="3"><b>#abracenaBeauty</b></td>
						</tr>
					</table>
			    </body>
			</html>
		';
		return $html;
	}


	/* @TODO - implementar Email MKT */
	public function shareEmailAction ( ) {
		$subject = 'Semana do Abraço by TheBeautyBox.com.br';
		$email = $this->_request->getParam('email');
		$email = filter_var( $email, FILTER_VALIDATE_EMAIL );
		if ( $email ) {
			$name = $this->_request->getParam('name');
			$this->doShareEmail( $name, $email );
		} else {
			header("HTTP/1.0 400 Bad Request");
			echo json_encode([ 'message' => 'E-mail inválido']);
			exit;
		}
	}

	public function doShareEmail( $name, $email ) {
		$subject = 'Semana do Abraço by TheBeautyBox.com.br';
		if ( $email ) {
			$html = '<h1> Conteúdo do E-mail para Compartilhar</h1>';
			$this->email( $name, $email, $subject, $html );
			die(json_encode(['message' => "E-mail enviado com sucesso ! "]));
		} else {
			header("HTTP/1.0 400 Bad Request");
			echo json_encode([ 'message' => 'E-mail inválido']);
			exit;
		}
	}

	private function email ( $nameTo, $emailTo, $subject, $html ) {
		$mail = new Cms_Mail();
		$mail->addTo($emailTo, $nameTo);
		$mail->setSubject( $subject );
		$mail->setBodyHtml($html);
		$mail->send();
	}
}

