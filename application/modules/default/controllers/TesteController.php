<?php

/**
 *
 */
class TesteController extends Zend_Controller_Action {

    /**
     *
     */

    private $mgKey = 'key-07331b0af7e8a29b6c1a911a6e1d002d';
    private $domain = 'semanadoabraco.com.br';

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction(){
        echo Cms_Hash::genCode(7);
        exit;
    }

    private function MailGunOne ( ) {
        $postParam = [
            'from' => 'TheBeautyBox <sacdabeauty@thebeautybox.com.br>',
            'to' => "Rafael <yukioxd@gmail.com>",
            'subject' => 'Semana do Abraço by TheBeautyBox.com.br',
            'html' => '<table><tr><td><h1><a href="http://semanadoabraco.com.br">verificando mailgun</a></h1></td></tr></table>'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $this->mgKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v2/{$this->domain}/messages");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParam );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function MailGunTwo ( ) {
        $postParam = [
            'from' => 'TheBeautyBox <sacdabeauty@thebeautybox.com.br>',
            'to' => "Rafael <yukioxd@gmail.com>",
            'subject' => 'Semana do Abraço by TheBeautyBox.com.br',
            'html' => '<table><tr><td><h1><a href="http://semanadoabraco.com.br">verificando mailgun</a></h1></td></tr></table>'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $this->mgKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v2/{$this->domain}/messages");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParam );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


    public function testeEmailAction() {
        $r = $this->MailGunTwo();
        var_dump( $r );
        die();
    }

}
