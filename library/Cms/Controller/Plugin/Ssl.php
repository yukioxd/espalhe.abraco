<?php
/**
 * Classe para redirecionamento SSL
 * 
 * @name Cms_Controller_Plugin_Ssl
 */
class Cms_Controller_Plugin_Ssl extends Zend_Controller_Plugin_Abstract {
	/**
	 * Armazena se tem suporte ao ssl
	 * 
	 * @access protected
	 * @name $_ssl
	 * @var boolean
	 */
	protected $_ssl;
	
	/**
	 * Método executado antes de executar a tela
	 *
	 * @name preDispatch
	 * @param Zend_Controller_Request_Abstract $request Objeto de requisição da tela
	 */
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
		// Verifica se é um ajax para nao dar redirect
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower ($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest") {
			// Retorna a ação na tela
			return $request;
		}
		
		// Verifica se é ambiente de desenvolvimento
		if(APPLICATION_ENV == "development") {
			// Retorna a ação na tela
			return $request;
		}
		
		// Armazena o nome do acesso
		$module = $request->getModuleName();
		$controller = $request->getControllerName();
		$action = $request->getActionName();
		
		// Verifica se o controller é o error
		if($controller == "error") {
			// Retorna a ação na tela
			return $request;
		}
		
		// Inicia o ssl
		$ssl = FALSE;
		
		// Verifica se a pagina acessada possui SSL
		if(($module == "default") && ($controller == "checkout")) {
			$ssl = TRUE;
		}

		if(($module == "default") && ($controller == "delivery") && ($action == "delivery")) {
			$ssl = TRUE;
		}

		if(($module == "default") && ($controller == "delivery") && ($action == "pagamento")) {
			$ssl = TRUE;
		}

		if(($module == "default") && ($controller == "delivery") && ($action == "pagar")) {
			$ssl = TRUE;
		}
		
		if(($module == "default") && ($controller == "clientes") && ($action == "cadastro")) {
			$ssl = TRUE;
		}
		
		if(($module == "default") && ($controller == "clientes") && ($action == "login")) {
			$ssl = TRUE;
		}
		
		if(($module == "default") && ($controller == "listapresentes") && ($action == "index")) {
			$ssl = TRUE;
		}
		
		if(($module == "default") && ($controller == "remotes")) {
			$ssl = FALSE;
		}
		
		// Verifica se precisa dar redirect
		if($ssl) {
			// Verifica se não está em https
			if(!isset($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS']) != "on") {
				// Redireciona o usuário para o https
				header("Location: https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
				exit();
			}
		}
		else {
			if(isset($_SERVER['HTTPS'])) {
				// Redireciona para a pagina normal
				header("Location: http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
				exit();
			}
		}
		
		// Retorna a ação na tela
		return $request;
	}
}
