<?php

/**
 * Cria o plugin para carregar informações para todo o site
 *
 * @name Cms_Controller_Plugin_Geral
 */
class Cms_Controller_Plugin_Geral extends Zend_Controller_Plugin_Abstract {
	/**
	 * Método da classe
	 * 
	 * @name preDispatch
	 */
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
		// Busca o view
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper("viewRenderer");
		$viewRenderer->initView();
		$view = $viewRenderer->view;
                
                $front  = Zend_Controller_Front::getInstance();
                $view->openedController = $front->getRequest()->getControllerName();
		
		if(substr($_SERVER['REQUEST_URI'], strrpos($_SERVER['REQUEST_URI'], ".")) == ".jpg") {
			return TRUE;
		}
		
		if($this->_request->getParam("module") == "admin") {
			$session = new Zend_Session_Namespace('painel');
			$view->painel = $session;
			return TRUE;
		}
	}

}
