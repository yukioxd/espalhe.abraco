<?php

/**
 * Cria o plugin do metas
 * 
 * @name Cms_Controller_Plugin_Metas
 */
class Cms_Controller_Plugin_Metas extends Zend_Controller_Plugin_Abstract {
	/**
	 * Método da classe
	 * 
	 * @name includejs
	 */
	public function preDispatch (Zend_Controller_Request_Abstract $request) {
		// Busca o view renderer
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper("viewRenderer");
		$viewRenderer->initView();
		$view = $viewRenderer->view;
		
		// Armazena o controller e o action
		$controller = (string)$request->getControllerName();
		$action = (string)$request->getActionName();
		
		// Verifica se é o detalhamento do produto
		if(($controller == "produtos") && ($action == "produto")) {
			// Cria o model dos produtos
			$model = new Admin_Model_Produtos();
				
			// Busca o produto
			$idproduto = $this->_request->getParam("idproduto", 0);
			$row = $model->fetchRow(array('idproduto = ?' => $idproduto));
				
			// Cria a meta do produto
			if($row !== NULL) {
				$view->headTitle($row->titulo);
				$view->headMeta()->setName("description", $row->meta_description);
				$view->headMeta()->setName("keywords", $row->meta_tags);
			}
		}
		// Verifica se é a listagem de categorias
		else if(($controller == "produtos") && ($action == "index")) {
			// Busca a categoria
			$idcategoria_produto = $this->_request->getParam("idcategoria_produto", 0);
			if($idcategoria_produto > 0) {
				// Cria o model dos produtos
				$model = new Admin_Model_Categoriasprodutos();
					
				// Busca a categoria
				$row = $model->fetchRow(array('idcategoria_produto = ?' => $idcategoria_produto));
					
				// Cria a meta do produto
				$view->headTitle($row->titulo);
				$view->headMeta()->setName("description", $row->meta_description);
				$view->headMeta()->setName("keywords", $row->meta_tags);
			}
		}
		
		// Busca as informações das metas
		try {
			$metas = new Zend_Config_Ini(APPLICATION_PATH . "/configs/metas.ini", $controller);
		}
		catch(Exception $e) {
			return FALSE;
		}
		
		// Verifica se existe a sessão das metas
		if($metas->$action !== NULL) {
			// Adiciona as meta tags
			$view->headTitle($metas->$action->title);
			$view->headMeta()->setName("description", $metas->$action->description);
			$view->headMeta()->setName("keywords", $metas->$action->keywords);
		}
	}
}
