<?php

/**
 * Cria o plugin do commons
 * 
 * @name Cms_Controller_Plugin_Commons
 */
class Cms_Controller_Plugin_Commons extends Zend_Controller_Plugin_Abstract {
	/**
	 * Método da classe
	 * 
	 * @name includejs
	 */
	public function preDispatch (Zend_Controller_Request_Abstract $request) {
		// Busca o view renderer
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper("viewRenderer");
		$viewRenderer->initView();
		$view = $viewRenderer->view;
		
		// Recupera o nome do modulo
		$module = strtolower($request->getParam("module", "default"));
		
		// Busca o basepath
		$options = Zend_Registry::get("config");
		$basePath = $options->cms->config->basepath;
		
		// Busca os arquivos css do commons
		$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/commons.ini", "css");
		
		// Percorre os arquivos
		foreach($config->$module as $file) {
			if(strpos($file, "http://") === FALSE) {
				// Troca a palavra chave do modulo
				$file = str_replace(":module", $module, $file);
				
				// Adiciona o arquivo
				$view->headLink()->appendStylesheet($basePath . "/" . $file);
			}
			else {
				// Adiciona o arquivo
				$view->headLink()->appendStylesheet($file);
			}
		}
		
		// Busca os arquivos js do commons
		$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/commons.ini", "js");
		
		// Percorre os arquivos js
		foreach($config->$module as $file) {
			if((strpos($file, "http://") !== FALSE) || (strpos($file, "https://") !== FALSE)) {
				// Adiciona o arquivo
				$view->headScript()->appendFile($file);
			}
			else {
				// Troca a palavra chave do modulo
				$file = str_replace(":module", $module, $file);
				
				// Adiciona o arquivo
				$view->headScript()->appendFile($basePath . "/" . $file);
			}
		}
		
		// Busca os titulos do commons
		$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/commons.ini", "title");
		
		// Percorre os arquivos js
		$view->headTitle($config->$module)->setSeparator(" - ");
	}
}
