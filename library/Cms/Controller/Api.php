<?php

/**
 * Controlador do json
 *
 * @name JsonController
 */
class Cms_Controller_Api extends Zend_Controller_Action
{
	/**
	 *
	 */

	public $_rawParams = [];

	public $_params = [];

	public $_return = ['success' => TRUE];

	public function init()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		$this->_setupParams();
	}

	private function isAssoc($arr)
	{
		return json_encode(array_keys($arr)) !== json_encode(range(0, sizeof($arr) - 1));
	}
	private function unNull($params) {
		$newArray = [];

		foreach ($params as $key => $param) {
			if(is_array($param) && $this->isAssoc($param)) {
				$newParam = $this->unNull($param);
			} else if( is_null($param) ){
				$newParam = '';
			} else {
				$newParam = $param;
			}
			$newArray[$key] = $newParam;
		}
		return $newArray;
	}

	public function validVar($var) {
		return (!is_null($var) && (strlen($var) > 0) && $var != '0');
	}

	private function _setupParams()
	{
        $body = $this->getRequest()->getRawBody();
        if ( $body ) {
            $dados = Zend_Json::decode($body);
        } else {
            $dados = [];
        }
		$this->_rawParams = $dados;
		$this->_params = $this->_request->getParams();
	}

	public function postDispatch()
	{
		$output = $this->unNull($this->_return);
		$this->_helper->json($output);
	}
}
