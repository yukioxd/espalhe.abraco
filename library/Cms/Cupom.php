<?php

class Cms_Cupom {
	
	/**
	 * Adiciona um novo cupom
	 * 
	 * @name addNew
	 * @param Array $oferta Array com as informações da compra
	 */
	static public function addNew($oferta, $idpedido) {
		$cliente = new Zend_Session_Namespace("cliente");
		
		// Cria o model dos itens do pedido
		$model_itens = new Admin_Model_Pedidositens();
		$model = new Admin_Model_Cupons();
		
		// Verifica se possui presente
		//if($oferta['presente'] == FALSE) {
			// Quantidade de cupons para gerar
			for($i = 1; $i <= $oferta['quantidade']; $i++) {
				// Monta array do cupom
				$data = array(
					'idoferta_ativacao' => $oferta['row']->idoferta_ativacao,
					'codigo' => 0,
					'data_processamento' => date("Y-m-d H:i:s"),
					'nome_contemplado' => $cliente->nome,
					'email_contemplado' => $cliente->email,
					'idcliente' => $cliente->idcliente
				);
				
				// Adiciona o cupom
				try {
					$idcupom = $model->insert($data);
				}
				catch(Exception $e) {
					throw new Exception($e->getMessage());
				}
				
				// Cria o código do cupom
				$codigo = $idcupom . date("His");
				for($w = strlen($codigo); $w < 10; $w++) {
					$codigo = $codigo . rand(0, 9);
				}
				
				try {
					$model->update(array('codigo' => $codigo), array('idcupom = ?' => $idcupom));
				}
				catch(Exception $e) {
					throw new Exception($e->getMessage());
				}
				
				// Armazena o item do pedido
				$data_item = array(
					'idpedido' => $idpedido,
					'identificador' => $idcupom
				);
				
				$model_itens->insert($data_item);
			}
			
		//}else{
			
			if(count($oferta['contemplado'])){
				foreach($oferta['contemplado'] as $index_presente => $presente){
					$ofertas[$index]['contemplado'][$index_presente] = array(
							'qtd' => $presente['qtd'],
							'nome' => $presente['nome'],
							'email' => $presente['email']
					);
					
					for($i = 1; $i <= $presente['qtd']; $i++) {
						// Monta array do cupom
						$data = array(
							'idoferta_ativacao' => $oferta['row']->idoferta_ativacao,
							'codigo' => 0,
							'data_processamento' => date("Y-m-d H:i:s"),
							'nome_contemplado' => $presente['nome'],
							'email_contemplado' => $presente['email'],
							'idcliente' => $cliente->idcliente
						);
						
						// Adiciona o cupom
						try {
							$model = new Admin_Model_Cupons();
							$idcupom = $model->insert($data);
						}
						catch(Exception $e) {
							throw new Exception($e->getMessage());
						}
						
						// Cria o código do cupom
						$codigo = $idcupom . date("His");
						for($w = strlen($codigo); $w < 10; $w++) {
							$codigo = $codigo . rand(0, 9);
						}
						
						try {
							$model->update(array('codigo' => $codigo), array('idcupom = ?' => $idcupom));
						}
						catch(Exception $e) {
							throw new Exception($e->getMessage());
						}
						
						// Armazena o item do pedido
						$data_item = array(
								'idpedido' => $idpedido,
								'identificador' => $idcupom
						);
						
						$model_itens->insert($data_item);
					}
				}
			}
		//}

		//return $idcupom;
	}
	
	/**
	 * Verifica se os cupons podem ser comprados
	 * 
	 * @name verifyCart
	 * @param array $idofertas Array com o ai das ofertas
	 * @param array $quantidade Array com a quantidade de cada oferta
	 */
	static public function verifyCart($idoferta, $quantidade, $quantidade_presente) {
		// Busca a informação ds oferta comprada
		$model = new Admin_Model_Ofertasativacoes();
		
		$list = $model->fetchRow(array('idoferta_ativacao = ?' => $idoferta));
		
		// Verifica a cota de ofertas de comprador
		if($quantidade > 0) {
			if($quantidade > $list->limite_comprador) {
				throw new Exception("Só podem ser compradas " . $list->limite_comprador .  " unidades da oferta \"" . $list->findParentRow("Admin_Model_Ofertas")->titulo);
			}
		}
		
		// Verifica a cota de ofertas de presente
		if($quantidade_presente > 0) {
			if($quantidade_presente > $list->limite_presente) {
				throw new Exception("Só podem ser compradas " . $list->limite_presente .  " unidades da oferta \"" . $list->findParentRow("Admin_Model_Ofertas")->titulo);
			}
		}
	}
}