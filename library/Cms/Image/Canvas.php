<?php 

// Inclui a classe canvas
require_once("Cms/Library/canvas.php");

/**
 * Classe de extensão da classe de manipulação de imagens, canvas
 * 
 * @name Cms_Image_Canvas
 *
 */
class Cms_Image_Canvas extends Canvas {
}
