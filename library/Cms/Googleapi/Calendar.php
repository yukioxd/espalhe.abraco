<?php

// Inclui a classe canvas

require_once __DIR__ . '/src/Google/autoload.php';

define('APPLICATION_NAME', 'Google Calendar API ');

define('CREDENTIALS_PATH', __DIR__ . '/calendar-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/gapps-tbb.json');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-php-quickstart.json
define('SCOPES', implode(' ', array( Google_Service_Calendar::CALENDAR_READONLY) ));


/**
 * Classe de extensão da classe de manipulação de api google
 *
 * @name Cms_Googleapi_Calendar
 *
 */

class Cms_Googleapi_Calendar {

}
