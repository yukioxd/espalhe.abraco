<?php

/**
 *
 * @todo Verificar o underline e underscore no textField
 * @todo Adicionar alinhamento de texto
 */
class Cms_Reports_Elements_Text {
	/**
	 * 
	 * @var unknown_type
	 */
	protected $_text = "";
	
	/**
	 * 
	 * @var Zend_Pdf_Style
	 */
	protected $_style = NULL;
	
	/**
	 * 
	 * @param unknown_type $node
	 */
	public function __construct($node) {
		// Busca o tamanho da fonte
		if(!isset($node['textElement']['font'])) {
			$font_size = 10;
		}
		else {
			if(!isset($node['textElement']['font']['@attributes']['size'])) {
				$font_size = 10;
			}
			else {
				$font_size = (int)$node['textElement']['font']['@attributes']['size'];
			}
		}
		
		// Busca o nome da fonte
		if(!isset($node['textElement']['font'])) {
			$font_name = "sansserif";
		}
		else {
			if(!isset($node['textElement']['font']['@attributes']['fontName'])) {
				$font_name = "sansserif";
			}
			else {
				$font_name = strtolower((string)$node['textElement']['font']['@attributes']['fontName']);
			}
		}
			
		// Busca o negrito
		if(!isset($node['textElement']['font'])) {
			$font_name .= "";
		}
		else {
			if(!isset($node['textElement']['font']['@attributes']['isBold'])) {
				$font_name .= "";
			}
			else {
				$font_name .= "_bold";
			}
		}
			
		// Busca o italico
		if(!isset($node['textElement']['font'])) {
			$font_name .= "";
		}
		else {
			if(!isset($node['textElement']['font']['@attributes']['isItalic'])) {
				$font_name .= "";
			}
			else {
				$font_name .= "_italic";
			}
		}
			
		// Cria o style
		$this->_style = new Zend_Pdf_Style();
		$this->_style->setLineColor(new Zend_Pdf_Color_RGB(0, 0, 0));
		$this->_style->setFont(
				Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . "/../library/Cms/Reports/Resources/Fonts/" . $font_name . ".ttf"), 
				$font_size
			);
		
		// Trata o texto
		$text = $node['textFieldExpression'];
		
		// Recupera o texto
		$this->_text = $text;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getText() {
		return $this->_text;
	}
	
	/**
	 *
	 * @return Zend_Pdf_Style
	 */
	public function getStyle() {
		return $this->_style;
	}
}