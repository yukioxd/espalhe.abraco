<?php
/**
 * Controla as ações do delivery
 * 
 * @name Cms_Delivery
 */
class Cms_Delivery {
    /**
     * Armazena a sessão das mensagens
     *
     * @access protected
     * @name $_messages
     * @var Zend_Session_Namespace
     */
    protected $_messages = NULL;
    
    /**
     * Armazena as informações do delivery
     *
     * @access protected
     * @name $_delivery
     * @var Zend_Session_Namespace
     */
    protected $_delivery = NULL;
    
    /**
     * Contrutor da classe
     * 
     * @name __construct
     */
    public function __construct(){
        // Cria a sessão do delivery
        $this->_delivery = new Zend_Session_Namespace("delivery");
    }

    /**
     * Apaga tudo da session do delivery
     * 
     * @name limpaDelivery
     */
    public function limpaCarrinho(){
        // Apaga a session do delivery
        Zend_Session::namespaceUnset('carrinho');
    }

    /**
     * Adiciona um produto ao delivery
     * 
     * @name adiciona
     * @param array $dadosProduto
     */
    public function adiciona($dadosProduto){
        // Limpa tudo do carrinho
        self::limpaCarrinho();

        $atributos  = $dadosProduto['valor_atributo'];

        // Cria o index unico
        end($this->_delivery->produtos);
        $index = key($this->_delivery->produtos) + 1;

        // Instancia o model de produtos
        $model = new Admin_Model_Produtos();

        // Monta o select
        $select = $model->select()
                    ->from(
                        array(
                            'p' => 'produtos'
                        )
                    )
                    ->where('p.idproduto = ?', $dadosProduto['idproduto']);
        // Busca os dados do produto
        $produtos = $model->fetchRow($select)->toArray();

        // Adiciona mais dados na variavel
        $produtos['quantidade'] = $dadosProduto['quantidade'];
        $produtos['observacao'] = $dadosProduto['observacoes'];
        $produtos['valor_final'] = $produtos['valor'];
        
        // Verifica se escolheu outro sabor
        if($dadosProduto['sabor_adicional']){
            // Monta o select
            $select = $model->select()
                        ->from(
                            array(
                                'p' => 'produtos'
                            )
                        )
                        ->where('p.idproduto = ?', $dadosProduto['sabor_adicional']);

            // Busca os dados do sabor adicional
            $dadosSaborAdicional = $model->fetchRow($select)->toArray();

            // Se os dados da adicional for maior, o produto principal passa a receber o preço maior
            if($dadosSaborAdicional['valor'] > $produtos['valor']){
                $produtos['valor_final'] = $dadosSaborAdicional['valor'];
            }

            // Busca os dados do produto
            $produtos['sabor_adicional'] = $dadosSaborAdicional;
        }

        // Instancia o model Produtosatributo
        $model = new Admin_Model_Produtosatributo();

        // Se tive atributos
        if(count($dadosProduto['valor_atributo'])){
            // Percorre todos os aributos escolhido
            foreach($dadosProduto['valor_atributo'] as $atributo){
                // Monta o select
                $select = $model->select()
                                ->from(
                                    array(
                                        'pa' => 'produtos_atributo'
                                    ),
                                    array('pa.preco_atributo')
                                )
                                ->join(
                                    array(
                                        'va' => 'valor_atributo'
                                    ),
                                    'va.idvalor_atributo = pa.fkvalor_atributo',
                                    array(
                                        'va.nome_valor_atributo', 
                                        'va.idvalor_atributo'
                                    )
                                )
                                ->join(
                                    array(
                                        'a' => 'atributo'
                                    ),
                                    'a.idatributo = va.fkatributo',
                                    array(
                                        'a.nome_atributo',
                                        'a.idatributo',
                                        'a.fktipo_atributo'
                                    )
                                )
                                ->where('va.idvalor_atributo = ?', $atributo)
                                ->where('pa.fkproduto = ?', $dadosProduto['idproduto'])
                                ->setIntegrityCheck(false);
                
                // Busca os dados dos atributos
                $produtos['atributos'][] = $model->fetchRow($select)->toArray();
            }            
        }
        
        $this->_delivery->produtos[$index] = $produtos;
    }

    /**
     * Remove um produto do delivery
     * 
     * @name remove
     * @param int $index Chave do produto para remover
     */
    public function remove($index){
        // Apaga a variavel que armazena os dados do produto
        unset($this->_delivery->produtos[$index]);
    }

    /**
     * Remove todos os produtos do delivery
     * 
     * @name limpaDelivery
     */
    public function limpaDelivery(){
        Zend_Session::namespaceUnset('delivery');
    }    

    /**
     * Busca os produtos do delivery
     * 
     * @name getProdutos
     * @return array
     */
    public function getProdutos(){
        // Retorna a session com os produtos
        return $this->_delivery->produtos;
    }

    /**
     * Retorna o total do valor do carrinho
     * 
     * @name getValorTotal
     * @return string
     */
    public function getValorTotal($somaEntrega = false){
        // Percorre todos os produtos
        foreach($this->_delivery->produtos as $produto){
            // Multiplica o valor do produto pela quantidade
            $valorTotal += $produto['quantidade'] * $produto['valor_final'];

            // Verifica se tem atributos
            if(count($produto['atributos'])){
                // Percorre todos os atributos
                foreach($produto['atributos'] as $atributo){
                    // Incrementa o valor dos atributos no valor total
                    $valorTotal += ($produto['quantidade'] * $atributo['preco_atributo']);
                }
            }
        }

        // Verifica se é para soma a entrega
        if($somaEntrega){
            $valorTotal += $this->_delivery->entrega['preco'];
        }

        // Formata o valor em moeda brasileira
        $valorTotal = number_format($valorTotal, 2, ',', '.');

        return $valorTotal;
    }

    /**
     * Retorna a quantidade total dos produtos
     * 
     * @name getValorTotal
     * @return string
     */
    public function getQuantidadeTotal(){
        // Percorre todos os produtos
        foreach($this->_delivery->produtos as $produto){
            // Incrementa na variavel a quantidade
            $quantidadeTotal += $produto['quantidade'];
        }

        // Verifica se a variavel esta vazia
        if(empty($quantidadeTotal)){
            $quantidadeTotal = "0";
        }
        return $quantidadeTotal;
    }

    /**
     * Seta os dados da entrega
     * 
     * @name setEntrega
     */
    public function setEntrega($dados){
        $this->_delivery->entrega['cep_entrega']         = $dados['cep'];
        $this->_delivery->entrega['endereco_entrega']    = $dados['endereco'];
        $this->_delivery->entrega['numero_entrega']      = $dados['numero'];
        $this->_delivery->entrega['complemento_entrega'] = $dados['complemento'];
        $this->_delivery->entrega['cidade_entrega']      = $dados['cidade'];
    }

    /**
     * Retorna os dados de entrega
     * 
     * @name setEntrega
     * @return array
     */
    public function getEntrega(){
        return $this->_delivery->entrega;
    }

    /**
     * Seta o tipo de entrega
     * 
     * @name setEntregaTipo
     */
    public function setTipoPedido($tipoPedido){
        $this->_delivery->entrega['tipo_pedido'] = $tipoPedido;
    }

    /**
     * Limpa os dados de entrega
     * 
     * @name limpaEntrega
     */
    public function limpaEntrega(){
        //return $this->_delivery->entrega = array();
    }

    /**
     * Seta o preço da entrega
     * 
     * @name setPrecoEntrega
     */
    public function setPrecoEntrega($preco){
        $this->_delivery->entrega['preco'] = $preco;
    }

    /**
     * Retorna o preço da entrega
     * 
     * @name getValorTotal
     * @return string
     */
    public function getPrecoEntrega(){
        // Verifica se a variavel esta vazia
        if($this->_delivery->entrega['preco'] == ""){
            $this->_delivery->entrega['preco'] = "0,00";
        }

        return number_format($this->_delivery->entrega['preco'], 2, ",",".");
        
    }
}