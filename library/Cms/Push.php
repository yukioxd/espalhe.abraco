<?php

class Cms_Push {
 
    static function enviar($deviceToken, $msg, $android){
        
        if($android == 1){
            $message = new Zend_Mobile_Push_Message_Gcm();
            $message->addToken($deviceToken);
            $message->setData(array(
                //'idUsuario' => $idUsuario,
                //'idType' => $idType,
                'message' => $msg
            ));
            
            $gcm = new Zend_Mobile_Push_Gcm();
            $gcm->setApiKey('AIzaSyBTG-2ZEWk2NGu_PMIG6BJEt2rrtWMBmf8');

            try {
                $gcm->send($message);
            } catch (Zend_Mobile_Push_Exception $e) {
                die($e->getMessage());
            }
            $gcm->close();
        }else{
            $message = new Zend_Mobile_Push_Message_Apns();
            $message->setAlert($msg, "open");

            $message->setSound('default');
            $message->setId(time());
            $message->setToken($deviceToken);
            $apns = new Zend_Mobile_Push_Apns();
            $apns->setCertificate(APPLICATION_PATH . '/../common/certificado/ck.pem');
            $apns->setCertificatePassphrase('gruairport');

            try {
                //$apns->connect(Zend_Mobile_Push_Apns::SERVER_SANDBOX_URI);
                $apns->connect(Zend_Mobile_Push_Apns::SERVER_PRODUCTION_URI);
            } catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
                echo 'APNS Connection Error:' . $e->getMessage();
                die($e->getMessage());
            } catch (Zend_Mobile_Push_Exception $e) {
                echo 'APNS Connection Error:' . $e->getMessage();
                die($e->getMessage());
            }

            try {
                $apns->send($message);
            } catch (Zend_Mobile_Push_Exception_InvalidToken $e) {
                die($e->getMessage());
            } catch (Zend_Mobile_Push_Exception $e) {
                die($deviceToken);
            }
            $apns->close();
        }

        return true;
    }


	public function bulkPush($rows, $msg,  $chunkSize = 300, $sleepTime = 1){
		$modelNotificacaoDevice = new Admin_Model_NotificacaoDevice();
		$chunks = array_chunk($rows, $chunkSize);
		$apns = new Zend_Mobile_Push_Apns();
		$apns->setCertificate(APPLICATION_PATH . '/../common/certificado/ck.pem');
		$apns->setCertificatePassphrase('gruairport');

		$gcm = new Zend_Mobile_Push_Gcm();
		$gcm->setApiKey('AIzaSyBTG-2ZEWk2NGu_PMIG6BJEt2rrtWMBmf8');

		$messageGCM = new Zend_Mobile_Push_Message_Gcm();
		$messageAPN = new Zend_Mobile_Push_Message_Apns();
		$outputGCM = array();
		$outputAPN = array();

		$messageGCM->setData(array(
			'message' => $msg . date("Y-m-d - H:i:s")
		));
		$messageAPN->setAlert($msg, "open");

		foreach($chunks as $key => $chunk){
			$devicesNotified = array();
			if(!$gcm->isConnected()) {
				$gcm->connect();
			}
			if(!$apns->isConnected()) {
				$apns->connect(Zend_Mobile_Push_Apns::SERVER_PRODUCTION_URI);
			}

			foreach($chunk as $ey => $device){
				if($device['android'] > 0){
					$messageGCM->addToken($device['device_token']);
					array_push($devicesNotified, $device['notificacao_device_id']);
				} else {
					$messageAPN->setToken($device['device_token']);
					$messageAPN->setSound('default');
					$messageAPN->setId(time());
					array_push($outputAPN, $apns->send($messageAPN));
					array_push($devicesNotified, $device['notificacao_device_id']);
				}
			}

			if (sizeof($messageGCM->getToken()) > 0) {
				array_push($outputGCM, $gcm->send($messageGCM));
				$messageGCM->clearToken();
			}

			if($gcm->isConnected()) {
				$gcm->close();
			}
			if($apns->isConnected()) {
				$apns->close();
			}

			$notifiedDevices = implode(',', $devicesNotified);
			if(strlen($notifiedDevices) > 0){
				$modelNotificacaoDevice->update(array(
					"sent"  => 1
				), "notificacao_device_id IN ({$notifiedDevices})");
			}
			sleep($sleepTime);
		}

		$folder = APPLICATION_PATH . '/tmp/headers/';
		$gcmFile   = 'gcm'. time() . '.log';
		$gcmLog = print_r($outputGCM, TRUE);

		$apnFile   = 'apn'. time() . '.log';
		$apnLog = print_r($outputAPN, TRUE);

		file_put_contents($folder . $gcmFile, $gcmLog);
		file_put_contents($folder . $apnFile, $apnLog);

	}

	public function massiveGcm($rows, $msg,  $chunkSize = 300, $sleepTime = 5){

		$chunks = array_chunk($rows, $chunkSize);

		foreach($chunks as $uey => $chunk){
			$gcm = new Zend_Mobile_Push_Gcm();
			$gcm->connect();
			$gcm->setApiKey('AIzaSyBTG-2ZEWk2NGu_PMIG6BJEt2rrtWMBmf8');
			$message = new Zend_Mobile_Push_Message_Gcm();
			foreach($chunk as $key => $device){
				$message->addToken($device['device_token']);
			}
			$message->setData(array(
				'message' => $msg
			));
			try {
				$chunks[$uey] = $gcm->send($message);
			} catch (Zend_Mobile_Push_Exception $e) {
				$folder = APPLICATION_PATH . '/tmp/headers/';
				$file   = time() . '.log';
				$log = print_r($e->getMessage(), TRUE);
				file_put_contents($folder . $file, $log);
			}
			$gcm->close();
			sleep($sleepTime);
		}

		return $chunks;
	}

	public function massiveApn($rows, $msg, $chunkSize = 300, $sleepTime = 5){

		$chunks = array_chunk($rows, $chunkSize);

		$chunkedSuccess = 0;
		$chunkedError = 0;

		foreach($chunks as $uey => $chunk){
			$apns = new Zend_Mobile_Push_Apns();
			$apns->setCertificate(APPLICATION_PATH . '/../common/certificado/ck.pem');
			$apns->setCertificatePassphrase('gruairport');
			try{
				while(!$apns->isConnected()){
					try{
						$apns->connect(Zend_Mobile_Push_Apns::SERVER_PRODUCTION_URI);
						sleep(3);
					} catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
						$folder = APPLICATION_PATH . '/tmp/headers/';
						$file   = time() . '.log';
						$log = print_r($e->getMessage(), TRUE);
						file_put_contents($folder . $file, $log);
					} catch (Zend_Mobile_Push_Exception $e) {
						$folder = APPLICATION_PATH . '/tmp/headers/';
						$file   = time() . '.log';
						$log = print_r($e->getMessage(), TRUE);
						file_put_contents($folder . $file, $log);
					} catch (Exception $er){
						$folder = APPLICATION_PATH . '/tmp/headers/';
						$file   = time() . '.log';
						$log = print_r($e->getMessage(), TRUE);
						file_put_contents($folder . $file, $log);
					}
				}
				foreach($chunk as $key => $row){
					if(!isset($chunk[$key]['attempt'])){
						$chunk[$key]['attempt'] = 1;
					}
					$message = new Zend_Mobile_Push_Message_Apns();
					$message->setAlert($msg, "open");
					$message->setSound('default');
					$message->setId(time());
					$message->setToken($row['device_token']);

					$sent = FALSE;
					$chunk[$key]['attempt']++;
					try {
						$sent = $apns->send($message);
					} catch (Zend_Mobile_Push_Exception_InvalidToken $e) {
						$chunk[$key]['exception'] = $e->getMessage();
						$folder = APPLICATION_PATH . '/tmp/headers/';
						$file   = time() . '.log';
						$log = print_r($e->getMessage(), TRUE);
						file_put_contents($folder . $file, $log);
					} catch (Zend_Mobile_Push_Exception $e) {
						$chunk[$key]['exception'] = $e->getMessage();
						$folder = APPLICATION_PATH . '/tmp/headers/';
						$file   = time() . '.log';
						$log = print_r($e->getMessage(), TRUE);
						file_put_contents($folder . $file, $log);
					}
					if($sent){
						$chunkedSuccess++;
					}else{
						$chunkedError++;
					}
					$chunk[$key]['sent'] = $sent;
				}
				$apns->close();
			}catch (Exception $er){
				$disconnected = TRUE;
			}
			sleep($sleepTime);
		}

		return array(
			'success' => $chunkedSuccess,
			'error'   => $chunkedError,
		);
	}

	public function globalPush($msg) {
		$modelDevice = new Admin_Model_Device();

		$iOSRows = $modelDevice->fetchAll('device_token <> "" AND device_token IS NOT NULL AND android = 0');
		//$iOSRows  = $modelDevice->fetchAll("device_id in (29773, 40396, 20261)");
		$iOSArray = $iOSRows->toArray();


		$androidRows = $modelDevice->fetchAll('device_token <> "" AND device_token IS NOT NULL AND android = 1');
		//$androidRows  = $modelDevice->fetchAll("device_id in (42917, 40392)");
		$androidArray = $androidRows->toArray();


		$iosResult  = $this->massiveApn($iOSArray, $msg);
		$gcmResults = $this->massiveGcm($androidArray, $msg);


		$successGCM = 0;
		$failGCM    = 0;
		foreach ($gcmResults as $gcmResult) {
			$response = $gcmResult->getResponse();
			$successGCM += $response['success'];
			$failGCM += $response['failure'];
		}


		return array(
			'successAndroid' => $successGCM,
			'successiOS'     => $iosResult['success'],
			'failAndroid'    => $failGCM,
			'failOS'         => $iosResult['error']
		);
	}


	public function selectivePush($msg, $iOSArray, $androidArray, $chunkSize = 300, $time = 5) {

		$iosResult  = $this->massiveApn($iOSArray, $msg, $chunkSize, $time);
		$gcmResults = $this->massiveGcm($androidArray, $msg, $chunkSize, $time);

		$successGCM = 0;
		$failGCM    = 0;
		foreach ($gcmResults as $gcmResult) {
			$response = $gcmResult->getResponse();
			$successGCM += $response['success'];
			$failGCM += $response['failure'];
		}

		return array(
			'successAndroid' => $successGCM,
			'successiOS'     => $iosResult['success'],
			'failAndroid'    => $failGCM,
			'failOS'         => $iosResult['error']
		);
	}
}
