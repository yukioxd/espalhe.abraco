<?php
/**
 * Classe de consulta de fraude da Clear sale
 * 
 * @name Cms_Fraudes_Clearsale
 */
class Cms_Fraudes_Clearsale extends Cms_Fraudes_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "Clearsale";
	
	/**
	 * Armazena o endereço do gateway
	 * 
	 * @access private
	 * @name $_gateway
	 * @var string
	 */
	private $_gateway = "https://www.clearsale.com.br/integracaov2/FreeClearSale/frame.aspx";
	
	/**
	 * Inicializa a classe de pagamento
	 *
	 * @name init
	 */
	public function init() {
	}
	
	/**
	 * Efetua a verificação da fraude
	 *
	 * @name analise
	 * @return boolean
	 */
	public function analise() {
		
		// Tipo do pagamento
		$tipo_pagamento = "1"; //1 - cartão; 2 - boleto
		
		// Bandeira
		$bandeira = 3; // 1 - Diners; 2 - Mastercard; 3 - Visa; 4 - Outros; 5 - American Express; 6 - Hipercard; 7 - Aura
		
		// Ajusta o telefone de pagamento
		$tmp = explode(" ", $this->_pedido->telefone_pagamento);
		$ddd_telefone = str_replace("(", "", str_replace(")", "", trim($tmp[0])));
		$telefone = str_replace("-", "", trim($tmp[1]));
		
		// Ajusta o celular de pagamento
		$tmp = explode(" ", $this->_pedido->celular_pagamento);
		$ddd_celular = str_replace("(", "", str_replace(")", "", trim($tmp[0])));
		$celular = str_replace("-", "", trim($tmp[1]));
		
		// Monta o vetor dos dados
		$param = array (
			'CodigoIntegracao' => $this->_config->codigo,
			'PedidoID' => $this->_pedido->idpedido,
			'Data' => $this->_pedido->data_compra,
			'IP' => $this->_pedido->endereco_ip,
			'Total' => $this->_pedido->valor_pedido,
			'TipoPagamento' => $tipo_pagamento,
			'TipoCartao' => $bandeira,
			'Parcelas' => $this->_pedido->parcelas,
			'Cobranca_Nome' => $this->_pedido->nome_pagamento,
			'Cobranca_Email' => $this->_pedido->email_pagamento,
			'Cobranca_Documento' => $this->_pedido->documento_pagamento,
			'Cobranca_Logradouro' => $this->_pedido->endereco_pagamento,
			'Cobranca_Logradouro_Numero' => $this->_pedido->numero_pagamento,
			'Cobranca_Logradouro_Complemento' => $this->_pedido->complemento_pagamento,
			'Cobranca_Bairro' => $this->_pedido->bairro_pagamento,
			'Cobranca_Cidade' => $this->_pedido->cidade_pagamento,
			'Cobranca_Estado' => $this->_pedido->estado_pagamento,
			'Cobranca_CEP' => $this->_pedido->cep_pagamento,
			'Cobranca_Pais' => $this->_pedido->pais_pagamento,
			'Cobranca_DDD_Telefone' => $ddd_telefone,
			'Cobranca_Telefone' => $telefone,
			'Cobranca_DDD_Celular' => $ddd_celular,
			'Cobranca_Celular' => $celular,
			'Entrega_Nome' => $this->_pedido->nome_pagamento,
			'Entrega_Email' => $this->_pedido->email_pagamento,
			'Entrega_Documento' => $this->_pedido->documento_pagamento,
			'Entrega_Logradouro' => $this->_pedido->endereco_entrega,
			'Entrega_Logradouro_Numero' => $this->_pedido->numero_entrega,
			'Entrega_Logradouro_Complemento' => $this->_pedido->complemento_entrega,
			'Entrega_Bairro' => $this->_pedido->bairro_pagamento,
			'Entrega_Cidade' => $this->_pedido->cidade_entrega,
			'Entrega_Estado' => $this->_pedido->estado_entrega,
			'Entrega_CEP' => $this->_pedido->cep_entrega,
			'Entrega_Pais' => $this->_pedido->pais_pagamento,
			'Entrega_DDD_Telefone' => $ddd_telefone,
			'Entrega_Telefone' => $telefone,
			'Entrega_DDD_Celular' => $ddd_celular,
			'Entrega_Celular' => $celular
		);
		
		try {
			// Faz a requisição para a analise
			$client = new Zend_Http_Client();
			$client->setUri($this->_gateway);
			$client->setParameterGet($param);
			$response = $client->request("POST");
			$result_body = $response->getBody();
		}
		catch(Exception $e) {
			$param['codigo_erro'] = $e->getCode();
			$param['mensagem_erro'] = $e->getMessage();
			$param['trace_erro'] = $e->getTrace();
			$param['params_erro'] = $this->_request->getParams();
			
			// Salva o log da transação
			$this->status(9, json_encode($param), $e->getMessage());
			
			// Retorna o erro
			return FALSE;
		}
		
		// Faz o parse do retorno
		preg_match_all("/Exibição da Faixa de Risco do Pedido\"><span>(.*?)<\/span><\/div>/s", $result_body, $content);
		if($content[1][0] == NULL) {
			// Salva o log da transação
			$this->status(9, json_encode($param), $result_body);
			
			// Retorna o erro
			return FALSE;
		}
		
		
		// Verifica o nivel da fraude
		switch($content[1][0]) {
			case "Critico":
			case "Alto":
				$status = 9;
				$return = FALSE;
				break;
				
			case "Medio":
			case "Baixo":
				$status = 6;
				$return = TRUE;
				break;
		}
		
		// Salva o log da transação
		$this->status($status, json_encode($param), $result_body);
		
		// Retorna o resultado da consulta
		return $return;
	}
}