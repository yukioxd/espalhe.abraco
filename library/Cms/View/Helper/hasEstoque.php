<?php 

/**
 * Faz as verificações de existencia do produto em estoque
 * 
 * @name Cms_View_Helper_hasEstoque
 */
class Cms_View_Helper_hasEstoque {
	
	/**
	 * Método acionado com a chamada do helper
	 * 
	 * @name Helper_hasEstoque
	 * @param int $idproduto ID do produto para verificar a existencia do produto
	 * @return boolean
	 */
	public function hasEstoque($idproduto) {
		// Recupera os modulos habilitados
		$modulos = Zend_Registry::get("modulos");
		
		// Verifica se o tipo do controle de estoque é por lugares
		if($modulos->estoque->lugares == 1) {
			// Busca as reservas do produto
			$estoque = Cms_Estoque::getEstoque($idproduto); 

			// Verifica se possui item no estoque
			if($estoque > 0) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		// Verifica se o tipo do controle de estoque é simples
		elseif($modulos->estoque->simples == 1) {
			// Busca as informações do produto
			$model = new Admin_Model_Produtos();
			$produto = $model->fetchRow(array('idproduto = ?' => $idproduto));
		
			// Verifica a quantidade de produto em estoque
			if($produto->estoque <= 0) {
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
	}
}
