<?php

/**
 * Cria o helper do bbcode
 * 
 * @name Zend_View_Helper_Bbcode
 */
class Cms_View_Helper_Bbcode extends Zend_View_Helper_Abstract {
	/**
	 * Método da classe
	 * 
	 * @param string $string Texto para converter para bbcode
	 */
	public function bbcode($string) {
		// Cria as buscas
		$simple_search = array(  
			"/\[br\]/is",
			"/\[b\](.*?)\[\/b\]/is",
			"/\[i\](.*?)\[\/i\]/is",
			"/\[u\](.*?)\[\/u\]/is",
			"/\[url\=(.*?)\](.*?)\[\/url\]/is",
			"/\[url\](.*?)\[\/url\]/is",
			"/\[align\=(left|center|right)\](.*?)\[\/align\]/is",
			"/\[img\](.*?)\[\/img\]/is",
			"/\[mail\=(.*?)\](.*?)\[\/mail\]/is",
			"/\[mail\](.*?)\[\/mail\]/is",
			"/\[font\=(.*?)\](.*?)\[\/font\]/is",
			"/\[size\=(.*?)\](.*?)\[\/size\]/is",
			"/\[color\=(.*?)\](.*?)\[\/color\]/is",
			"/\[codearea\](.*?)\[\/codearea\]/is",
			"/\[code\](.*?)\[\/code\]/is",
			"/\[p\](.*?)\[\/p\]/is",
			"/\[h1\](.*?)\[\/h1\]/is",
			"/\[h2\](.*?)\[\/h2\]/is",
			"/\[h3\](.*?)\[\/h3\]/is",
			"/\[h4\](.*?)\[\/h4\]/is",
			'/\[list\](.*?)\[\/list\]/',
			'/\[\*\](.*?)/'
		);  
		
		// Cria as trocas
		$simple_replace = array(  
			"<br />",
			"<strong>$1</strong>",
			"<em>$1</em>",
			"<u>$1</u>",
			"<a href=\"$1\" rel=\"nofollow\" title=\"$2 - $1\">$2</a>",
			"<a href=\"$1\" rel=\"nofollow\" title=\"$1\">$1</a>",
			"<div style=\"text-align: $1;\">$2</div>",
			"<img src=\"$1\" alt=\"\" />",
			"<a href=\"mailto:$1\">$2</a>",
			"<a href=\"mailto:$1\">$1</a>",
			"<span style=\"font-family: $1;\">$2</span>",
			"<span style=\"font-size: $1;\">$2</span>",
			"<span style=\"color: $1;\">$2</span>",
			"<textarea class=\"code_container\" rows=\"30\" cols=\"70\">$1</textarea>",
			"<pre class=\"code\">$1</pre>",
			"<p>$1</p>",
			"<h1>$1</h1>",
			"<h2>$1</h2>",
			"<h3>$1</h3>",
			"<h4>$1</h4>",
			"<ul>$1</ul>",
			"<li>$1</li>",
		);
		
		// Refaz as trocas
		$string = preg_replace($simple_search, $simple_replace, $string); 
		
		// Busca o link do youtube
		$match = preg_match_all("/\[youtube\](.*?)\[\/youtube\]/", $string, $matches);
		foreach($matches[1] as $key => $match) {
			// Recupera somente o código do video
			$tag = $match;
			$code = $this->youtube_code($tag);
			
			// Verifica se existe o código
			if($code !== FALSE) {
				// Cria o embed
				$embed  = "<object width=\"560\" height=\"315\">";
				$embed .= "<param name=\"movie\" value=\"https://youtube.googleapis.com/v/" . $code . "?version=2&fs=1\"></param>";
				$embed .= "<param name=\"allowFullScreen\" value=\"true\"></param>";
				$embed .= "<param name=\"allowScriptAccess\" value=\"always\"></param>";
				$embed .= "<embed src=\"https://youtube.googleapis.com/v/" . $code . "?version=2&fs=1\" type=\"application/x-shockwave-flash\" allowfullscreen=\"true\" allowscriptaccess=\"always\" width=\"560\" height=\"315\">";
				$embed .= "</embed>";
				$embed .= "</object>";
				
				// Efetua a troca entre a tag e o embed
				$string = str_replace($matches[0], $embed, $string);
			}
		}
		
		// Quebras de linha
		$string = nl2br($string);
		
		// Retorna a string formatada
		return $string;
	}
	
	/**
	 * Método que busca código do youtube
	 *
	 * @param string $string Texto para buscar o código
	 * @return string|boolean
	 */
	private function youtube_code($string) {
		// Define a expressão regular
		$expYoutube = "/http(s)?:\/\/(www\.)?youtu(\.)?be(-nocookie)?(\.com)?\/(embed\/)?(v\/)?(watch\?v=)?([a-z,0-9]*-?_?[a-z,0-9]*-?_?[a-z,0-9]*-?_?[a-z,0-9]*-?_?[a-z,0-9]*)/i";
		
		// Executa a busca
		preg_match_all($expYoutube, $string, $result);
		
		// Cria um laço para listar o link
		foreach($result[sizeof($result)-1] as $code) {
			// Verifica se o retorno é numérico
			if(strlen($code) == 11) {
				// Atribui o valor encontrado a uma variavel e encerra o laço
				return $code;
				break;
			}
			else{
				// Retorna uma mensgem de erro
				return FALSE;
			}
		}
	}
}
