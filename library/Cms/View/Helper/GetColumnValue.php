<?php 

class Cms_View_Helper_GetColumnValue {
	
	/**
	 * Busca o valor da coluna, buscando o valor caso seja o valor de uma tabela relacionada
	 * 
	 * @name GetColumnValue
	 * @param Zend_Db_Table_Row
	 * @param string $field Nome da coluna
	 * @return string
	 */
	public function GetColumnValue($row, $field) {
		//
		$model = $row->getTable();
		
		// Busca a classe referencia da coluna
		$reference_table = $model->getAutocomplete($field);
		$reference_table = $reference_table['model'];
		if($reference_table != NULL) {
			// Busca os modificadores
			$mod = $model->getModifier($field);
			
			// Instancia a classe model
			$table = NULL;
			if($mod['table'] > 0) {
				// Cria o model das tabelas relacionadas
				$model = new Admin_Model_Camposusuariotabelasrelacionadas();
				$row_table = $model->fetchRow(array('idcampo_usuario_tabela = ?' => $mod['table']));
				$table = "U_" . $row_table->tabela;
				
				// Cria o model de referencia
				$referece_model = new Admin_Model_Dynamic($table);
				
				// Busca a coluna descrição
				$description_column = $referece_model->getDescription();
				
				try {
					// Busca o registro
					$result = $referece_model->fetchRow(array('idkey = ?' => $row->$field));
					if($result !== NULL) {
						// Armazena o valor
						$value = $result->$description_column;
					}
				}
				catch(Exception $e) {
					$value = "";
				}
				
//				
//				// Busca o valor
//				if($row->U_editora != NULL) {
//					$value = $row->findParentRow("Admin_Model_Dynamic", "Ueditora")->U_nome;
//				}
//				else {
//					$value = "";
//				}
				
				//
			}
			else {
				// Cria o model de referencia
				$referece_model = new $reference_table();
				
				// Busca a coluna descrição
				$description_column = $referece_model->getDescription();
				
				// Busca o valor
				$value = $row->findParentRow($referece_model)->$description_column;
			}
		}
		else {
			// Busca o valor
			$value = $row->$field;
		}
		
		// Busca a descrição da tabela
		$columns = $model->describeTable();
		
		// Verifica o tipo do campo
		switch($columns[$field]['DATA_TYPE']) {
			case "decimal":
			case "float8":
				// Formata a data
				$value = number_format($value, 2, ",", ".");
				break;
				
			case "date":
			case "timestamp":
				// Remove a parte da hora
				$values = explode(" ", $value);
				
				// Formata a data
				$data = implode("/", array_reverse(explode("-", $values[0])));
				
				// Verifica se a data existe
				if($data == "00/00/0000") {
					$data = "--/--/----";
				}
				
				$value = $data;
				break;
				
			case "datetime":
// 				// Remove a parte da hora
// 				$values = explode(" ", $value);
//				
// 				// Formata a data
// 				$data = implode("/", array_reverse(explode("-", $values[0])));
//				
// 				// Verifica se a data existe
// 				if($data == "00/00/0000") {
// 					$data = "--/--/----";
// 				}
//				
// 				$value = $data . " " . $values;
				$timestamp = strtotime($value);
				$value = date("d/m/Y H:i:s", $timestamp);

				break;
				
			case "tinyint":
			case "bool":
				if($value) {
					$value = "Sim";
				}
				else {
					$value = "Não";
				}
				break;
				
			default:
				if(strlen($value) == 0) {
					$value = "--";
				}
				break;
		}
		
		
		//
		return $value;
	}
}
