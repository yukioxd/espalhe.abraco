<?php

/**
 * Cria o helper calcular o preco do produto
 * 
 * @name Zend_View_Helper_Preco
 */
class Cms_View_Helper_Preco extends Zend_View_Helper_Abstract {
	/**
	 * Método da classe
	 * 
	 * $modo = "normal", "venda", "codigo_pagamento"
	 * 
	 * @param Int $idproduto Código do produto
	 * @param String $modo Valor á ser retornado
	 */
	public function preco($idproduto, $modo) {
		// Cria o objeto de calculo de valores
		static $preco;
		static $id;
		if($id != $idproduto) {
			$preco = new Cms_Precos($idproduto);
			$id = $idproduto;
		}
		
		// Verifica o modo
		switch($modo) {
			// Preço normal do produto
			case "normal":
				$valor = $preco->Normal();
				break;
				
			// Preço de venda do produto
			case "venda":
				$valor = $preco->Venda();
				break;
			
			default:
				// Preço no método de pagamento informado
				if($modo > 0) {
					$valor = $preco->Metodo($modo);
				}
				else {
					$valor = $preco->Venda();
				}
				break;
		}
		
		// Retorna o valor do produto
		return $valor;
	}
}
