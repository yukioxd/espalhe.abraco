<?php

/**
 * Cria o helper do bbcode
 * 
 * @name Zend_View_Helper_Bbcode
 */
class Cms_View_Helper_Qrcode extends Zend_View_Helper_Abstract {
	/**
	 * Método da classe
	 * 
	 * @param string $string Texto para converter para bbcode
	 */
	public function Qrcode($string) {
		$qr = new Cms_Qrcode();
		
		$qr->link($string);  
		
		return $qr->get_link();
	}
}
