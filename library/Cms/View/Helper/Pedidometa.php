<?php

/**
 * Cria o helper para exibição das metas dos pedidos
 * 
 * @name Zend_View_Helper_Pedidometa
 */
class Cms_View_Helper_Pedidometa extends Zend_View_Helper_Abstract {
	/**
	 * Método da classe
	 * 
	 * @param Zend_Table_Row $row Registro do status
	 */
	public function pedidometa($row) {
		// Inicializa o retorno
		$string = "";
		
		// Verifica o tipo do histórico
		switch($row->identificacao) {
			case "locaweb":
				$json = json_decode($row->meta_dados);
				$string = $json->mensagem_autorizacao;
				break;
				
			case "manual":
				$json = json_decode($row->meta_dados);
				$string = "Modificação manual efetuada por: " . $json->nome;
				break;
				
			case "boletophp":
				$json = json_decode($row->meta_dados);
				$string = "Boleto " . $json->status;
				break;
		}
		
		// Retorna a string formatada
		return $string;
	}
}
