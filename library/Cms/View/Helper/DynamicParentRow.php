<?php

/**
 * Cria o helper para buscar referencia à uma tabela dinamica
 * 
 * @name Cms_View_Helper_DynamicParentRow
 */
class Cms_View_Helper_DynamicParentRow extends Zend_View_Helper_Abstract {
	/**
	 * Método da classe
	 * 
	 * @param string $table Tabela onde irá buscar o registro
	 * @param string $field Valor do registro à recuperar
	 */
	public function dynamicParentRow($table, $field) {
		// Verifica se existe registro
		if($field > 0) {
			// Cria o model dinamico
			$model = new Admin_Model_Dynamic($table);
			
			// Faz a busca da linha do registro
			$result = $model->fetchRow(array('idkey = ?' => $field));
		}
		else {
			$result = (object)array();
		}
		
		// Retorna a string formatada
		return $result;
	}
}