<?php
/**
 * Controla o estoque dos produtos
 * 
 * @name Cms_Estoque
 */
class Cms_Estoque {
	/**
	 * Armazena as configurações gerais
	 *
	 * @access private
	 * @name _config
	 * @var Zend_Config
	 */
	private $_config = NULL;
	
	/**
	 * Contrutor da classe
	 * 
	 * @name __construct
	 */
	public function __construct() {
	}
	
	/**
	 * Remove a reserva dos produtos
	 *
	 * @name delReserva
	 * @param int $idproduto ID do produto
	 * @param int $idpedido ID do pedido
	 * @param int $quantidade Quantidade à ser retirada da reserva
	 */
	static public function delReserva($idproduto, $idpedido, $quantidade=NULL) {
		// Cria o model da reserva
		$model = new Admin_Model_Estoquereservas();
		
		// Adiciona o where do pedido
		$where['idpedido = ?'] = $idpedido;
		
		// Verifica se existe codigo do produto
		if($idproduto > 0) {
			$where['idproduto = ?'] = $idproduto;
		}
		
		// Verifica se é para remover ou retirar
		if($quantidade === NULL) {
			// Remove o registro
			$model->delete($where);
		}
		else {
			// Busca a quantidade atual da reserva
			$row = $model->fetchRow($where);
			
			// Verifica a data
			if($quantidade >= $row->quantidade) {
				// Remove o registro
				$model->delete($where);
			}
			else {
				// Atualiza a quantidade
				$data = array();
				$data['quantidade'] = $row->quantidade - $quantidade;
				$model->update($data, $where);
			}
		}
	}
	
	/**
	 * Adiciona um produto em reserva
	 * 
	 * @name addReserva
	 * @param int $idproduto ID do produto
	 * @param int $idpedido ID do pedido
	 * @param int $quantidade Quantidade reservada do produto
	 */
	static public function addReserva($idproduto, $idpedido, $quantidade) {
		// Cria o model da reserva
		$model = new Admin_Model_Estoquereservas();
		
		// Recupera os modulos habilitados
		$modulos = Zend_Registry::get("modulos");
		if($modulos->estoque->lugares == 1) {
			// Cria o vetor dos dados
			$data = array();
			$data['idproduto'] = $idproduto;
			$data['idpedido'] = $idpedido;
			$data['quantidade'] = $quantidade;
			
			// Adiciona o registro ao estoque
			try {
				$model->insert($data);
			}
			catch(Exception $e) {
				throw $e;
			}
		}
	}
	
	/**
	 * Busca a quantidade de produtos em estoque
	 *
	 * @name getEstoque
	 * @param int $idproduto ID do produto
	 * @return boolean
	 */
	static public function getEstoque($idproduto) {
		// Recupera os modulos habilitados
		$modulos = Zend_Registry::get("modulos");
		
		// Controle de estoque por locais
		if($modulos->estoque->lugares == 1) {
			// Busca o estoque disponivel
			$model = new Admin_Model_Produtos();
			$select = $model->select()
				->from("produtos", array())
				->columns(array(
					'estoque' => "
						(SELECT COALESCE(SUM(quantidade), 0) FROM estoques WHERE estoques.idproduto = produtos.idproduto) - (SELECT COALESCE(SUM(quantidade), 0) FROM estoque_reservas WHERE estoque_reservas.idproduto = produtos.idproduto)
					"
				))
				->where("produtos.idproduto = ?", $idproduto);
			$row = $model->fetchRow($select);
			
			// Calcula o total disponivel
			$estoque = $row->estoque;
		}
		// Controle de estoque simples
		if($modulos->estoque->simples == 1) {
			// Busca as informações do produto
			$model = new Admin_Model_Produtos();
			$produto = $model->fetchRow(array('idproduto = ?' => $idproduto));
			
			// Retorna a quantidade de produto em estoque
			$estoque = $produto->estoque;
		}
		
		// Retorna a quantidade de produto em estoque
		return $estoque;
	}
	
	/**
	 * Efetua a baixa do estoque
	 *
	 * @name baixaEstoque
	 * @param int $idproduto_pedido ID do produto no pedido
	 * @param int $quantidade Quantidade do produto para baixar
	 * @return boolean
	 */
	static public function baixaEstoque($idproduto_pedido, $idestoque, $quantidade) {
		// Busca as informações do produto no pedido
		$model = new Admin_Model_Produtospedidos();
		$produto_pedido = $model->fetchRow(array('idproduto_pedido = ?' => $idproduto_pedido));
		
		// Busca as baixas ja existentes deste produto
		$model = new Admin_Model_Estoquesbaixas();
		$select = $model->select()->from("estoques_baixas")->columns(array('total' => "sum(quantidade)"))->where("idproduto_pedido = ?", $idproduto_pedido);
		$baixas = $model->fetchRow($select);
		
		// Verifica se ja foi baixado todos os produtos
		if($produto_pedido->quantidade <= $baixas->total) {
			throw new Exception("Ja foram baixados todos itens deste produto");
		}
		
		// Busca as informações do estoque
		$model = new Admin_Model_Estoques();
		$estoque = $model->fetchRow(array('idestoque = ?' => $idestoque));
		
		// Atualiza a quantidade de produtos em estoque
		$data = array();
		$data['quantidade'] = $estoque->quantidade - $quantidade;
		
		// Verifica se a quantidade é maior do que a do estoque
		if($quantidade > $estoque->quantidade) {
			// Retorna o erro
			throw new Exception($estoque->findParentRow("Admin_Model_Estoqueslugares")->descricao . " só possui " . $estoque->quantidade . " unidade(s)");
		}
		else {
			// Verifica se zerou o estoque
			if($data['quantidade'] == 0) {
				$model->delete(array('idestoque = ?' => $idestoque));
			}
			else {
				$model->update($data, array('idestoque = ?' => $idestoque));
			}
			
			// Busca a sessão do usuário logado
			$session = new Zend_Session_Namespace("login");
			
			// Adiciona a baixa
			$model = new Admin_Model_Estoquesbaixas();
			$data = array();
			$data['quantidade'] = $quantidade;
			$data['idusuario'] = $session->logged_usuario['idusuario'];
			$data['idproduto_pedido'] = $idproduto_pedido;
			$data['local'] = $estoque->findParentRow("Admin_Model_Estoqueslugares")->descricao;
			$data['data_execucao'] = date("Y-m-d H:i:s");
			$model->insert($data);
				
			// Baixa a reserva
			Cms_Estoque::delReserva($estoque->idproduto, $produto_pedido->idpedido, $quantidade);
		}
	}
}
