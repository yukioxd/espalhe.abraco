<?php

/**
 * Elemento textarea do formulario
 *
 * @name Cms_Form_Textarea
 * @package Cms
 * @subpackage Form
 */
class Cms_Form_Textarea extends Zend_Form_Element_Textarea {
	/**
	 * Configura o elemento
	 * 
	 * @name init
	 */
	public function init() {
		parent::setAttrib("field-type", "textarea");
		parent::setAttrib("class", "string textarea");
	}
}
