<?php

/**
 * Elemento de checagem do formulario
 *
 * @name Cms_Form_Checkbox
 * @package Cms
 * @subpackage Form
 */
class Cms_Form_Checkbox extends Zend_Form_Element_Checkbox {
	/**
	 * Configura o elemento
	 * 
	 * @name init
	 */
	public function init() {
		parent::setAttrib("field-type", "checkbox");
	}
}
