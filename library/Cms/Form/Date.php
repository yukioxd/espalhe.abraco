<?php

/**
 * Elemento date do formulario
 *
 * @name Cms_Form_Date
 * @package Cms
 * @subpackage Form
 */
class Cms_Form_Date extends Zend_Form_Element_Text {
	/**
	 * Configura o elemento
	 * 
	 * @name init
	 */
	public function init() {
		parent::setAttrib("field-type", "date");
		parent::setAttrib("class", "datepicker form-control");
	}
}
