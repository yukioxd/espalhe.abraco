<?php
/**
 * Controla o valor dos produtos
 * 
 * @name Cms_Precos
 */
class Cms_Precos {
	/**
	 * Armazena as configurações gerais
	 *
	 * @access private
	 * @name _config
	 * @var Zend_Config
	 */
	private $_config = NULL;
	
	/**
	 * Armazena o id do produto
	 *
	 * @access private
	 * @name _idproduto
	 * @var int
	 */
	private $_idproduto = NULL;
	
	/**
	 * Armazena a promoção valida do produto
	 *
	 * @access private
	 * @name _promocao
	 * @var int
	 */
	private $_promocao = NULL;
	
	/**
	 * Armazena as informações do produto
	 *
	 * @access private
	 * @name _produto
	 * @var Zend_Db_Table_Row
	 */
	private $_produto = NULL;
	
	/**
	 * Armazena a quantidade de numeros de casas decimais
	 *
	 * @access private
	 * @name _decimais
	 * @var Integer
	 */
	private $_decimais = 2;
	
	/**
	 * Contrutor da classe
	 * 
	 * @name __construct
	 */
	public function __construct($idproduto) {
		// Armazena o código do produto
		$this->_idproduto = $idproduto;
		
		// Cria o model do produto
		$model = new Admin_Model_Produtos();
		
		// Busca o produto
		$this->_produto = $model->fetchRow(array('idproduto = ?' => $this->_idproduto));
		
		// Grupo para as promoções
		// @todo Modificar caso seja rede livraria
		$idgrupo_cliente = 1;
		
		// Cria o model das promoções
		$model = new Admin_Model_Produtospromocoes();
		$select = $model->select()
			->from("produtos_promocoes")
			->join("promocoes", "produtos_promocoes.idpromocao = promocoes.idpromocao")
			->where("produtos_promocoes.idproduto = ?", $this->_idproduto)
			->where("promocoes.ativo = 1")
			->where("((CURRENT_DATE() BETWEEN promocoes.data_inicial AND promocoes.data_final) OR COALESCE(data_inicial, '0000-00-00') = '0000-00-00')")
			->where("idgrupo_cliente = ?", $idgrupo_cliente)
			->order("produtos_promocoes.idproduto_promocao DESC")
			->setIntegrityCheck(FALSE);
			
		// Busca a promoção ativa
		$this->_promocao = $model->fetchRow($select);
	}
	
	/**
	 * Retorna o valor normal do produto
	 *
	 * @name Normal
	 * @return Decimal
	 */
	public function Normal() {
		// Retorna o valor normal do produto
		$valor = number_format($this->_produto->preco_produto, $this->_decimais, ".", "");
		return $valor;
	}
	
	/**
	 * Retorna o preço de venda do produto
	 *
	 * @name Venda
	 * @return Decimal
	 */
	public function Venda() {
		// Recupera a promoção valida
		$promocao = $this->Promocao();
		
		// 
		if($promocao !== NULL) {
			// Calcula o valor do desconto
			$desconto = ($this->_produto->preco_produto * $promocao->porcentagem) / 100;
			
			// Calcula o valor do produto com desconto
			$valor = $this->_produto->preco_produto - $desconto;
		}
		else {
			$valor = $this->_produto->preco_venda;
		}
		
		// Retorna o valor
		$valor = number_format($valor, $this->_decimais, ".", "");
		return $valor;
	}
	
	/**
	 * Retorna o preço pelo metodo de venda
	 *
	 * @name Metodo
	 * @param Int $idmetodo_pagamento ID do metodo de pagamento
	 * @return Decimal
	 */
	public function Metodo($idmetodo_pagamento) {
		// Recupera o valor de venda
		$valor = $this->Venda();
		
		// Busca o metodo de pagamento
		$model = new Admin_Model_Metodospagamento();
		$metodo = $model->fetchRow(array('idmetodo_pagamento = ?' => $idmetodo_pagamento));
		
		// Calcula o porcentual
		$valor = ($valor - ($valor * ($metodo->porcentagem_desconto / 100)));
		
		// Retorna o valor
		$valor = number_format($valor, $this->_decimais, ".", "");
		return $valor;
	}
	
	/**
	 * Retorna a promoção valida para o produto
	 *
	 * @name Promocao
	 * @return Zend_Db_Table_Row
	 */
	public function Promocao() {
		// Retorna a promoção
		return $this->_promocao;
	}
}
