<?php
/**
 * Classe de pagamentos pelo bradesco (boleto bancário)
 *
 * @name Cms_Pagamentos_Bradesco
 */
class Cms_Pagamentos_Bradesco extends Cms_Pagamentos_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "bradesco";
	
	/**
	 * Armazena o endereço do para geração do boleto
	 *
	 * @access private
	 * @name $_url_boleto
	 * @var string
	 */
	private $_url_boleto = "https://mup.comercioeletronico.com.br/paymethods/boletoret/model1/prepara_pagto.asp";
	// private $_gateway_pagamento = "http://mupteste.comercioeletronico.com.br/paymethods/boletoret/model1/prepara_pagto.asp?merchantid=17980&orderid=0006&idpedido=0006";
	
	/**
	 * Inicializa a classe de pagamento
	 *
	 * @name init
	 */
	public function init() {
	}
	
	/**
	 * Efetua o pagamento
	 * 
	 * @name pagamento
	 * @return boolean
	 */
	public function pagamento() {
		// Verifica se o método de pagamento e boleto
		if($this->_idmetodo_pagamento == 1210) {
			// Monta a url para geração do boleto
			$url = $this->_url_boleto . "?merchantid=" . $this->_config->loja . "&orderid=" . $this->_idpedido . "&idpedido=" . $this->_idpedido;
			
			// Monta os parametros
			$param = array(
				'tipo' => "boleto",
				'url' => $url,
				'processamento' => date("Y-m-d H:i:s")
			);
			
			// Armazena o log do pedido
			$this->status(6, json_encode($param), "url");
			
			// Retorna a confirmação
			return TRUE;
		}
	}
	
	/**
	 * Confirmação de captura
	 * 
	 * @name captura
	 * @return boolean
	 */
	public function captura() {
		return TRUE;
	}
	
	/**
	 * Efetua o retorno
	 * 
	 * @name retorno
	 * @return boolean
	 */
	public function retorno() {
		// Busca o tipo da transação
		switch($this->_request->getParam("transId")) {
		
			// Busca das informações do boleto
			case "getBoleto":
				// Cria o model dos pedidos
				$model = new Admin_Model_Pedidos();
				
				// Busca as informações do pedido
				$row = $model->fetchRow(array("idpedido = ?" => $this->_idpedido));
				
				// Busca as informações da url
				$model = new Admin_Model_Pedidosstatus();
				$pedido_status = $model->fetchRow(array('idpedido = ?' => $this->_idpedido, "dados = 'url'"));
				$pedido_status_data = json_decode($pedido_status->meta_dados);
				
				// Monta a descrição do pedido
				$retorno = "<BEGIN_ORDER_DESCRIPTION><orderid>=(" . $row->idpedido . ")\r\n";
				
				// Verifica se é um ambiente de teste
				if($this->_config->ambiente == "TESTE") {
					$row->valor_frete = 0;
					$row->valor_pedido = 1.00;
					
					$retorno .= "<descritivo>=(produto de teste)\r\n";
					$retorno .= "<quantidade>=(1)\r\n";
					$retorno .= "<unidade>=(un)\r\n";
					$retorno .= "<valor>=(100)\r\n";
				}
				else {
					foreach($row->findDependentRowset("Admin_Model_Produtospedidos") as $produto) {
						$retorno .= "<descritivo>=(" . $produto->titulo . ")\r\n";
						$retorno .= "<quantidade>=(" . (int)$produto->quantidade . ")\r\n";
						$retorno .= "<unidade>=(un)\r\n";
						$forvalor1 = $row->valor_pedido - $row->valor_frete;
						$forvalor2 = number_format($forvalor1, 2);
						$forvalor2 = str_replace(",", "", $forvalor2);
						$retorno .= "<valor>=(" . str_replace(".", "", $forvalor2) . ")\r\n";
					}
				}
				
				// Verifica se tem frete
				if($row->valor_frete > 0) {
					$retorno .= "<adicional>=(Taxa de envio)\r\n";
					$retorno .= "<valorAdicional>=(" . str_replace(".", "", str_replace(",", "", $row->valor_frete)) . ")\r\n";
				}
				$retorno .= "<END_ORDER_DESCRIPTION>\r\n";
				
				// Monta a validade
				$vencimento = time() + ($this->_config->validade * 86400);
				
				// Monta os dados do boleto
				$retorno .= "<BEGIN_BOLETO_DESCRIPTION><CEDENTE>=(" . $this->_config->cedente . ")\r\n";
				$retorno .= "<BANCO>=(" . $this->_config->banco . ")\r\n";
				$retorno .= "<NUMEROAGENCIA>=(" . $this->_config->agencia . ")\r\n";
				$retorno .= "<NUMEROCONTA>=(" . $this->_config->conta . ")\r\n";
				$retorno .= "<ASSINATURA>=(" . $this->_config->assinatura . ")\r\n";
				$retorno .= "<DATAEMISSAO>=(" . date("d/m/Y", strtotime($row->data_criacao)) . ")\r\n";
				$retorno .= "<DATAPROCESSAMENTO>=(" . date("d/m/Y", strtotime($pedido_status_data->processamento)) . ")\r\n";
				$retorno .= "<DATAVENCIMENTO>=(" . date("d/m/Y", $vencimento) . ")\r\n";
				$retorno .= "<NOMESACADO>=(" . utf8_decode($row->nome_pagamento . " " . $row->sobrenome_pagamento) . ")\r\n";
				$retorno .= "<ENDERECOSACADO>=(" . $row->endereco_pagamento . ", " . $row->numero_pagamento . ")\r\n";
				$retorno .= "<CIDADESACADO>=(" . $row->cidade_pagamento . ")\r\n";
				$retorno .= "<UFSACADO>=(" . $row->estado_pagamento . ")\r\n";
				$retorno .= "<CEPSACADO>=(" . str_replace("-", "", trim($row->cep_pagamento)) . ")\r\n";
				$retorno .= "<CPFSACADO>=(" . str_replace(".", "", str_replace("-", "", $row->documento_pagamento)) . ")\r\n";
				$retorno .= "<NUMEROPEDIDO>=(" . $this->_idpedido . ")\r\n";
				$retorno .= "<VALORDOCUMENTOFORMATADO>=(R$" . number_format($row->valor_pedido, 2, ",", ".") . ")\r\n";
				$retorno .= "<SHOPPINGID>=(0)<END_BOLETO_DESCRIPTION>\r\n";
				
				// Atualiza as informações do vencimento
				$model = new Admin_Model_Pedidosstatus();
				$data = (array)$pedido_status_data;
				$data['vencimento'] = date("Y-m-d H:i:s", $vencimento);
				$data['valor'] = $row->valor_pedido;
				$model->update(array('meta_dados' => json_encode($data)), array('idpedido = ?' => $this->_idpedido, "dados = 'url'"));
				
				// Exibe o retorno
				header("Content-type: text/plain");
				echo utf8_decode($retorno);
				
				// Salva o retorno
				$this->status(6, json_encode($row->toArray()), $retorno);
				
				// Finaliza a busca do boleto
				break;
			
			// Autentica o boleto
			case "putAuthBoleto":
			
				// Cria o model dos pedidos
				$model = new Admin_Model_Pedidos();
				
				// Busca as informações do pedido
				$row = $model->fetchRow(array("idpedido = ?" => $this->_idpedido));
				
				// Monta o vetor do retorno
				$data = array();
				$data['if'] = $this->_request->getParam("if");
				$data['numero_titulo'] = $this->_request->getParam("NumeroTitulo");
				
				// Salva o retorno
				$this->status(6, json_encode($data), "autenticacao");
				
				// Retorna a confirmação
				header("Content-type: text/plain");
				echo "<PUT_AUTH_OK>";
				
				// Finaliza a busca do boleto
				break;
				
			// Caso der erro
			default:
				// Armazena as informações do erro
				$data = array();
				$data['codigo_erro'] = $this->_request->getParam("cod");
				$data['descricao_erro'] = $this->_request->getParam("ErrorDesc");
				
				// Salva o retorno
				$this->status(6, json_encode($data), "erro");
				
				// Exibe o retorno
				header("Content-type: text/plain");
				echo "<ERRO>";
				
				// Finaliza a busca do boleto
				break;
		}
	}
}