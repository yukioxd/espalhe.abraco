<?php
/**
 * Classe de pagamentos pelo gateway da locaweb 
 *
 * @name Cms_Pagamentos_Locaweb
 */
class Cms_Pagamentos_Locaweb extends Cms_Pagamentos_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "Locaweb";
	
	/**
	 * Armazena o endereço do gateway
	 * 
	 * @access private
	 * @name $_gateway
	 * @var string
	 */
	private $_gateway = "https://comercio.locaweb.com.br/comercio.comp";
	
	/**
	 * Armazena as informações do cartão
	 *
	 * @access private
	 * @name $_cartao
	 * @var array
	 */
	private $_cartao = array();
	
	/**
	 * Armazena a autorização
	 *
	 * @access private
	 * @name $_info_autorizacao
	 * @var array
	 */
	private $_info_autorizacao = array();
	
	/**
	 * Armazena a captura
	 *
	 * @access private
	 * @name $_info_captura
	 * @var array
	 */
	private $_info_captura = array();
	
	/**
	 * Inicializa a classe de pagamento
	 *
	 * @name init
	 */
	public function init() {
		// Prepara a validade do cartão
		//$validade = explode("/", $this->_request->getParam("validade_cartao", NULL));
		
		// Verifica a bandeira
		if($this->_idmetodo_pagamento == 1130) {
			//
			$bandeira = "visa";
			$modulo = "CIELO";
		}
		elseif($this->_idmetodo_pagamento == 1140) {
			$bandeira = "mastercard";
			$modulo = "CIELO";
		}
		elseif($this->_idmetodo_pagamento == 1150) {
			$bandeira = "diners";
			$modulo = "CIELO";
		}
		elseif($this->_idmetodo_pagamento == 1160) {
			$bandeira = "amex";
			$modulo = "CIELO";
		}
		elseif($this->_idmetodo_pagamento == 1170) {
			$bandeira = "elo";
			$modulo = "CIELO";
		}
		elseif($this->_idmetodo_pagamento == 1180) {
			$bandeira = "visaelectron";
			$modulo = "CIELO";
		}
		elseif($this->_idmetodo_pagamento == 1190) {
			$bandeira = "discover";
			$modulo = "CIELO";
		}
		
		// Recupera as informações do cartão
		$this->_cartao = array(
			'numero' => $this->_request->getParam("numerocartao", NULL),
			'nome' => $this->_request->getParam("nomeimpresso", NULL),
			'validade' => $this->_request->getParam("mesvalidade", NULL) . "/" . $this->_request->getParam("anovalidade", NULL),
			'ano' => $this->_request->getParam("anovalidade", NULL),
			'mes' => $this->_request->getParam("mesvalidade", NULL),
			'codigo' => $this->_request->getParam("codigoseguranca", NULL),
			'parcelas' => $this->_request->getParam("parcelas", 1),
			'bandeira' => $bandeira,
			'modulo' => $modulo
		);
		
		// Verifica se é ambiente de teste
		if($this->_config->ambiente == "TESTE") {
			// Ajusta o valor
			$this->_pedido->valor_pedido = (int)$this->_pedido->valor_pedido . "00";
		}
		else {
			// Ajusta o valor
			$this->_pedido->valor_pedido = str_replace(",", "", str_replace(".", "", $this->_pedido->valor_pedido));
		}
	}
	
	/**
	 * Efetua o pagamento
	 * 
	 * @name pagamento
	 * @param int $idpedido Numero do pedido à ser pago
	 * @return boolean
	 */
	public function pagamento() {
		// Monta os parametros para o formulario de pagamento
		$param = array(
			'identificacao' => $this->_config->identificacao,
			'modulo' => $this->_cartao['modulo'],
			'operacao' => "Autorizacao-Direta",
			'ambiente' => $this->_config->ambiente,
			'nome_portador_cartao' => $this->_cartao['nome'],
			'numero_cartao' => $this->_cartao['numero'],
			'validade_cartao' => $this->_cartao['ano'] . $this->_cartao['mes'],
			'indicador_cartao' => "1",
			'codigo_seguranca_cartao' => $this->_cartao['codigo'],
			'idioma' => "PT",
			'valor' => $this->_pedido->valor_pedido,
			'pedido' => $this->_pedido->idpedido,
			'descricao' => "",
			'bandeira' => $this->_cartao['bandeira'],
			'forma_pagamento' => ($this->_pedido->parcelas == 1) ? "1" : "2",
			'parcelas' => $this->_pedido->parcelas,
			'capturar' => "false",
			'campo_livre' => ""
		);
		
		//Zend_Debug::dump($param); exit;
		
		// Faz a requisição para o pagamento
		$client = new Zend_Http_Client();
		$client->setUri($this->_gateway);
		$client->setParameterPost($param);
		$response = $client->request("POST");
		$result_body = $response->getBody();
		
		// Da o parse no xml de retorno
		$this->_info_autorizacao = $this->_autorizacao($result_body);
		
		// Verifica o retorno
		if($this->_info_autorizacao['codigo_erro'] > 0) {
			// Monta o log de erro
			$data = array();
			$data['erro'] = $this->_info_autorizacao['codigo_erro'];
			$data['mensagem'] = $this->_info_autorizacao['mensagem_erro'];
			
			// Salva o log da transação
			$this->status(11, json_encode($data), $result_body);
			
			// Retorna o erro
			return FALSE;
		}
		else {
			// Verifica o status do retorno
			switch($this->_info_autorizacao['status']) {
				// Não aprovado
				case 3:
				case 5:
					$status = 11;
					break;
				
				// Pendente
				case 8:
					$status = 1;
					break;
					
				// Cancelada
				case 9:
					$status = 3;
					break;
					
				// Em andamento
				case 0:
				case 1:
				case 2:
				case 4:
				case 6:
				case 10:
				default:
					$status = 6;
					break;
			}

			// Salva o log da transação
			$this->status($status, json_encode($this->_info_autorizacao), $result_body);
			
			// Retorna ao controlador principal
			if($status == 6) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
	}
	
	
	/**
	 * Captura de pagamento
	 * 
	 * @name captura
	 */
	public function captura() {
		// Monta os parametros para o formulario de pagamento
		$param = array(
			'identificacao' => $this->_config->identificacao,
			'modulo' => $this->_cartao['modulo'],
			'operacao' => "Captura",
			'ambiente' => $this->_config->ambiente,
			'tid' => $this->_info_autorizacao['tid'],
			'valor' => $this->_pedido->valor_pedido,
			'campo_livre' => ""
		);
		
		// Faz a requisição para o pagamento
		$client = new Zend_Http_Client();
		$client->setUri($this->_gateway);
		$client->setParameterPost($param);
		$response = $client->request("POST");
		$result_body = $response->getBody();
		
		// Da o parse no xml de retorno
		$this->_info_captura = $this->_captura($result_body);
		
		// Verifica o retorno
		if($this->_info_captura['codigo_erro'] > 0) {
			// Monta o log de erro
			$data = array();
			$data['erro'] = $this->_info_captura['codigo_erro'];
			$data['mensagem'] = $this->_info_captura['mensagem_erro'];
			
			// Salva o log da transação
			$this->status(11, json_encode($data), $result_body);
			
			// Retorna o erro
			return FALSE;
		}
		else {
			// Verifica o status do retorno
			switch($this->_info_captura['status']) {
				// Não aprovado
				case 3:
				case 5:
					$status = 11;
					break;
					
				// Cancelada
				case 9:
					$status = 3;
					break;

				// Concluida
				case 6:
					$status = 4;
					break;
					
				// Em andamento
				case 0:
				case 1:
				case 2:
				case 4:
				case 8:
				case 10:
				default:
					$status = 6;
					break;
			}
			
			//die("Autorização: " . $this->_info_captura['status']);
			
			// Salva o log da transação
			$this->status($status, json_encode($this->_info_captura), $result_body);
			
			// Retorna ao controlador principal
			if($status == 6 || $status == 4) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
	}
	
	/**
	 * Da o parse no retorno da captura
	 * 
	 * @access private
	 * @name _captura
	 * @return array
	 */
	private function _captura($XMLtransacao) {
		// Inicia o vetor de retorno
		$retorno = array();
		
		// Carrega o XML
		$objDom = new DomDocument();
		$loadDom = $objDom->loadXML($XMLtransacao);
		
		$nodeErro = $objDom->getElementsByTagName('erro')->item(0);
		if ($nodeErro != '') {
			$nodeCodigoErro = $nodeErro->getElementsByTagName('codigo')->item(0);
			$retorno['codigo_erro'] = $nodeCodigoErro->nodeValue;
		
			$nodeMensagemErro = $nodeErro->getElementsByTagName('mensagem')->item(0);
			$retorno['mensagem_erro'] = $nodeMensagemErro->nodeValue;
		}
		
		$nodeTransacao = $objDom->getElementsByTagName('transacao')->item(0);
		if ($nodeTransacao != '') {
			$nodeTID = $nodeTransacao->getElementsByTagName('tid')->item(0);
			$retorno['tid_erro'] = $nodeTID->nodeValue;
		
			$nodePAN = $nodeTransacao->getElementsByTagName('pan')->item(0);
			$retorno['pan_erro'] = $nodePAN->nodeValue;
		
			$nodeDadosPedido = $nodeTransacao->getElementsByTagName('dados-pedido')->item(0);
			if ($nodeTransacao != '') {
				$nodeNumero = $nodeDadosPedido->getElementsByTagName('numero')->item(0);
				$retorno['numero_pedido'] = $nodeNumero->nodeValue;
		
				$nodeValor = $nodeDadosPedido->getElementsByTagName('valor')->item(0);
				$retorno['valor_pedido'] = $nodeValor->nodeValue;
		
				$nodeMoeda = $nodeDadosPedido->getElementsByTagName('moeda')->item(0);
				$retorno['moeda_pedido'] = $nodeMoeda->nodeValue;
		
				$nodeDataHora = $nodeDadosPedido->getElementsByTagName('data-hora')->item(0);
				$retorno['data_hora_pedido'] = $nodeDataHora->nodeValue;
		
				$nodeDescricao = $nodeDadosPedido->getElementsByTagName('descricao')->item(0);
				$retorno['descricao_pedido'] = $nodeDescricao->nodeValue;
		
				$nodeIdioma = $nodeDadosPedido->getElementsByTagName('idioma')->item(0);
				$retorno['idioma_pedido'] = $nodeIdioma->nodeValue;
			}
		
			$nodeFormaPagamento = $nodeTransacao->getElementsByTagName('forma-pagamento')->item(0);
			if ($nodeFormaPagamento != '') {
				$nodeBandeira = $nodeFormaPagamento->getElementsByTagName('bandeira')->item(0);
				$retorno['bandeira_pagamento'] = $nodeBandeira->nodeValue;
		
				$nodeProduto = $nodeFormaPagamento->getElementsByTagName('produto')->item(0);
				$retorno['produto_pagamento'] = $nodeProduto->nodeValue;
		
				$nodeParcelas = $nodeFormaPagamento->getElementsByTagName('parcelas')->item(0);
				$retorno['parcelas_pagamento'] = $nodeParcelas->nodeValue;
			}
		
			$nodeStatus = $nodeTransacao->getElementsByTagName('status')->item(0);
			$retorno['status'] = $nodeStatus->nodeValue;
		
			$nodeCaptura = $nodeTransacao->getElementsByTagName('captura')->item(0);
			if ($nodeCaptura != '') {
				$nodeCodigoCaptura = $nodeCaptura->getElementsByTagName('codigo')->item(0);
				$retorno['codigo_captura'] = $nodeCodigoCaptura->nodeValue;
		
				$nodeMensagemCaptura = $nodeCaptura->getElementsByTagName('mensagem')->item(0);
				$retorno['mensagem_captura'] = $nodeMensagemCaptura->nodeValue;
		
				$nodeDataHoraCaptura = $nodeCaptura->getElementsByTagName('data-hora')->item(0);
				$retorno['data_hora_captura'] = $nodeDataHoraCaptura->nodeValue;
		
				$nodeValorCaptura = $nodeCaptura->getElementsByTagName('valor')->item(0);
				$retorno['valor_captura'] = $nodeValorCaptura->nodeValue;
			}
		
			$nodeURLAutenticacao = $nodeTransacao->getElementsByTagName('url-autenticacao')->item(0);
			$retorno['url'] = $nodeURLAutenticacao->nodeValue;
		}
		
		// Retorno da autenticacao
		return $retorno;
	}
	
	/**
	 * Da o parse no retorno da autorização
	 * 
	 * @access private
	 * @name _autorizacao
	 * @return array 
	 */
	private function _autorizacao($XMLtransacao) {
		// Inicia o vetor de retorno
		$retorno = array();
		
		// Carrega o XML
		$objDom = new DomDocument();
		$loadDom = $objDom->loadXML($XMLtransacao);
		
		$nodeErro = $objDom->getElementsByTagName('erro')->item(0);
		if ($nodeErro != '') {
			$nodeCodigoErro = $nodeErro->getElementsByTagName('codigo')->item(0);
			$retorno['codigo_erro'] = $nodeCodigoErro->nodeValue;
		
			$nodeMensagemErro = $nodeErro->getElementsByTagName('mensagem')->item(0);
			$retorno['mensagem_erro'] = $nodeMensagemErro->nodeValue;
		}
		
		$nodeTransacao = $objDom->getElementsByTagName('transacao')->item(0);
		if ($nodeTransacao != '') {
			$nodeTID = $nodeTransacao->getElementsByTagName('tid')->item(0);
			$retorno['tid'] = $nodeTID->nodeValue;
		
			$nodePAN = $nodeTransacao->getElementsByTagName('pan')->item(0);
			$retorno['pan'] = $nodePAN->nodeValue;
		
			$nodeDadosPedido = $nodeTransacao->getElementsByTagName('dados-pedido')->item(0);
			if ($nodeTransacao != '') {
				$nodeNumero = $nodeDadosPedido->getElementsByTagName('numero')->item(0);
				$retorno['numero_pedido'] = $nodeNumero->nodeValue;
		
				$nodeValor = $nodeDadosPedido->getElementsByTagName('valor')->item(0);
				$retorno['valor_pedido'] = $nodeValor->nodeValue;
				
				$nodeMoeda = $nodeDadosPedido->getElementsByTagName('moeda')->item(0);
				$retorno['moeda_pedido'] = $nodeMoeda->nodeValue;
		
				$nodeDataHora = $nodeDadosPedido->getElementsByTagName('data-hora')->item(0);
				$retorno['data_hora_pedido'] = $nodeDataHora->nodeValue;
		
				$nodeDescricao = $nodeDadosPedido->getElementsByTagName('descricao')->item(0);
				$retorno['descricao_pedido'] = $nodeDescricao->nodeValue;
		
				$nodeIdioma = $nodeDadosPedido->getElementsByTagName('idioma')->item(0);
				$retorno['idioma_pedido'] = $nodeIdioma->nodeValue;
			}
		
			$nodeFormaPagamento = $nodeTransacao->getElementsByTagName('forma-pagamento')->item(0);
			if ($nodeFormaPagamento != '') {
				$nodeBandeira = $nodeFormaPagamento->getElementsByTagName('bandeira')->item(0);
				$retorno['bandeira_pagamento'] = $nodeBandeira->nodeValue;
		
				$nodeProduto = $nodeFormaPagamento->getElementsByTagName('produto')->item(0);
				$retorno['produto_pagamento'] = $nodeProduto->nodeValue;
		
				$nodeParcelas = $nodeFormaPagamento->getElementsByTagName('parcelas')->item(0);
				$retorno['parcelas_pagamento'] = $nodeParcelas->nodeValue;
			}
		
			$nodeStatus = $nodeTransacao->getElementsByTagName('status')->item(0);
			$retorno['status'] = $nodeStatus->nodeValue;
		
			$nodeAutenticacao = $nodeTransacao->getElementsByTagName('autenticacao')->item(0);
			if ($nodeAutenticacao != '') {
				$nodeCodigoAutenticacao = $nodeAutenticacao->getElementsByTagName('codigo')->item(0);
				$retorno['codigo_autenticacao'] = $nodeCodigoAutenticacao->nodeValue;
		
				$nodeMensagemAutenticacao = $nodeAutenticacao->getElementsByTagName('mensagem')->item(0);
				$retorno['mensagem_autenticacao'] = $nodeMensagemAutenticacao->nodeValue;
		
				$nodeDataHoraAutenticacao = $nodeAutenticacao->getElementsByTagName('data-hora')->item(0);
				$retorno['data_hora_autenticacao'] = $nodeDataHoraAutenticacao->nodeValue;
		
				$nodeValorAutenticacao = $nodeAutenticacao->getElementsByTagName('valor')->item(0);
				$retorno['valor_autenticacao'] = $nodeValorAutenticacao->nodeValue;
		
				$nodeECIAutenticacao = $nodeAutenticacao->getElementsByTagName('eci')->item(0);
				$retorno['eci_autenticacao'] = $nodeECIAutenticacao->nodeValue;
			}
		
			$nodeAutorizacao = $nodeTransacao->getElementsByTagName('autorizacao')->item(0);
			if ($nodeAutorizacao != '') {
				$nodeCodigoAutorizacao = $nodeAutorizacao->getElementsByTagName('codigo')->item(0);
				$retorno['codigo_autorizacao'] = $nodeCodigoAutorizacao->nodeValue;
		
				$nodeMensagemAutorizacao = $nodeAutorizacao->getElementsByTagName('mensagem')->item(0);
				$retorno['mensagem_autorizacao'] = $nodeMensagemAutorizacao->nodeValue;
		
				$nodeDataHoraAutorizacao = $nodeAutorizacao->getElementsByTagName('data-hora')->item(0);
				$retorno['data_hora_autorizacao'] = $nodeDataHoraAutorizacao->nodeValue;
		
				$nodeValorAutorizacao = $nodeAutorizacao->getElementsByTagName('valor')->item(0);
				$retorno['valor_autorizacao'] = $nodeValorAutorizacao->nodeValue;
		
				$nodeLRAutorizacao = $nodeAutorizacao->getElementsByTagName('lr')->item(0);
				$retorno['lr'] = $nodeLRAutorizacao->nodeValue;
		
				$nodeARPAutorizacao = $nodeAutorizacao->getElementsByTagName('arp')->item(0);
				$retorno['arp'] = $nodeARPAutorizacao->nodeValue;
			}
		
			$nodeURLAutenticacao = $nodeTransacao->getElementsByTagName('url-autenticacao')->item(0);
			$retorno['url'] = $nodeURLAutenticacao->nodeValue;
		}
		
		// Retorno da autenticacao
		return $retorno;
	}
}
