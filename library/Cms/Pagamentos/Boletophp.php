<?php
/**
 * Classe de pagamentos por boleto bancário (Boleto PHP)
 *
 * @name Cms_Pagamentos_Boletophp
 */
class Cms_Pagamentos_Boletophp extends Cms_Pagamentos_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "boletophp";
	
	/**
	 * Inicializa a classe de pagamento
	 *
	 * @name init
	 */
	public function init() {
		// Armazena as configurações globais
		$this->_application_config = Zend_Registry::get("config");
		$this->_modules = Zend_Registry::get("modulos");
	}
	
	/**
	 * Efetua o pagamento
	 * 
	 * @name pagamento
	 * @return boolean
	 */
	public function pagamento() {
		// Verifica se o método de pagamento e boleto
		if($this->_idmetodo_pagamento == 1410) {
			// Monta os parametros
			$param = array(
				'status' => 'processado',
				'processamento' => date("Y-m-d H:i:s")
			);
			
			// Armazena o log do pedido
			$this->status(6, json_encode($param), "boleto");
			
			// Retorna a confirmação
			return TRUE;
		}
	}
	
	/**
	 * Confirmação de captura
	 * 
	 * @name captura
	 * @return boolean
	 */
	public function captura() {
		return TRUE;
	}
	
	/**
	 * Gera o boleto
	 * 
	 * @name gerar
	 */
	public function gerar() {
		// Cria o model dos status do pedido
		$model = new Admin_Model_Pedidosstatus();
		
		// Busca o ultimo status do pedido
		$row = $model->fetchRow(array('idpedido = ?' => $this->_idpedido, "identificacao = 'boletophp'"), "data_execucao DESC");
		$meta_data = json_decode($row->meta_dados);
		
		// Verifica se o boleto ja foi pago
		if($meta_data->status == "pago") {
			die("Boleto ja foi pago");
		}
		
		// Monta os parametros
		$param = array(
			'status' => 'gerado'
		);
			
		// Armazena o log do pedido
		$this->status(6, json_encode($param), "boleto");
		
		// Verifica se o arquivo existe
		$boleto_filename = APPLICATION_PATH . "/../library/Cms/Library/boletophp/boleto_" . $this->_config->boleto . ".php";
		if(!file_exists($boleto_filename)) {
			throw new Zend_Exception("Não foi possivel encontrar o arquivo do boleto");
		}
		
		// Ajusta as variaveis
		$_application_config = $this->_application_config;
		
		// inclui as funções para geração do boleto
		include($boleto_filename);
		include(APPLICATION_PATH . "/../library/Cms/Library/boletophp/include/funcoes_" . $this->_config->boleto . ".php"); 
		include(APPLICATION_PATH . "/../library/Cms/Library/boletophp/include/layout_" . $this->_config->boleto . ".php");
	}
}
