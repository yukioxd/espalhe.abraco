<?php
/**
 * Classe de pagamentos por bcash
 *
 * @name Cms_Pagamentos_Bcash
 */
class Cms_Pagamentos_Bcash extends Cms_Pagamentos_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "bcash";
	
	/**
	 * Inicializa a classe de pagamento
	 *
	 * @name init
	 */
	public function init() {
	}
	
	/**
	 * Efetua o pagamento
	 * 
	 * @name pagamento
	 * @return boolean
	 */
	public function pagamento() {
		// Monta os parametros
		$param = array(
			'status' => 'processado',
			'processamento' => date("Y-m-d H:i:s")
		);
			
		// Armazena o log do pedido
		$this->status(1, json_encode($param), "inicial");
		
		// Retorna a confirmação
		return TRUE;
	}
	
	/**
	 * Confirmação de captura
	 * 
	 * @name captura
	 * @return boolean
	 */
	public function captura() {
		return TRUE;
	}
	
	/**
	 * Verifica se os dados são verdadeiros
	 *
	 * @name verify
	 * @return boolean
	 */
	public function verify() {
		
		// Recupera a chave da loja
		$token = $this->_config->chave;
		
		// Recupera os dados do bcash
		$id_transacao = $this->_request->getParam("id_transacao", 0);
		$data_transacao = $this->_request->getParam("data_transacao", 0);
		$data_credito = $this->_request->getParam("data_credito", 0);
		$valor_original = $this->_request->getParam("valor_original", 0);
		$valor_loja = $this->_request->getParam("valor_loja", 0);
		$valor_total = $this->_request->getParam("valor_total", 0);
		$desconto = $this->_request->getParam("desconto", 0);
		$acrescimo = $this->_request->getParam("acrescimo", 0);
		$tipo_pagamento = $this->_request->getParam("tipo_pagamento", 0);
		$parcelas = $this->_request->getParam("parcelas", 0);
		$cliente_nome = $this->_request->getParam("cliente_nome", 0);
		$cliente_email = $this->_request->getParam("cliente_email", 0);
		$cliente_rg = $this->_request->getParam("cliente_rg", 0);
		$cliente_data_emissao_rg = $this->_request->getParam("cliente_data_emissao_rg", 0);
		$cliente_orgao_emissor_rg = $this->_request->getParam("cliente_orgao_emissor_rg", 0);
		$cliente_estado_emissor_rg = $this->_request->getParam("cliente_estado_emissor_rg", 0);
		$cliente_cpf = $this->_request->getParam("cliente_cpf", 0);
		$cliente_sexo = $this->_request->getParam("cliente_sexo", 0);
		$cliente_data_nascimento = $this->_request->getParam("cliente_data_nascimento", 0);
		$cliente_endereco = $this->_request->getParam("cliente_endereco", 0);
		$cliente_complemento = $this->_request->getParam("cliente_complemento", 0);
		$status = $this->_request->getParam("status", 0);
		$cod_status = $this->_request->getParam("cod_status", 0);
		$cliente_bairro = $this->_request->getParam("cliente_bairro", 0);
		$cliente_cidade = $this->_request->getParam("cliente_cidade", 0);
		$cliente_estado = $this->_request->getParam("cliente_estado", 0);
		$cliente_cep = $this->_request->getParam("cliente_cep", 0);
		$frete = $this->_request->getParam("frete", 0);
		$tipo_frete = $this->_request->getParam("tipo_frete", 0);
		$informacoes_loja = $this->_request->getParam("informacoes_loja", 0);
		$id_pedido = $this->_request->getParam("id_pedido", 0);
		$free = $this->_request->getParam("free", 0);
		$qtde_produtos = $this->_request->getParam("qtde_produtos", 0);
		
		// Cria os dados para a virificação dos dados
		$params = "transacao=" . $id_transacao . "&status=" . $status . "&cod_status=" . $cod_status . "&valor_original=" . $valor_original . "&valor_loja=" . $valor_loja . "&token=" . $token;
		$url = "https://www.bcash.com.br/checkout/verify/";
		
		// Faz a requisição
		ob_start();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_exec($ch);
		$resposta = ob_get_contents();
		ob_end_clean();
		
		// Verifica o retorno
		if(trim($resposta) == "VERIFICADO") {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	/**
	 * Executa o processamento do retorno
	 * 
	 * @name run
	 */
	public function run() {
		// Recupera os dados do bcash
		$param = array();
		$param['id_transacao'] = $this->_request->getParam("id_transacao", 0);
		$param['data_transacao'] = $this->_request->getParam("data_transacao", 0);
		$param['data_credito'] = $this->_request->getParam("data_credito", 0);
		$param['valor_original'] = $this->_request->getParam("valor_original", 0);
		$param['valor_loja'] = $this->_request->getParam("valor_loja", 0);
		$param['valor_total'] = $this->_request->getParam("valor_total", 0);
		$param['desconto'] = $this->_request->getParam("desconto", 0);
		$param['acrescimo'] = $this->_request->getParam("acrescimo", 0);
		$param['tipo_pagamento'] = utf8_encode($this->_request->getParam("tipo_pagamento", 0));
		$param['parcelas'] = $this->_request->getParam("parcelas", 0);
		$param['cliente_nome'] = utf8_encode($this->_request->getParam("cliente_nome", 0));
		$param['cliente_email'] = $this->_request->getParam("cliente_email", 0);
		$param['cliente_rg'] = $this->_request->getParam("cliente_rg", 0);
		$param['cliente_data_emissao_rg'] = $this->_request->getParam("cliente_data_emissao_rg", 0);
		$param['cliente_orgao_emissor_rg'] = $this->_request->getParam("cliente_orgao_emissor_rg", 0);
		$param['cliente_estado_emissor_rg'] = $this->_request->getParam("cliente_estado_emissor_rg", 0);
		$param['cliente_cpf'] = $this->_request->getParam("cliente_cpf", 0);
		$param['cliente_sexo'] = $this->_request->getParam("cliente_sexo", 0);
		$param['cliente_data_nascimento'] = $this->_request->getParam("cliente_data_nascimento", 0);
		$param['cliente_endereco'] = $this->_request->getParam("cliente_endereco", 0);
		$param['cliente_complemento'] = $this->_request->getParam("cliente_complemento", 0);
		$param['status'] = utf8_encode($this->_request->getParam("status", 0));
		$param['cod_status'] = $this->_request->getParam("cod_status", 0);
		$param['cliente_bairro'] = utf8_encode($this->_request->getParam("cliente_bairro", 0));
		$param['cliente_cidade'] = utf8_encode($this->_request->getParam("cliente_cidade", 0));
		$param['cliente_estado'] = utf8_encode($this->_request->getParam("cliente_estado", 0));
		$param['cliente_cep'] = $this->_request->getParam("cliente_cep", 0);
		$param['frete'] = $this->_request->getParam("frete", 0);
		$param['tipo_frete'] = $this->_request->getParam("tipo_frete", 0);
		$param['informacoes_loja'] = utf8_encode($this->_request->getParam("informacoes_loja", 0));
		$param['id_pedido'] = $this->_request->getParam("id_pedido", 0);
		$param['free'] = utf8_encode($this->_request->getParam("free", 0));
		$param['qtde_produtos'] = $this->_request->getParam("qtde_produtos", 0);
		
		// Verifica o status da transação
		switch($param['cod_status']) {
			case 0:
				$status = 6;
				break;
			case 1:
				$status = 2;
				break;
			case 2:
				$status = 3;
				// Remove a reserva do produto
				//Cms_Estoque::delReserva(NULL, $param['id_pedido']);
				break;
		}
		
		// Armazena o log do pedido
		$param['processamento'] = date("Y-m-d H:i:s");
		$this->status($status, json_encode($param), "automatico");
		
		// Retorna ao controlador
		return TRUE;
	}
}
