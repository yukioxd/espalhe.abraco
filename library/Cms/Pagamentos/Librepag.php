<?php
/**
 * Classe de pagamentos pelo gateway da librepag
 *
 * @name Cms_Pagamentos_Librepag
 */
class Cms_Pagamentos_Librepag extends Cms_Pagamentos_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "Librepag";
	
	/**
	 * Armazena o endereço do gateway de pagamento
	 * 
	 * @access private
	 * @name $_gateway_pagamento
	 * @var string
	 */
	private $_gateway_pagamento = "http://www.librepag.com.br/pagamento";
	
	/**
	 * Armazena o endereço do gateway de retorno
	 *
	 * @access private
	 * @name $_gateway_retorno
	 * @var string
	 */
	private $_gateway_retorno = "http://www.librepag.com.br/retorno";
	
	/**
	 * Armazena as informações do cartão
	 *
	 * @access private
	 * @name $_cartao
	 * @var array
	 */
	private $_cartao = array();
	
	/**
	 * Armazena os parametros para pagina de retorno
	 *
	 * @access protected
	 * @name $_return_param
	 * @var Array
	 */
	protected $_return_param = array();
	
	/**
	 * Inicializa a classe de pagamento
	 *
	 * @name init
	 */
	public function init() {
		// Prepara a validade do cartão
		$validade = explode("/", $this->_request->getParam("validade_cartao", NULL));
		
		// Verifica a bandeira
		if($this->_idmetodo_pagamento == 1030) {
			$bandeira = "visa";
		}
		elseif($this->_idmetodo_pagamento == 1040) {
			$bandeira = "mastercard";
		}
		
		// Recupera as informações do cartão
		$this->_cartao = array(
			'numero' => $this->_request->getParam("numero_cartao", NULL),
			'nome' => $this->_request->getParam("nome_cartao", NULL),
			'validade' => $this->_request->getParam("validade_cartao", NULL),
			'ano' => $validade[1],
			'mes' => $validade[0],
			'codigo' => $this->_request->getParam("codigo_cartao", NULL),
			'parcelas' => $this->_request->getParam("parcelas", 1),
			'bandeira' => $bandeira
		);
		
		// Busca a sessão de mensagens
		$session = new Zend_Session_Namespace("messages");
		
		// Verifica os dados do cartão
		if($this->_request->getParam("numero_cartao", NULL) == NULL) {
			// Redireciona o usuário para a pagina anterior
			$session->error = "Informe o numero do cartão";
			$this->_redirect($_SERVER['HTTP_REFERER']);
		}
		if($this->_request->getParam("nome_cartao", NULL) == NULL) {
			// Redireciona o usuário para a pagina anterior
			$session->error = "Informe o nome do cartão";
			$this->_redirect($_SERVER['HTTP_REFERER']);
		}
		if($this->_request->getParam("validade_cartao", NULL) == NULL) {
			// Redireciona o usuário para a pagina anterior
			$session->error = "Informe a validade do cartão";
			$this->_redirect($_SERVER['HTTP_REFERER']);
		}
		if($this->_request->getParam("codigo_cartao", NULL) == NULL) {
			// Redireciona o usuário para a pagina anterior
			$session->error = "Informe o código do cartão";
			$this->_redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	/**
	 * Efetua o pagamento
	 * 
	 * @name pagamento
	 * @return boolean
	 */
	public function pagamento() {
		// Verifica se é ambiente de teste
		if($this->_config->ambiente == "TESTE") {
			$idpedido = "CWB" . $this->_idpedido;
			$valor_pedido = "1.01";
		}
		else {
			$idpedido = $this->_idpedido;
			$valor_pedido = $this->_pedido->valor_pedido;
		}
		
		// Monta os parametros para o formulario de pagamento
		$param = array(
			'identificacao' => $this->_config->identificacao,
			'metodo' => $this->_cartao['bandeira'],
			'operacao' => "Pagamento",
			'pedido' => $idpedido,
			'valor' => $valor_pedido,
			'parcelas' => $this->_cartao['parcelas'],
			'nome_cartao' => $this->_cartao['nome'],
			'num_cartao' => $this->_cartao['numero'],
			'cvv_cartao' => $this->_cartao['codigo'],
			'mes_cartao' => $this->_cartao['mes'],
			'ano_cartao' => $this->_cartao['ano'],
			'nome' => $this->_cartao['nome'],
			'tipo_pessoa' => ($cliente->fisica) ? "f" : "j",
			'documento' => $this->_pedido->documento_pagamento,
			'email' => $this->_pedido->email_pagamento,
			'fone' => $this->_pedido->telefone_pagamento,
			'endereco' => $this->_pedido->endereco_pagamento,
			'numero_endereco' => $this->_pedido->numero_pagamento,
			'complemento' => $this->_pedido->complemento_pagamento,
			'bairro' => $this->_pedido->bairro_pagamento,
			'cidade' => $this->_pedido->cidade_pagamento,
			'estado' => $this->_pedido->estado_pagamento,
			'pais' => $this->_pedido->pais_pagamento,
			'cep' => $this->_pedido->cep_pagamento,
			'url_retorno' => ""
		);
		
		// Faz a requisição para o pagamento
		$client = new Zend_Http_Client();
		$client->setUri($this->_gateway_pagamento);
		$client->setParameterPost($param);
		$response = $client->request("POST");
		$result_body = $response->getBody();
		
		// Armazena o log do pedido
		$this->status(6, json_encode($param), $result_body);
		
		// Da o parse no resultado do pagamento
		preg_match("/<\?xml(.*)<\/AUTHORIZATION>/si", $result_body, $xml);
		$result = simplexml_load_string($xml[0]);
		
		// Monta o vetor do retorno
		$param = array (
			'CODRET' => (string) $result->CODRET[0],
			'MSGRET' => (string) $result->MSGRET[0],
			'NUMPEDIDO' => (string) $result->NUMPEDIDO[0],
			'DATA' => (string) $result->DATA[0],
			'NUMAUTOR' => (string) $result->NUMAUTOR[0],
			'NUMCV' => (string) $result->NUMCV[0],
			'NUMAUTENT' => (string) $result->NUMAUTENT[0],
			'NUMSQN' => (string) $result->NUMSQN[0],
			'ORIGEM_BIN' => (string) $result->ORIGEM_BIN[0],
			'CONFCODRET' => (string) $result->CONFCODRET[0],
			'CONFMSGRET' => (string) $result->CONFMSGRET[0],
			'PAX1' => $this->_config->identificacao
		);
		$this->_return_param = $param;
		
		// Verifica o retorno
		if(($param['CODRET'] >= 0) && ($param['CODRET'] < 50)) {
			// Armazena o log do pedido
			$this->status(6, json_encode($param), $result_body);
			
			// Retorna ao controlador principal
			return TRUE;
		}
		else if($param['CODRET'] > 49)  {
			// Armazena o log do pedido
			$this->status(11, json_encode($param), $result_body);
			
			// Retorna ao controlador principal
			return FALSE;
		}
		else {
			// Armazena o log do pedido
			$this->status(1, json_encode($param), $result_body);
			
			// Retorna ao controlador principal
			return FALSE;
		}
	}
	
	/**
	 * Confirmação de captura
	 * 
	 * @name captura
	 * @return boolean
	 */
	public function captura() {
		// Recupera os parametros para o retorn
		$param = $this->_return_param;
		
		// Faz a requisição do retorno
		$client = new Zend_Http_Client();
		$client->setUri($this->_gateway_retorno);
		$client->setParameterPost($param);
		$response = $client->request("POST");
		$result_body = $response->getBody();
		
		// Faz o parse do retorno
		preg_match_all("/value=\"(.*?)\"/s", $result_body, $xml);
		$return = array (
			'idtransacao' => $xml[1][0],
			'valor' => $xml[1][1],
			'num_pedido' => $xml[1][2],
			'status_pagamento' => $xml[1][3],
			'mensagem_transacao' => $xml[1][4],
			'metodo' => $xml[1][5]
		);
		
		// Verifica o status do retorno
		switch($return['status_pagamento']) {
			case 1:
			case 2:
			case 4:
			case 5:
			case 6:
			case 8:
				$status = 6; // Em andamento
				break;
				
			case 3:
			case 7:
				$status = 11; // Não aprovado
				break;
				
			default:
				$status = 1; // Pendente
		}
		
		// Armazena o log do pedido
		$this->status($status, json_encode($return), $result_body);
		
		// Retorna o status
		if($status == 6) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}