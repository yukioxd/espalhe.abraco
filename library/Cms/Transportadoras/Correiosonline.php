<?php
/**
 * Faz requisições ao webservice dos correios 
 *
 * @name Cms_Transportadoras_Correiosonline
 */
class Cms_Transportadoras_Correiosonline extends Cms_Transportadoras_Abstract {
	/**
	 * Armazena o nome da integração
	 *
	 * @access protected
	 * @name $_name
	 * @var string
	 */
	protected $_name = "correios";
	
	/**
	 * Armazena os métodos de envio à buscar
	 * 
	 * @access protected
	 * @name $metodos_envio
	 * @var array
	 */
	protected $metodos_envio = array();
	
	/**
	 * Armazena as informações dos produtos necessarias para o calculo do frete
	 *
	 * @access protected
	 * @name $infos
	 * @var array
	 */
	protected $infos = array();
	
	/**
	 * Inicializador da classe
	 * 
	 * @name init
	 */
	public function init() {
		// Verifica quais os métodos de envio buscar
		foreach($this->_metodos_envio as $metodo_envio) {
			// Verifica se o código unico faz parte da transportadora
			if((40010 == $metodo_envio) || (40101 == $metodo_envio) || (40045 == $metodo_envio) || (40215 == $metodo_envio) || (41106 == $metodo_envio)) {
				$this->metodos_envio[] = $metodo_envio;
			}
		}
		
		// Cria o where
		$where = array(0);
		foreach($this->_produtos as $idproduto => $quantidade) {
			$where[] = $idproduto;
		}

		// Instancia o model dos produtos
		$model = new Admin_Model_Produtos();

		// Busca os produtos da lista
		$produtos = $model->fetchAll(array("idproduto in (" . implode(", ", $where) . ")"), array("largura DESC", "comprimento DESC"), "idproduto DESC");
		
		// Inicializa as informações dos produtos
		$this->infos['peso'] = 0;
		$this->infos['comprimento'] = $produtos->current()->comprimento;
		$this->infos['largura'] = $produtos->current()->largura;
		$this->infos['altura'] = 0;
		$this->infos['valor_declarado'] = 0;
		
		// Percorre os produtos
		foreach($produtos as $produto) {
			// Soma o peso dos produtos
			$this->infos['peso'] += $produto->peso * $this->_produtos[$produto->idproduto]['quantidade'];
			
			// Soma a altura dos produtos
			$this->infos['altura'] += $produto->altura * $this->_produtos[$produto->idproduto]['quantidade'];
		
			// Soma o preço dos produtos
			$this->infos['valor_declarado'] += $produto->preco_venda * $this->_produtos[$produto->idproduto]['quantidade'];
		}
		
		// Verifica se o comprimento é menor que 16
		if($this->infos['comprimento'] < 16) {
			$this->infos['comprimento'] = 16;
		}
		
		// Verifica se a largura é menor que 11
		if($this->infos['largura'] < 11) {
			$this->infos['largura'] = 11;
		}
		
		// Verifica se a altura é menor que 2
		if($this->infos['altura'] < 2) {
			$this->infos['altura'] = 2;
		}
	}
	
	/**
	 * Faz requisições de frete
	 * 
	 * @name frete
	 * @return array
	 */
	public function fretes() {
		// Cria a url
		$url  = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx";
			$url .= "?nCdEmpresa=";
			$url .= "&sDsSenha=";
			$url .= "&sCepOrigem=" . $this->_config->cep_origem;
			$url .= "&sCepDestino=" . $this->_cep_destino;
			$url .= "&nVlPeso=" . $this->infos['peso'];
			$url .= "&nCdFormato=1";
			$url .= "&nVlComprimento=" . str_replace(".", ",", $this->infos['comprimento']);
			$url .= "&nVlAltura=" . str_replace(".", ",", $this->infos['altura']);
			$url .= "&nVlLargura=" . str_replace(".", ",", $this->infos['largura']);
			$url .= "&sCdMaoPropria=n";
			$url .= "&nVlValorDeclarado=" . number_format($this->infos['valor_declarado'], 2, ",", ".");
			$url .= "&sCdAvisoRecebimento=n";
			$url .= "&nCdServico=" . implode(",", $this->metodos_envio);
			$url .= "&nVlDiametro=0&StrRetorno=xml";
			
		// Cria o objeto de requisição remota e configura
		$client = new Zend_Http_Client();
		$client->setUri($url);
		
		// Faz a requisição
		$response = $client->request("GET");
		$xml = $response->getBody();
		
		// Cria o model dos metodos de envio
		$model = new Admin_Model_Metodosenvio();
		
		// Percorre os retornos
		$fretes = array();
		foreach(simplexml_load_string($xml) as $frete) {
			// Busca o nome do serviço
			$nome = $model->fetchRow(array('idmetodo_envio = ?' => (string)$frete->Codigo))->descricao;

			// Verifica se existe erro
			if(strlen((string)$frete->MsgErro) <= 0) {
				
				// Cria o vetor amigavel
				$fretes[(string)$frete->Codigo] = array(
					'codigo' => (int)$frete->Codigo,
					'nome' => $nome,
					'valor' => (float)str_replace(",", ".", (string)$frete->Valor),
					'entrega' => ((int)$frete->PrazoEntrega) + $modules->frete->somar_dias,
					'erro' => (string)$frete->MsgErro
				);
			}
		}
		
		// Retorna os fretes
		return $fretes;
	}
}