<?php
/**
 * Controla as ações do carrinho
 * 
 * @name Cms_Carrinho
 */
class Cms_Carrinho {
	/**
	 * Armazena a sessão das mensagens
	 *
	 * @access protected
	 * @name $_messages
	 * @var Zend_Session_Namespace
	 */
	protected $_messages = NULL;
	
	/**
	 * Armazena as informações do carrinho
	 *
	 * @access protected
	 * @name $_carrinho
	 * @var Zend_Session_Namespace
	 */
	protected $_carrinho = NULL;
	
	/**
	 * Contrutor da classe
	 * 
	 * @name __construct
	 */
	public function __construct() {
		// Cria a sessão do carrinho
		$this->_carrinho = new Zend_Session_Namespace("carrinho");
	}
	/**
	 * Apaga tudo da session do delivery
	 * 
	 * @name limpaDelivery
	 */
	public function limpaDelivery(){
		// Apaga a session do delivery
		Zend_Session::namespaceUnset('delivery');
	}
	
	/**
	 * Adiciona uma oferta ao carrinho
	 * 
	 * @name adiciona
	 * @param int $idoferta Id da oferta para adicionar
	 * @param bool $presente Flag que indica se a oferta é um presente
	 */
	public function adiciona($idoferta, $quantidade, $presente) {

		// Limpa a session do delivery
		self::limpaDelivery();

		// Monta a linha da oferta
		$oferta = array(
			'idoferta' => $idoferta,
			'quantidade' => $quantidade,
			'qtd_presente' => '0',
			'presente' => $presente
		);
		
		// Cria o index unico
		end($this->_carrinho->ofertas);
		$index = key($this->_carrinho->ofertas) + 1;
		
		// Adiciona a oferta no carrinho
		$this->_carrinho->ofertas[$index] = $oferta;
	}
	
	/**
	 * Remove uma oferta do carrinho
	 *
	 * @name remove
	 * @param int $index Codigo interno da oferta no carrinho
	 */
	public function remove($index) {
		// Remove a entrada no vetor
		unset($this->_carrinho->ofertas[$index]);
	}
	
	/**
	 * Adiciona um contemplado
	 *
	 * @name addContemplado
	 * @param int $index Codigo interno da oferta no carrinho
	 * @param string $nome Nome do contemplado
	 * @param string $email Email do contemplado
	 */
	public function addContemplado($index, $nome, $email) {
		// Adiciona a entrada no vetor
		$this->_carrinho->ofertas[$index]['contemplado'] = array(
			'nome' => $nome,
			'email' => $email
		);
	}
	
	/**
	 * Busca as ofertas do carrinho
	 * 
	 * @name getOfertas
	 * @return array
	 */
	public function getOfertas() {
		// Cria o model das ofertas
		$model = new Admin_Model_Ofertasativacoes();
		
		// Percorre os itens do carrinho
		$ofertas = array();
		
// 		Zend_Session::namespaceUnset('carrinho');
// 		Zend_Debug::dump($this->_carrinho->ofertas); exit;
		foreach($this->_carrinho->ofertas as $index => $oferta) {
			// Busca as informações da oferta
			$select = $model->select()
				->from("ofertas_ativacoes", array(""))
				->columns(array(
					"ofertas_ativacoes.idoferta_ativacao",
					"ofertas.titulo",
					"ofertas_ativacoes.valor_promocional",
					"ofertas_ativacoes.valor",
					"ofertas_ativacoes.limite_comprador",
					"ofertas_ativacoes.limite_presente",
					"ofertas_ativacoes.qtd_parcela_max",
					"ofertas_ativacoes.valor_parcela_min",
					"ofertas_ativacoes.qtd_parcela_sem_juro",
					"ofertas_ativacoes.porcentagem_juro"
				))
				->join("ofertas", "ofertas.idoferta = ofertas_ativacoes.idoferta", array(""))
				->where("ofertas.idoferta = ?", $oferta['idoferta'])
				->where("ofertas_ativacoes.data_inicial <= DATE_FORMAT(NOW(), '%Y-%m-%d') AND ofertas_ativacoes.data_final >= DATE_FORMAT(NOW(), '%Y-%m-%d')")
				->where("ofertas_ativacoes.aprovado = 1")
				->where("ofertas.tipo_emarketing = 0")
				->setIntegrityCheck(FALSE);
			$row = $model->fetchRow($select);
			
			// Cria o vetor das ofertas
			$ofertas[$index] = array(
				'row' => $row,
				'quantidade' => $oferta['quantidade'],
				'qtd_presente' => "0",
				'presente' => $oferta['presente']
			);
		
			$quantidad_present = 0;
			if(count($oferta['contemplado'])){
				foreach($oferta['contemplado'] as $index_presente => $presente){
					$ofertas[$index]['contemplado'][$index_presente] = array(
							'qtd' => $presente['qtd'],
							'nome' => $presente['nome'],
							'email' => $presente['email']
					);
									
					// Quantidade total de presente de cada oferta
					$quantidad_present = $quantidad_present + $presente['qtd'];
				}
				
			}
			
			// Armazena no array da sessão a quantidade total de presente de cada oferta
			$this->_carrinho->ofertas[$index]['qtd_presente'] = $quantidad_present;
		}
	
		return $ofertas;
	}
	
	/**
	 * Retorna a quantidade do produto no carrinho
	 * 
	 * @name getQuantidade
	 * @param int $idproduto ID do produto
	 * @return int
	 */
	//public function getQuantidade($idproduto) {
		//return $this->_carrinho->produtos[$idproduto]['quantidade'];
	//}
}
