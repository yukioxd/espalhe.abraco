<?php

date_default_timezone_set("UTC");

class RespostaEnvio {
	public $Lote, $StatusMensagem, $Status, $Destinatarios;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class RespostaStatus {
	public $StatusSequencias, $StatusCodigo, $StatusMensagem;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class RespostaResposta {
	public $Respostas, $Status, $StatusMensagem;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class RespostaSaldo {
	public $Mailing, $Status, $StatusMensagem;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class RespostaMaisClientes {
	public $Pesquisa, $Celular, $Nome;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class RespostaEntrevistado {
	public $Sequencia, $Status, $Celular, $StatusMensagem;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class RespostaUsuario {
	public $Status;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}



//implementação
class RespostaTransferencia {
	public $Status, $StatusMensagem;
	public function set($data) {
		foreach ($data AS $key => $value) $this->{$key} = $value;
	}
}

class Cms_SMS {
	public $usuario, $senha;
	private $url = 'https://ws.smsdigital.com.br/';

	function __construct($usuario, $senha){
		$this->usuario = $usuario;
		$this->senha = $senha;
	}

	private function invoque_metodo ($metodo, $parametros, $classe){
		//dados da requisição
		$dados = array(
			'http' => array(
				'header'  => array('Content-type: application/json',
					'Authorization: Basic ' . base64_encode($this->usuario . ':' . $this->senha)
				),
				'method'  => 'POST',
				'content' => json_encode($parametros),
			),
		);

		//faça a requisição
		$retorno = file_get_contents($this->url . $metodo, false, stream_context_create($dados));

		//crie o tipo da resposta
		$resposta = new $classe();
		$resposta->set(json_decode($retorno,true));

		return $resposta;
	}

	function envie_sms($destinatarios, $mensagem, $assunto, $data){
		//parâmetros para envio
		$envio = array(
			'destinatarios' => $destinatarios,
			'assunto' => $assunto,
			'mensagem' => $mensagem,
			'data' => $data
		);
		//chamada & resposta
		return $this->invoque_metodo('sms/envio', $envio, 'RespostaEnvio');
	}

	function envie_sms_personzalido($destinatarios){
		//parâmetros para envio
		$envio = array ('destinatarios' => $destinatarios);
		//chamada & resposta
		return $this->invoque_metodo('sms/envioPersonalizado', $envio, 'RespostaEnvio');
	}

	function consulte_status($sequencias)  {
		$sequencias_sms = array ('sequencias' => $sequencias);
		//chamada & resposta
		return $this->invoque_metodo('sms/status', $sequencias_sms, 'RespostaStatus');
	}

	function consulte_resposta($sequencias)  {
		$sequencias_sms = array ('sequencias' => $sequencias);
		//chamada & resposta
		return $this->invoque_metodo('sms/resposta', $sequencias_sms, 'RespostaResposta');
	}

	function consulte_saldo($sub_usuario = '')  {
		$parametro = array ('subUsuario' => $sub_usuario);
		//chamada & resposta
		return $this->invoque_metodo('sms/saldo', $parametro, 'RespostaSaldo');
	}

	function consulte_maisclientes ($evento, $senha, $todosCelulares){
		$parametros = array ('evento' => $evento, 'senha' => $senha ,'todosCelulares' => $todosCelulares);
		//chamada & resposta
		return $this->invoque_metodo('sms/mailingMaisClientes', $parametros, 'RespostaMaisClientes');
	}

	function cadastre_entrevistado ($pesquisa, $celular, $nome){
		$parametros = array ('pesquisa' => $pesquisa, 'celular' => $celular ,'nome' => $nome);
		//chamada & resposta
		return $this->invoque_metodo('sms/entrevistado', $parametros, 'RespostaEntrevistado');
	}

	function cadastre_usuario ($login, $senha, $celular, $email, $nome, $confirmacao, $resposta) {
		$parametros = array ('login' => $login, 'senha' => $senha, 'celular' => $celular, 'email' => $email,'nome' => $nome,
			'confirmacao' => $confirmacao, 'resposta' => $resposta);
		//chamada & resposta
		return $this->invoque_metodo('sms/usuario', $parametros, 'RespostaUsuario');
	}

	function transfira_creditos($sub_usuario, $quantidade)  {
		$parametro = array ('subUsuario' => $sub_usuario, 'quantidade' => $quantidade);
		//chamada & resposta
		return $this->invoque_metodo('sms/transferencia', $parametro, 'RespostaTransferencia');
	}

}

?>