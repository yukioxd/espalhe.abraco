var webpack = require('webpack');
var WebpackClearConsole = require("webpack-clear-console").WebpackClearConsole;

module.exports = {
    entry: './common/default/es6/main.js',
    output: {
        filename: '[name].js'
    },
    progress: true,
    devtool: 'source-map',
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new WebpackClearConsole()
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'stage-2']
                }
            }
        ]
    }
};
